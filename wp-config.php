<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'W~![-bCA?,Ztl!mby) /.Y{hpRc*>40+)rD~ =-a+*ia99^S`&BFA1iftK]Nz@z=' );
define( 'SECURE_AUTH_KEY',  '6e/ITE)ES%F`t0}_@W]v7bw<,]MH+0-@=-i~iod>rL9.=Kce;b/#>6V.=Vim ~3E' );
define( 'LOGGED_IN_KEY',    'lhTFsNC+0:k|iM+L/[-v1n#1ZWS,EW6`xweqtjA{NQ]w1c_(e?bF!fec.Q3^Y31C' );
define( 'NONCE_KEY',        'Ih|N]_lo~l2QrM{!9A#aEB`dC<i]?#h]pb]/r{Nw2:>^?g8|9sC8fE5`0.{*vJ,a' );
define( 'AUTH_SALT',        'Sm<0o2dz:*pqN8Q*{6<bw-S<}]CpGlh8D&s]St]lr<Yk?!4gqVZvBGj$~?[hHk``' );
define( 'SECURE_AUTH_SALT', '8I0K{*<DpDt@Bc_Q3&pq1GHu+TT!KQ5*W0WDXq[});L1m.&{a9_g<!@<hvQg>O j' );
define( 'LOGGED_IN_SALT',   'qVs7VHl<A2^C7vqX@Y!5L/Syr!2T!Z,+TZ{cNqcu[#S(w?B@w^}(0Xs[?^%#MXrr' );
define( 'NONCE_SALT',       'O25f0:;O-w#cTzMcN~Wq0n;|3 j4AWX&|E3.jJ~,+a=`XGzF(m[Pt{Kvf^QUV<1m' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
