# The CODE

Musical store and events organising

Steps to run the project:
(project has been created 2019 and not updated since - uses PHP 7.4)

1 - on WAMP/XAMPP change the Apache port to "9090"

2 - WAMP/XAMPP must be configured to "PHP 7.4"

3 - git clone https://gitlab.com/Staneva/the-code.git inside: 
 - for WAMP - /wamp64/www/
 - for XAMPP - /xampp/htdocs/

4 - rename the folder "the-code" to "wordpress"

5 - open phpMyAdin in localhost:9090 and create a new database named "wordpress" with "utf8mb4"

6 - import into the newly creted database the file from the repository "wordpress.sql" ("utf8")

7 - open site on localhost:9090/wordpress

8 - admin - localhost:9090/wordpress/admin
  - test shop manager profile - user: test, pass: test
