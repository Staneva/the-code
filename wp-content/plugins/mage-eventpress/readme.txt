=== WooCommerce Event Manager ===
Contributors: magepeopleteam, aamahin
Tags: event, woocomerce events manager, events manager, Wordpress Events plugin, Events plugin, wp events
Requires at least: 4.5
Stable tag: trunk
Version: 2.1.3
Tested up to: 5.3
Requires PHP: 5.6
WC requires at least: 3.0
WC tested up to: 3.6.3
Requires PHP: 5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

WooCommerce Event Manager is a Event Plugin for WordPress which is based on WooCommerce.

== Description ==
WooCommerce Event Manager Plugin- is one of the best and simple event management plugin available in WordPress directory & the best event manager plugin for WordPress. It uses WooCommerce to take payment, which provides freedom for using popular payment getaway via WooCommerce. This plugin supports all WordPress version and can be used to create any types of any types of events. 


###  Free Version Plugin Features:

➡ Support All Payment Method available in WooCommerce.
➡ Cross Browser Support – (Chrome, Firefox, Safari, Opera, Etc.)
➡ Responsive and SEO Friendly.  
➡ Multilingual Translation Support
➡ Available Short Code for Customizing Events & For Showing Event In Any Web Page. 
➡ Event Name & Event Description
➡ Manage Event Ticket Type, Quantity, Prices, Category & Organizer. 
➡ Event Extra Service with Price & Quantity Management.
➡ Show Event Venue Using Google Map.
➡ Option of Adding Event Start Date, End Date and Multi Date & Time. 
➡ One Click Event Rest Option. 
➡ View Registered Attendee from WooCommerce Order Details.
➡ Multiple Event Templates (You can select a different template for each event) 
➡ Works with Any Standards Compliant WordPress Theme.
➡ WooCommerce Support, You can Sale product and Event Separately. There is no mixing with Event with the WooCommerce Products.
➡ [View Live Demo](https://demo.vaincode.com/mage-event/)

###  [Pro Version](https://mage-people.com/product/mage-woo-event-booking-manager-pro/) Plugin Features 
➡   Download PDF tickets for event attendee.
➡   Custom Registration Form for Event Attendees.
➡   Automatic Email Conformation Message and Pdf Tickets Mailing Features
➡   Export Attendee List as CSV Format. 
➡   [Buy WooCommerce Event Manager Pro](https://mage-people.com/product/mage-woo-event-booking-manager-pro/) 


###  Other Available Addons 
➡ QR Code Addon- PDF Event Tickets with Printed QR Code (For attendee check-in).
➡ Backend Order Addon- Book Event from Dashboard/Backend
➡ Coupon Code Addon- Event Coupon Code Adding Features 
➡ Event Duplicator Addon- This Addon will Allow You to Duplicate any Events for Re-using.
➡ Event Calendar Pro Addon- Nice Event Calendar Images Support.  Showcase Your Events into Awesome Calendar.
➡ User Registration Form Position Addon– choose where you want to display attendee registration     form- 1. Event Details Page Or 2. Checkout Page.
➡ Waitlist Addon -If Event Ticket Quantity Is Finished, This Addon Will Automatically Activate and Subscriber Can Join the Waitlist. Admin Can Send Email to The User for Next Free Slot.
➡ Related Events Addon - You can select related event and display to event details page.


👉  [View Live Demo](https://demo.vaincode.com/mage-event/)
👉  [Plugin Documentation](https://demo.vaincode.com/mage-event/documentation/)

👉[Buy WooCommerce Event Manager Pro](https://mage-people.com/product/mage-woo-event-booking-manager-pro/) 👈



### Shortcodes included with Woocommerce Events Manager

`
[event-list cat='' org='' column='2' style='grid' cat-filter='no' org-filter='no' show='' pagination='no' sort='ASC']
`

**cat:**  
By default showing all event, but if you want to show event list of a particular category you can use this attribute, just put the category id with this. example:  

`
[event-list cat='ID']
`

**org:**  
By default showing all event, but if you want to show event list of a particular organization you can use this attribute, just put the category id with this. example:  

`
[event-list org='ID']
`
**style:**  
By default showing all event as grid. If you want to change the display style to list just change it to list. example:  

`
[event-list style='list']
`

**cat-filter:**  
By default showing all event without any filter option. If you want to change it to with category filter use this. example:  

`
[event-list cat-filter='yes']
`
**org-filter:**  
By default showing all event without any filter option. If you want to change it to with organization filter use this. example:  

`
[event-list org-filter='yes']
`

**show:**  
By default showing all event without any limition. If you want to change it and set limit input the limit number. example:  

`
[event-list show='10']
`

**pagination:**  
By default there is no pagination with the event list but if you want to show pagination set yes the value. example:  

`
[event-list pagination='yes']
`

**sort:**
You can sort event list by event start datetime. By default it showing as Assending format, If you want to change the sorting format you need you use this attribute. example:  

`
[event-list sort='ASC/DESC']
`

### Event Calender ShortCode:

`
[event-calendar]

`


= Some Others Very Nice Addon Support Available =

👉[Event QR Code Addon](https://mage-people.com/product/woocommerce-event-qr-code-addon/)👈

👉[Event Calendar Addon](https://mage-people.com/product/woocommerce-event-calendar-addon/) 👈

👉[Event Coupon Code Addon](https://mage-people.com/product/woocommerce-event-coupon-code-addon/) 👈

👉[Event Waitlist Addon](https://mage-people.com/product/woocommerce-event-waitlist-addon/) 👈

👉[Event Book an Event From Dashboard](https://mage-people.com/product/woocommerce-event-book-an-event-from-dashboard/) 👈

👉[Event User Registration Form Position](https://mage-people.com/product/woocommerce-event-user-registration-form-position/) 👈

👉[Event Duplicator Addon](https://mage-people.com/product/woocommerce-event-duplicator-addon/) 👈



### Note:
We do best support for our plugin, we have fully confidence that our plugin working very well for any theme. in case you face any problem with our plugin or any customization needed please email us: magepeopleteam@gmail.com
our support team will back to you shortly.



### How to install Woo Commerce Event Manager Plugin in 3 Minutes
https://www.youtube.com/watch?v=_CRPyKHPhTU

### WooCommerce Event Manager Pro Addons Setup And Full Functionality Explained
https://www.youtube.com/watch?v=LZj6oUavTQg

### How to Configure Event Manager Attendee Registration Form 
https://www.youtube.com/watch?v=F9wnlUjXa6I


**CHECKOUT OUR OTHER PRODUCTS**

**CHECKOUR OUR OTHER PLUGINS** 
[Bus Booking Manager](https://wordpress.org/plugins/bus-booking-manager/)
[Bus Ticket Booking with Seat Reservation](https://wordpress.org/plugins/bus-ticket-booking-with-seat-reservation/)
[Woocommerce Events Manager](https://wordpress.org/plugins/mage-eventpress/)




== Installation ==

Extract the zip file and just drop the contents in the wp-content/plugins/ directory of your WordPress installation and then activate the Plugin from Plugins page.



== Frequently Asked Questions ==

= Q.Is Woocommerce Events Manager Free? =
A. Yes! Woocommerce Events Manager is free.

You can check the demo of this plugin from here [Live Demo](https://demo.vaincode.com/mage-event/) 

= Q.Any Documentation? =
A. Yes! Here is the [Online Documentation](https://demo.vaincode.com/mage-event/documentation/).
 
= Q.I installed correctly but 404 error what can I do ?  =
A. You need to Re-save permalink settings it will solve the 404. if still not work that means you permalink not working, you may have httaccess problem or you have server permission problem. 

= Q.How its work? =
A. Woocommerce Events Manager one of the simple event plugin for WordPress which is based on Woocommerce. Its work as an individual event and its payment functionality handle with woocommerce so there are no worries about payment gateway you can use every payment gateway which is support woocommerce. The interesting part is the event post type is completely different there is no connection with woocommerce product so if you can sell anything from woocommerce product. 

= Q.How can i get the attendee list? =
A. After successfull booking user data will be saved under event attendee list, You can find the list from Events -> attendee list  or Go to event list and click on the attendee list on right side of event to get that particular event attendee list.

= Q.How can i display event list? =
A. You can display the event list on any page by using the ShortCode. Just use this shortcode to display all events list [event-list]. If you want to display any particular category events just use [event-list cat=ID] here ID will be the category id which will you find in the category section of the dashboard.

= Q.why my shortcode not displaying anything ?   =
A. Please make sure that you added shortcode in Text mode. if you add shortcode in tag embeded it may not work 


= Q.Where Can I see Attendee list ?   =
A. Attendee list includes in our  [pro version](https://mage-people.com/product/mage-woo-event-booking-manager-pro/)

= Q.What features are included in pro version that are not in Free version ?   =
A. Event Pro Version has major two version. 
    ➡ Attendee form builder and attendee list with CSV export 
    ➡ PDF ticketing and Email Attachment


= Q.I need More features what can I do ?   =
A. you can create support ticket here with features details that you need, one of our support member will back to you shortly. [Support Ticket](https://mage-people.com/submit-ticket-2/)

= Q.How can I get best support from plugin Company   =
A. you can create support ticket here with problem details with possible screenshot that you have problem, one of our support member will back to you shortly. [Support Ticket](https://mage-people.com/submit-ticket-2/)

== Changelog ==

= 1.0.0 =
*Initial Release Date - 02 February 2018*

= 1.0.2 =
*Quantity Manage Added - 14 February 2018*

= 1.0.5 =
*Update Release, Google Map and Add to calender feture added and some bug fixed - 22 February 2018*

= 1.0.6 =
*Update Release, Event Extra option feature added. So now you can add as many option with a event with price - 27 February 2018*

= 2.0 =
*Update Release, A Huge update released, added so many features. - 02 July 2018*

= 2.0.3 =
*Update Release, Responsive issue fixed, added label setting panel so now easily all lable changable from dashboard. 03 July 2018*

= 2.0.4 =
*Update Release, Minor Bug Fixs. 04 July 2018*

= 2.0.5 =
*Update Release,F.A.Q Feature added into every events. 07 July 2018*

= 2.0.6 =
*Update Release,Styling Section added into Event Setting Section, Now users can control all type of styling issue from dashboard. 09 July 2018*

= 2.0.8 =
*Update Release,Minor Bux Fixed 13 July 2018*

= 2.0.9 =
*Update Release,Quantity Box issue Fixed & Datepcker missing Next and Previuos button fixed 6 Aug 2018*

= 2.1.0 =
*Update Release, jQuery Conflict fixed with DiVi theme. 8 Aug 2018*

= 2.1.1 =
*Update Release, php warning and notice removed and date issue fixed. 16 Aug 2018*

= 2.1.2 =
*Update Release, php warning and notice removed and date issue fixed. 25 Aug 2018*

= 2.1.3 =
*Update Release, Date display issue fixed. 14 Sep 2018*

= 2.1.4 =
*Update Release, Sorting feture added into shortcode. 18 Sep 2018*

= 2.1.5 =
*Update Release, Fixed Google Calender Time issue fixed. 06 Nov 2018

= 2.1.6 =
*Update Release, Event Expired Listing Query Issue has been fixed in this version.. 06 Nov 2018*

= 2.1.9 =
*Update Release, Custom email sending issue has been fixed. 28 Nov 2018*

= 2.2.0 =
*Update Release, Fixed no numeric error, and add feature to hide available seat count. 29 Nov 2018*

= 2.2.1 & 2.2.2 =
*Update Release, fixed cart wrong calculation issue. 02 Dec 2018*

= 2.2.3 =
*Update Release:
Added custom excerpt option into event, Fix zero amount add to cart issue, Showing Not Ticket available message when all tickets sold out, Price Label settings has been added into Event Settings. 02 Dec 2018*

= 2.2.6 =
*Update Release:Translation ready,Bangla language fully supported,NL language fully supported. 18 Dec 2018*

= 2.3.1 =
*Update Release: PDF Ticket New Design,Now you can add custom image as background of PDF Ticket,Terms and Condition text in PDF Ticket,Seat Reservation Feature added. Now owner can reserve of event seat,Add to cart Button issue fixed. 31 Jan 2019*

= 2.3.2 =
*Update Release: France Language Pack Ready. 05 Feb 2019*

= 2.5.8 =
*Update Release: Security Vulnerability Issue Fixed. 28 April 2019*

= 2.6 =
*Update Release: CSV Blank issue fixed, Multiple Add Calender feature added. 21 May 2019*

= 2.6.1 =
*Update Release: Templating issue fixed in child theme. 03 June 2019*

= 2.6.2 & 2.6.3 =
*Update Release: Calendar Button issue fixed, Calendar Button label text issue fixed, Template broken issue fixed ,Add Ticket Number into CSV File, Dynamic Column feature in CSV file. 11 June 2019*

= 2.6.4 =
*Update Release: Booking counting duplicating issue fixed in free event, Add new feature Reset Booking count option. 19 June 2019*

= 2.6.5 =
*Update Release: Fixed Security issue in Attandee Page. 15 June 2019*

= 2.6.6 =
*Update Release: Added new strings into Translation setting page, Fixed event details page not showing in some theme issue, Mixitup Filter for multiple sub category issue fixed. 1 July 2019*

= 2.6.7 =
* Update Release: Timezone issue fixed, Added Event expire date change settings, Negative value display issue fixed. 3 July 2019*

= 2.6.8 =
* Update Release: Quick fix, Double booking issue fixed. 5 July 2019*

= 2.6.9 =
* Update Release: PHP warning fixed, Date Display. 18 July 2019*

= 2.7.0 =
* Update Release: Removed Single Price Section, Added missing values into product set_props. 01 Aug 2019*

= 2.7.1 =
* Update Release: Removed Organizer address php warning issue in dashboard, Fix woocomerce product showing warning issue in the thankyou page, 07 Aug 2019*

= 2.7.2 =
* Update Release: Bug Fixed, 15 Sep 2019*


= 2.8.0 =
* Update Release: Bug Fixed, Event seating issue fixed while order status chganged. 04 Nov 2019*

= 3.0.0 =
* Update Release: Code Improved, Facebook For Woocommerce Issue Fixed, Bug Fixed. 10 Dec 2019*

= 3.0.1 =
* Update Release: Shortcode not showing issue fixed, Timezone issue fixed, Time Format issue fixed. 15 Dec 2019*

= 3.0.2 =
* Update Release: Add Calendar Button issue fixed, Event Checkout Validation, Missing Translation added. 17 Dec 2019*

= 3.0.3 =
* Update Release: Code Improved, Date Issue Fixed in Some template and organizer page & PDF Ticket, Email HTML support issue fixed. 02 Jan 2020*

= 3.0.5 =
* Update Release: Code Improved, Date Issue Fixed in Event List, Email HTML support issue fixed. 15 Jan 2020*

= 3.0.6 =
* Update Release: Code Improved, Design Issue Fixed, Template Missing issue fixed. 22 Jan 2020*

= 3.0.7 =
* Update Release: Design Issue Fixed, Event Image is Cart not Showing issue fixed, Event List Last item broken issue fixed. 23 Jan 2020*

= 3.0.9 =
* Update Release:
Design Issue Fixed
Extra Service Intentory System Added, 
Cart Display Information Improved, 
Tax Issue solved, 
Google Map Display Issue fixed, 
Cart Message Display issue fixed in Event Details Page, 
Event Date Wrong Icon issue fixed, 
Event Date removed start & end text from first date. 
29 Jan 2020*

= 3.1.0 =
* Update Release:
Default Value option Added
jQuery Validation Added
CatID & OrgID Column Added
Some Bug Fixed
Code Improved
Expire Event List Shortcode issue fixed
05 Feb 2020*