<?php
if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.

require_once(dirname(__DIR__) . "/inc/class/mep_settings_api.php");
require_once(dirname(__DIR__) . "/inc/mep_cpt.php");
require_once(dirname(__DIR__) . "/inc/mep_tax.php");
require_once(dirname(__DIR__) . "/inc/mep_event_meta.php");
require_once(dirname(__DIR__) . "/inc/mep_extra_price.php");
require_once(dirname(__DIR__) . "/inc/mep_shortcode.php");
require_once(dirname(__DIR__) . "/inc/admin_setting_panel.php");
require_once(dirname(__DIR__) . "/inc/mep_enque.php");
require_once(dirname(__DIR__) . "/inc/mep_user_custom_style.php");
require_once(dirname(__DIR__) . "/inc/mep_tax_meta.php");
require_once(dirname(__DIR__) . "/inc/mep_addon_list.php");
require_once(dirname(__DIR__) . "/inc/mep_upgrade.php");
require_once(dirname(__DIR__) . "/inc/mep_functions.php");
