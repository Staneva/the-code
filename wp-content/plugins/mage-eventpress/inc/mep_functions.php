<?php
if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.

// Language Load
add_action( 'init', 'mep_language_load');
function mep_language_load(){
    $plugin_dir = basename(dirname(__DIR__))."/languages/";
    load_plugin_textdomain( 'mage-eventpress', false, $plugin_dir );
}


function mep_check_builder_status(){
    $version = '3.2';
    if(is_plugin_active( 'woocommerce-event-manager-addon-form-builder/addon-builder.php' )){
        $data = get_plugin_data( ABSPATH . "wp-content/plugins/woocommerce-event-manager-addon-form-builder/addon-builder.php", false, false );
      if ( is_plugin_active( 'woocommerce-event-manager-addon-form-builder/addon-builder.php' ) && $data['Version'] >= $version ) {
      return true;
    }elseif ( is_plugin_active( 'woocommerce-event-manager-addon-form-builder/addon-builder.php' ) && $data['Version'] < $version ) {
      return false;
    }else{
      return true;
    }
    }else{
      return true;
    }
  }




function mep_get_all_tax_list($current_tax=null){
    global $wpdb;
    $table_name = $wpdb->prefix . 'wc_tax_rate_classes';
    $result = $wpdb->get_results( "SELECT * FROM $table_name" );
  
    foreach ( $result as $tax ){
    ?>
    <option value="<?php echo $tax->slug;  ?>" <?php if($current_tax == $tax->slug ){ echo 'Selected'; } ?>><?php echo $tax->name;  ?></option>
    <?php
    }
  }
  
  
  
  
  // Class for Linking with Woocommerce with Event Pricing 
  add_action('plugins_loaded', 'mep_load_wc_class');
  function mep_load_wc_class() {
      
    if ( class_exists('WC_Product_Data_Store_CPT') ) {
  
     class MEP_Product_Data_Store_CPT extends WC_Product_Data_Store_CPT {
  
      public function read( &$product ) {
          $product->set_defaults();
          if ( ! $product->get_id() || ! ( $post_object = get_post( $product->get_id() ) ) || ! in_array( $post_object->post_type, array( 'mep_events', 'product' ) ) ) { // change birds with your post type
              throw new Exception( __( 'Invalid product.', 'woocommerce' ) );
          }
  
          $id = $product->get_id();
  
          $product->set_props( array(
              'name'              => $post_object->post_title,
              'slug'              => $post_object->post_name,
              'date_created'      => 0 < $post_object->post_date_gmt ? wc_string_to_timestamp( $post_object->post_date_gmt ) : null,
              'date_modified'     => 0 < $post_object->post_modified_gmt ? wc_string_to_timestamp( $post_object->post_modified_gmt ) : null,
              'product_id'        => $post_object->ID,
              'sku'               => $post_object->ID,
              'status'            => $post_object->post_status,
              'description'       => $post_object->post_content,
              'short_description' => $post_object->post_excerpt,
              'parent_id'         => $post_object->post_parent,
              'menu_order'        => $post_object->menu_order,
              'reviews_allowed'   => 'open' === $post_object->comment_status,
          ) );
  
          $this->read_attributes( $product );
          $this->read_downloads( $product );
          $this->read_visibility( $product );
          $this->read_product_data( $product );
          $this->read_extra_data( $product );
          $product->set_object_read( true );
      }
  
      /**
       * Get the product type based on product ID.
       *
       * @since 3.0.0
       * @param int $product_id
       * @return bool|string
       */
      public function get_product_type( $product_id ) {
          $post_type = get_post_type( $product_id );
          if ( 'product_variation' === $post_type ) {
              return 'variation';
          } elseif ( in_array( $post_type, array( 'mep_events', 'product' ) ) ) { // change birds with your post type
              $terms = get_the_terms( $product_id, 'product_type' );
              return ! empty( $terms ) ? sanitize_title( current( $terms )->name ) : 'simple';
          } else {
              return false;
          }
      }
  }
  

  add_filter( 'woocommerce_data_stores', 'mep_woocommerce_data_stores' );
  function mep_woocommerce_data_stores ( $stores ) {     
        $stores['product'] = 'MEP_Product_Data_Store_CPT';
        return $stores;
  }
  
    } else {
  
      add_action('admin_notices', 'wc_not_loaded');
    }

  }
  


  function mep_get_order_info($info,$id){
    if($info){
      $stock_msg  = $info;
      $koba = explode("_", $stock_msg);
      return $koba[$id];
    }else{
      return null;
    }
  }


    function wc_not_loaded() {
        printf(
        '<div class="error" style="background:red; color:#fff;"><p>%s</p></div>',
        __('You Must Install WooCommerce Plugin before activating WooCommerce Event Manager, Becuase It is dependent on Woocommerce Plugin')
        );
    }


  add_action('woocommerce_before_checkout_form', 'mep_displays_cart_products_feature_image');
  function mep_displays_cart_products_feature_image() {
      foreach ( WC()->cart->get_cart() as $cart_item ) {
          $item = $cart_item['data'];
      }
  }


 // Send Confirmation email to customer
 function mep_event_confirmation_email_sent($event_id,$sent_email){
    $values = get_post_custom($event_id);
    
    $global_email_text = mep_get_option( 'mep_confirmation_email_text', 'email_setting_sec', '');
    $global_email_form_email = mep_get_option( 'mep_email_form_email', 'email_setting_sec', '');
    $global_email_form = mep_get_option( 'mep_email_form_name', 'email_setting_sec', '');
    $global_email_sub = mep_get_option( 'mep_email_subject', 'email_setting_sec', '');
    $event_email_text = $values['mep_event_cc_email_text'][0];
    $admin_email = get_option( 'admin_email' );
    $site_name = get_option( 'blogname' );
    
    
      if($global_email_sub){
        $email_sub = $global_email_sub;
      }else{
        $email_sub = 'Confirmation Email';
      }
    
      if($global_email_form){
        $form_name = $global_email_form;
      }else{
        $form_name = $site_name;
      }
    
      if($global_email_form_email){
        $form_email = $global_email_form_email;
      }else{
        $form_email = $admin_email;
      }
    
      if($event_email_text){
        $email_body = $event_email_text;
      }else{
        $email_body = $global_email_text;
      }
    
      $headers[] = "From: $form_name <$form_email>";
    
      if($email_body){
      $sent = wp_mail( $sent_email, $email_sub, nl2br($email_body), $headers );
      }
    }


  function mep_event_get_order_meta($item_id,$key){
  global $wpdb;
    $table_name = $wpdb->prefix."woocommerce_order_itemmeta";
    $sql = 'SELECT meta_value FROM '.$table_name.' WHERE order_item_id ='.$item_id.' AND meta_key="'.$key.'"';
    $results = $wpdb->get_results($sql); //or die(mysql_error());
    foreach( $results as $result ) {
       $value = $result->meta_value;
    }
    $val = isset($value) ? $value : '';
    return $val;
  }
  
  
  
  
  
  function mep_attendee_create($type,$order_id,$event_id,$_user_info = array()){
  
  // Getting an instance of the order object
  $order              = wc_get_order( $order_id );
  $order_meta         = get_post_meta($order_id); 
  $order_status       = $order->get_status();
  
  
  $billing_intotal  = isset($order_meta['_billing_address_index'][0]) ? $order_meta['_billing_address_index'][0] : '';
  $payment_method   = isset($order_meta['_payment_method_title'][0]) ? $order_meta['_payment_method_title'][0] : '';
  $user_id          = isset($order_meta['_customer_user'][0]) ? $order_meta['_customer_user'][0] : '';
  
  if($type == 'billing'){
  // Billing Information 
    $first_name       = isset($order_meta['_billing_first_name'][0]) ? $order_meta['_billing_first_name'][0] : '';
    $last_name        = isset($order_meta['_billing_last_name'][0]) ? $order_meta['_billing_last_name'][0] : '';
    $uname            = $first_name.' '.$last_name;
    $company          = isset($order_meta['_billing_company'][0]) ?  $order_meta['_billing_company'][0] : '';
    $address_1        = isset($order_meta['_billing_address_1'][0]) ? $order_meta['_billing_address_1'][0] : '';
    $address_2        = isset($order_meta['_billing_address_2'][0]) ? $order_meta['_billing_address_2'][0] : '';
    $address          = $address_1.' '.$address_2;
    $gender = '';
    $designation = '';
    $website = '';
    $vegetarian = '';
    $tshirtsize = '';
    $city             = isset($order_meta['_billing_city'][0]) ? $order_meta['_billing_city'][0] : '';
    $state            = isset($order_meta['_billing_state'][0]) ? $order_meta['_billing_state'][0] : '';
    $postcode         = isset($order_meta['_billing_postcode'][0]) ? $order_meta['_billing_postcode'][0] : '';
    $country          = isset($order_meta['_billing_country'][0]) ? $order_meta['_billing_country'][0] : '';
    $email            = isset($order_meta['_billing_email'][0]) ? $order_meta['_billing_email'][0] : '';
    $phone            = isset($order_meta['_billing_phone'][0]) ? $order_meta['_billing_phone'][0] : '';
    $ticket_type      = $_user_info['ticket_name'];
    $event_date       = $_user_info['event_date'];
      $ticket_qty =  $_user_info['ticket_qty'];
  
  }elseif($type == 'user_form'){
  
  
      $uname          = $_user_info['user_name'];
      $email          = $_user_info['user_email'];
      $phone          = $_user_info['user_phone'];
      $address        = $_user_info['user_address'];
      $gender         = $_user_info['user_gender'];
      $company        = $_user_info['user_company'];
      $designation    = $_user_info['user_designation'];
      $website        = $_user_info['user_website'];
      $vegetarian     = $_user_info['user_vegetarian'];
      $tshirtsize     = $_user_info['user_tshirtsize'];
      $ticket_type    = $_user_info['user_ticket_type'];
      $ticket_qty = $_user_info['user_ticket_qty'];
      $event_date     = $_user_info['user_event_date'];
      $event_id       = $_user_info['user_event_id'] ? $_user_info['user_event_id'] : $event_id;
      $mep_ucf        = isset($_user_info['mep_ucf']) ? $_user_info['mep_ucf'] : "";
  
  }
  
  
  
  $new_post = array(
    'post_title'    =>   $uname,
    'post_content'  =>   '',
    'post_category' =>   array(),  // Usable for custom taxonomies too
    'tags_input'    =>   array(),
    'post_status'   =>   'publish', // Choose: publish, preview, future, draft, etc.
    'post_type'     =>   'mep_events_attendees'  //'post',page' or use a custom post type if you want to
    );
  
    //SAVE THE POST
    $pid                = wp_insert_post($new_post);
    $pin                = $user_id.$order_id.$event_id.$pid;
      update_post_meta( $pid, 'ea_name', $uname );
      update_post_meta( $pid, 'ea_address_1', $address );
      update_post_meta( $pid, 'ea_email', $email );
      update_post_meta( $pid, 'ea_phone', $phone );
      update_post_meta( $pid, 'ea_gender', $gender );
      update_post_meta( $pid, 'ea_company', $company );
      update_post_meta( $pid, 'ea_desg', $designation );
      update_post_meta( $pid, 'ea_website', $website );
      update_post_meta( $pid, 'ea_vegetarian', $vegetarian );
      update_post_meta( $pid, 'ea_tshirtsize', $tshirtsize );
      update_post_meta( $pid, 'ea_ticket_type', $ticket_type );
      update_post_meta( $pid, 'ea_ticket_qty', $ticket_qty);
      update_post_meta( $order_id, 'ea_ticket_qty', $ticket_qty);
      update_post_meta( $order_id, 'ea_ticket_type', $ticket_type );
      update_post_meta( $order_id, 'ea_event_id', $event_id );
      update_post_meta( $pid, 'ea_payment_method', $payment_method );
      update_post_meta( $pid, 'ea_event_name', get_the_title( $event_id ) );
      update_post_meta( $pid, 'ea_event_id', $event_id );
      update_post_meta( $pid, 'ea_order_id', $order_id );
      update_post_meta( $pid, 'ea_user_id', $user_id );
      update_post_meta( $order_id, 'ea_user_id', $user_id );
      update_post_meta( $pid, 'ea_ticket_no', $pin );
      update_post_meta( $pid, 'ea_event_date', $event_date );
      update_post_meta( $pid, 'ea_order_status', $order_status );
      update_post_meta( $order_id, 'ea_order_status', $order_status );
  
    // Checking if the form builder addon is active and have any custom fields
    $mep_form_builder_data = get_post_meta($event_id, 'mep_form_builder_data', true);
      if ( $mep_form_builder_data ) {
        foreach ( $mep_form_builder_data as $_field ) {
          update_post_meta( $pid, "ea_".$_field['mep_fbc_id'], $_user_info[$_field['mep_fbc_id']]); 
        }
    } // End User Form builder data update loop
  
  }
  
  // add_action('admin_init','ttest');
  
  function ttest(){
      mep_attendee_extra_service_create(11289,8938);
  }
  
  function mep_attendee_extra_service_create($order_id,$event_id){
  
      $order              = wc_get_order( $order_id );
      $order_meta         = get_post_meta($order_id); 
      $order_status       = $order->get_status();
    foreach ( $order->get_items() as $item_id => $item_values ) {
      $item_id                    = $item_id;
    }
     $_event_extra_service   = wc_get_order_item_meta($item_id,'_event_extra_service',true);
     $date   = date('Y-m-d H:i:s',strtotime(wc_get_order_item_meta($item_id,'Date',true)));
     
     if(is_array($_event_extra_service) && sizeof($_event_extra_service) > 0){
  
     foreach($_event_extra_service as $extra_serive){
      $uname = 'Extra Service for '.get_the_title($event_id).' Order #'.$order_id;
      // echo $extra_serive['service_price'];
      //   die();
      $new_post = array(
        'post_title'    =>   $uname,
        'post_content'  =>   '',
        'post_category' =>   array(), 
        'tags_input'    =>   array(),
        'post_status'   =>   'publish', 
        'post_type'     =>   'mep_extra_service'
      );
  
       $pid             = wp_insert_post($new_post);
     
      
      update_post_meta( $pid, 'ea_extra_service_name', $extra_serive['service_name'] );
      update_post_meta( $pid, 'ea_extra_service_qty', $extra_serive['service_qty'] );
      update_post_meta( $pid, 'ea_extra_service_unit_price', $extra_serive['service_price'] );
      update_post_meta( $pid, 'ea_extra_service_total_price', $extra_serive['service_qty'] * $extra_serive['service_price'] );
      update_post_meta( $pid, 'ea_extra_service_event', $event_id );
      update_post_meta( $pid, 'ea_extra_service_order', $order_id );
      update_post_meta( $pid, 'ea_extra_service_order_status', $order_status );
      update_post_meta( $pid, 'ea_extra_service_event_date', $date );
     }
  
  }
  
  }
  
  
  
  add_action('woocommerce_checkout_order_processed', 'mep_event_booking_management', 10);
  function mep_event_booking_management( $order_id) {

  
  if ( ! $order_id )
    return;
  
  // Getting an instance of the order object
  $order              = wc_get_order( $order_id );
  $order_meta         = get_post_meta($order_id); 
  $order_status       = $order->get_status();
  
  $form_position = mep_get_option( 'mep_user_form_position', 'general_attendee_sec', 'details_page' );
  
  if($form_position=='checkout_page'){
  
    foreach ( $order->get_items() as $item_id => $item_values ) {
      $item_id                    = $item_id;
    }
    $event_id                     = wc_get_order_item_meta($item_id,'event_id',true);
    if (get_post_type($event_id)  == 'mep_events') { 
       
      $event_name               = get_the_title($event_id);
      $user_info_arr            = wc_get_order_item_meta($item_id,'_event_user_info',true);
      $service_info_arr         = wc_get_order_item_meta($item_id,'_event_service_info',true);
      $event_ticket_info_arr    = wc_get_order_item_meta($item_id,'_event_ticket_info',true);
      $item_quantity            = 0;
      
  
      foreach ( $event_ticket_info_arr as $field ) {
        if($field['ticket_qty']>0){
            $item_quantity = $item_quantity + $field['ticket_qty'];
        }
      } 
      if(is_array($user_info_arr) & sizeof($user_info_arr) > 0){
        foreach ($user_info_arr as $_user_info) {
            mep_attendee_create('user_form',$order_id,$event_id,$_user_info);
        } 
      }else{
          foreach($event_ticket_info_arr as $tinfo){
            for ($x = 1; $x <= $tinfo['ticket_qty']; $x++) {
                mep_attendee_create('billing',$order_id,$event_id,$tinfo);
            } 
          }
      }  
    }
  }else{
    foreach ( $order->get_items() as $item_id => $item_values ) {
      $item_id                    = $item_id;
      $event_id                   = wc_get_order_item_meta($item_id,'event_id',true);
        if (get_post_type($event_id) == 'mep_events') { 
          $event_name             = get_the_title($event_id);
          $user_info_arr          = wc_get_order_item_meta($item_id,'_event_user_info',true);
          $service_info_arr       = wc_get_order_item_meta($item_id,'_event_service_info',true);
          $event_ticket_info_arr  = wc_get_order_item_meta($item_id,'_event_ticket_info',true);
          $item_quantity          = 0;
          mep_attendee_extra_service_create($order_id,$event_id);
          foreach ( $event_ticket_info_arr as $field ) {
            if($field['ticket_qty']>0){
                $item_quantity    = $item_quantity + $field['ticket_qty'];
            }
          } 
          if(is_array($user_info_arr) & sizeof($user_info_arr) > 0){
            foreach ($user_info_arr as $_user_info) {
                mep_attendee_create('user_form',$order_id,$event_id,$_user_info);
            } 
          }else{
              foreach($event_ticket_info_arr as $tinfo){
                for ($x = 1; $x <= $tinfo['ticket_qty']; $x++) {
                    mep_attendee_create('billing',$order_id,$event_id,$tinfo);
                } 
              }
          }  
        } // end of check post type
    }
  }
  do_action('mep_after_event_booking',$order_id,$order->get_status());
  
    
  }
  
  
  
  add_action('woocommerce_checkout_order_processed', 'mep_event_order_status_make_pending', 10, 1);
  
  function mep_event_order_status_make_pending($order_id)
  {
     // Getting an instance of the order object
      $order      = wc_get_order( $order_id );
      $order_meta = get_post_meta($order_id); 
  
     # Iterating through each order items (WC_Order_Item_Product objects in WC 3+)
      foreach ( $order->get_items() as $item_id => $item_values ) {
          $item_quantity = $item_values->get_quantity();
          $item_id = $item_id;
      }
    $ordr_total             = $order->get_total();
    $product_id             = mep_event_get_order_meta($item_id,'_product_id');
    if($product_id==0){
    $event_id             = mep_event_get_order_meta($item_id,'event_id');
    if (get_post_type($event_id) == 'mep_events') { 
        $mep_atnd           = "_mep_atnd_".$order_id;
        update_post_meta( $event_id, $mep_atnd, "a1");
        $order_meta_text  = "_stock_msg_".$order_id;
        $order_processing = "pending_".$order_id;
        update_post_meta( $event_id, $order_meta_text, $order_processing);
    }
  }
  }
  
  
  function change_attandee_order_status($order_id,$set_status,$post_status,$qr_status=null){
  
      $args = array (
          'post_type'         => array( 'mep_events_attendees' ),
          'posts_per_page'    => -1,
          'post_status' => $post_status,
          'meta_query' => array(
               array(
                               'key'       => 'ea_order_id',
                               'value'     => $order_id,
                               'compare'   => '='
                       )
               )
           );
          $loop = new WP_Query($args);
           $tid = array();
          foreach ($loop->posts as $ticket) {
              $post_id = $ticket->ID;
          
              update_post_meta($post_id, 'ea_order_status', $qr_status);
              
              $current_post = get_post( $post_id, 'ARRAY_A' );
              $current_post['post_status'] = $set_status;
              wp_update_post($current_post);
          }
  }
  
  function change_extra_service_status($order_id,$set_status,$post_status,$qr_status=null){
  
      $args = array (
          'post_type'         => array( 'mep_extra_service' ),
          'posts_per_page'    => -1,
          'post_status' => $post_status,
          'meta_query' => array(
               array(
                               'key'       => 'ea_extra_service_order',
                               'value'     => $order_id,
                               'compare'   => '='
                       )
               )
           );
          $loop = new WP_Query($args);
           $tid = array();
          foreach ($loop->posts as $ticket) {
              $post_id = $ticket->ID;
              
              update_post_meta($post_id, 'ea_extra_service_order_status', $qr_status);
              
              $current_post = get_post( $post_id, 'ARRAY_A' );
              $current_post['post_status'] = $set_status;
              wp_update_post($current_post);
          }
  }
  
  
  
  
  function change_wc_event_product_status($order_id,$set_status,$post_status,$qr_status=null){
  
      $args = array (
          'post_type'         => array( 'product' ),
          'posts_per_page'    => -1,
          'post_status' => $post_status,
          'meta_query' => array(
               array(
                               'key'       => 'link_mep_event',
                               'value'     => $order_id,
                               'compare'   => '='
                       )
               )
           );
          $loop = new WP_Query($args);
           $tid = array();
          foreach ($loop->posts as $ticket) {
              $post_id = $ticket->ID;
              if(!empty($qr_status)){
                  //update_post_meta($post_id, 'ea_order_status', $qr_status);
              }
              $current_post = get_post( $post_id, 'ARRAY_A' );
              $current_post['post_status'] = $set_status;
              wp_update_post($current_post);
          }
  }
  
  
  
  
  
  add_action('wp_trash_post','mep_addendee_trash',90);
  function mep_addendee_trash( $post_id ) {
    $post_type   = get_post_type( $post_id );
    $post_status = get_post_status( $post_id );
    
    if ( $post_type == 'shop_order' ) {
      change_attandee_order_status( $post_id, 'trash', 'publish', '' );
      change_extra_service_status( $post_id, 'trash', 'publish', '' );
    }
    
    
    if ( $post_type == 'mep_events' ) {
      change_wc_event_product_status( $post_id, 'trash', 'publish', '' );
    }
    
    
    
  }
  
  
  add_action('untrash_post','mep_addendee_untrash',90);
  function mep_addendee_untrash( $post_id ) {
    $post_type   = get_post_type( $post_id );
    $post_status = get_post_status( $post_id );
    if ( $post_type == 'shop_order' ) {
      $order            = wc_get_order( $post_id );
      $order_status     = $order->get_status();
      change_attandee_order_status( $post_id, 'publish', 'trash', '' );
      change_extra_service_status( $post_id, 'publish', 'trash', '' );
    }
    
    if ( $post_type == 'mep_events' ) {
      change_wc_event_product_status( $post_id, 'publish', 'trash', '' );
    }  
    
  }
  
  
  add_action('woocommerce_order_status_changed', 'mep_attendee_status_update', 10, 4);
  function mep_attendee_status_update($order_id, $from_status, $to_status, $order ){

    global $wpdb,$wotm;
    // Getting an instance of the order object
     $order      = wc_get_order( $order_id );
     $order_meta = get_post_meta($order_id); 
     $email            = isset($order_meta['_billing_email'][0]) ? $order_meta['_billing_email'][0] : array();
  
  
  
     foreach ( $order->get_items() as $item_id => $item_values ) {
      $item_id        = $item_id;
      $event_id      = mep_event_get_order_meta($item_id,'event_id');
      
      if (get_post_type($event_id) == 'mep_events') {
  
  
        if($order->has_status( 'processing' ) ) {
          change_attandee_order_status($order_id,'publish','trash','processing');
          change_attandee_order_status($order_id,'publish','publish','processing');        
          
          change_extra_service_status($order_id,'publish','trash','processing');
          change_extra_service_status($order_id,'publish','publish','processing');
        }
        if($order->has_status( 'pending' )) {
          change_attandee_order_status($order_id,'publish','trash','pending');
          change_attandee_order_status($order_id,'publish','publish','pending');        
          
          change_extra_service_status($order_id,'publish','trash','pending');
          change_extra_service_status($order_id,'publish','publish','pending');
        }
        if($order->has_status( 'on-hold' )) {
          change_attandee_order_status($order_id,'publish','trash','on-hold');
          change_attandee_order_status($order_id,'publish','publish','on-hold');
        }
        if($order->has_status( 'completed' ) ) {
          mep_event_confirmation_email_sent($event_id,$email);
          change_attandee_order_status($order_id,'publish','trash','completed');
          change_attandee_order_status($order_id,'publish','publish','completed');        
          
          change_extra_service_status($order_id,'publish','trash','completed');
          change_extra_service_status($order_id,'publish','publish','completed');
        }
        if($order->has_status( 'cancelled' ) ) {
          change_attandee_order_status($order_id,'trash','publish','cancelled');
          
          change_extra_service_status($order_id,'trash','publish','cancelled');
        }
        if($order->has_status( 'refunded' ) ) {
          change_attandee_order_status($order_id,'trash','publish','refunded');
          
          change_extra_service_status($order_id,'trash','publish','refunded');
        }
        if($order->has_status( 'failed' ) ) {
          change_attandee_order_status($order_id,'trash','publish','failed');
          
          change_extra_service_status($order_id,'trash','publish','failed');
        }
      } // End of Post Type Check
     } // End order item foreach

  } // End Function
  
  
  
  
  // add_action('woocommerce_order_status_changed', 'mep_event_seat_management', 10, 4);
  function mep_event_seat_management( $order_id, $from_status, $to_status, $order ) {
  global $wpdb;
  
     // Getting an instance of the order object
      $order      = wc_get_order( $order_id );
      $order_meta = get_post_meta($order_id); 
  
      $c = 1;
     # Iterating through each order items (WC_Order_Item_Product objects in WC 3+)
      foreach ( $order->get_items() as $item_id => $item_values ) {
          $item_quantity = $item_values->get_quantity();
          $item_id = $item_id;
      }
  $ordr_total             = $order->get_total();
  $product_id             = mep_event_get_order_meta($item_id,'_product_id');
  if($product_id==0){
  
  $event_id               = mep_event_get_order_meta($item_id,'event_id');
  
  if (get_post_type($event_id) == 'mep_events') { 
  
  
  $table_name = $wpdb->prefix . 'woocommerce_order_itemmeta';
  $result = $wpdb->get_results( "SELECT * FROM $table_name WHERE order_item_id=$item_id" );
  
  
      $mep_total    = get_post_meta($event_id,'total_booking', true);
      if($mep_total){
        $mep_total_booking = $mep_total;
      }else{
        $mep_total_booking =0;
      }
      
  
      $email            = $order_meta['_billing_email'][0];
      $order_meta_text  = "_stock_msg_".$order_id;
      $order_processing = "processing_".$order_id;
      $order_completed  = "completed_".$order_id;
      $order_cancelled  = "cancelled_".$order_id;
      $mep_atnd         = "_mep_atnd_".$order_id;
  
  
  // if($order->has_status( 'processing' ) || $order->has_status( 'pending' )) {
  if($order->has_status( 'processing' ) || $order->has_status( 'pending' )) {
  // update_post_meta( $event_id, $mep_atnd, "a2");
  
  $mep_stock_msgc = mep_get_order_info(get_post_meta($event_id,$order_meta_text, true),0);
  $mep_stock_orderc = mep_get_order_info(get_post_meta($event_id,$order_meta_text, true),1);
  
  if($mep_stock_orderc==$order_id){
        if($mep_stock_msgc=='cancelled'){
  
          foreach ( $result as $page ){
            if (strpos($page->meta_key, '_') !== 0) {
  
               $order_option_name = $event_id.str_replace(' ', '', mep_get_string_part($page->meta_key,0));
  
               $order_option_qty = mep_get_string_part($page->meta_key,1);
               $tes = get_post_meta($event_id,"mep_xtra_$order_option_name",true);
            $ntes = ($tes+$order_option_qty);
            update_post_meta( $event_id, "mep_xtra_$order_option_name",$ntes);
             }
          }
        }
      }
  
      update_post_meta( $event_id, $order_meta_text, $order_processing);
      
      $mep_stock_msg = mep_get_order_info(get_post_meta($event_id,$order_meta_text, true),0);
      $mep_stock_order = mep_get_order_info(get_post_meta($event_id,$order_meta_text, true),1);
  
  
  if($mep_stock_order==$order_id){
        if($mep_stock_msg=='completed'){
            update_post_meta( $event_id, $order_meta_text, $order_processing);
        }
        else{
            update_post_meta( $event_id, 'total_booking', ($mep_total_booking+$item_quantity));
            update_post_meta( $event_id, $order_meta_text, $order_processing);
  
        }
      } 
  }
  
  
  if($order->has_status( 'cancelled' ) || $order->has_status( 'refunded' ) || $order->has_status( 'failed' )) {
      
    update_post_meta( $event_id,$mep_atnd, "a2");
    update_post_meta( $event_id, $order_meta_text, $order_cancelled);
    $mep_stock_msg = mep_get_order_info(get_post_meta($event_id,$order_meta_text, true),0);
    $mep_stock_order = mep_get_order_info(get_post_meta($event_id,$order_meta_text, true),1);
  
  
      if($mep_stock_order==$order_id){        
          $update_total_booking    = update_post_meta( $event_id, 'total_booking', ($mep_total_booking-$item_quantity));
  
      foreach ( $result as $page ){
        if (strpos($page->meta_key, '_') !== 0) {
         $order_option_name = $event_id.str_replace(' ', '', mep_get_string_part($page->meta_key,0));
         $order_option_qty = mep_get_string_part($page->meta_key,1);
         $tes = get_post_meta($event_id,"mep_xtra_$order_option_name",true);
      $ntes = ($tes-$order_option_qty);
      if($tes>0){
      update_post_meta( $event_id, "mep_xtra_$order_option_name",$ntes);
    }
       }
      }
      }
  
  }
  
  if( $order->has_status( 'completed' )) {
  update_post_meta( $event_id, $mep_atnd, "a2");
        // update_post_meta( $event_id, $order_meta_text, $order_completed);
        $mep_stock_msg = mep_get_order_info(get_post_meta($event_id,$order_meta_text, true),0);
        $mep_stock_order = mep_get_order_info(get_post_meta($event_id,$order_meta_text, true),1);
       // mep_event_confirmation_email_sent($event_id,$email);
            if($ordr_total==0){
              update_post_meta( $event_id, 'total_booking', ($mep_total_booking+$item_quantity));
            } 
  
      if($mep_stock_order==$order_id){
  
        if($mep_stock_msg=='processing'){
            update_post_meta( $event_id, $order_meta_text, $order_completed);
        }elseif($mep_stock_msg=='pending'){
  
        if($ordr_total>0){
            update_post_meta( $event_id, 'total_booking', ($mep_total_booking+$item_quantity));
            update_post_meta( $event_id, $order_meta_text, $order_completed);
            
          //   foreach ( $result as $page ){
          //   if (strpos($page->meta_key, '_') !== 0) {
          //   $order_option_name = $event_id.str_replace(' ', '', mep_get_string_part($page->meta_key,0));
          //   $order_option_qty = mep_get_string_part($page->meta_key,1);
          //   $tes = get_post_meta($event_id,"mep_xtra_$order_option_name",true);
          // $ntes = ($tes+$order_option_qty);
          // update_post_meta( $event_id, "mep_xtra_$order_option_name",$ntes);
          //  }
          // }
        }
        }
        else{
  
            // update_post_meta( $event_id, 'total_booking', ($mep_total_booking+$item_quantity));
            update_post_meta( $event_id, $order_meta_text, $order_completed);
            
            foreach ( $result as $page ){
            if (strpos($page->meta_key, '_') !== 0) {
             $order_option_name = $event_id.str_replace(' ', '', mep_get_string_part($page->meta_key,0));
             $order_option_qty = mep_get_string_part($page->meta_key,1);
             $tes = get_post_meta($event_id,"mep_xtra_$order_option_name",true);
          $ntes = ($tes+$order_option_qty);
          update_post_meta( $event_id, "mep_xtra_$order_option_name",$ntes);
           }
          }
        }
  
      }
    }
  }
  }
  }
  
  
  
  add_action('restrict_manage_posts', 'mep_filter_post_type_by_taxonomy');
  function mep_filter_post_type_by_taxonomy() {
    global $typenow;
    $post_type = 'mep_events'; // change to your post type
    $taxonomy  = 'mep_cat'; // change to your taxonomy
    if ($typenow == $post_type) {
      $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
      $info_taxonomy = get_taxonomy($taxonomy);
      wp_dropdown_categories(array(
        'show_option_all' => __("Show All {$info_taxonomy->label}"),
        'taxonomy'        => $taxonomy,
        'name'            => $taxonomy,
        'orderby'         => 'name',
        'selected'        => $selected,
        'show_count'      => true,
        'hide_empty'      => true,
      ));
    };
  }
  
  
  
  
  add_filter('parse_query', 'mep_convert_id_to_term_in_query');
  function mep_convert_id_to_term_in_query($query) {
    global $pagenow;
    $post_type = 'mep_events'; // change to your post type
    $taxonomy  = 'mep_cat'; // change to your taxonomy
    $q_vars    = &$query->query_vars;
  
    if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
      $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
      $q_vars[$taxonomy] = $term->slug;
    }
  
  }
  
  
  
  add_filter('parse_query', 'mep_attendee_filter_query');
  function mep_attendee_filter_query($query) {
    global $pagenow;
    $post_type = 'mep_events_attendees'; 
    $q_vars    = &$query->query_vars;
  
    if ( $pagenow == 'edit.php' && isset($_GET['post_type']) && $_GET['post_type'] == $post_type && isset($_GET['meta_value']) && $_GET['meta_value'] != 0) {
  
      $q_vars['meta_key'] = 'ea_event_id';
      $q_vars['meta_value'] = $_GET['meta_value'];
  
    }elseif ( $pagenow == 'edit.php' && isset($_GET['post_type']) && $_GET['post_type'] == $post_type && isset($_GET['event_id']) && $_GET['event_id'] != 0 && !isset($_GET['action']) ) {
      
      $event_date = date('Y-m-d',strtotime($_GET['ea_event_date']));
      $meta_query = array([
        'key'     => 'ea_event_id',
        'value'   => $_GET['event_id'],
        'compare' => '='
      ],[
        'key'     => 'ea_event_date',
        'value'   => $event_date,
        'compare' => 'LIKE'
      ],[
        'key'     => 'ea_order_status',
        'value'   => 'completed',
        'compare' => '='
      ]);
        
        $query->set( 'meta_query', $meta_query );
  
    }
  }
  
  
  
  
  
  
  
  // Add the data to the custom columns for the book post type:
  add_action( 'manage_mep_events_posts_custom_column' , 'mep_custom_event_column', 10, 2 );
  function mep_custom_event_column( $column, $post_id ) {
  switch ( $column ) {
  
          case 'mep_status' :          
            $values               = get_post_custom( $post_id );  
            $recurring            = get_post_meta($post_id, 'mep_enable_recurring', true) ? get_post_meta($post_id, 'mep_enable_recurring', true) : 'no';
  
  
            if($recurring == 'yes'){
              $event_more_dates    = get_post_meta($post_id,'mep_event_more_date',true);
              $seat_left = 10;
              $md = end($event_more_dates);
              $more_date = $md['event_more_start_date'].' '.$md['event_more_start_time'];
              $event_date = date('Y-m-d H:i:s',strtotime($more_date));
            }else{
              $event_expire_on      = mep_get_option( 'mep_event_expire_on_datetime', 'general_setting_sec', 'event_start_date');$event_date = $values['event_start_date'][0].' '.$values['event_start_time'][0];  
            }           
            echo mep_get_event_status($event_date); 
          break;
  
          case 'mep_atten' :
            $multi_date           = get_post_meta($post_id,'mep_event_more_date',true) ? get_post_meta($post_id,'mep_event_more_date',true) : array();
            $recurring            = get_post_meta($post_id, 'mep_enable_recurring', true) ? get_post_meta($post_id, 'mep_enable_recurring', true) : 'no';
            // print_r($multi_date);
            // if($recurring == 'yes'){ 
            ?>
            <form action="" method="get">
              <select name="ea_event_date" id="" style='font-size: 14px;border: 1px solid blue;width: 110px;display:<?php if($recurring == 'yes'){ echo 'block'; }else{ echo 'none'; } ?>'>
                  <option value="<?php echo get_post_meta($post_id,'event_start_date',true).' '.get_post_meta($post_id,'event_start_time',true); ?>"><?php echo get_mep_datetime(get_post_meta($post_id,'event_start_date',true),'date-text').' '.get_mep_datetime(get_post_meta($post_id,'event_start_date',true).' '.get_post_meta($post_id,'event_start_time',true),'time'); ?></option>
                  <?php foreach($multi_date as $multi){ ?>
                    <option value="<?php echo $multi['event_more_start_date'].' '.$multi['event_more_start_time']; ?>"><?php echo get_mep_datetime($multi['event_more_start_date'],'date-text').' '.get_mep_datetime($multi['event_more_start_time'],'time'); ?></option>
                  <?php } ?>
              </select>
              <input type="hidden" name='post_type' value='mep_events_attendees'>
              <input type="hidden" name='event_id' value='<?php echo $post_id; ?>'>
              <button class="button button-primary button-large">Attendees List</button>
            </form>
            <?php
            // }else{
            //  echo '<a class="button button-primary button-large" href="'.get_site_url().'/wp-admin/edit.php?post_type=mep_events_attendees&meta_value='.$post_id.'">Attendees List</a></form>'; 
            // }
              break;
      }
  }
  
  
  // Getting event exprie date & time
  function mep_get_event_status($startdatetime){
  
    $default_timezone_val     = get_option('timezone_string') ? get_option('timezone_string') : 'UTC';
    date_default_timezone_set($default_timezone_val);
  
    $current = current_time('Y-m-d H:i:s');
    $newformat = date('Y-m-d H:i:s',strtotime($startdatetime));
  
  
  //   date_default_timezone_set(get_option('timezone_string'));
  
    $datetime1 = new DateTime($newformat);
    $datetime2 = new DateTime($current);
  
    $interval = date_diff($datetime2, $datetime1);
  
    if(current_time('Y-m-d H:i:s') > $newformat){
      return "<span class=err>Expired</span>";
    }
    else{
    $days = $interval->days;
    $hours = $interval->h;
    $minutes = $interval->i;
    if($days>0){ $dd = $days." days "; }else{ $dd=""; }
    if($hours>0){ $hh = $hours." hours "; }else{ $hh=""; }
    if($minutes>0){ $mm = $minutes." minutes "; }else{ $mm=""; }
     return "<span class='active'>$dd $hh $mm</span>";
    }
  }
  
  
  
  
  
  // Redirect to Checkout after successfuly event registration
  add_filter ('woocommerce_add_to_cart_redirect', 'mep_event_redirect_to_checkout');
  function mep_event_redirect_to_checkout() {
      global $woocommerce;
      $redirect_status = mep_get_option( 'mep_event_direct_checkout', 'general_setting_sec', 'yes' );
      if($redirect_status=='yes'){
      $checkout_url = wc_get_checkout_url();
      return $checkout_url;
    }
  }
  
  
  add_action('init','mep_include_template_parts');
  function mep_include_template_parts(){
          //   $template_name = 'templating.php';
          //   $template_path = get_stylesheet_directory().'/mage-events/template-prts/';
          // if(file_exists($template_path . $template_name)) {
          //   require_once(get_stylesheet_directory() . "/mage-events/template-prts/templating.php");
          //     }else{
          //       require_once(dirname(__FILE__) . "/inc/template-prts/templating.php");
          //     }
          require_once(dirname(__DIR__) . "/inc/template-prts/templating.php");
  }
  
  
  function mep_load_events_templates($template) {
      global $post;
    if ($post->post_type == "mep_events"){
            $template_name = 'single-events.php';
            $template_path = 'mage-events/';
            $default_path = plugin_dir_path( __DIR__ ) . 'templates/'; 
            $template = locate_template( array($template_path . $template_name) );
  
            
          if ( ! $template ) :
            $template = $default_path . $template_name;
          endif;
      return $template;
    }
  
      if ($post->post_type == "mep_events_attendees"){
          $plugin_path = plugin_dir_path( __DIR__ );
          $template_name = 'templates/single-mep_events_attendees.php';
          if($template === get_stylesheet_directory() . '/' . $template_name
              || !file_exists($plugin_path . $template_name)) {
              return $template;
          }
          return $plugin_path . $template_name;
      }
  
      return $template;
  }
  add_filter('single_template', 'mep_load_events_templates');
  
  
  
  
  
  add_filter('template_include', 'mep_organizer_set_template');
  function mep_organizer_set_template( $template ){
  
        $cat_template_name = 'taxonomy-category.php';
        $org_template_name = 'taxonomy-organozer.php';
        $template_path = get_stylesheet_directory().'/mage-events/';
        
      if( is_tax('mep_org')){
          
        if(file_exists($template_path . $org_template_name)) {
          $template = get_stylesheet_directory().'/mage-events/taxonomy-organozer.php';
          }else{
          $template = plugin_dir_path( __DIR__ ).'templates/taxonomy-organozer.php';;
          }
      }
  
      if( is_tax('mep_cat')){
          
          if(file_exists($template_path . $cat_template_name)) {
          $template = get_stylesheet_directory().'/mage-events/taxonomy-category.php';
          }else{
          $template = plugin_dir_path( __DIR__ ).'templates/taxonomy-category.php';
          }
      }    
  
      return $template;
  }
  
  
  
  function mep_social_share(){
  ?>
  <ul class='mep-social-share'>
         <li> <a data-toggle="tooltip" title="" class="facebook" onclick="window.open('https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" data-original-title="Share on Facebook"><i class="fa fa-facebook"></i></a></li>
  
          <!-- <li><a data-toggle="tooltip" title="" class="gpuls" onclick="window.open('https://plus.google.com/share?url=<?php the_permalink(); ?>','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" data-original-title="Share on Google Plus"><i class="fa fa-google-plus"></i></a> </li>  -->                 
  
          <li><a data-toggle="tooltip" title="" class="twitter" onclick="window.open('https://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>" data-original-title="Twittet it"><i class="fa fa-twitter"></i></a></li>
          </ul>
  <?php
  }
  
  function mep_calender_date($datetime){
    $time       = strtotime($datetime);
    $newdate    = date('Ymd',$time);
    $newtime    = date('Hi',$time);
    $newformat  = $newdate."T".$newtime."00";
  return $newformat;
  }
  
  
  
  function mep_add_to_google_calender_link($pid){
    $event        = get_post($pid);
    $event_meta   = get_post_custom($pid);
    $event_start  = $event_meta['event_start_date'][0].' '.$event_meta['event_start_time'][0];
    $event_end    = $event_meta['event_end_date'][0].' '.$event_meta['event_end_time'][0];
  
  $location = $event_meta['mep_location_venue'][0]." ".$event_meta['mep_street'][0]." ".$event_meta['mep_city'][0]." ".$event_meta['mep_state'][0]." ".$event_meta['mep_postcode'][0]." ".$event_meta['mep_country'][0];
  ob_start();
  ?>
  
  <div id="mep_add_calender_button" class='mep-add-calender'><i class="fa fa-calendar"></i><?php _e(mep_get_label($pid,'mep_calender_btn_text','Add Calendar'),'mage-eventpress'); ?></div>
  
  <ul id="mep_add_calender_links">
  
  <li><a href="http://www.google.com/calendar/event?
  action=TEMPLATE&text=<?php echo $event->post_title; ?>&dates=<?php echo mep_calender_date($event_start); ?>/<?php echo mep_calender_date($event_end); ?>&details=<?php echo substr(strip_tags($event->post_content),0,1000); ?>&location=<?php echo $location; ?>&trp=false&sprop=&sprop=name:'" target="_blank" class='mep-add-calender' rel="nofollow">Google</a></li>
  
  <li><a href="https://calendar.yahoo.com/?v=60&view=d&type=20&title=<?php echo $event->post_title; ?>&st=<?php echo mep_calender_date($event_start); ?>&et=<?php echo mep_calender_date($event_end); ?>&desc=<?php echo substr(strip_tags($event->post_content),0,1000); ?>&in_loc=<?php echo $location; ?>&uid=" target="_blank" class='mep-add-calender' rel="nofollow">Yahoo</a></li>
  
  
  <li><a href ="https://outlook.live.com/owa/?path=/calendar/view/Month&rru=addevent&dtstart=<?php echo mep_calender_date($event_start); ?>&dtend=<?php echo mep_calender_date($event_end); ?>&subject=<?php echo $event->post_title; ?>" target="_blank" class='mep-add-calender' rel="nofollow">Outlook</a></li>
  
  <li><a href="https://webapps.genprod.com/wa/cal/download-ics.php?date_end=<?php echo mep_calender_date($event_end); ?>&date_start=<?php echo mep_calender_date($event_start); ?>&summary=<?php echo $event->post_title; ?>&location=<?php echo $location; ?>&description=<?php echo substr(strip_tags($event->post_content),0,1000); ?>" class='mep-add-calender'>APPlE</a></li>
  </ul>
  
  
  <script type="text/javascript">
    
  jQuery(document).ready(function() {
  jQuery("#mep_add_calender_button").click(function () {
  jQuery("#mep_add_calender_links").toggle()
  });
  });
  
  </script>
  <style type="text/css">
    #mep_add_calender_links{    display: none;
      background: transparent;
      margin-top: -7px;
      list-style: navajowhite;
      margin: 0;
      padding: 0;}
  /*  #mep_add_calender_links li{list-style: none !important; line-height: 0.2px; border:1px solid #d5d5d5; border-radius: 10px; margin-bottom: 5px;}
    #mep_add_calender_links a{background: none !important; color: #333 !important; line-height: 0.5px !important; padding:10px; margin-bottom: 3px;}
    #mep_add_calender_links a:hover{color:#ffbe30;}*/
    #mep_add_calender_button{
     /*background: #ffbe30 none repeat scroll 0 0;*/
      border: 0 none;
      border-radius: 50px;
      /*color: #ffffff !important;*/
      display: inline-flex;
      font-size: 14px;
      font-weight: 600;
      overflow: hidden;
      padding: 15px 35px;
      position: relative;
      text-align: center;
      text-transform: uppercase;
      z-index: 1;
      cursor: pointer;
    }
  .mep-default-sidrbar-social .mep-event-meta{text-align: center;}
  </style>
  <?php
    $content = ob_get_clean();
    echo $content;
  }
  
  
  
  
  function mep_get_item_name($name){
    $explode_name = explode('_', $name, 2);
    $the_item_name = str_replace('-', ' ', $explode_name[0]);
    return $the_item_name;
  }
  
  
  
  function mep_get_item_price($name){
    $explode_name = explode('_', $name, 2);
    $the_item_name = str_replace('-', ' ', $explode_name[1]);
    return $the_item_name;
  }
  
  
  
  function mep_get_string_part($data,$string){  
    $pieces = explode(" x ", $data);
  return $pieces[$string]; // piece1
  }
  
  
  function mep_get_event_order_metadata($id,$part){
  global $wpdb;
  $table_name = $wpdb->prefix . 'woocommerce_order_itemmeta';
  $result = $wpdb->get_results( "SELECT * FROM $table_name WHERE order_item_id=$id" );
  
  foreach ( $result as $page )
  {
    if (strpos($page->meta_key, '_') !== 0) {
     echo mep_get_string_part($page->meta_key,$part).'<br/>';
   }
  }
  
  }
  
  add_action('woocommerce_account_dashboard','mep_ticket_lits_users');
  function mep_ticket_lits_users(){
  ob_start();
  ?>
  <div class="mep-user-ticket-list">
    <table>
      <tr>
        <th><?php _e('Name','mage-eventpress'); ?></th>
        <th><?php _e('Ticket','mage-eventpress'); ?></th>
        <th><?php _e('Event','mage-eventpress'); ?></th>
        <th><?php _e('Download','mage-eventpress'); ?></th>
      </tr>
      <?php 
       $args_search_qqq = array (
                       'post_type'        => array( 'mep_events_attendees' ),
                       'posts_per_page'   => -1,
      'meta_query' => array(
          array(
              'key' => 'ea_user_id',
              'value' => get_current_user_id()
          )
      )
    );
    $loop = new WP_Query( $args_search_qqq );
    while ($loop->have_posts()) {
    $loop->the_post(); 
  $event_id = get_post_meta( get_the_id(), 'ea_event_id', true );
    $event_meta = get_post_custom($event_id);
  
    $time = strtotime($event_meta['event_start_date'][0].' '.$event_meta['event_start_time'][0]);
      $newformat = date('Y-m-d H:i:s',$time);
        $order_array = array('processing', 'completed');
        $order_status = get_post_meta(get_the_ID(), 'ea_order_status', true);
  
  
        if ( time() < strtotime( $newformat ) ) {
            ?>
            <tr>
                <td><?php echo get_post_meta( get_the_id(), 'ea_name', true ); ?></td>
                <td><?php echo get_post_meta( get_the_id(), 'ea_ticket_type', true ); ?></td>
                <td><?php echo get_post_meta( get_the_id(), 'ea_event_name', true ); ?></td>
                <?php if (in_array($order_status, $order_array)){ ?>
                    <td><a href="<?php the_permalink(); ?>"><?php _e( 'Download', 'mage-eventpress' ); ?></a>
                    </td>
                <?php    }else{ ?>
                    <td></td>
                <?php    } ?>
            </tr>
            <?php
        }
    }    
      ?>
    </table>
  </div>
  <?php
  $content = ob_get_clean();
  echo $content;
  }
  
  // event_template_name();
  function event_template_name(){
  
            $template_name = 'index.php';
            $template_path = get_stylesheet_directory().'/mage-events/themes/';
            $default_path = plugin_dir_path( __DIR__ ) . 'templates/themes/'; 
  
          $template = locate_template( array($template_path . $template_name) );
  
         if ( ! $template ) :
           $template = $default_path . $template_name;
         endif;
  
  // echo $template_path;
  if (is_dir($template_path)) {
    $thedir = glob($template_path."*");
  }else{
  $thedir = glob($default_path."*");
  // file_get_contents('./people.txt', FALSE, NULL, 20, 14);
  }
  
  $theme = array();
  foreach($thedir as $filename){
      //Use the is_file function to make sure that it is not a directory.
      if(is_file($filename)){
        $file = basename($filename);
       $naame = str_replace("?>","",strip_tags(file_get_contents($filename, FALSE, NULL, 24, 14))); 
      }   
       $theme[$file] = $naame;
  }
  return $theme;
  }
  
  
  
  function event_single_template_list($current_theme){
  $themes = event_template_name();
          $buffer = '<select name="mep_event_template">';
          foreach ($themes as $num=>$desc){
            if($current_theme==$num){ $cc = 'selected'; }else{ $cc = ''; }
              $buffer .= "<option value=$num $cc>$desc</option>";
          }//end foreach
          $buffer .= '</select>';
          echo $buffer;
  }
  
  function mep_title_cutoff_words($text, $length){
      if(strlen($text) > $length) {
          $text = substr($text, 0, strpos($text, ' ', $length));
      }
  
      return $text;
  }
  
  function mep_get_tshirts_sizes($event_id){
    $event_meta   = get_post_custom($event_id);
    $tee_sizes  = $event_meta['mep_reg_tshirtsize_list'][0];
    $tszrray = explode(',', $tee_sizes);
  $ts = "";
    foreach ($tszrray as $value) {
      $ts .= "<option value='$value'>$value</option>";
    }
  return $ts;
  }
  
  
  function my_function_meta_deta() {
    global $order;
  
  $order_id = $_GET['post'];
      // Getting an instance of the order object
      $order      = wc_get_order( $order_id );
      $order_meta = get_post_meta($order_id); 
  
     # Iterating through each order items (WC_Order_Item_Product objects in WC 3+)
      foreach ( $order->get_items() as $item_id => $item_values ) {
          $product_id     = $item_values->get_product_id(); 
          $item_data      = $item_values->get_data();
          $product_id     = $item_data['product_id'];
          $item_quantity  = $item_values->get_quantity();
          $product        = get_page_by_title( $item_data['name'], OBJECT, 'mep_events' );
          $event_name     = $item_data['name'];
          $event_id       = $product->ID;
          $item_id        = $item_id;
      }
  
  $user_info_arr = wc_get_order_item_meta($item_id,'_event_user_info',true);
  
  // print_r($user_info_arr);
  
   ob_start();
  ?>
  <div class='event-atendee-infos'>
  <table class="atendee-info">
    <tr>
      <th>Name</th>
      <th>City</th>
    </tr>
    <?php 
    foreach ($user_info_arr as $_user_info) {
      $uname          = $_user_info['user_name'];
      $email          = $_user_info['user_email'];
      $phone          = $_user_info['user_phone'];
      $address        = $_user_info['user_address'];
      $gender         = $_user_info['user_gender'];
      $company        = $_user_info['user_company'];
      $designation    = $_user_info['user_designation'];
      $website        = $_user_info['user_website'];
      $vegetarian     = $_user_info['user_vegetarian'];
      $tshirtsize     = $_user_info['user_tshirtsize'];
      $ticket_type    = $_user_info['user_ticket_type'];
      $ticket_qty = $_user_info['user_ticket_qty'];
  ?>
  <tr><td><?php echo $uname; ?></td><td><?php echo $address; ?></td></tr>
  <?php
    }
    ?>
  </table>
  </div>
  <?php
   $content = ob_get_clean();
   echo $content;
  }
   // add_action( 'woocommerce_admin_order_totals_after_refunded','my_function_meta_deta', $order->id );
  
  
  
  // add_action( 'woocommerce_thankyou', 'woocommerce_thankyou_change_order_status', 10, 1 );
  function woocommerce_thankyou_change_order_status( $order_id ){
      if( ! $order_id ) return;
  
      $order = wc_get_order( $order_id );
  
      if( $order->get_status() == 'processing' )
          $order->update_status( 'completed' );
  }
  
  
  
  
  function mep_event_list_price($pid){
  global $post;
    $cur = get_woocommerce_currency_symbol();
    $mep_event_ticket_type = get_post_meta($pid, 'mep_event_ticket_type', true);
    $mep_events_extra_prices = get_post_meta($pid, 'mep_events_extra_prices', true);
    $n_price = get_post_meta($pid, '_price', true);
  
    if($n_price==0){
      $gn_price = "Free";
    }else{
      $gn_price = wc_price($n_price);
    }
  
    // if($mep_events_extra_prices){
    //   $gn_price = $cur.$mep_events_extra_prices[0]['option_price'];
    // }
  
    if($mep_event_ticket_type){
      $gn_price = wc_price($mep_event_ticket_type[0]['option_price_t']);
    }
    
  return $gn_price;
  }
  
  function mep_get_label($pid,$label_id,$default_text){
   return  mep_get_option( $label_id, 'label_setting_sec', $default_text);
  }
  
  // Add the custom columns to the book post type:
  add_filter( 'manage_mep_events_posts_columns', 'mep_set_custom_edit_event_columns' );
  function mep_set_custom_edit_event_columns($columns) {
  
      unset( $columns['date'] );
  
      $columns['mep_status'] = __( 'Status', 'mage-eventpress' );
  
      return $columns;
  }
  
  
  function mep_get_full_time_and_date($datetime){
     $date_format = get_option( 'date_format' );
     $time_format = get_option( 'time_format' );
     $wpdatesettings = $date_format.'  '.$time_format; 
     $user_set_format = mep_get_option( 'mep_event_time_format','general_setting_sec',12);
  
      if($user_set_format==12){
        echo date_i18n('D, d M Y  h:i A', strtotime($datetime));
      }
      if($user_set_format==24){
        echo date_i18n('D, d M Y  H:i', strtotime($datetime));
      }
      if($user_set_format=='wtss'){
        echo date_i18n($wpdatesettings, strtotime($datetime));
      }
  }
  
  
  function mep_get_only_time($datetime){
    $user_set_format = mep_get_option( 'mep_event_time_format','general_setting_sec',12);
  
  //echo $date_format = get_option( 'date_format' );
    $time_format = get_option( 'time_format' );
       if($user_set_format==12){
        echo date_i18n('h:i A', strtotime($datetime));
      }
      if($user_set_format==24){
        echo date_i18n('H:i', strtotime($datetime));
      }
      if($user_set_format=='wtss'){
        echo date_i18n($time_format, strtotime($datetime));
      }
  }
   
  
  function mep_get_event_city($id){
  $location_sts = get_post_meta($id,'mep_org_address',true);
  $event_meta = get_post_custom($id);
  if($location_sts){
  $org_arr = get_the_terms( $id, 'mep_org' );
  if(is_array($org_arr) && sizeof($org_arr) > 0 ){
  $org_id = $org_arr[0]->term_id;
    echo "<span>".get_term_meta( $org_id, 'org_city', true )."</span>";
  }
  }else{
  
    echo "<span>".$event_meta['mep_city'][0]."</span>";
  
  }
  }
  
   
  
  function mep_get_total_available_seat($event_id, $event_meta){
  $total_seat = mep_event_total_seat($event_id,'total');
  $total_resv = mep_event_total_seat($event_id,'resv');
  $total_sold = mep_ticket_sold($event_id);
  $total_left = $total_seat - ($total_sold + $total_resv);
  return $total_left;
  }
  
  
  
  
  function mep_event_location_item($event_id,$item_name){
    return get_post_meta($event_id,$item_name,true);
  }
  
  function mep_event_org_location_item($event_id,$item_name){
    $location_sts = get_post_meta($event_id,'mep_org_address',true);
  
      $org_arr      = get_the_terms( $event_id, 'mep_org' );
      if($org_arr){
      $org_id       = $org_arr[0]->term_id;
      return get_term_meta( $org_id, $item_name, true );
  }
  }
  
  function mep_get_all_date_time( $start_datetime, $more_datetime, $end_datetime ) {
  ob_start();
  
   $date_format = get_option( 'date_format' );
   $time_format = get_option( 'time_format' );
   $wpdatesettings = $date_format.$time_format; 
  $user_set_format = mep_get_option( 'mep_event_time_format','general_setting_sec',12);
    ?>
        <ul>
       <?php if($user_set_format==12){ ?>
        <?php $timeformatassettings = 'h:i A'; ?>
            <li><i class="fa fa-calendar"></i> <?php echo date_i18n($date_format, strtotime( $start_datetime ) ); ?>   <i class="fa fa-clock-o"></i> <?php echo date_i18n( 'h:i A', strtotime( $start_datetime ) ); ?></li>
        <?php } ?>    
      <?php  if($user_set_format==24){ ?>
      <?php $timeformatassettings = 'H:i'; ?>
            <li><i class="fa fa-calendar"></i> <?php echo date_i18n($date_format, strtotime( $start_datetime ) ); ?>   <i class="fa fa-clock-o"></i> <?php echo date_i18n( 'H:i', strtotime( $start_datetime ) );  ?></li>
        <?php } ?>
      <?php if($user_set_format=='wtss'){ ?>
      <?php $timeformatassettings = get_option( 'time_format' ); ?>
            <li><i class="fa fa-calendar"></i> <?php echo date_i18n($date_format, strtotime( $start_datetime ) ); ?>   <i class="fa fa-clock-o"></i> <?php echo date_i18n( $time_format, strtotime( $start_datetime ) ); } ?></li>
         }
         }
  
        ?>
      <?php
    
  
      foreach ( $more_datetime as $_more_datetime ) {
        ?>
                <li><i class="fa fa-calendar"></i> <?php echo date_i18n($date_format, strtotime( $_more_datetime['event_more_date'] ) ); ?> <i class="fa fa-clock-o"></i> <?php echo date_i18n($timeformatassettings, strtotime( $_more_datetime['event_more_date'] ) ) ?></li>
        <?php
      }
      ?>
  
       <?php 
       if($user_set_format==12){ 
         $timeformatassettings = 'h:i A'; 
       }   
       if($user_set_format==24){ 
       $timeformatassettings = 'H:i'; 
        } 
        if($user_set_format=='wtss'){ 
        $timeformatassettings = get_option( 'time_format' );
        }
  
        ?>
  
  
            <li><i class="fa fa-calendar"></i> <?php echo date_i18n($date_format, strtotime( $end_datetime ) ); ?>   <i class="fa fa-clock-o"></i> <?php echo date_i18n($timeformatassettings, strtotime( $end_datetime ) ); ?> <span style='font-size: 12px;font-weight: bold;'>(<?php _e('End','mage-eventpress'); ?>)</span></li>
        </ul>
    <?php
  $content = ob_get_clean();
  echo $content;
  }
  
  function get_single_date_time( $start_datetime, $end_datetime ) {
  
    $date_format = get_option( 'date_format' );
    $time_format = get_option( 'time_format' );
    $wpdatesettings = $date_format.$time_format;
  
    $start_date = date_i18n( $date_format, strtotime( $start_datetime ) );
    $end_date   = date_i18n( $date_format, strtotime( $end_datetime ) );
  
    $nameOfDay    = date_i18n( $date_format, strtotime( $start_date ) );
    $nameOfDayEnd = date_i18n( $date_format, strtotime( $end_date ) );
  
    $start_time = date_i18n($time_format, strtotime( $start_datetime ) );
    $end_time   = date_i18n($time_format, strtotime( $end_datetime ) );
  
    if ( $start_date == $end_date ) {
      return $nameOfDay . " " . $start_time . " - " . $end_time;
    } else {
      return $nameOfDay . " " . $start_time . "  " . $nameOfDayEnd . " " . $end_time;
    }
  
  }
  
  
  
  function mep_get_event_locaion_item($event_id,$item_name){
    if($event_id){
  $location_sts = get_post_meta($event_id,'mep_org_address',true);
  
  
  if($item_name=='mep_location_venue'){
    if($location_sts){
      $org_arr      = get_the_terms( $event_id, 'mep_org' );
     
      if(is_array($org_arr) && sizeof($org_arr)>0 ){
      $org_id       = $org_arr[0]->term_id;
        return get_term_meta( $org_id, 'org_location', true );
      }
    }else{
      return get_post_meta($event_id,'mep_location_venue',true);
    }
    return null;
  }
  
  if($item_name=='mep_location_venue'){
    if($location_sts){
      $org_arr      = get_the_terms( $event_id, 'mep_org' );
  if(is_array($org_arr) && sizeof($org_arr)>0 ){
      $org_id       = $org_arr[0]->term_id;
        return get_term_meta( $org_id, 'org_location', true );
      }
      
    }else{
      return get_post_meta($event_id,'mep_location_venue',true);
    }
  }
  
  
  if($item_name=='mep_street'){
    if($location_sts){
      $org_arr      = get_the_terms( $event_id, 'mep_org' );
      if(is_array($org_arr) && sizeof($org_arr)>0 ){
      $org_id       = $org_arr[0]->term_id;
        return get_term_meta( $org_id, 'org_street', true );
      }
    }else{
      return get_post_meta($event_id,'mep_street',true);
    }
  }
  
  
  if($item_name=='mep_city'){
    if($location_sts){
      $org_arr      = get_the_terms( $event_id, 'mep_org' );
      if(is_array($org_arr) && sizeof($org_arr)>0 ){
      $org_id       = $org_arr[0]->term_id;
        return get_term_meta( $org_id, 'org_city', true );
      }
    }else{
      return get_post_meta($event_id,'mep_city',true);
    }
  }
  
  
  if($item_name=='mep_state'){
    if($location_sts){
      $org_arr      = get_the_terms( $event_id, 'mep_org' );
      if(is_array($org_arr) && sizeof($org_arr)>0 ){
      $org_id       = $org_arr[0]->term_id;
        return get_term_meta( $org_id, 'org_state', true );
      }
    }else{
      return get_post_meta($event_id,'mep_state',true);
    }
  }
  
  
  
  if($item_name=='mep_postcode'){
    if($location_sts){
      $org_arr      = get_the_terms( $event_id, 'mep_org' );
      if(is_array($org_arr) && sizeof($org_arr)>0 ){
      $org_id       = $org_arr[0]->term_id;
        return get_term_meta( $org_id, 'org_postcode', true );
      }
    }else{
      return get_post_meta($event_id,'mep_postcode',true);
    }
  }
  
  
  if($item_name=='mep_country'){
    if($location_sts){
      $org_arr      = get_the_terms( $event_id, 'mep_org' );
      if(is_array($org_arr) && sizeof($org_arr)>0 ){
      $org_id       = $org_arr[0]->term_id;
        return get_term_meta( $org_id, 'org_country', true );
      }
    }else{
      return get_post_meta($event_id,'mep_country',true);
    }
  }
  
  
  }
  
  }
  
  function mep_save_attendee_info_into_cart($product_id){
  
    $user = array();
  
    if(isset($_POST['user_name'])){
      $mep_user_name          = $_POST['user_name'];
    }else{ $mep_user_name=""; } 
  
    if(isset($_POST['user_email'])){  
      $mep_user_email         = $_POST['user_email'];
    }else{ $mep_user_email=""; } 
  
    if(isset($_POST['user_phone'])){  
      $mep_user_phone         = $_POST['user_phone'];
    }else{ $mep_user_phone=""; } 
  
    if(isset($_POST['user_address'])){  
      $mep_user_address       = $_POST['user_address'];
    }else{ $mep_user_address=""; } 
  
    if(isset($_POST['gender'])){  
      $mep_user_gender        = $_POST['gender'];
    }else{ $mep_user_gender=""; } 
  
    if(isset($_POST['tshirtsize'])){  
      $mep_user_tshirtsize    = $_POST['tshirtsize'];
    }else{ $mep_user_tshirtsize=""; } 
  
    if(isset($_POST['user_company'])){  
      $mep_user_company       = $_POST['user_company'];
    }else{ $mep_user_company=""; } 
  
    if(isset($_POST['user_designation'])){  
      $mep_user_desg          = $_POST['user_designation'];
    }else{ $mep_user_desg=""; } 
  
    if(isset($_POST['user_website'])){  
      $mep_user_website       = $_POST['user_website'];
    }else{ $mep_user_website=""; } 
  
    if(isset($_POST['vegetarian'])){  
      $mep_user_vegetarian    = $_POST['vegetarian'];
    }else{ $mep_user_vegetarian=""; } 
  
  
    
    if(isset($_POST['ticket_type'])){  
      $mep_user_ticket_type   = $_POST['ticket_type'];
    }else{ $mep_user_ticket_type=""; } 
  
  
  
    if(isset($_POST['event_date'])){  
      $event_date   = $_POST['event_date'];
    }else{ $event_date=""; } 
  
  
    if(isset($_POST['mep_event_id'])){  
      $mep_event_id   = $_POST['mep_event_id'];
    }else{ $mep_event_id=""; }
  
      if ( isset( $_POST['option_qty'] ) ) {
          $mep_user_option_qty = $_POST['option_qty'];
      } else {
          $mep_user_option_qty = "";
      }
  
  
  
  
  // print_r($event_date);
  // die();
  
  
  
  
  
  
  
    if(isset($_POST['mep_ucf'])){
      $mep_user_cfd           = $_POST['mep_ucf'];
    }else{
      $mep_user_cfd           = "";
    }
  
    if($mep_user_name){ $count_user = count($mep_user_name); } else{ $count_user = 0; }
  
    for ( $iu = 0; $iu < $count_user; $iu++ ) {
      
      if (isset($mep_user_name[$iu])):
        $user[$iu]['user_name'] = stripslashes( strip_tags( $mep_user_name[$iu] ) );
        endif;
  
      if (isset($mep_user_email[$iu])) :
        $user[$iu]['user_email'] = stripslashes( strip_tags( $mep_user_email[$iu] ) );
        endif;
  
      if (isset($mep_user_phone[$iu])) :
        $user[$iu]['user_phone'] = stripslashes( strip_tags( $mep_user_phone[$iu] ) );
        endif;
  
      if (isset($mep_user_address[$iu])) :
        $user[$iu]['user_address'] = stripslashes( strip_tags( $mep_user_address[$iu] ) );
        endif;
  
      if (isset($mep_user_gender[$iu])) :
        $user[$iu]['user_gender'] = stripslashes( strip_tags( $mep_user_gender[$iu] ) );
        endif;
  
      if (isset($mep_user_tshirtsize[$iu])) :
        $user[$iu]['user_tshirtsize'] = stripslashes( strip_tags( $mep_user_tshirtsize[$iu] ) );
        endif;
  
      if (isset($mep_user_company[$iu])) :
        $user[$iu]['user_company'] = stripslashes( strip_tags( $mep_user_company[$iu] ) );
        endif;
  
      if (isset($mep_user_desg[$iu])) :
        $user[$iu]['user_designation'] = stripslashes( strip_tags( $mep_user_desg[$iu] ) );
        endif;
  
      if (isset($mep_user_website[$iu])) :
        $user[$iu]['user_website'] = stripslashes( strip_tags( $mep_user_website[$iu] ) );
        endif;
  
      if (isset($mep_user_vegetarian[$iu])) :
        $user[$iu]['user_vegetarian'] = stripslashes( strip_tags( $mep_user_vegetarian[$iu] ) );
        endif;
  
      if (isset($mep_user_ticket_type[$iu])) :
        $user[$iu]['user_ticket_type'] = stripslashes( strip_tags( $mep_user_ticket_type[$iu] ) );
        endif;    
  
      if (isset($event_date[$iu])) :
        $user[$iu]['user_event_date'] = stripslashes( strip_tags( $event_date[$iu] ) );
        endif;    
  
      if (isset($mep_event_id[$iu])) :
        $user[$iu]['user_event_id'] = stripslashes( strip_tags( $mep_event_id[$iu] ) );
        endif;
  
        if ( isset( $mep_user_option_qty[ $iu ] ) ) :
            $user[ $iu ]['user_ticket_qty'] = stripslashes( strip_tags( $mep_user_option_qty[ $iu ] ) );
        endif;
  
  
    $mep_form_builder_data = get_post_meta($product_id, 'mep_form_builder_data', true);
    if ( $mep_form_builder_data ) {
      foreach ( $mep_form_builder_data as $_field ) {
            $user[$iu][$_field['mep_fbc_id']] = stripslashes( strip_tags( $_POST[$_field['mep_fbc_id']][$iu] ) );
        }
      }
    }
    return $user;
  }
  
  
  
  function mep_wc_price( $price, $args = array() ) {
    $args = apply_filters(
      'wc_price_args', wp_parse_args(
        $args, array(
          'ex_tax_label'       => false,
          'currency'           => '',
          'decimal_separator'  => wc_get_price_decimal_separator(),
          'thousand_separator' => wc_get_price_thousand_separator(),
          'decimals'           => wc_get_price_decimals(),
          'price_format'       => get_woocommerce_price_format(),
        )
      )
    );
  
    $unformatted_price = $price;
    $negative          = $price < 0;
    $price             = apply_filters( 'raw_woocommerce_price', floatval( $negative ? $price * -1 : $price ) );
    $price             = apply_filters( 'formatted_woocommerce_price', number_format( $price, $args['decimals'], $args['decimal_separator'], $args['thousand_separator'] ), $price, $args['decimals'], $args['decimal_separator'], $args['thousand_separator'] );
  
    if ( apply_filters( 'woocommerce_price_trim_zeros', false ) && $args['decimals'] > 0 ) {
      $price = wc_trim_zeros( $price );
    }
  
    $formatted_price = ( $negative ? '-' : '' ) . sprintf( $args['price_format'], '' . '' . '', $price );
    $return          = '' . $formatted_price . '';
  
    if ( $args['ex_tax_label'] && wc_tax_enabled() ) {
      $return .= '' . WC()->countries->ex_tax_or_vat() . '';
    }
  
    /**
     * Filters the string of price markup.
     *
     * @param string $return            Price HTML markup.
     * @param string $price             Formatted price.
     * @param array  $args              Pass on the args.
     * @param float  $unformatted_price Price as float to allow plugins custom formatting. Since 3.2.0.
     */
    return apply_filters( 'mep_wc_price', $return, $price, $args, $unformatted_price );
  }
  
  
  
  function mep_get_event_total_seat($event_id,$m=null,$t=null){
  $total_seat = mep_event_total_seat($event_id,'total');
  $total_resv = mep_event_total_seat($event_id,'resv');
  $total_sold = mep_ticket_sold($event_id);
  $total_left = $total_seat - ($total_sold + $total_resv);
  if($t=='multi'){
    ?>
      <span style="background: #dc3232;color: #fff;padding: 5px 10px;"> <?php echo ($total_seat * $m) - ($total_sold + $total_resv); ?>/<?php echo $total_seat * $m; ?> </span>
    <?php
  }else{
    ?>
  <span style="background: #dc3232;color: #fff;padding: 5px 10px;"> <?php echo $total_left; ?>/<?php echo $total_seat; ?> </span>
    <?php
  }
  }
  
  
  
  
  function mep_reset_event_booking($event_id){
    $mep_event_ticket_type = get_post_meta($event_id, 'mep_event_ticket_type', true);
    if($mep_event_ticket_type){
        foreach ( $mep_event_ticket_type as $field ) {
          $qm = $field['option_name_t'];
          $tesqn = $event_id.str_replace(' ', '', $qm);
          $reset =  update_post_meta($event_id,"mep_xtra_$tesqn",0);
        }
      // if($reset){ return 'Reset Done!'; }
    }else{
      $reset =  update_post_meta($event_id,"total_booking",0);
      // if($reset){ return 'Reset Done!'; }
    }
    $args_search_qqq = array (
                       'post_type'        => array( 'mep_events_attendees' ),
                       'posts_per_page'   => -1,
                       'post_status'      => 'publish',
                       'meta_query'       => array(
                          array(
                              'key'       => 'ea_event_id',
                              'value'     => $event_id,
                              'compare'   => '='
                          )
                      )                      
                  );  
    $loop = new WP_Query($args_search_qqq);
    while ($loop->have_posts()) {
    $loop->the_post(); 
      $post_id = get_the_id();
      $status = 'trash';
      $current_post = get_post( $post_id, 'ARRAY_A' );
      $current_post['post_status'] = $status;
      wp_update_post($current_post);
    }
  }
  
  
  
  // Add the custom columns to the book post type:
  add_filter( 'manage_mep_events_posts_columns', 'mep_set_custom_mep_events_columns' );
  function mep_set_custom_mep_events_columns($columns) {
  
      $columns['mep_event_seat'] = __( 'Seats', 'mage-eventpress' );
  
      return $columns;
  }
  
  
  // Add the data to the custom columns for the book post type:
  add_action( 'manage_mep_events_posts_custom_column' , 'mep_mep_events_column', 10, 2 );
  function mep_mep_events_column( $column, $post_id ) {
      switch ( $column ) {
  
          case 'mep_event_seat' : 
            $recurring            = get_post_meta($post_id, 'mep_enable_recurring', true) ? get_post_meta($post_id, 'mep_enable_recurring', true) : 'no';
  
  
            if($recurring == 'yes'){
              $event_more_dates    = count(get_post_meta($post_id,'mep_event_more_date',true))+1;
              echo mep_get_event_total_seat($post_id,$event_more_dates,'multi'); 
            }else{
              echo mep_get_event_total_seat($post_id); 
            }          
            
          break; 
      }
  }
  
  
  function mep_get_term_as_class($post_id,$taxonomy){
      $tt     = get_the_terms($post_id,$taxonomy);
      if($tt){
      $t_class = array();
      foreach($tt as $tclass){
          $t_class[] = $tclass->slug;         
      }
      $main_class = implode(' ',$t_class);
      return $main_class;
    }else{
      return null;
    }
  }
  
  
  function mep_ticket_type_sold($event_id,$type,$date=''){
     if($date){
    $args = array(
            'post_type' => 'mep_events_attendees',
            'posts_per_page' => -1,
  
        'meta_query' => array(    
          'relation' => 'AND',
          array(    
            'relation' => 'AND',           
            array(
              'key'       => 'ea_event_id',
              'value'     => $event_id,
              'compare'   => '='
            ),		        
            array(
              'key'       => 'ea_ticket_type',
              'value'     => $type,
              'compare'   => '='          
            ),		        
            array(
              'key'       => 'ea_event_date',
              'value'     => $date,
              'compare'   => 'LIKE'
            )
            ),array(    
              'relation' => 'OR',           
              array(
                'key'       => 'ea_order_status',
                'value'     => 'processing',
                'compare'   => '='
              ),		        
              array(
                'key'       => 'ea_order_status',
                'value'     => 'completed',
                'compare'   => '='
              )
              )
          )            
        ); 
     }else{
         $args = array(
            'post_type' => 'mep_events_attendees',
            'posts_per_page' => -1,
  
        'meta_query' => array(    
          'relation' => 'AND',
          array(    
            'relation' => 'AND',           
            array(
              'key'       => 'ea_event_id',
              'value'     => $event_id,
              'compare'   => '='
            ),		        
            array(
              'key'       => 'ea_ticket_type',
              'value'     => $type,
              'compare'   => '='          
            )
            ),array(    
              'relation' => 'OR',           
              array(
                'key'       => 'ea_order_status',
                'value'     => 'processing',
                'compare'   => '='
              ),		        
              array(
                'key'       => 'ea_order_status',
                'value'     => 'completed',
                'compare'   => '='
              )
              )
          )            
        ); 
     }
     $loop = new WP_Query($args);
    return $loop->post_count;
  }
  
  
  function mep_extra_service_sold($event_id,$type,$date){
      //echo $date;
    $args = array(
            'post_type' => 'mep_extra_service',
            'posts_per_page' => -1,
  
        'meta_query' => array(    
          'relation' => 'AND',
          array(    
            'relation' => 'AND',           
            array(
              'key'       => 'ea_extra_service_event',
              'value'     => $event_id,
              'compare'   => '='
            ),		        
            array(
              'key'       => 'ea_extra_service_name',
              'value'     => $type,
              'compare'   => '='          
            ),		        
            array(
              'key'       => 'ea_extra_service_event_date',
              'value'     => $date,
              'compare'   => 'LIKE'
            )
            ),array(    
              'relation' => 'OR',           
              array(
                'key'       => 'ea_extra_service_order_status',
                'value'     => 'processing',
                'compare'   => '='
              ),		        
              array(
                'key'       => 'ea_extra_service_order_status',
                'value'     => 'completed',
                'compare'   => '='
              )
              )
          )            
        );            
     $loop = new WP_Query($args);
     $count = 0;
     foreach($loop->posts as $sold_service){
         $pid = $sold_service->ID;
         $count = $count + get_post_meta($pid,'ea_extra_service_qty',true);
     }
     
     
     
    return $count;
  }
  
  
  function mep_ticket_sold($event_id){
      
    $args = array(
            'post_type' => 'mep_events_attendees',
            'posts_per_page' => -1,
  
        'meta_query' => array(    
          'relation' => 'AND',
          array(    
            'relation' => 'AND',           
            array(
              'key'       => 'ea_event_id',
              'value'     => $event_id,
              'compare'   => '='
            )
            ),array(    
              'relation' => 'OR',           
              array(
                'key'       => 'ea_order_status',
                'value'     => 'processing',
                'compare'   => '='
              ),		        
              array(
                'key'       => 'ea_order_status',
                'value'     => 'completed',
                'compare'   => '='
              )
              )
          )            
        );            
     $loop = new WP_Query($args);
    return $loop->post_count;
  }
  
  
  
  function mep_event_total_seat($event_id,$type){
    $mep_event_ticket_type = get_post_meta($event_id, 'mep_event_ticket_type', true);
    // print_r($mep_event_ticket_type);
    $total = 0;
    if(is_array($mep_event_ticket_type) && sizeof($mep_event_ticket_type) > 0){
    foreach ( $mep_event_ticket_type as $field ) {
        if($type == 'total'){
          $total_name = (int) $field['option_qty_t'];
        }elseif($type == 'resv'){
          $total_name = (int) $field['option_rsv_t'];
        }
      $total = $total_name + $total;
    }
    }
    return $total;
  }
  
  function get_mep_datetime($date,$type){
      
  $user_set_format    = mep_get_option( 'mep_event_time_format','general_setting_sec',12);
  $date_format        = get_option( 'date_format' );
  $time_format        = get_option( 'time_format' );
  $wpdatesettings     = $date_format.'  '.$time_format; 
  
  
  
    if($type == 'date'){
      if($user_set_format == 'wtss'){
        return date_i18n( $date_format, strtotime( $date ) );
      }else{
        return date_i18n( 'Y-m-d', strtotime( $date ) );
      }
    }
    if($type == 'date-time'){
        if($user_set_format == 12){
          return date_i18n( 'Y-m-d h:i A', strtotime( $date ) );
        }elseif($user_set_format == 24){
          return date_i18n( 'Y-m-d H:i', strtotime( $date ) );
        }elseif($user_set_format == 'wtss'){
          return date_i18n( $wpdatesettings, strtotime( $date ) );
        }
    }
    if($type == 'date-text'){
      if($user_set_format == 12){
        return date_i18n( 'd,D M Y', strtotime( $date ) );
      }elseif($user_set_format == 'wtss'){
        return date_i18n( $date_format, strtotime( $date ) );      
      }else{
        return date_i18n( 'd, D M Y', strtotime( $date ) );
      }    
    }
  
    if($type == 'date-time-text'){
      if($user_set_format == 12){
        return date_i18n( 'd, D M Y h:i A', strtotime( $date ) );
      }elseif($user_set_format == 'wtss'){
        return date_i18n( $wpdatesettings, strtotime( $date ) );
      }else{
        return date_i18n( 'd, D M Y H:i', strtotime( $date ) );
      }
    }
    if($type == 'time'){
      if($user_set_format == 12){
        return date( 'h:i A', strtotime( $date ) );
      }elseif($user_set_format == 'wtss'){
        return date( $time_format, strtotime( $date ) );
      }else{
        return date( 'H:i', strtotime( $date ) );
      }
    }
  }
  
  
  
  
  
  function mep_on_post_publish( $post_id, $post, $update ) {
    if ($post->post_type == 'mep_events' && $post->post_status == 'publish' && empty(get_post_meta( $post_id, 'check_if_run_once' ))) {
       // print_r($post);
  
      // ADD THE FORM INPUT TO $new_post ARRAY
      $new_post = array(
        'post_title'    =>   $post->post_title,
        'post_content'  =>   '',
        'post_category' =>   array(),  // Usable for custom taxonomies too
        'tags_input'    =>   array(),
        'post_status'   =>   'publish', // Choose: publish, preview, future, draft, etc.
        'post_type'     =>   'product'  //'post',page' or use a custom post type if you want to
        );
        //SAVE THE POST
        $pid                = wp_insert_post($new_post);
  
        update_post_meta( $post_id, 'link_wc_product', $pid );
        update_post_meta( $pid, 'link_mep_event', $post_id );
        update_post_meta( $pid, '_price', 0.01 );
        update_post_meta( $pid, '_sold_individually', 'yes' );
        update_post_meta( $pid, '_virtual', 'yes' );
        $terms = array( 'exclude-from-catalog', 'exclude-from-search' );
        wp_set_object_terms( $pid, $terms, 'product_visibility' );
  
        update_post_meta( $post_id, 'check_if_run_once', true );
      //die();
    }
  }
  add_action(  'wp_insert_post',  'mep_on_post_publish', 10, 3 );
  
  function mep_count_hidden_wc_product($event_id){
    $args = array(
      'post_type'      => 'product',
      'posts_per_page' => -1,            
          'meta_query' => array(		        					 
              array(
                  'key'       => 'link_mep_event',
                  'value'     => $event_id,
                  'compare'   => '='
              )            
      )            
  );                
  $loop = new WP_Query($args);
  print_r($loop->posts);
  return $loop->post_count;
  }
  
  
  add_action('save_post','mep_wc_link_product_on_save',99,1);
  function mep_wc_link_product_on_save($post_id){
  
    if (get_post_type($post_id) == 'mep_events') { 
  
      if ( ! isset( $_POST['mep_event_reg_btn_nonce'] ) ||
      ! wp_verify_nonce( $_POST['mep_event_reg_btn_nonce'], 'mep_event_reg_btn_nonce' ) )
        return;
      
      if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;
      
      if (!current_user_can('edit_post', $post_id))
        return;
        $event_name = get_the_title($post_id);
  
        if(mep_count_hidden_wc_product($post_id) == 0 || empty(get_post_meta($post_id,'link_wc_product',true))){
          mep_create_hidden_event_product($post_id,$event_name);
        }
  
        $product_id = get_post_meta($post_id,'link_wc_product',true) ? get_post_meta($post_id,'link_wc_product',true) : $post_id;
        set_post_thumbnail( $product_id, get_post_thumbnail_id($post_id) );
        wp_publish_post( $product_id );
        
        
   
          $_tax_status                = isset($_POST['_tax_status']) ? strip_tags($_POST['_tax_status']) : 'none';
          $_tax_class                 = isset($_POST['_tax_class']) ? strip_tags($_POST['_tax_class']) : '';
      
          $update__tax_status         = update_post_meta( $product_id, '_tax_status', $_tax_status);
          $update__tax_class          = update_post_meta( $product_id, '_tax_class', $_tax_class);
          $update__tax_class          = update_post_meta( $product_id, '_stock_status', 'instock');
          $update__tax_class          = update_post_meta( $product_id, '_manage_stock', 'no');
          $update__tax_class          = update_post_meta( $product_id, '_virtual', 'yes');
          $update__tax_class          = update_post_meta( $product_id, '_sold_individually', 'yes');
        
        
        
        // Update post
        $my_post = array(
          'ID'           => $product_id,
          'post_title'   => $event_name, // new title
        );
  
        // unhook this function so it doesn't loop infinitely
        remove_action('save_post', 'mep_wc_link_product_on_save');
        // update the post, which calls save_post again
        wp_update_post( $my_post );
        // re-hook this function
        add_action('save_post', 'mep_wc_link_product_on_save');
        // Update the post into the database
       
  
    }
  
  }
  
  
  
  
  
  add_action('parse_query', 'product_tags_sorting_query');
  function product_tags_sorting_query($query) {
      global $pagenow;
  
      $taxonomy  = 'product_visibility';
  
      $q_vars    = &$query->query_vars;
  
      if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == 'product') {
  
 
          $tax_query = array([
            'taxonomy' => 'product_visibility',
            'field' => 'slug',
            'terms' => 'exclude-from-catalog',
            'operator' => 'NOT IN',
            ]);          
            $query->set( 'tax_query', $tax_query );
      }
  
  }
  
  
  
  
  function get_event_list_js($id,$event_meta,$currency_pos){
      ob_start();
  ?>
  <script>
  jQuery(document).ready( function() {
  
  
  
  jQuery(document).on("change", ".etp_<?php echo $id; ?>", function() {
      var sum = 0;
      jQuery(".etp_<?php echo $id; ?>").each(function(){
          sum += +jQuery(this).val();
      });
      jQuery("#ttyttl_<?php echo $id; ?>").html(sum);
  });
  
  jQuery(".extra-qty-box_<?php echo $id; ?>").on('change', function() {
          var sum = 0;
          var total = <?php if($event_meta['_price'][0]){ echo $event_meta['_price'][0]; }else{ echo 0; } ?>;
  
          jQuery('.price_jq_<?php echo $id; ?>').each(function () {
              var price = jQuery(this);
              var count = price.closest('tr').find('.extra-qty-box_<?php echo $id; ?>');
              sum = (price.html() * count.val());
              total = total + sum;
              // price.closest('tr').find('.cart_total_price').html(sum + "â‚´");
  
          });
  
          jQuery('#usertotal_<?php echo $id; ?>').html("<?php if($currency_pos=="left"){ echo get_woocommerce_currency_symbol(); } ?>" + total + "<?php if($currency_pos=="right"){ echo get_woocommerce_currency_symbol(); } ?>");
          jQuery('#rowtotal_<?php echo $id; ?>').val(total);
  
      }).change(); //trigger change event on page load
  
  
  <?php 
  $mep_event_ticket_type = get_post_meta($id, 'mep_event_ticket_type', true);
  if($mep_event_ticket_type){
  $count =1;
  foreach ( $mep_event_ticket_type as $field ) {
  $qm = $field['option_name_t'];
  ?>
  
  //jQuery('.btn-mep-event-cart').hide();
  
  jQuery('.btn-mep-event-cart_<?php echo $id; ?>').attr('disabled','disabled');
  
  jQuery('#eventpxtp_<?php echo $id; ?>_<?php echo $count; ?>').on('change', function () {
          
          var inputs = jQuery("#ttyttl_<?php echo $id; ?>").html() || 0;
          var inputs = jQuery('#eventpxtp_<?php echo $id; ?>_<?php echo $count; ?>').val() || 0;
          var input = parseInt(inputs);
          var children=jQuery('#dadainfo_<?php echo $count; ?> > div').length || 0; 
             
          jQuery(document).on("change", ".etp_<?php echo $id; ?>", function() {
              var TotalQty = 0;
              jQuery(".etp_<?php echo $id; ?>").each(function(){
              TotalQty += +jQuery(this).val();
              });
              //alert(sum);
  
              if(TotalQty == 0){
                  //jQuery('.btn-mep-event-cart').hide();
                  jQuery('.btn-mep-event-cart_<?php echo $id; ?>').attr('disabled','disabled');
                  jQuery('#mep_btn_notice_<?php echo $id; ?>').show();
              }else{
                  //jQuery('.btn-mep-event-cart').show();
                  jQuery('.btn-mep-event-cart_<?php echo $id; ?>').removeAttr('disabled');
                  jQuery('#mep_btn_notice_<?php echo $id; ?>').hide();
              }     
  
          });
  
          if(input < children){
              jQuery('#dadainfo_<?php echo $count; ?>').empty();
              children=0;
          }
          
          for (var i = children+1; i <= input; i++) {
              jQuery('#dadainfo_<?php echo $count; ?>').append(
              jQuery('<div/>')
                  .attr("id", "newDiv" + i)
                  .html("<?php do_action("mep_reg_fields",$id); ?>")
                  );
          }
      });
  <?php 
      $count++;
      }
   }else{
  ?>
  
  jQuery('#mep_btn_notice_<?php echo $id; ?>').hide();
  
  jQuery('#quantity_5a7abbd1bff73').on('change', function () {        
          var input = jQuery('#quantity_5a7abbd1bff73').val() || 0;
          var children=jQuery('#divParent > div').length || 0;     
          
          if(input < children){
              jQuery('#divParent').empty();
              children=0;
          }        
          for (var i = children+1; i <= input; i++) {
              jQuery('#divParent').append(
              jQuery('<div/>')
                  .attr("id", "newDiv" + i)
                  .html("<?php do_action('mep_reg_fields',$id); ?>")
                  );
          }
      });
  <?php
  } 
  ?>
  });
  </script>
  
  <?php
  echo $content = ob_get_clean();
  }
  
  function mep_set_email_content_type(){
      return "text/html";
  }
  add_filter( 'wp_mail_content_type','mep_set_email_content_type' );
  
  
  add_filter('woocommerce_cart_item_price', 'mep_avada_mini_cart_price_fixed', 100, 3);
  function mep_avada_mini_cart_price_fixed($price,$cart_item,$r){
  $price = wc_price($cart_item['line_total']);
  return $price;
  }
  
  
  
  function mage_array_strip($string, $allowed_tags = NULL){
                        if (is_array($string))
                        {
                            foreach ($string as $k => $v)
                            {
                                $string[$k] = mage_array_strip($v, $allowed_tags);
                            }
                            return $string;
                        }
                        return strip_tags($string, $allowed_tags);
                    }