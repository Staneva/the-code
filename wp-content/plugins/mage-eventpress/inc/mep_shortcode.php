<?php
if (!defined('ABSPATH')) {
    die;
} // Cannot access pages directly.


add_shortcode('event-calendar', 'mep_cal_func');


function mep_cal_func($atts, $content = null){
    ob_start();
    echo mep_event_calender();
    return ob_get_clean();
}


function mep_event_calender(){
$event_expire_on 			= mep_get_option( 'mep_event_expire_on_datetimes', 'general_setting_sec', 'event_start_datetime');
    ?>
    <div class="event-calendar"></div>
    <script>
        jQuery(document).ready(function () {
            const myEvents = [
                <?php
                $now = current_time('Y-m-d H:i:s');
                $args_search_qqq = array(
                    'post_type' => array('mep_events'),
                    'posts_per_page' => -1,
                    'meta_query' => array(
                        array(
                            'key' => $event_expire_on,
                            'value' => $now,
                            'compare' => '>'
                        )
                    )

                );

                $loop = new WP_Query($args_search_qqq);
                $i = 1;
                $count = $loop->post_count - 1;

                while ($loop->have_posts()) {
                $loop->the_post();
                $event_meta = get_post_custom(get_the_id());
                $author_terms = get_the_terms(get_the_id(), 'mep_org');
                $time = strtotime($event_meta['event_start_date'][0] . ' ' . $event_meta['event_start_time'][0]);
                $newformat = date_i18n('Y-m-d H:i:s', $time);

                // echo $newformat;
                // if(time() < strtotime($newformat)){
                ?>
                {
                    start: '<?php echo date_i18n('Y-m-d H:i', strtotime($event_meta['event_start_date'][0] . ' ' . $event_meta['event_start_time'][0])); ?>',
                    end: '<?php echo date_i18n('Y-m-d H:i', strtotime($event_meta['event_end_date'][0] . ' ' . $event_meta['event_end_time'][0])); ?>',
                    title: '<?php the_title(); ?>',
                    url: '<?php the_permalink(); ?>',
                    class: '',
                    color: '#000',
                    data: {}
                },<?php //if ($i == $count) { echo "";}else{ echo ","; } ?><?php $i++;  } wp_reset_postdata(); ?>]

            jQuery('.event-calendar').equinox({
                events: myEvents
            });
        });
    </script>
    <?php
}


add_shortcode('event-list', 'mep_event_list');
function mep_event_list($atts, $content = null){
    $defaults = array(
        "cat" => "0",
        "org" => "0",
        "style" => "grid",
        "column" => 2,
        "cat-filter" => "no",
        "org-filter" => "no",
        "show" => "-1",
        "pagination" => "no",
        'sort' => 'ASC'
    );

    $params         = shortcode_atts($defaults, $atts);
    $cat            = $params['cat'];
    $org            = $params['org'];
    $style          = $params['style'];
    $cat_f          = $params['cat-filter'];
    $org_f          = $params['org-filter'];
    $show           = $params['show'];
    $pagination     = $params['pagination'];
    $sort           = $params['sort'];
    $column         = $params['column'];
    $flex_column    = $column;
    $mage_div_count = 0;
    $event_expire_on = mep_get_option('mep_event_expire_on_datetimes', 'general_setting_sec', 'event_start_datetime');
    ob_start();
    ?>
    <div class='mep_event_list'>


        <?php if ($cat_f == 'yes') { ?>
            <div class="mep-events-cats-list">
                <?php
                $terms = get_terms(array(
                    'taxonomy' => 'mep_cat'
                ));
                ?>
                <div class="mep-event-cat-controls">
                    <button type="button" class="mep-cat-control" data-filter="all"><?php _e('All', 'mage-eventpress'); ?></button><?php
                    foreach ($terms as $_terms) {
                        ?>
                        <button type="button" class="mep-cat-control" data-filter=".<?php echo 'mage-' . $_terms->slug; ?>"><?php echo $_terms->name; ?></button><?php
                    }
                    ?>
                </div>
            </div>

        <?php }
        if ($org_f == 'yes') { ?>
            <div class="mep-events-cats-list">
                <?php
                $terms = get_terms(
                    array(
                        'taxonomy' => 'mep_org'
                    )
                );
                ?>
                <div class="mep-event-cat-controls">
                    <button type="button" class="mep-cat-control" data-filter="all"><?php _e('All', 'mage-eventpress'); ?></button><?php
                    foreach ($terms as $_terms) {
                        ?>
                        <button type="button" class="mep-cat-control" data-filter=".<?php echo 'mage-' . $_terms->slug; ?>"><?php echo $_terms->name; ?></button><?php
                    }
                    ?>
                </div>
            </div>
        <?php } ?>

        <div class="mep_event_list_sec">
            <?php
            $now = current_time('Y-m-d H:i:s');
            $show_price = mep_get_option('mep_event_price_show', 'general_setting_sec', 'yes');
            $show_price_label = mep_get_option('event-price-label', 'general_setting_sec', 'Price Starts from:');
            $paged = get_query_var("paged") ? get_query_var("paged") : 1;


            if ($cat > 0) {
                $args_search_qqq = array(
                    'post_type' => array('mep_events'),
                    'paged' => $paged,
                    'posts_per_page' => $show,
                    'order' => $sort,
                    'orderby' => 'meta_value',
                    'meta_key' => 'event_start_datetime',
                    'meta_query' => array(
                        array(
                            'key' => $event_expire_on,
                            'value' => $now,
                            'compare' => '>'
                        )
                    ),
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'mep_cat',
                            'field' => 'term_id',
                            'terms' => $cat
                        )
                    )

                );
            } elseif ($org > 0) {
                $args_search_qqq = array(
                    'post_type' => array('mep_events'),
                    'posts_per_page' => $show,
                    'paged' => $paged,
                    'order' => $sort,
                    'orderby' => 'meta_value',
                    'meta_key' => 'event_start_datetime',
                    'meta_query' => array(
                        array(
                            'key' => $event_expire_on,
                            'value' => $now,
                            'compare' => '>'
                        )
                    ),
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'mep_org',
                            'field' => 'term_id',
                            'terms' => $org
                        )
                    )

                );
            } else {
                $args_search_qqq = array(
                    'post_type' => array('mep_events'),
                    'paged' => $paged,
                    'posts_per_page' => $show,
                    'order' => $sort,
                    'orderby' => 'meta_value',
                    'meta_key' => 'event_start_datetime',
                    'meta_query' => array(
                        array(
                            'key' => $event_expire_on,
                            'value' => $now,
                            'compare' => '>'
                        )
                    )

                );
            }

            $loop = new WP_Query($args_search_qqq);
            $total_post=$loop->post_count;
            while ($loop->have_posts()) {
                $loop->the_post();
                $event_meta = get_post_custom(get_the_id());
                $author_terms = get_the_terms(get_the_id(), 'mep_org');
                $time = strtotime($event_meta['event_start_date'][0] . ' ' . $event_meta['event_start_time'][0]);
                $newformat = date_i18n('Y-m-d H:i:s', $time);

                $tt = get_the_terms(get_the_id(), 'mep_cat');
                $torg = get_the_terms(get_the_id(), 'mep_org');


                $org_class = mep_get_term_as_class(get_the_id(), 'mep_org');
                $cat_class = mep_get_term_as_class(get_the_id(), 'mep_cat');


                $available_seat = mep_get_total_available_seat(get_the_id(), $event_meta);
                if ($style == 'grid') {
                    if ($column == 2) {
                        $columnNumber = 'two_column';
                    } elseif ($column == 3) {
                        $columnNumber = 'three_column';
                    } elseif ($column == 4) {
                        $columnNumber = 'four_column';
                    } else {
                        $columnNumber = 'two_column';
                    }
                } else {
                    $columnNumber = 'one_column';
                }

                ?>
                <?php if ($flex_column > 0 && $style == 'grid' && $mage_div_count % $flex_column == 0) {
                    echo '<div class="mage_grid_box">';
                } ?>
                <div class='<?php echo $columnNumber; ?> mep_event_<?php echo $style; ?>_item mix <?php if ($tt) {
                    echo 'mage-' . $org_class;
                } ?> <?php if ($torg) {
                    echo 'mage-' . $cat_class;
                } ?>'>
                    <div class="mep_list_thumb">
                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>
                        <div class="mep-ev-start-date">
                            <div class="mep-day"><?php echo date_i18n('d', strtotime($event_meta['event_start_date'][0])); ?></div>
                            <div class="mep-month"><?php echo date_i18n('M', strtotime($event_meta['event_start_date'][0])); ?></div>
                        </div>
                    </div>
                    <div class="mep_list_event_details">
                        <a href="<?php the_permalink(); ?>">
                            <div class="mep-list-header">
                                <h2 class='mep_list_title'><?php the_title(); ?></h2>
                                <?php if ($available_seat == 0) {
                                    do_action('mep_show_waitlist_label');
                                } ?>
                                <h3 class='mep_list_date'> <?php if ($show_price == 'yes') {
                                        echo $show_price_label . " " . mep_event_list_price(get_the_id());
                                    } ?><!-- <i class="fa fa-calendar"></i> <?php echo date_i18n('h:i A', strtotime($event_meta['event_start_date'][0] . ' ' . $event_meta['event_start_time'][0])); ?> - <?php echo $event_meta['event_end_time'][0]; ?> --></h3>
                            </div>

                            <?php
                            if ($style == 'list') {
                                ?>
                                <div class="mep-event-excerpt">
                                    <?php the_excerpt(); ?>
                                </div>
                            <?php }


                            // List Settings Values::::::::::::::::::::::::
                            $hide_org_list = mep_get_option('mep_event_hide_organizer_list', 'general_setting_sec', 'no');
                            $hide_location_list = mep_get_option('mep_event_hide_location_list', 'general_setting_sec', 'no');
                            $hide_time_list = mep_get_option('mep_event_hide_time_list', 'general_setting_sec', 'no');
                            $hide_only_end_time_list = mep_get_option('mep_event_hide_end_time_list', 'general_setting_sec', 'no');


                            ?>

                            <div class="mep-list-footer">
                                <ul>
                                    <?php if ($hide_org_list == 'no') { ?>
                                        <li>
                                            <div class="evl-ico"><i class="fa fa-university"></i></div>
                                            <div class="evl-cc">
                                                <h5>
                                                    <?php echo mep_get_option('mep_organized_by_text', 'label_setting_sec') ? mep_get_option('mep_organized_by_text', 'label_setting_sec') : _e('Organized By:', 'mage-eventpress'); ?>
                                                </h5>
                                                <h6><?php if ($author_terms) {
                                                        echo $author_terms[0]->name;
                                                    } ?></h6>
                                            </div>
                                        </li>
                                    <?php }
                                    if ($hide_location_list == 'no') { ?>

                                        <li>
                                            <div class="evl-ico"><i class="fa fa-location-arrow"></i></div>
                                            <div class="evl-cc">
                                                <h5>
                                                    <?php echo mep_get_option('mep_location_text', 'label_setting_sec') ? mep_get_option('mep_location_text', 'label_setting_sec') : _e('Location:', 'mage-eventpress'); ?>

                                                </h5>
                                                <h6><?php mep_get_event_city(get_the_id()); ?></h6>
                                            </div>
                                        </li>
                                    <?php }
                                    if ($hide_time_list == 'no') { ?>
                                        <li>
                                            <div class="evl-ico"><i class="fa fa-calendar"></i></div>
                                            <div class="evl-cc">
                                                <h5>
                                                    <?php echo mep_get_option('mep_time_text', 'label_setting_sec') ? mep_get_option('mep_time_text', 'label_setting_sec') : _e('Time:', 'mage-eventpress'); ?>
                                                </h5>
                                                <h6><?php mep_get_only_time($event_meta['event_start_time'][0]);
                                                    if ($hide_only_end_time_list == 'no') { ?> - <?php mep_get_only_time($event_meta['event_end_time'][0]);
                                                    } ?></h6>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </a>
                    </div>
                </div>
                <!--                <p>--><?php //echo count($args_search_qqq);?><!--</p>-->
               <?php
                $mage_div_count++;
                if ($flex_column > 0 && $style == 'grid' && ($mage_div_count % $flex_column == 0 || $total_post == $mage_div_count)) {
                    $blank_div = $flex_column - ($mage_div_count % $flex_column);
                    if ($total_post == $mage_div_count && $blank_div > 0 && $flex_column > 1 && $blank_div < $flex_column) {
                        while ($blank_div > 0) {
                            echo '<div class="blank_div"></div>';
                            $blank_div--;
                        }
                    }
                    echo '</div>';
                }
            }
            wp_reset_postdata();
            //}
            if ($pagination == 'yes') {
                ?>

                <div class="row">
                    <div class="col-md-12"><?php
                        $pargs = array(
                            "current" => $paged,
                            "total" => $loop->max_num_pages
                        );
                        echo "<div class='pagination-sec'>" . paginate_links($pargs) . "</div>";
                        ?>
                    </div>
                </div>
            <?php } ?>


        </div>
    </div>
    <script>
        jQuery(document).ready(function () {
            var containerEl = document.querySelector('.mep_event_list_sec');
            var mixer = mixitup(containerEl);
        });
    </script>
    <?php
    $content = ob_get_clean();
    return $content;
}


add_shortcode('expire-event-list', 'mep_expire_event_list');
function mep_expire_event_list($atts, $content = null)
{
    $defaults = array(
        "cat" => "0",
        "org" => "0",
        "style" => "grid",
        "cat-filter" => "no",
        "org-filter" => "no",
        "show" => "-1",
        "pagination" => "no",
        'sort' => 'DESC'
    );

    $params = shortcode_atts($defaults, $atts);
    $cat = $params['cat'];
    $org = $params['org'];
    $style = $params['style'];
    $cat_f = $params['cat-filter'];
    $org_f = $params['org-filter'];
    $show = $params['show'];
    $pagination = $params['pagination'];
    $sort = $params['sort'];
   $event_expire_on 			= mep_get_option( 'mep_event_expire_on_datetimes', 'general_setting_sec', 'event_start_datetime');
    ob_start();
    ?>
    <div class='mep_event_list'>
        <?php if ($cat_f == 'yes') { ?>
            <div class="mep-events-cats-list">
                <?php
                $terms = get_terms(array(
                    'taxonomy' => 'mep_cat'
                ));
                ?>
                <div class="mep-event-cat-controls">
                    <button type="button" class="mep-cat-control" data-filter="all">All</button><?php
                    foreach ($terms as $_terms) {
                        ?>
                        <button type="button" class="mep-cat-control" data-filter=".<?php echo $_terms->slug; ?>"><?php echo $_terms->name; ?></button><?php
                    }
                    ?>
                </div>
            </div>

        <?php }
        if ($org_f == 'yes') { ?>
            <div class="mep-events-cats-list">
                <?php
                $terms = get_terms(array(
                    'taxonomy' => 'mep_org'
                ));
                ?>
                <div class="mep-event-cat-controls">
                    <button type="button" class="mep-cat-control" data-filter="all">All</button><?php
                    foreach ($terms as $_terms) {
                        ?>
                        <button type="button" class="mep-cat-control" data-filter=".<?php echo $_terms->slug; ?>"><?php echo $_terms->name; ?></button><?php
                    }
                    ?>
                </div>
            </div>
        <?php } ?>

        <div class="mep_event_list_sec">
            <?php
            $now = current_time('Y-m-d H:i:s');
            $paged = get_query_var("paged") ? get_query_var("paged") : 1;
            if ($cat > 0) {
                $args_search_qqq        = array(
                    'post_type'         => array('mep_events'),
                    'paged'             => $paged,
                    'posts_per_page'    => $show,
                    'order'             => $sort,
                    'orderby'           => 'meta_value',
                    'meta_key'          => 'event_start_datetime',
                    'meta_query'        => array(
                        array(
                            'key' => $event_expire_on,
                            'value' => $now,
                            'compare' => '<'
                        )
                    ),
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'mep_cat',
                            'field' => 'term_id',
                            'terms' => $cat
                        )
                    )

                );
            } elseif ($org > 0) {
                $args_search_qqq = array(
                    'post_type' => array('mep_events'),
                    'posts_per_page' => $show,
                    'paged' => $paged,
                    'order' => $sort,
                    'orderby' => 'meta_value',
                    'meta_key' => 'event_start_datetime',
                    'meta_query' => array(
                        array(
                            'key' => $event_expire_on,
                            'value' => $now,
                            'compare' => '<'
                        )
                    ),
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'mep_org',
                            'field' => 'term_id',
                            'terms' => $org
                        )
                    )

                );
            } else {
                $args_search_qqq = array(
                    'post_type' => array('mep_events'),
                    'paged' => $paged,
                    'posts_per_page' => $show,
                    'order' => $sort,
                    'orderby' => 'meta_value',
                    'meta_key' => 'event_start_datetime',
                    'meta_query' => array(
                        array(
                            'key' => $event_expire_on,
                            'value' => $now,
                            'compare' => '<'
                        )
                    )

                );
            }

            $loop = new WP_Query($args_search_qqq);
            while ($loop->have_posts()) {
                $loop->the_post();
                $event_meta = get_post_custom(get_the_id());
                $author_terms = get_the_terms(get_the_id(), 'mep_org');
                $time = strtotime($event_meta['event_start_date'][0] . ' ' . $event_meta['event_start_time'][0]);
                $newformat = date_i18n('Y-m-d H:i:s', $time);


                //if(time() > strtotime($newformat)){
                $tt = get_the_terms(get_the_id(), 'mep_cat');
                $torg = get_the_terms(get_the_id(), 'mep_org');
                // print_r($tt);


                ?>
                <div class='mep_event_list_item mix <?php echo $tt[0]->slug; ?> <?php echo $torg[0]->slug; ?>'>
                    <div class="mep_list_thumb">
                        <?php the_post_thumbnail('full'); ?>
                        <div class="mep-ev-start-date">
                            <div class="mep-day"><?php echo date_i18n('d', strtotime($event_meta['event_start_date'][0])); ?></div>
                            <div class="mep-month"><?php echo date_i18n('M', strtotime($event_meta['event_start_date'][0])); ?></div>
                        </div>
                    </div>
                    <div class="mep_list_event_details"><a href="<?php the_permalink(); ?>">
                            <div class="mep-list-header">
                                <h2 class='mep_list_title'><?php the_title(); ?></h2>
                                <h3 class='mep_list_date'> Price Start from: <?php echo mep_event_list_price(get_the_id()); ?><!-- <i class="fa fa-calendar"></i> <?php echo date_i18n('h:i A', strtotime($event_meta['event_start_date'][0] . ' ' . $event_meta['event_start_time'][0])); ?> - <?php echo $event_meta['event_end_date'][0] . ' ' . $event_meta['event_end_time'][0]; ?> --></h3>
                            </div>

                            <?php
                            if ($style == 'list') {
                                ?>
                                <div class="mep-event-excerpt">
                                    <?php the_excerpt(); ?>
                                </div>
                            <?php } ?>

                            <div class="mep-list-footer">
                                <ul>
                                    <li>
                                        <div class="evl-ico"><i class="fa fa-university"></i></div>
                                        <div class="evl-cc">
                                            <h5>
                                                <?php echo mep_get_option('mep_organized_by_text', 'label_setting_sec') ? mep_get_option('mep_organized_by_text', 'label_setting_sec') : _e('Organized By:', 'mage-eventpress'); ?>
                                            </h5>
                                            <h6><?php echo $author_terms[0]->name; ?></h6>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="evl-ico"><i class="fa fa-location-arrow"></i></div>
                                        <div class="evl-cc">
                                            <h5>
                                                <?php echo mep_get_option('mep_location_text', 'label_setting_sec') ? mep_get_option('mep_location_text', 'label_setting_sec') : _e('Location:', 'mage-eventpress'); ?>

                                            </h5>
                                            <h6><?php echo $event_meta['mep_city'][0]; ?></h6>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="evl-ico"><i class="fa fa-calendar"></i></div>
                                        <div class="evl-cc">
                                            <h5>
                                                <?php echo mep_get_option('mep_time_text', 'label_setting_sec') ? mep_get_option('mep_time_text', 'label_setting_sec') : _e('Time:', 'mage-eventpress'); ?>

                                            </h5>
                                            <h6><?php mep_get_only_time($event_meta['event_start_time'][0]); ?> - <?php mep_get_only_time($event_meta['event_end_time'][0]); ?></h6>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
//}
            }
            wp_reset_postdata();
            if ($pagination == 'yes') {
                ?>
                <div class="row">
                    <div class="col-md-12"><?php
                        $pargs = array(
                            "current" => $paged,
                            "total" => $loop->max_num_pages
                        );
                        echo "<div class='pagination-sec'>" . paginate_links($pargs) . "</div>";
                        ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <script>
        jQuery(document).ready(function () {
            var containerEl = document.querySelector('.mep_event_list_sec');
            var mixer = mixitup(containerEl);
        });
    </script>
    <?php
    $content = ob_get_clean();
    return $content;
}


add_shortcode('event-list-onepage', 'mep_event_onepage_list');
function mep_event_onepage_list($atts, $content = null)
{
    $defaults = array(
        "cat" => "0",
        "org" => "0",
        "style" => "grid",
        "cat-filter" => "no",
        "org-filter" => "no",
        "show" => "-1",
        "pagination" => "no",
        'sort' => 'ASC'
    );

    $params = shortcode_atts($defaults, $atts);
    $cat = $params['cat'];
    $org = $params['org'];
    $style = $params['style'];
    $cat_f = $params['cat-filter'];
    $org_f = $params['org-filter'];
    $show = $params['show'];
    $pagination = $params['pagination'];
    $sort = $params['sort'];
   $event_expire_on 			= mep_get_option( 'mep_event_expire_on_datetimes', 'general_setting_sec', 'event_start_datetime');
    ob_start();
    do_action('woocommerce_before_single_product');
    ?>
    <div class='mep_event_list'>
        <?php if ($cat_f == 'yes') { ?>
            <div class="mep-events-cats-list">
                <?php
                $terms = get_terms(array(
                    'taxonomy' => 'mep_cat'
                ));
                ?>
                <div class="mep-event-cat-controls">
                    <button type="button" class="mep-cat-control" data-filter="all"><?php _e('All', 'mage-eventpress'); ?></button><?php
                    foreach ($terms as $_terms) {
                        ?>
                        <button type="button" class="mep-cat-control" data-filter=".<?php echo 'mage-' . $_terms->slug; ?>"><?php echo $_terms->name; ?></button><?php
                    }
                    ?>
                </div>
            </div>

        <?php }
        if ($org_f == 'yes') { ?>
            <div class="mep-events-cats-list">
                <?php
                $terms = get_terms(array(
                    'taxonomy' => 'mep_org'
                ));
                ?>
                <div class="mep-event-cat-controls">
                    <button type="button" class="mep-cat-control" data-filter="all"><?php _e('All', 'mage-eventpress'); ?></button><?php
                    foreach ($terms as $_terms) {
                        ?>
                        <button type="button" class="mep-cat-control" data-filter=".<?php echo 'mage-' . $_terms->slug; ?>"><?php echo $_terms->name; ?></button><?php
                    }
                    ?>
                </div>
            </div>
        <?php } ?>

        <div class="mep_event_list_sec">
            <?php
            $now = current_time('Y-m-d H:i:s');
            $show_price = mep_get_option('mep_event_price_show', 'general_setting_sec', 'yes');
            $show_price_label = mep_get_option('event-price-label', 'general_setting_sec', 'Price Starts from:');
            $paged = get_query_var("paged") ? get_query_var("paged") : 1;
            if ($cat > 0) {
                $args_search_qqq = array(
                    'post_type' => array('mep_events'),
                    'paged' => $paged,
                    'posts_per_page' => $show,
                    'order' => $sort,
                    'orderby' => 'meta_value',
                    'meta_key' => 'event_start_datetime',
                    'meta_query' => array(
                        array(
                            'key' => $event_expire_on,
                            'value' => $now,
                            'compare' => '>'
                        )
                    ),
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'mep_cat',
                            'field' => 'term_id',
                            'terms' => $cat
                        )
                    )

                );
            } elseif ($org > 0) {
                $args_search_qqq = array(
                    'post_type' => array('mep_events'),
                    'posts_per_page' => $show,
                    'paged' => $paged,
                    'order' => $sort,
                    'orderby' => 'meta_value',
                    'meta_key' => 'event_start_datetime',
                    'meta_query' => array(
                        array(
                            'key' => $event_expire_on,
                            'value' => $now,
                            'compare' => '>'
                        )
                    ),
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'mep_org',
                            'field' => 'term_id',
                            'terms' => $org
                        )
                    )

                );
            } else {
                $args_search_qqq = array(
                    'post_type' => array('mep_events'),
                    'paged' => $paged,
                    'posts_per_page' => $show,
                    'order' => $sort,
                    'orderby' => 'meta_value',
                    'meta_key' => 'event_start_datetime',
                    'meta_query' => array(
                        array(
                            'key' => $event_expire_on,
                            'value' => $now,
                            'compare' => '>'
                        )
                    )

                );
            }

            $loop = new WP_Query($args_search_qqq);
            while ($loop->have_posts()) {
                $loop->the_post();
                $event_meta = get_post_custom(get_the_id());
                $author_terms = get_the_terms(get_the_id(), 'mep_org');
                $start_datetime = $event_meta['event_start_date'][0] . ' ' . $event_meta['event_start_time'][0];
                $time = strtotime($start_datetime);
                $newformat = date_i18n('Y-m-d H:i:s', $time);

                $tt = get_the_terms(get_the_id(), 'mep_cat');
                $torg = get_the_terms(get_the_id(), 'mep_org');


                $org_class = mep_get_term_as_class(get_the_id(), 'mep_org');
                $cat_class = mep_get_term_as_class(get_the_id(), 'mep_cat');


                $available_seat = mep_get_total_available_seat(get_the_id(), $event_meta);
                ?>
                <div class='mep_event_<?php echo $style; ?>_item mix <?php if ($tt) {
                    echo 'mage-' . $org_class;
                } ?> <?php if ($torg) {
                    echo 'mage-' . $cat_class;
                } ?>'>
                    <div class="mep_list_thumb">
                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>
                        <div class="mep-ev-start-date">
                            <div class="mep-day"><?php echo date_i18n('d', strtotime($event_meta['event_start_date'][0])); ?></div>
                            <div class="mep-month"><?php echo date_i18n('M', strtotime($event_meta['event_start_date'][0])); ?></div>
                        </div>
                    </div>
                    <div class="mep_list_event_details"><a href="<?php the_permalink(); ?>">
                            <div class="mep-list-header">
                                <h2 class='mep_list_title'><?php the_title(); ?></h2>
                                <?php if ($available_seat == 0) {
                                    do_action('mep_show_waitlist_label');
                                } ?>
                                <h3 class='mep_list_date'> <?php if ($show_price == 'yes') {
                                        echo $show_price_label . " " . mep_event_list_price(get_the_id());
                                    } ?><!-- <i class="fa fa-calendar"></i> <?php echo date_i18n('h:i A', strtotime($event_meta['event_start_time'][0])); ?> - <?php echo $event_meta['event_end_date'][0]; ?> --></h3>
                            </div>

                            <div class="mep-list-footer">
                                <ul>
                                    <li>
                                        <div class="evl-ico"><i class="fa fa-university"></i></div>
                                        <div class="evl-cc">
                                            <h5>
                                                <?php echo mep_get_option('mep_organized_by_text', 'label_setting_sec') ? mep_get_option('mep_organized_by_text', 'label_setting_sec') : _e('Organized By:', 'mage-eventpress'); ?>
                                            </h5>
                                            <h6><?php if ($author_terms) {
                                                    echo $author_terms[0]->name;
                                                } ?></h6>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="evl-ico"><i class="fa fa-location-arrow"></i></div>
                                        <div class="evl-cc">
                                            <h5>
                                                <?php echo mep_get_option('mep_location_text', 'label_setting_sec') ? mep_get_option('mep_location_text', 'label_setting_sec') : _e('Location:', 'mage-eventpress'); ?>

                                            </h5>
                                            <h6><?php mep_get_event_city(get_the_id()); ?></h6>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="evl-ico"><i class="fa fa-calendar"></i></div>
                                        <div class="evl-cc">
                                            <h5>
                                                <?php echo mep_get_option('mep_time_text', 'label_setting_sec') ? mep_get_option('mep_time_text', 'label_setting_sec') : _e('Time:', 'mage-eventpress'); ?>
                                            </h5>
                                            <h6><?php mep_get_only_time($event_meta['event_start_time'][0]); ?> - <?php mep_get_only_time($event_meta['event_end_time'][0]); ?></h6>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </a>

                        <?php
                        $currency_pos = get_option('woocommerce_currency_pos');

                        $mep_full_name = strip_tags($event_meta['mep_full_name'][0]);
                        $mep_reg_email = strip_tags($event_meta['mep_reg_email'][0]);
                        $mep_reg_phone = strip_tags($event_meta['mep_reg_phone'][0]);
                        $mep_reg_address = strip_tags($event_meta['mep_reg_address'][0]);
                        $mep_reg_designation = strip_tags($event_meta['mep_reg_designation'][0]);
                        $mep_reg_website = strip_tags($event_meta['mep_reg_website'][0]);
                        $mep_reg_veg = strip_tags($event_meta['mep_reg_veg'][0]);
                        $mep_reg_company = strip_tags($event_meta['mep_reg_company'][0]);
                        $mep_reg_gender = strip_tags($event_meta['mep_reg_gender'][0]);
                        $mep_reg_tshirtsize = strip_tags($event_meta['mep_reg_tshirtsize'][0]);
                        echo '<div class=event-cart-section-list>';
                        do_action('mep_add_to_cart_list');
                        echo '</div>';
                        ?>
                    </div>
                </div>

                <?php
                get_event_list_js(get_the_id(), $event_meta, $currency_pos);
            }
            wp_reset_postdata();
            //}
            if ($pagination == 'yes') {
                ?>

                <div class="row">
                    <div class="col-md-12"><?php
                        $pargs = array(
                            "current" => $paged,
                            "total" => $loop->max_num_pages
                        );
                        echo "<div class='pagination-sec'>" . paginate_links($pargs) . "</div>";
                        ?>
                    </div>
                </div>
            <?php }
            ?>
        </div>
    </div>

    <?php
    $content = ob_get_clean();
    return $content;
}