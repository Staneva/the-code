<?php 
add_action('mep_event_seat','mep_ev_seat');
function mep_ev_seat(){
global $post,$event_meta;
$recurring = get_post_meta(get_the_id(), 'mep_enable_recurring', true) ? get_post_meta(get_the_id(), 'mep_enable_recurring', true) : 'no';
if($recurring == 'no'){
$mep_event_ticket_type = get_post_meta($post->ID, 'mep_event_ticket_type', true);
	if(array_key_exists('mep_available_seat', $event_meta)){ 
			$mep_available_seat = $event_meta['mep_available_seat'][0];
		}else{
			$mep_available_seat = 'on';
	}
	if($mep_event_ticket_type){
		$total_seat = mep_event_total_seat(get_the_id(),'total');
		$total_resv = mep_event_total_seat(get_the_id(),'resv');
		$total_sold = mep_ticket_sold(get_the_id());
		$total_left = $total_seat - ($total_sold + $total_resv);
		?>
			<h5><strong><?php echo mep_get_option('mep_total_seat_text', 'label_setting_sec') ? mep_get_option('mep_total_seat_text', 'label_setting_sec') : _e('Total Seat:','mage-eventpress');  ?></strong> <?php echo $total_seat; if($mep_available_seat=='on'){ ?> (<strong><?php echo max($total_left,0); ?></strong> <?php _e('Left','mage-eventpress'); ?>)<?php } ?></h5>
		<?php
	}
}
}