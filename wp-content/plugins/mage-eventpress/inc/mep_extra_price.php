<?php
function mep_add_custom_fields_text_to_cart_item( $cart_item_data, $product_id, $variation_id ){

  $product_id = get_post_meta($product_id,'link_mep_event',true) ? get_post_meta($product_id,'link_mep_event',true) : $product_id;

  if (get_post_type($product_id) == 'mep_events') { 
  $tp = get_post_meta($product_id,'_price',true);

  $new = array();

  if(isset($_POST['mep_event_location_cart'])){
    $event_cart_location   =  $_POST['mep_event_location_cart'];
  }else{ $event_cart_location   = ""; } 

  if(isset($_POST['mep_event_date_cart'])){
    $event_cart_date   =  $_POST['mep_event_date_cart'];
  }else{ $event_cart_date   = ""; } 

  if(isset($_POST['mep_event_start_date'])){
    $mep_event_start_date   =  $_POST['mep_event_start_date'];
  }else{ $mep_event_start_date   = ""; } 

  if(isset($_POST['event_addt_price'])){
    $checked                = $_POST['event_addt_price'];
  }else{ $checked=""; } 

  if(isset($_POST['option_name'])){
    $names                  = $_POST['option_name'];
  }else{ $names=array(); } 

  if(isset($_POST['option_qty'])){  
    $qty                    = $_POST['option_qty'];
  }else{ $qty=""; }
  
  if(isset($_POST['max_qty'])){
		  $max_qty                    = $_POST['max_qty'];
	  }else{ $max_qty=""; }

  if(isset($_POST['option_price'])){  
    $price                  = $_POST['option_price'];
  }else{ $price=""; } 

  $count = count( $names );

 if(isset($_POST['option_name'])){
  for ( $i = 0; $i < $count; $i++ ) {
    if ( $names[$i] != '' ) :
      $new[$i]['option_name'] = stripslashes( strip_tags( $names[$i] ) );
      endif;
    if ( $price[$i] != '' ) :
      $new[$i]['option_price'] = stripslashes( strip_tags( $price[$i] ) );
      endif;
    if ( $qty[$i] != '' ) :
      $new[$i]['option_qty'] = stripslashes( strip_tags( $qty[$i] ) );
      endif;
      if ( $max_qty[$i] != '' ) :
		  $new[$i]['max_qty'] = stripslashes( strip_tags( $max_qty[$i] ) );
	  endif;
      
      
    $opttprice =   ($price[$i]*$qty[$i]);
    $tp = ($tp+$opttprice);
  }
}


$extra_service_name = isset($_POST['event_extra_service_name']) ? mage_array_strip($_POST['event_extra_service_name']) : array();
$extra_service_qty = isset($_POST['event_extra_service_qty'])? mage_array_strip($_POST['event_extra_service_qty']):array();
$extra_service_price = isset($_POST['event_extra_service_price'])? mage_array_strip($_POST['event_extra_service_price']):array();



 if($extra_service_name){
  for ( $i = 0; $i < count($extra_service_name); $i++ ) {
      if($extra_service_qty[$i] > 0){
    if ( $extra_service_name[$i] != '' ) :
      $event_extra[$i]['service_name'] = stripslashes( strip_tags( $extra_service_name[$i] ) );
      endif;
    if ( $extra_service_price[$i] != '' ) :
      $event_extra[$i]['service_price'] = stripslashes( strip_tags( $extra_service_price[$i] ) );
      endif;
    if ( $extra_service_qty[$i] != '' ) :
      $event_extra[$i]['service_qty'] = stripslashes( strip_tags( $extra_service_qty[$i] ) );
      endif;
      }
      
          $extprice =   ($extra_service_price[$i]*$extra_service_qty[$i]);
    $tp = ($tp+$extprice);
  }
}




if(isset($_POST['mep_event_ticket_type'])){
  $ttp                                  = $_POST['mep_event_ticket_type'];
  $ttpqt                                = $_POST['tcp_qty'];
  $ticket_type                          = mep_get_order_info($ttp,1);
  $ticket_type_price                    = (mep_get_order_info($ttp,0)*$ttpqt);
  $cart_item_data['event_ticket_type']  = $ticket_type;
  $cart_item_data['event_ticket_price'] = $ticket_type_price;
  $cart_item_data['event_ticket_qty']   = $ttpqt;
  $tp                                   = $tp+$ticket_type_price;
}







    $form_position = mep_get_option( 'mep_user_form_position', 'general_attendee_sec', 'details_page' );
    if($form_position=='details_page'){
      $user = mep_save_attendee_info_into_cart($product_id);
    }else{
      $user = '';
    }


  $mep_event_ticket_type = get_post_meta($product_id, 'mep_event_ticket_type', true) ? get_post_meta($product_id, 'mep_event_ticket_type', true) : array();
    $cnt = 0;
    $vald = 0;
    $ticket_type_arr = array();
    if(is_array($mep_event_ticket_type) && sizeof($mep_event_ticket_type) > 0){
    foreach($mep_event_ticket_type as $_type){
        // echo $_type['option_name_t'];
        $cart_arr = $new[$cnt];
        // print_r($cart_arr);
       $name_key = array_search($_type['option_name_t'],$cart_arr);
       $qty_key = array_search($_type['option_qty_t'],$cart_arr);
         if(is_array($name_key)){
            $total_found = count($name_key);
         }else{
            $total_found = 0; 
         }
         if($cart_arr['option_qty'] > 0){
             
              $ticket_type_arr[$cnt]['ticket_name'] = stripslashes( strip_tags( $cart_arr[$name_key] ) );              
              $ticket_type_arr[$cnt]['ticket_qty'] = stripslashes( strip_tags( $cart_arr['option_qty'] ) );              
              $ticket_type_arr[$cnt]['ticket_price'] = stripslashes( strip_tags( $cart_arr['option_price'] ) );
              $ticket_type_arr[$cnt]['event_date'] = stripslashes( strip_tags( $mep_event_start_date ) );

              $validate[$cnt]['ticket_qty'] = $vald + stripslashes( strip_tags( $cart_arr['option_qty'] ) );              
              $validate[$cnt]['event_id'] = stripslashes( strip_tags( $product_id ) );              
        }
        
        $cnt++;
    }
    }
    

  $cart_item_data['event_ticket_info']    = $ticket_type_arr;
  $cart_item_data['event_validate_info']    = $validate;
  $cart_item_data['event_extra_option']   = $new;
  $cart_item_data['event_user_info']      = $user;
  $cart_item_data['event_tp']             = $tp;
  $cart_item_data['line_total']           = $tp;
  $cart_item_data['line_subtotal']        = $tp;
  $cart_item_data['event_extra_service']  = $event_extra;
  $cart_item_data['event_cart_location']  = $event_cart_location;
  $cart_item_data['event_cart_date']      = $mep_event_start_date;
  // $cart_item_data['event_cart_date']      = $event_cart_date;
}
  $cart_item_data['event_id']             = $product_id;

  return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'mep_add_custom_fields_text_to_cart_item', 90, 3);



add_action( 'woocommerce_before_calculate_totals', 'add_custom_price',90,1 );
function add_custom_price( $cart_object ) {

foreach ( $cart_object->cart_contents as $key => $value ) {
$eid = $value['event_id'];
if (get_post_type($eid) == 'mep_events') {      
            $cp = $value['event_tp'];
            $value['data']->set_price($cp);
            $value['data']->set_regular_price($cp);
            $value['data']->set_sale_price($cp);
            $value['data']->set_sold_individually('yes');
            $new_price = $value['data']->get_price();
    }
  }
}





function mep_display_custom_fields_text_cart( $item_data, $cart_item ) {
$mep_events_extra_prices = $cart_item['event_extra_option'];
// print_r($cart_item);
$eid                    = $cart_item['event_id'];

if (get_post_type($eid) == 'mep_events') { 
  $user_info                  = $cart_item['event_user_info'];
  $ticket_type_arr            = $cart_item['event_ticket_info'];
  $event_extra_service            = $cart_item['event_extra_service'];

 

echo "<ul class='event-custom-price'>";
if(is_array($user_info) && sizeof($user_info) > 0){
  foreach($user_info as $userinf){
  ?>
      <li><?php _e('Event Date','mage-eventpress'); ?>: <?php echo date('D, d M Y',strtotime($userinf['user_event_date'])); ?></li>
  <?php
  }
}else{
  ?>
    <li><?php _e('Event Date','mage-eventpress'); ?>: <?php echo date('D, d M Y',strtotime($cart_item['event_cart_date'])); ?></li>
  <?php
}
?>
<li><?php _e('Event Location','mage-eventpress'); ?>: <?php echo $cart_item['event_cart_location']; //echo $cart_item['event_ticket_type']; ?></li>
<?php

if(is_array($ticket_type_arr) && sizeof($ticket_type_arr) > 0){
    foreach($ticket_type_arr as $ticket){
        echo '<li>'.$ticket['ticket_name']." - ".wc_price($ticket['ticket_price']).' x '.$ticket['ticket_qty'].' = '.wc_price($ticket['ticket_price'] * $ticket['ticket_qty']).'</li>';
    }
}

if(is_array($event_extra_service) && sizeof($event_extra_service) > 0){
    foreach($event_extra_service as $extra_service){
        echo '<li>'.$extra_service['service_name']." - ".wc_price($extra_service['service_price']).' x '.$extra_service['service_qty'].' = '.wc_price($extra_service['service_price'] * $extra_service['service_qty']).'</li>';
    }
}




  echo "</ul>";
}
  return $item_data;
}
add_filter( 'woocommerce_get_item_data', 'mep_display_custom_fields_text_cart', 90, 2 );



add_action( 'woocommerce_after_checkout_validation', 'mep_checkout_validation');
function mep_checkout_validation( $posted ) {
  global $woocommerce;
  $items    = $woocommerce->cart->get_cart();
  foreach($items as $item => $values) { 
    $event_id              = $values['event_id'];
    $total_seat = mep_event_total_seat($event_id,'total');
		$total_resv = mep_event_total_seat($event_id,'resv');
		$total_sold = mep_ticket_sold($event_id);
    $total_left = $total_seat - ($total_sold + $total_resv);
    ;
    $event_validate_info        = $values['event_validate_info'];  
    $ee = 0;
    foreach($event_validate_info as $inf){
       $ee = $ee + $inf['ticket_qty'];
    }
    if($ee > $total_left) {
      $event = get_the_title($event_id);
      wc_add_notice( __( "Sorry, Seats are not available in <b>$event</b>, Available Seats <b>$total_left</b> but you selected <b>$ee</b>", 'mage-eventpress' ), 'error' );
    }
  }
}










function mep_add_custom_fields_text_to_order_items( $item, $cart_item_key, $values, $order ) {
$eid                    = $values['event_id'];
if (get_post_type($eid) == 'mep_events') { 
$mep_events_extra_prices = $values['event_extra_option'];
if(isset($values['event_ticket_type'])){
  $event_ticket_type       = $values['event_ticket_type'];
}else{
  $event_ticket_type = " "; 
}
if(isset($values['event_ticket_price'])){
  $event_ticket_price      = $values['event_ticket_price'];
}else{
  $event_ticket_price      = " ";
}
if(isset($values['event_ticket_qty'])){
  $event_ticket_qty        = $values['event_ticket_qty'];
}else{
  $event_ticket_qty        = " ";  
}



        $user_set_format    = mep_get_option( 'mep_event_time_format','general_setting_sec',12);
        $date_format        = get_option( 'date_format' );
        $time_format        = get_option( 'time_format' );
        $wpdatesettings     = $date_format.'  '.$time_format; 

      if($user_set_format == 12){
            $cart_date =  date_i18n( 'Y-m-d h:i A', strtotime( $values['event_cart_date'] ) );
      }elseif($user_set_format == 24){
            $cart_date = date_i18n( 'Y-m-d H:i', strtotime( $values['event_cart_date'] ) );
      }elseif($user_set_format == 'wtss'){
           $cart_date = date_i18n( $wpdatesettings, strtotime( $values['event_cart_date'] ) );
      }






  $product_id              = $values['product_id'];
  $cart_location           = $values['event_cart_location'];
  $event_extra_service     = $values['event_extra_service'];
  $ticket_type_arr     = $values['event_ticket_info'];
    
$form_position = mep_get_option( 'mep_user_form_position', 'general_attendee_sec', 'details_page' );

    if($form_position=='details_page'){
      $event_user_info         = $values['event_user_info'];
    }else{
      $event_user_info = mep_save_attendee_info_into_cart($eid);
    }




  $item->add_meta_data('Date',$cart_date);

  $item->add_meta_data('Location',$cart_location);
  $item->add_meta_data('_event_ticket_info',$values['event_ticket_info']);

// if (is_array($mep_events_extra_prices) || is_object($mep_events_extra_prices)){
//   foreach ( $mep_events_extra_prices as $field ) {
//       if($field['option_qty']>0){
//         $item->add_meta_data(esc_attr( $field['option_name'] )." x ".$field['option_qty'], wc_price($field['option_qty'] *$field['option_price'] ) );
//         // $opt_name =  $product_id.str_replace(' ', '', $field['option_name']);
//         // $opt_qty = $field['option_qty'];
//         // $tes = get_post_meta($product_id,"mep_xtra_$opt_name",true);
//         // $ntes = ($tes+$opt_qty);
//         // update_post_meta( $product_id, "mep_xtra_$opt_name",$ntes);
//     }
//   } 
// }


if(is_array($ticket_type_arr) && sizeof($ticket_type_arr) > 0){
    foreach($ticket_type_arr as $ticket){
        $ticket_type_name = $ticket['ticket_name']." - ".wc_price($ticket['ticket_price']).' x '.$ticket['ticket_qty'].' = ';
        $ticket_type_val= wc_price($ticket['ticket_price'] * $ticket['ticket_qty']);
        $item->add_meta_data($ticket_type_name, $ticket_type_val );
       
    }
}

if(is_array($event_extra_service) && sizeof($event_extra_service) > 0){
    foreach($event_extra_service as $extra_service){
        
        $service_type_name = $extra_service['service_name']." - ".wc_price($extra_service['service_price']).' x '.$extra_service['service_qty'].' = ';
        $service_type_val= wc_price($extra_service['service_price'] * $extra_service['service_qty']);
        $item->add_meta_data($service_type_name, $service_type_val );
        
    }
}


if($event_ticket_type){

}else{
    $item->add_meta_data('_event_ticket_type','normal');
}
    $item->add_meta_data('_event_user_info',$event_user_info);
    $item->add_meta_data('_no_of_ticket',count($event_user_info));
    $item->add_meta_data('_event_service_info',$mep_events_extra_prices);
    $item->add_meta_data('event_id',$eid);
    $item->add_meta_data('_product_id',$eid);
    $item->add_meta_data('_event_extra_service',$event_extra_service);
}

}
add_action( 'woocommerce_checkout_create_order_line_item', 'mep_add_custom_fields_text_to_order_items', 90, 4 );