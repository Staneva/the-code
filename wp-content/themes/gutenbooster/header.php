<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gutenbooster
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebPage">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!-- IE Edge Meta Tag -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Viewport -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body
	<?php
	body_class();
	gutenbooster_body_atts();
	?>
 itemscope="itemscope" itemtype="http://schema.org/WebPage" >
 <?php gutenbooster_wp_body_open(); ?>
<a class="skip-link faux-button" href="#content"><?php esc_html_e( 'Skip to content', 'gutenbooster' ); ?></a>

	<?php
	//get mobile navigation
	get_template_part( 'template-parts/mobile-nav' );
	?>


	<!-- main content wrapper -->
	<main id="site-wrap" role="main">

		<!-- site inner -->
		<div class="site-inner" >

		<?php
		// open up preloader container if it's on
		gutenbooster_kt_preloader( 'brb-contain-on' );

		?>
		<!-- main menu bar & top search -->
		<header id="masthead" class="menu-wrapper">

			<div class="inner-wrap <?php gutenbooster_header_class(); ?>">
				<div class="header-search d-flex w-100 align-items-center">
					<a class="close-form" href="javascript:;"><?php gutenbooster_icon_url( 'x', true ); ?></a>
					<?php get_search_form(); ?>
				</div>
					<?php do_action( 'gutenbooster_header' ); ?>
			</div>
		</header>


				<?php /* check page banner */ do_action( 'gutenbooster_after_header' ); ?>


				<div id="content" class="site-content">

					<?php

						// blog category nav, single post header for no page banner posts
						// and opening of content container
						do_action( 'gutenbooster_before_content' );

						// page title if page banner turned off
						// and page title is turned on for blog posts page
						gutenbooster_nobanner_pagetitle( 'blog' );

					?>
