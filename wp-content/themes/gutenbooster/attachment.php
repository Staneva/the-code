<?php
/**
 * The template for displaying attachments
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gutenbooster
 */

get_header();
?>


<div class="k-content-inner w-100 <?php gutenbooster_content_col_classes(); ?>">
	<?php
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', 'attachment' );
			endwhile;

		else :
				get_template_part( 'template-parts/content', 'none' );
		endif;
	?>


</div><!-- .k-content-inner -->

<?php
get_sidebar();
get_footer();
