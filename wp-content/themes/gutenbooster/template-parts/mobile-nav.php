<div id="mobile-navigation" class="expanding dark" data-barba-update="">
    <a class="close-jsnav" href="#"><?php gutenbooster_icon_url( 'x', true ); ?></a>
    <div class="inner-wrap">
        <?php get_search_form(  ); ?>

        <?php
        has_nav_menu('gbb-mobile') ? gutenbooster_header_menus("gbb-mobile") : gutenbooster_header_menus("gbb-primary"); ?>
    </div>
    <div class="utils ml-auto d-block">
       <ul>
         <?php echo gutenbooster_header_icons('right','v1'); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
       </ul>
    </div>
</div>
<div class="body-jsnav-overlay"></div>
