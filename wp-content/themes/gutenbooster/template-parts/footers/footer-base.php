<?php
/**
 * The template for displaying the footer base
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GutenBooster
 */

?>
<?php
    if (
        gutenbooster_getmod( 'footer_text_left' ) || get_theme_mod( 'footer_text_right' )
        || is_active_sidebar('footer-1')  || is_active_sidebar('footer-2')  || is_active_sidebar('footer-3') || is_active_sidebar('footer-4')
        ) : ?>
    <footer id="colophon" class="site-footer" role="contentinfo" itemtype="https://schema.org/WPFooter" itemscope>
        <div class="inner <?php echo esc_attr( gutenbooster_footer_classes() ); ?>">

          <?php do_action('gutenbooster_footer'); ?>

        <?php if ( gutenbooster_getmod( 'footer_text_left' ) || get_theme_mod( 'footer_text_right' ) ) : ?>
          <div class="row final-row">
            <div class="col-sm copyright-col <?php echo esc_attr( gutenbooster_footercol_maxpad() ); ?>">
                <div class="ktft--text-center text-center mr-0 mb-4">
                  <?php echo gutenbooster_render_footer_txt_left(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
                </div>
            </div>
          </div>
        <?php endif; ?>
          <?php if ( has_nav_menu( 'gbb-footer' ) ) : ?>
            <div class="row ft-menu-row">
              <div class="col-sm pb-4 text-center">
              <?php
                    wp_nav_menu( array(
                      'container'       => 'ul',
                      'theme_location'  => 'gbb-footer',
                      'menu_id'         => 'footer-menu',
                      'menu_class'      => 'menu d-inline-block',
                      'depth'           => 1,
                    ) );
                  ?>
              </div>
            </div>
          <?php endif; ?>

        </div>
        <div class="footer--bg-image" <?php echo gutenbooster_footer_bg_image(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>></div>
    </footer>
<?php endif; ?>

