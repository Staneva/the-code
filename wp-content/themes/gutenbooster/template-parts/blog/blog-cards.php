<?php
/**
 * Template part for displaying card posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gutenbooster
 */

 ?>
 <?php if ( has_post_thumbnail() ) : ?>

    <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark" class="post-thumbnail">
        <?php the_post_thumbnail( gutenbooster_thumb_size() ); ?>
            <div class="card-img-overlay">
                <div class="card-body">
                    <h3 class="entry-title"><?php echo esc_attr( get_the_title( ) ); ?></h3>
                    <div class="entry-meta d-md-none d-lg-block"><?php gutenbooster_post_meta("cards"); ?></div>
                </div>
            </div>
    </a>


<?php else: ?>

    <div class="card-body no-thumb">

      <?php the_title( '<h3 class="card-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
      <?php if ( ! is_page() ) : ?>
          <div class="entry-meta"><?php gutenbooster_post_meta(); ?></div>
      <?php endif; ?>
      <div class="entry-summary"><?php the_excerpt(); ?></div><!-- .entry-content -->

      <a class="underlined read-more" href="<?php the_permalink() ?>"><?php esc_html_e( 'Read more', 'gutenbooster' ) ?></a>

    </div>


<?php endif; ?>
