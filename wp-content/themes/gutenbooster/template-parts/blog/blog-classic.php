<?php
/**
 * Template part for displaying classic posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gutenbooster
 */

 ?>
<div class="media">

    <?php $gutenbooster_format = get_post_format(); ?>
     <?php if ( has_post_thumbnail() ) : ?>
        <a href="<?php the_permalink() ?>" class="post-thumbnail mb-4 mb-md-0">
           <?php the_post_thumbnail( gutenbooster_thumb_size(),
                                      array('class'=> '') ); ?>
        </a>
    <?php elseif ( $gutenbooster_format==="gallery" ) :
        gutenbooster_format_gallery_slider();
        // gutenbooster_fetch_gallery_images();
          elseif ( $gutenbooster_format==="image" ) :
            ?>
        <a href="<?php the_permalink() ?>" class="post-thumbnail mb-4 mb-md-0">
          <?php echo gutenbooster_grab_first_image(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
        </a>

     <?php endif; ?>

  <div class="post-summary media-body">
    <?php $gutenbooster_stickyness = is_sticky() ? '<span class="gb-sticky-indicator">'.esc_attr('Featured','gutenbooster').'</span>' : ''; ?>
     <?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '' . $gutenbooster_stickyness . '</a></h3>' ); ?>
     <?php if ( ! is_page() ) : ?>
         <div class="entry-meta"><?php gutenbooster_post_meta(); ?></div>
     <?php else: ?>
         <div class="entry-meta"></div>
     <?php endif; ?>
     <div class="entry-summary"><?php the_excerpt(); ?></div><!-- .entry-content -->

     <a class="underlined read-more" href="<?php the_permalink() ?>"><?php esc_html_e( 'Read more', 'gutenbooster' ) ?></a>
  </div>

</div><!-- .media -->
