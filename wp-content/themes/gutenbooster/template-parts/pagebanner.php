<?php
/**
 * Template part for displaying Page Banner
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GutenBooster
 */
$gutenboooster_has_img_class = gutenbooster_pagebanner_bg() ? 'has-pb-img' : '';
?>


<header id="page-banner-wrap" class="<?php echo esc_html($gutenboooster_has_img_class); ?>" role="banner" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
   <div class="kontain <?php echo esc_attr( gutenbooster_pageban_props('subtitle_around') ) .' '. esc_attr( gutenbooster_pageban_props("parallax_class") ); ?>" <?php echo esc_attr( gutenbooster_pageban_props("bg_anim_data") ); ?>>


	   <!-- <video autoplay muted loop id="video-header">
		   <source src="http://localhost:9090/wordpress/wp-content/uploads/2020/01/Concerto-11832.mp4" type="video/mp4">
		   Your browser does not support HTML5 video.
	   </video> -->

     <div class="page-banner-bg" data-curr-opacity="<?php echo esc_html( gutenbooster_pb_bg_opacity() ); ?>" <?php echo gutenbooster_pagebanner_bg() ? 'style="background-image:url(' . esc_url( gutenbooster_pagebanner_bg() ) .');"' : ''; ?>></div>


     <div class="d-flex w-100 h-100 text-center justify-content-center align-items-center">
       <div class="container">
       <div class="row">
          <div class="col">
            <div class="banner-heading-wrap w-100">

        <?php
        //if pb has animation and is single post or page
        if ( is_singular() && gutenbooster_pageban_single_has_anim() && ( gutenbooster_postmeta('pb_title_line_1', true ) || gutenbooster_postmeta( 'pagebanner_title_line', true) ) ) :
              get_template_part( 'template-parts/pagebanner/pbtitle', 'page' );
        //if pb has animation and is shop home
        elseif ( gutenbooster_is_shop_home() && gutenbooster_shop_page_id() && get_post_meta( gutenbooster_shop_page_id(), 'enable_page_banner_animate', true )) :

              get_template_part( 'template-parts/pagebanner/pbtitle', 'shop' );

        //if pb has animation and is blog home
        elseif ( is_home() && get_option('show_on_front') !=='posts' && get_post_meta( get_queried_object_id(), 'enable_page_banner_animate', true )) :

              get_template_part( 'template-parts/pagebanner/pbtitle', 'blog' );

        else: ?>

               <?php if (is_404()) :
                get_template_part( 'template-parts/pagebanner/pb', '404' );
               ?>

              <?php  else: ?>

                <div class="heading-content"><div>
                  <?php
                    if ( ! is_singular() ) {
                      the_archive_title( '<h1 class="page-title">', '</h1>' );
                  } else { ?>
                      <?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
                    <?php
                    }
                  ?>
                </div></div>

              <?php endif; ?>
          <!-- .heading content - title heading -->
        <?php
        endif;
            //subtitles for pages
            if ( is_singular('page') && !empty(get_post_meta(get_queried_object_id(), 'pagebanner_subtitle', true)) ) {
              get_template_part( 'template-parts/pagebanner/pbsubtitle', 'page' );
            }
            //subtitles for post singles
            elseif (is_singular('post')) {
              get_template_part( 'template-parts/pagebanner/pbsubtitle', 'post' );
            }

            ?>

       </div><!-- banner-heading-wrap -->
          </div>
       </div>
     </div><!-- .page-banner-inner -->
     </div>

   </div><!-- .kontain -->
</header>
