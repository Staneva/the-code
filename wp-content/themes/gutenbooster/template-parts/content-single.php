<?php
/**
 * Template part for display single post content
 *
 * @package GutenBooster
 */
?>

<div class="entry-content w-100">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">
		<?php the_content( ); ?>
		<?php
			wp_link_pages(
				array(
					'before'      => '<div class="page-links">' . esc_html__( 'Pages:', 'gutenbooster' ),
					'after'       => '</div>',
					'link_before' => '<span class="page-number">',
					'link_after'  => '</span>',
				)
			);
		?>

		<?php if ( has_tag() ) : ?>
			<footer class="entry-footer clearfix d-flex justify-content-between">
				<div class="entry-footer__taxonomy justify-content-start align-self-center align-items-center">
					<?php gutenbooster_post_tags(); ?>
				</div>
			</footer>
		<?php endif; ?>

		<?php if( get_theme_mod('single_show_author') ) : ?>
			<?php	get_template_part( 'template-parts/bio' ); ?>
		<?php endif; ?>

	</article>
</div>
