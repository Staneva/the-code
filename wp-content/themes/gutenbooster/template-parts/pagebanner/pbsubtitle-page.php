<?php
/**
 * Template part for displaying Page Banner in Pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GutenBooster
 */

?>
<div class="heading-content subheading-wrap">
   <div class="subheading">
      <h3><?php echo esc_html( gutenbooster_postmeta( 'pagebanner_subtitle', true ) ); ?></h3>
   </div>
</div>

<?php gutenbooster_pagebanner_button(); ?>