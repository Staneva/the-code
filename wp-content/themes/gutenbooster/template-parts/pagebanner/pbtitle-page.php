<?php
/**
 * Template part for displaying Page Banner title in pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GutenBooster
 */

?>

  <?php
if ( ! gutenbooster_has_kioken_metaplugin() && gutenbooster_postmeta( 'pagebanner_title_line', true ) ) :
    $pb_page_titles = gutenbooster_postmeta( 'pagebanner_title_line', true );
    foreach ( $pb_page_titles as $pb_title ) :
  ?>

  <div class="heading-content">

     <div>
       <h1><?php echo esc_html($pb_title); ?></h1>
     </div>

  </div>
    <?php endforeach; ?>
<?php else: ?>

    <?php if (gutenbooster_postmeta('pb_title_line_1', true)) { ?>
      <div class="heading-content">

         <div>
           <h1><?php echo esc_html(gutenbooster_postmeta('pb_title_line_1', true)); ?></h1>
         </div>

      </div>
    <?php } ?>
    <?php if (gutenbooster_postmeta('pb_title_line_2', true)) { ?>
      <div class="heading-content">

         <div>
           <h1><?php echo esc_html(gutenbooster_postmeta('pb_title_line_2', true)); ?></h1>
         </div>

      </div>
    <?php } ?>
    <?php if (gutenbooster_postmeta('pb_title_line_3', true)) { ?>
      <div class="heading-content">

         <div>
           <h1><?php echo esc_html(gutenbooster_postmeta('pb_title_line_3', true)); ?></h1>
         </div>

      </div>
    <?php } ?>

<?php endif; ?>