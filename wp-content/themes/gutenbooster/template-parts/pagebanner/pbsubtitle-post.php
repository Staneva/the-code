<?php
/**
 * Template part for displaying page banner in posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gutenbooster
 */


?>
     <div class="heading-content ">
         <div class="post-meta">
           <?php gutenbooster_post_meta(); ?>
         </div>
     </div>
