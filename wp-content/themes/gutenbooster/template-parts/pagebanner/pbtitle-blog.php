<?php
/**
 * Template part for displaying Page Banner title in blog homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GutenBooster
 */

?>

<?php
 if ( ! gutenbooster_has_kioken_metaplugin()  && rwmb_meta( 'pagebanner_title_line', '', get_queried_object_id() ) ) :
  $gutenbooster_pb_blog_page_titles = rwmb_meta( 'pagebanner_title_line', '', get_queried_object_id() );
  foreach ( $gutenbooster_pb_blog_page_titles as $pb_title ) :
?>

<div class="heading-content">

   <div>
     <h1><?php echo esc_html($pb_title); ?></h1>
   </div>

</div>
  <?php endforeach; ?>
<?php else: ?>

  <?php if (gutenbooster_postmeta('pb_title_line_1', true)) { ?>
    <div class="heading-content">

       <div>
         <h1><?php echo esc_html(gutenbooster_postmeta('pb_title_line_1', true)); ?></h1>
       </div>

    </div>
  <?php } ?>
  <?php if (gutenbooster_postmeta('pb_title_line_2', true)) { ?>
    <div class="heading-content">

       <div>
         <h1><?php echo esc_html(gutenbooster_postmeta('pb_title_line_2', true)); ?></h1>
       </div>

    </div>
  <?php } ?>
  <?php if (gutenbooster_postmeta('pb_title_line_3', true)) { ?>
    <div class="heading-content">

       <div>
         <h1><?php echo esc_html(gutenbooster_postmeta('pb_title_line_3', true)); ?></h1>
       </div>

    </div>
  <?php } ?>

<?php endif; ?>