<?php
/**
 * Template part for displaying Page Banner in 404 Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GutenBooster
 */

?>
<!-- THEN COMES 404 -->
 <div class="heading-content"><div><h1 class="fourohfour"><?php echo esc_html__('404', 'gutenbooster') ?></h1></div></div>
 <div class="heading-content"><div><h1><?php echo esc_html__('Page Not Found', 'gutenbooster') ?></h1></div></div>
 <div class="heading-content subheading-wrap">
   <div class="subheading">
      <h3><?php echo esc_html__('Sorry about that', 'gutenbooster') ?></h3>
   </div>
</div>
<?php get_search_form(); ?>
<div class="heading-content button-wrap">
   <div><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="k-btn btn__primary btn__lg btn__uc btn__innerborder btn__arrvrt"><span><?php echo esc_html_e('Go Back Home', 'gutenbooster') ?></span></a></div>

</div>