<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gutenbooster
 */

 global $wp_query;

 $gutenbooster_blog_layout         = gutenbooster_getmod( 'blog_layout' );
 $gutenbooster_page_layout         = gutenbooster_getmod( 'layout_default' );

 $gutenbooster_load_template       = 'cards';
 $gutenbooster_load_template       = ($gutenbooster_blog_layout == 'cards') ? $gutenbooster_load_template : 'classic';


 //Condition for Grid Layout
 if ( $gutenbooster_blog_layout == 'grid' ) {

 		$gutenbooster_blog_post_class = 'kt-grid-post col-sm-12 col-lg-6  pb-4 pt-4 feed-item';
 }
 elseif ($gutenbooster_blog_layout == 'cards') {
   // $gutenbooster_blog_post_class = 'kt-card-post col-sm-12 col-lg-4  pb-4 pt-4';
   $gutenbooster_blog_post_class = 'kt-card-post card mb-3 mt-2 feed-item';
 }
 //Condition for Classic Layout
 else {
 		$gutenbooster_blog_post_class = 'kt-classic-post col-12 pb-4 pt-4 mb-4 feed-item';
 }

 // Apply Large post display every 1st and 5th posts when has post thumbnail
 // and blog layout is not "cards"
 if ( (( $wp_query->current_post + 1 ) % 5 == 1 &&
        has_post_thumbnail() && $gutenbooster_blog_layout !== "cards"
         ) ) {

   $gutenbooster_blog_post_class = ' kt-large-post col-12 pb-4 pt-4 feed-item';
 }



 ?>
 <article id="post-<?php the_ID(); ?>" <?php post_class( $gutenbooster_blog_post_class ); ?> role="article">
    <?php get_template_part( 'template-parts/blog/blog', $gutenbooster_load_template ); ?>
 </article><!-- #post-## -->