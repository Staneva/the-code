<?php
/**
 * Template part for displaying attachments
 *
 * @package GutenBooster
 */
?>

<div class="entry-content w-100">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">
            <?php gutenbooster_nobanner_pagetitle("page"); ?>

					<div class="entry-attachment section section-text">
						<?php
						if ( wp_attachment_is_image( $post->ID ) ) :
							$att_image = wp_get_attachment_image_src( $post->id, 'full' );

                            // get image metadata
                            $attachment_path = get_attached_file( $post->ID );

	                        $meta_alt = get_post_meta( $post->ID , '_wp_attachment_image_alt', true );
                            $att_desc = $post->post_content;
                            $caption = $post->post_excerpt;

							?>
							<a href="<?php echo esc_url( wp_get_attachment_url( $post->id ) ); ?>"
									title="<?php the_title_attribute(); ?>" rel="attachment">
                                <figure>
    								<img src="<?php echo esc_url( $att_image[0] ); ?>"
    										width="<?php echo esc_attr( $att_image[1] ); ?>"
    										height="<?php echo esc_attr( $att_image[2] ); ?>" class="attachment-medium"
    										alt="<?php esc_attr( $post->post_excerpt ); ?>"/>
                                    <?php if ( $caption ) : ?><figcaption><?php echo esc_html( $caption ); ?></figcaption><?php endif; ?>
                                </figure>
							</a>
                            <?php if ( $att_desc ) : ?><p><?php echo esc_html( $att_desc ); ?></p><?php endif; ?>


						<?php else : ?>
							<a href="<?php echo esc_url( wp_get_attachment_url( $post->ID ) ); ?>"
									title="<?php the_title_attribute(); ?>" rel="attachment">
								<?php echo basename( $post->guid ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
							</a>
							<?php
						endif;

						echo '<p class="sizes">';if ( wp_attachment_is_image( get_the_ID() ) ) {
							echo '<div class="image-meta">';
							/* translators: %s is Image sizes for attachment single page. */
							printf( esc_html__( 'Sizes: %s', 'gutenbooster' ), gutenbooster_get_image_sizes() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

							echo '</div>';
						}
						echo '</p>';
						?>
					</div>


		<?php if( gutenbooster_getmod('single_show_author') ) : ?>
			<?php	get_template_part( 'template-parts/bio' ); ?>
		<?php endif; ?>

	</article>
</div>
