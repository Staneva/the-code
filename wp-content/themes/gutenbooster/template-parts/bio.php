<?php
/**
 * The template part for displaying an Author biography
 *
 * @package GutenBooster
 */
?>

<div class="author-info mb-0 clearfix">
	<div class="author-vcard pull-left" itemtype="https://schema.org/Person" itemscope itemprop="author">
		<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) ?>"><?php echo get_avatar( get_the_author_meta( 'user_email' ), 80 ); ?></a>
		<h2 class="author-title">
			<span class="author-heading"><?php esc_html_e( 'Author', 'gutenbooster' ); ?></span>
			<span class="author-name"><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) ?>"><?php echo get_the_author(); ?></a></span>
		</h2>
		<?php if (get_the_author_meta('description')) : // Checking if the user has added any author descript or not. If it is added only, then lets move ahead ?>
	      <p class="author-description"><?php esc_textarea(the_author_meta('description')); // Displays the author description added in Biographical Info ?></p>
		<?php endif; ?>
	</div><!-- .author-avatar -->

	<div class="author-socials text-right pull-right">
		<?php
		$gutenbooster_socials = array( 'facebook', 'twitter', 'google', 'instagram', 'pinterest' );
		foreach ( $gutenbooster_socials as $gutenbooster_social ) {
			$gutenbooster_author_link = get_the_author_meta( $gutenbooster_social );

			if ( empty( $gutenbooster_author_link ) ) {
				continue;
			}

			printf(
				'<a href="%s" target="_blank" rel="nofollow"><i class="fa fa-%s" aria-hidden="true"></i></a>',
				esc_url( $gutenbooster_author_link ),
				esc_attr( str_replace( array( 'google', 'pinterest' ), array( 'google-plus', 'pinterest-p' ), $gutenbooster_social ) )
			);
		}
		?>
	</div><!-- .author-description -->
</div><!-- .author-info -->
