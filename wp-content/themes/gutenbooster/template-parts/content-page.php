<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gutenbooster
 */

?>
<div class="entry-content">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">

		<?php gutenbooster_nobanner_pagetitle("page"); ?>
		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links clearfix">' . esc_html__( 'Pages:', 'gutenbooster' ),
			'after'  => '</div>',
			'link_before' => '<span class="page-number">',
			'link_after'  => '</span>',
		) );
		?>


		<?php if ( get_edit_post_link() ) : ?>
		<!-- edit-post-link -->
	<?php endif; ?>
	</article><!-- #post-<?php the_ID(); ?> -->
</div><!-- .entry-content -->
