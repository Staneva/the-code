<div class="row v2 justify-content-center inner-wrap flex-nowrap">
   <div class="left-side d-flex justify-content-end pr-0 col-md-4 col-sm-4 col-xs-4 col-lg-5">

      <nav class="main-nav w-100 site-nav-top text-uppercase d-none d-lg-block">
         <?php gutenbooster_header_menus("gbb-primary"); ?>
      </nav>
      <nav class="main-nav w-100 site-nav-top d-lg-none mobile-drawer">
        <ul class="nav-menu">
          <li class="menu-item menu-item-jsnav inv2">
            <a href="javascript:;"> <span><?php gutenbooster_icon_url( 'menu', true ); ?></span></a>
          </li>
         </ul>
      </nav>
   </div>

   <!-- LOGO -->
   <div class=" d-flex p-0 pl-1 pr-1 site-logo justify-content-center text-center col-md-4 col-sm-4 col-xs-4 col-lg-2">
      <?php get_template_part( 'template-parts/headers/logo' ); ?>
   </div>

   <div class="right-side  d-flex justify-content-start pl-0 col-md-4 col-sm-4 col-xs-4 col-lg-5">
      <nav class="main-nav w-100 site-nav-top text-uppercase d-none d-lg-block" itemtype="https://schema.org/SiteNavigationElement" itemscope role="navigation" aria-label="<?php echo esc_html_e( 'Header Menu', 'gutenbooster' ); ?>">
         <?php gutenbooster_header_menus("gbb-secondary"); ?>
      </nav><!-- .site-nav-top -->

      <nav class="main-nav w-100 site-nav-top d-lg-none mobile-drawer">
        <ul class="nav-menu">
          <li class="menu-item invisible">
            <a href="javascript:;"> <span>&nbsp;</span></a>
          </li>
         </ul>
      </nav>


   </div>
</div><!-- end of menu wrapper row -->