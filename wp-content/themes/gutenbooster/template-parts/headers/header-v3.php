<?php

  $gutenboooster_v3_navpos = get_theme_mod('headerv3_nav_pos') ? get_theme_mod('headerv3_nav_pos') : 'right';

  $gutenbooster_nav_v3_class         = '';
  $gutenbooster_nav_v3_utils_class   = 'pl-3';

  $gutenbooster_nav3_container_class = $gutenboooster_v3_navpos=="right" ? 'justify-content-end' : '';

  if ($gutenboooster_v3_navpos=="left") {
    $gutenbooster_nav_v3_class = 'flex-grow-1';
  } else if ($gutenboooster_v3_navpos=="center") {
    $gutenbooster_nav_v3_class = 'mx-auto';
    $gutenbooster_nav_v3_utils_class   = 'no-left-padding';
  }

  $gutenboooster_zeroLeftPad = has_nav_menu('gbb-primary') || has_nav_menu('primary') ? 'pl-0' : '';

?>


<div class="row v3 justify-content-center align-items-center">

   <!-- LOGO -->

   <div class="d-flex site-logo col">
      <?php get_template_part( 'template-parts/headers/logo' ); ?>
   </div>

     <div class="d-flex col align-items-center <?php echo esc_html($gutenbooster_nav3_container_class); ?>">
      <nav class="main-nav <?php echo esc_html( $gutenbooster_nav_v3_class ); ?> site-nav-top d-none d-lg-block" itemtype="https://schema.org/SiteNavigationElement" itemscope role="navigation" aria-label="<?php echo esc_html_e( 'Header Menu', 'gutenbooster' ); ?>">
         <?php gutenbooster_header_menus("gbb-primary"); ?>
      </nav><!-- .site-nav-top -->

      <div class="utils <?php echo esc_attr( $gutenbooster_nav_v3_utils_class ); ?> d-none d-lg-block">
         <ul>
           <?php echo gutenbooster_header_icons('right','v3'); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
         </ul>
      </div>

      <nav class="main-nav w-100 site-nav-top d-lg-none text-right mobile-drawer">
        <ul class="nav-menu">
          <li class="menu-item menu-item-jsnav inv2">
            <a href="javascript:;"> <span><?php gutenbooster_icon_url( 'menu', true ); ?></span></a>
          </li>
         </ul>
      </nav>

   </div>

</div>
