<?php
/**
 * The template part for displaying the main logo on header
 *
 * @package gutenbooster
 */

$gutenbooster_logo_type = gutenbooster_getmod( 'logo_type' );
$gutenbooster_logo_show_tagline = gutenbooster_getmod( 'core_display_tagline' );

$gutenbooster_logo_mx_auto = ( gutenbooster_getmod( 'header_layout' ) !== '' && !has_nav_menu('gbb-primary') ) || gutenbooster_getmod( 'header_layout' ) === 'v3' && !has_nav_menu('gbb-primary') ? 'mx-auto' : '';
if ( 'text' == $gutenbooster_logo_type ) :
	$gutenbooster_logo       = get_bloginfo( 'name' );
else:
	$gutenbooster_logo       = gutenbooster_getmod( 'custom_logo' );
		$gutenbooster_logo     = wp_get_attachment_image_src($gutenbooster_logo, 'full');
		$gutenbooster_logo     = $gutenbooster_logo[0];

	$gutenbooster_logo_light = gutenbooster_getmod( 'logo_light' );
	$gutenbooster_logo_light = $gutenbooster_logo_light;


endif;

?>

<a href="<?php echo esc_url( home_url() ) ?>" class="logo <?php echo esc_html($gutenbooster_logo_mx_auto); ?>">
	<?php if ( 'text' == $gutenbooster_logo_type ) : ?>
		<span class="logo-text"><?php echo esc_html( $gutenbooster_logo ) ?></span>
		<?php if ($gutenbooster_logo_show_tagline ): ?>
			<span class="logo-minitext"><?php bloginfo( 'description' ); ?></span>
		<?php endif; ?>
	<?php else : ?>
		<?php if ( ! $gutenbooster_logo && ! $gutenbooster_logo_light ) : ?>
			<span class="logo-minitext"><?php esc_html_e( 'Upload default and light logo images', 'gutenbooster' ); ?></span>
		<?php else: ?>
			<img src="<?php echo esc_url( $gutenbooster_logo ); ?>" alt="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" class="logo-dark">
			<img src="<?php echo esc_url( $gutenbooster_logo_light ); ?>" alt="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" class="logo-light">
		<?php endif; ?>
	<?php endif; ?>
</a>

<?php if ( is_front_page() && is_home() ) : ?>
	<h1 class="site-title">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
	</h1>
<?php else : ?>
	<p class="site-title">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
	</p>
<?php endif; ?>
