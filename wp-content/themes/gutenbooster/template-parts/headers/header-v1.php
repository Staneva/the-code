<div class="row v1 align-items-center justify-content-center">
   <div class="left-side d-flex col-3 col-md-4 col-lg-5">
      <div class="utils mr-auto d-none d-md-block">
         <ul>
           <?php echo gutenbooster_header_icons('left','v1'); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
         </ul>
      </div>
      <nav class="main-nav site-nav-top text-uppercase d-none d-lg-block">
         <?php gutenbooster_header_menus("gbb-primary"); ?>
      </nav>

      <nav class="utils w-100 site-nav-top d-lg-none mobile-drawer">
        <ul class="nav-menu">
          <li class="menu-item menu-item-jsnav inv2">
            <a href="javascript:;"> <span><?php gutenbooster_icon_url( 'menu', true ); ?></span></a>
          </li>
         </ul>
      </nav>

   </div>

   <!-- LOGO -->
   <div class=" site-logo d-flex align-items-center justify-content-center text-center col-6 col-md-auto col-lg-2">
      <?php get_template_part( 'template-parts/headers/logo' ); ?>
   </div>

   <div class="right-side d-flex col-3 col-md-4 col-lg-5">

      <nav class="main-nav site-nav-top text-uppercase d-none d-lg-block" itemtype="https://schema.org/SiteNavigationElement" itemscope role="navigation" aria-label="<?php echo esc_html_e( 'Header Menu', 'gutenbooster' ); ?>">
         <?php gutenbooster_header_menus("gbb-secondary"); ?>
      </nav><!-- .site-nav-top -->

      <div class="utils ml-auto d-none d-lg-block">
         <ul>
           <?php echo gutenbooster_header_icons('right','v1'); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
         </ul>

      </div>

      <nav class="main-nav w-100 site-nav-top d-md-none mobile-drawer">
        <ul class="nav-menu">
          <li class="menu-item invisible">
            <a href="javascript:;"> <span>&nbsp;</span></a>
          </li>
         </ul>
      </nav>

   </div>
</div><!-- end of menu wrapper row -->
