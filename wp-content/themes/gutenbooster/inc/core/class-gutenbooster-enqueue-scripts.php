<?php
/**
 * Loader Functions
 *
 * @package     GutenBooster
 * @author      GutenBooster
 * @copyright   Copyright (c) 2019, GutenBooster
 * @link        https://kiokengutenberg.com/
 * @since       GutenBooster 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Theme Enqueue Scripts
 */
if ( ! class_exists( 'GutenBooster_Enqueue_Scripts' ) ) {

	/**
	 * Theme Enqueue Scripts
	 */
	class GutenBooster_Enqueue_Scripts {

		/**
		 * Class styles.
		 *
		 * @access public
		 * @var $styles Enqueued styles.
		 */
		public static $styles;

		/**
		 * Constructor
		 */
		public function __construct() {
			// front end styles
            add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ), 1 );

			// front end scripts
            add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), 10 );

			//admin assets
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_assets' ) );

			// gutenberg customizer driven styles
			add_action( 'enqueue_block_editor_assets', array( $this, 'gutenberg_assets' ) );

		}


		/**
		 * List of all assets.
		 *
		 * @return array assets array.
		 */
		public static function theme_assets() {

            $default_assets = array(

				// handle => location ( in /assets/css/ ) ( without .css ext).
				'css' => array(
					'gutenbooster-theme-css' => 'inline_override',
				),
			);

			return apply_filters( 'gutenbooster_theme_assets', $default_assets );
		}

        /**
         * Enqueue Scripts
         */
        public function enqueue_scripts() {

            global $is_IE, $is_safari;

			$vendor_js_uri = GUTENBOOSTER_THEME_URI . 'js/vendor/';


            // swiper. don't load if already exists from the plugin
			// exclude from singular posts and page singles since it's currently only used for gallery post format in the feed view
            if ( ( ! wp_script_is( 'swiper-js' ) && ! class_exists( 'Kioken_Blocks_Frontend' ) ) && ( is_home() || is_archive() ) ) {
                wp_enqueue_script( 'swiper-js', $vendor_js_uri . 'swiper.min.js', array(), '4.5.0', true );
            }

            //superfish
            wp_enqueue_script( 'superfish', $vendor_js_uri . 'superfish.min.js', array(), '1.7.10', true );

            //stickybits - get only for WooCommerce since right now only WooCommerce product page uses it
			if ( gutenbooster_is_woocommerce_on() ) {
            	wp_enqueue_script( 'stickybits', $vendor_js_uri . 'stickybits.min.js', array(), '3.6.6', true );
			}

            //skip link focus fix
            $is_IE ? wp_enqueue_script( 'skip-link-focus-fix', $vendor_js_uri . 'skip-link-focus-fix.js', array(), GUTENBOOSTER_THEMEVERSION, true ) : '';

            //object fit js - enables object fit css property work on IE
            ( $is_IE || $is_safari ) ? wp_enqueue_script( 'object-fit-images', $vendor_js_uri . 'ofi.min.js', array(), '3.2.4', true ) : '';

            //magnific popup
            is_singular() ? wp_enqueue_script( 'magnific-popup', $vendor_js_uri . 'jquery.magnific-popup.min.js', array( 'jquery' ), '1.1.0', true ) : '';

			// commment reply
        	( is_singular() && comments_open() && get_option( 'thread_comments' ) ) ? wp_enqueue_script( 'comment-reply' ) : '';

            //main js
        	wp_enqueue_script( 'gutenbooster-main-js', get_template_directory_uri() . '/js/ignite-min.js', array( 'jquery' ), GUTENBOOSTER_THEMEVERSION, true );

        }


		/**
		 * Enqueue Styles
		 */
		public function enqueue_styles() {

            $gutenbooster_enqueue = apply_filters( 'gutenbooster_enqueue_theme_assets', true );

			if ( ! $gutenbooster_enqueue ) {
				return;
			}

			$css_uri = GUTENBOOSTER_THEME_URI . 'assets/css/';

            // All assets.
			$all_assets = self::theme_assets();
			$styles     = $all_assets['css'];

			$google_fonts_url = GutenBooster_Google_Fonts::get_google_fonts_url();

			$body_font_family = gutenbooster_get_font_data( 'typo_body' );
			$headings_font_family = gutenbooster_get_font_data( 'typo_headings' );

			$system_stack = [ 'Arial', 'Verdana','Helvetica', 'Tahoma', 'Georgia', 'Times New Roman' ];

			//fonts
			if ( $google_fonts_url ) {
				if ( ! ( in_array( $body_font_family, $system_stack ) && in_array( $headings_font_family, $system_stack ) ) ) {
					wp_enqueue_style( 'gutenbooster-google-fonts', $google_fonts_url, false, 1.0, 'all' );
				}
			} else {
				if ( ! ( in_array( $body_font_family, $system_stack ) && in_array( $headings_font_family, $system_stack ) ) ) {
					wp_enqueue_style( 'gutenbooster-google-fonts', gutenbooster_default_fonts(), array(), GUTENBOOSTER_THEMEVERSION );
				}
			}


			if ( is_array( $styles ) && ! empty( $styles ) ) {
				// Register & Enqueue Styles.
				foreach ( $styles as $key => $style ) {

					// Generate CSS URL.
					$css_file = $css_uri . $style . '.css';

					// Register.
					wp_register_style( $key, $css_file, array('gutenbooster-style'), GUTENBOOSTER_THEMEVERSION, 'all' );

					// Enqueue.
					wp_enqueue_style( $key );

				}
			}



        	wp_enqueue_style( 'bootstrap', $css_uri . 'vendor/bootstrap.min.css', array(), '4.1.3' );
			// load magnific popup assets only for single page and posts.
        	is_singular() ? wp_enqueue_style( 'magnific-popup', $css_uri . 'vendor/magnific-popup.css', array(), '1.1.0' ) : '';

        	if ( ( ! wp_style_is( 'swiper' ) && ! class_exists( 'Kioken_Blocks_Frontend' ) ) && ( is_home() || is_archive() ) ) {
        		wp_enqueue_style( 'swiper', $css_uri . 'vendor/swiper.min.css', array(), '4.5.0' );
        	}


            //main stylesheet of the theme
        	wp_enqueue_style( 'gutenbooster-style', get_stylesheet_uri(), array( 'bootstrap' ), GUTENBOOSTER_THEMEVERSION );

            /**
    		 * Inline styles
    		 */
    		wp_add_inline_style( 'gutenbooster-theme-css', GutenBooster_Dynamic_CSS::return_output() );
			wp_add_inline_style( 'gutenbooster-theme-css', GutenBooster_Dynamic_CSS::return_meta_output( true ) );


		}

		/**
		 * Trim CSS
		 *
		 * @since 1.0.0
		 * @param string $css CSS content to trim.
		 * @return string
		 */
		public static function trim_css( $css = '' ) {

			// Trim white space for faster page loading.
			if ( ! empty( $css ) ) {
				$css = preg_replace( '!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css );
				$css = str_replace( array( "\r\n", "\r", "\n", "\t", '  ', '    ', '    ' ), '', $css );
				$css = str_replace( ', ', ',', $css );
			}

			return $css;
		}

		/**
		 * Enqueue Gutenberg assets.
		 *
		 * @since 1.5.2
		 *
		 * @return void
		 */
		public function gutenberg_assets() {

			$css_uri = GUTENBOOSTER_THEME_URI . 'assets/css/editor/gutenberg-style.css';

			wp_enqueue_style( 'gutenbooster-block-editor-styles', $css_uri, false, GUTENBOOSTER_THEMEVERSION, 'all' );
			wp_add_inline_style( 'gutenbooster-block-editor-styles', apply_filters( 'gutenbooster_block_editor_dynamic_css', Gutenberg_Editor_CSS::get_css() ) );

			$google_fonts_url = GutenBooster_Google_Fonts::get_google_fonts_url();

			//fonts
			if ( $google_fonts_url ) {
				wp_enqueue_style( 'gutenbooster-google-fonts', $google_fonts_url, false, 1.0, 'all' );
			} else {
				wp_enqueue_style( 'gutenbooster-google-fonts', gutenbooster_default_fonts(), array(), GUTENBOOSTER_THEMEVERSION );
			}

		}

		public function admin_assets() {

			wp_register_style( 'gutenbooster_admin_css', get_template_directory_uri() . '/assets/css/gb-admin.css', null, GUTENBOOSTER_THEMEVERSION, 'all' );
			wp_enqueue_style( 'gutenbooster_admin_css' );

		}

	}

	new GutenBooster_Enqueue_Scripts();
}
