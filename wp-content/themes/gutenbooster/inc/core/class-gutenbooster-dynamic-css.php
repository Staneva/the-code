<?php
/**
 * Custom Styling output for GutenBooster Theme.
 *
 * @package     GutenBooster
 * @subpackage  Class
 * @author      GutenBooster
 * @copyright   Copyright (c) 2019, GutenBooster
 * @link        https://kiokengutenberg.com/
 * @since       GutenBooster 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Dynamic CSS
 */
if ( ! class_exists( 'GutenBooster_Dynamic_CSS' ) ) {

	/**
	 * Dynamic CSS
	 */
	class GutenBooster_Dynamic_CSS {

		/**
		 * Return CSS Output
		 *
		 * @return string Generated CSS.
		 */
		public static function return_output() {

			$dynamic_css = '';

			$css_common = array();
			$css_colors_common = array();

			$css_output = array();


			/**
			 *
			 * Contents
			 * - Variable Declaration
			 */

			/*=============================================>>>>>
			= Variables =
			===============================================>>>>>*/

			// Page banner vars
			$pbg = get_theme_mod( 'page_banner_bgcolor' );
			$pbh = gutenbooster_getmod( 'page_banner_height' );
			$pbg_opacity = get_theme_mod( 'page_bannerbg_opacity' );

			// Header vars
			$header_pad = gutenbooster_getmod( 'header_topbottom_pad' );
			$header_submenu_space = gutenbooster_getmod( 'header_submenu_space' );
				//shrink
				$header_shrink = gutenbooster_getmod( 'shrink' );
				$shrink_bgcolor = gutenbooster_getmod( 'shrinkbg' );

			//logo
			$logo_topmargin = get_theme_mod( 'logo_topmargin' );
			$logo_bottommargin = get_theme_mod( 'logo_bottommargin' );
			$logo_leftmargin = get_theme_mod( 'logo_leftmargin' );
			$logo_rightmargin = get_theme_mod( 'logo_rightmargin' );
				$logo_height = get_theme_mod( 'logo_height' );


			// Footer Vars
			$footer_bg_color = gutenbooster_getmod( 'footer_bg_color' );
			$footer_bg_opacity = gutenbooster_getmod( 'footer_bg_opacity' );
			$footer_txt_color = get_theme_mod('footer_txt_color');
			$footer_link_color = get_theme_mod('footer_link_color');
			$footer_link_hover_color = get_theme_mod('footer_link_hover_color');
			$footer_topbottom_pad = get_theme_mod('footer_pad');


			// Layout Vars
			$layout_width = gutenbooster_getmod( 'layout_content_width' );
			$content_toppad = gutenbooster_getmod( 'content_toppad' );
			$units_topbottom_margin = gutenbooster_getmod( 'units_topbottom_margin' );
			$sitewrap_border_color = gutenbooster_getmod( 'sitewrap_border_color' );


			//color vars
			$background_color = sanitize_hex_color_no_hash( get_background_color() );
			$override_colors  = gutenbooster_getmod( 'override_colors' );

			//main colors
			$color_primary    = get_theme_mod( 'color_gb_primary' );
			$color_secondary  = get_theme_mod( 'color_gb_secondary' );
			$color_tertiary   = get_theme_mod( 'color_gb_tertiary' );

			$color_gb_one     = get_theme_mod( 'color_gb_one' );
			$color_gb_two     = get_theme_mod( 'color_gb_two' );
			$color_gb_three   = get_theme_mod( 'color_gb_three' );
			$color_gb_four    = get_theme_mod( 'color_gb_four' );
			$color_gb_five    = get_theme_mod( 'color_gb_five' );
			$color_gb_six     = get_theme_mod( 'color_gb_six' );
			$color_gb_seven   = get_theme_mod( 'color_gb_seven' );
			$color_gb_eight   = get_theme_mod( 'color_gb_eight' );
			$color_gb_nine    = get_theme_mod( 'color_gb_nine' );
			$color_gb_ten     = get_theme_mod( 'color_gb_ten' );
			$color_gb_eleven  = get_theme_mod( 'color_gb_eleven' );
			$color_gb_twelve  = get_theme_mod( 'color_gb_twelve' );
			$color_gb_thirteen = get_theme_mod( 'color_gb_thirteen' );
				//sidebar colors
				$color_sb_bg = get_theme_mod( 'color_sb_bg' );
				$color_sb_text = get_theme_mod( 'color_sb_text' );
				$color_sb_widget_titles = get_theme_mod( 'color_sb_widget_titles' );
				$color_sb_link = get_theme_mod( 'color_sb_link' );
				$color_sb_link_hov = get_theme_mod( 'color_sb_link_hov' );


			// typography vars
			$body_font_family = gutenbooster_get_font_data( 'typo_body' );
			$body_font_weight = gutenbooster_get_font_data( 'typo_body', 'fontweight' );
			$body_line_height = get_theme_mod( 'typobody_lh' );

			$body_fontsize = get_theme_mod( 'body_fontsize' );

				$headings_font_family = gutenbooster_get_font_data( 'typo_headings' );
				$headings_font_weight = gutenbooster_get_font_data( 'typo_headings', 'fontweight' );

				$header_menu_font_family = gutenbooster_get_font_data( 'font_menu' );
	            $header_menu_font_weight = gutenbooster_get_font_data( 'font_menu', 'fontweight' );
	            $header_menu_text_transform = get_theme_mod( 'font_menu_text_transform' );
	            $header_menu_font_size = get_theme_mod( 'font_menu_size' );


			// preloader
			$preloader_bg_switch = get_theme_mod( 'preloader_bg_switch' );
			$preloader_bg_color = get_theme_mod( 'preloader_bg_color' );


			$css_404 = array();

			if ( is_404() ) {
				$css_404 = array(
					'#page-banner-wrap' => array(
						'height' => esc_attr( '100vh' ),
					),
				);
			}


			$css_common = array(

				// Page Banner
				'#page-banner-wrap' => array(
					'background-color' => gutenbooster_defaults( 'page_banner_bgcolor' ) !== $pbg ? esc_attr( $pbg ) : '',
					'height' => (int) gutenbooster_defaults( 'page_banner_height' ) !== (int) $pbh ? esc_attr( $pbh ) . 'vh' : '',
				),
				'.page-banner-bg,
				#page-banner-wrap .kontain[data-kiomotion="true"].animated-in .page-banner-bg' => array(
					'opacity' => esc_attr( $pbg_opacity ),
				),

				// Background Color
				'#content, .woocommerce div.product div.images .woocommerce-product-gallery__wrapper .zoomImg,
				.kt-classic-post.format-gallery .media .swiper-container + .post-summary,
				.k-content-inner .kt-large-post .post-summary' => array(
					'background-color' => $background_color && 'ffffff' !== $background_color ? '#' . esc_attr( $background_color ) : '',
				),
				'.hfeed .kt-classic-post.sticky, .hfeed .type-post.tag-sticky-2' => array(
					'box-shadow' => $background_color && 'ffffff' !== $background_color ? '-10px 0 0 ' .  '#' . $background_color . ', -15px 0 0 ' . '#' . $background_color . ''  : '',
				),

				// Header
				'.menu-wrapper .inner-wrap' => array(
					'padding-top' => $header_pad && $header_pad !== gutenbooster_defaults( 'header_topbottom_pad' ) ? esc_attr( $header_pad ) . 'px' : '',
					'padding-bottom' => $header_pad && $header_pad !== gutenbooster_defaults( 'header_topbottom_pad' ) ? esc_attr( $header_pad ) . 'px' : '',
				),
				'.site-nav-top .nav-menu li > ul.sub-menu' => array(
					'top' => $header_submenu_space && $header_submenu_space !== gutenbooster_defaults( 'header_submenu_space' ) ? esc_attr( $header_submenu_space ) . 'px' : '',
				),
				'.menu-wrapper.shrink,
				.header--bg-transparent .menu-wrapper.sticking.shrink,
				.header--v2 .menu-wrapper.shrink .nav-menu,
				.header--v2 .header--bg-transparent .menu-wrapper.sticking.shrink .nav-menu,
				.header--v2 .menu-wrapper.shrink .site-logo,
				.header--v2 .header--bg-transparent .menu-wrapper.sticking.shrink .site-logo' => array(
					'background-color' => $header_shrink && $shrink_bgcolor ? esc_attr( $shrink_bgcolor ) : '',
				),

				// Layout
				'.layout-no-sidebar .entry-content .post > *,
				.layout-no-sidebar .entry-content .page > *,
				.layout-no-sidebar .entry-content .attachment>*,
				body.single .post-intro .title-heading h1,
				.layout-no-sidebar.hfeed.blog-classic #content > .container,
				.comment-respond,
				.comment-list' => array(
					'max-width' => $layout_width && $layout_width !== gutenbooster_defaults( 'layout_content_width' ) ? esc_attr( $layout_width ) . 'px' : '',
				),
				'#content' => array(
					'padding-top' => esc_attr( $content_toppad ) . 'px',
				),
				'.entry-content .post > *, .entry-content .page > *' => array(
					'margin-top' => esc_attr( $units_topbottom_margin ) . 'px',
					'margin-bottom' => esc_attr( $units_topbottom_margin ) . 'px',
				),

				// Typography
				'body, button, input, select, textarea' => array(
					'font-family' => esc_attr( $body_font_family ),
					'font-weight' => esc_attr( $body_font_weight ),
					'line-height' => esc_attr( $body_line_height ),
				),
				'h1,h2,h3,h4,h5,h6,
				.comment article .comment-meta .author-name,
				.comment article .comment-meta .author-name a' => array(
					'font-family' => esc_attr( $headings_font_family ),
					'font-weight' => esc_attr( $headings_font_weight ),
				),
				'.menu-wrapper .nav-menu a' => array(
					'font-family' => esc_attr( $header_menu_font_family ),
					'font-weight' => esc_attr( $header_menu_font_weight ),
					'text-transform' => esc_attr( $header_menu_text_transform ),
					'font-size' => $header_menu_font_size ? esc_attr( $header_menu_font_size ) . 'px' : '',
				),

				// Footer
				'.site-footer' => array(
					'background-color' => esc_attr( $footer_bg_color ),
					'color' => esc_attr( $footer_txt_color ),
				),
				'.site-footer a' => array(
					'color' => esc_attr( $footer_link_color ),
				),
				'.site-footer a:hover' => array(
					'color' => esc_attr( $footer_link_hover_color ),
				),
				'.footer--bg-image' => array(
					'opacity' => esc_attr( $footer_bg_opacity ),
				),
				'.site-footer .inner' => array(
					'padding-top' => esc_attr( $footer_topbottom_pad ) . 'px',
				),

				// Preloader
				'.kt-preloader-obj' => array(
					'background-color' => $preloader_bg_color && $preloader_bg_switch ? esc_attr( $preloader_bg_color . ' !important') : '',
				),

			);

			if ( $override_colors ) {

				$css_colors_common = array(
					//primary color
					'a, .entry-content a,
					.comments-area a,
					.has-gb-primary-color,
					.hfeed .media-body .entry-title a:hover,
					.hfeed .media-body .entry-meta a:hover,
					.pagination .page-numbers.current, .pagination .page-numbers:hover,
					.site-footer .inner.txt-white a:hover,
					.site-footer .inner.invert_links a' => array(
						'color' => esc_attr( $color_primary ),
					),
					'.has-gb-primary-background-color,
					.comment-respond .comment-form input[type="submit"], a.underlined::after,
					.hfeed .kt-classic-post.sticky .gb-sticky-indicator,
					.hfeed .type-post.tag-sticky-2 .gb-sticky-indicator' => array(
						'background-color' => esc_attr( $color_primary ),
					),
					//secondary color
					'a:not(.wp-block-button__link):hover,
					.has-gb-secondary-color,
					.hfeed .media-body .entry-title a,
					.hfeed .media-body .entry-meta a,
					.entry-content a:not(.wp-block-button__link):hover,
					h1, h2, h3, h4, h5, h6,
					.widget_working_hours .day,
					.pagination .page-numbers,
					.blog-cats-row ul.cat-list a,
					.entry-content .post a:not(.wp-block-button__link):hover,
					.comment article .comment-meta .author-name, .comment article .comment-meta .author-name a,
					select,
					.hfeed .type-post.tag-sticky-2 .gb-sticky-indicator' => array(
						'color' => esc_attr( $color_secondary ),
					),
					'.has-gb-secondary-background-color,
					.entry-content .cat-links a, .entry-content .tags-links a,
					.btn__primary:hover, .btn__primary.btn__arrvrt:hover::before' => array(
						'background-color' => esc_attr( $color_secondary ),
					),
					//tertiary - body color
					'body, .has-gb-tertiary-color' => array(
						'color' => esc_attr( $color_tertiary ),
					),
					'.has-gb-tertiary-background-color' => array(
						'background-color' => esc_attr( $color_tertiary ),
					),
					//custom one
					'.has-gb-customone-color' => array(
						'color' => esc_attr( $color_gb_one ),
					),
					'.has-gb-customone-background-color' => array(
						'background-color' => esc_attr( $color_gb_one ),
					),
					//custom two
					'.has-gb-customtwo-color' => array(
						'color' => esc_attr( $color_gb_two ),
					),
					'.has-gb-customtwo-background-color' => array(
						'background-color' => esc_attr( $color_gb_two ),
					),
					//custom three
					'.has-gb-customthree-color' => array(
						'color' => esc_attr( $color_gb_three ),
					),
					'.has-gb-customthree-background-color' => array(
						'background-color' => esc_attr( $color_gb_three ),
					),
					//custom four
					'.has-gb-customfour-color' => array(
						'color' => esc_attr( $color_gb_four ),
					),
					'.has-gb-customfour-background-color' => array(
						'background-color' => esc_attr( $color_gb_four ),
					),
					//custom five
					'.has-gb-customfive-color' => array(
						'color' => esc_attr( $color_gb_five ),
					),
					'.has-gb-customfive-background-color' => array(
						'background-color' => esc_attr( $color_gb_five ),
					),
					//custom six
					'.has-gb-customsix-color' => array(
						'color' => esc_attr( $color_gb_six ),
					),
					'.has-gb-customsix-background-color' => array(
						'background-color' => esc_attr( $color_gb_six ),
					),
					//custom seven
					'.has-gb-customseven-color' => array(
						'color' => esc_attr( $color_gb_seven ),
					),
					'.has-gb-customseven-background-color' => array(
						'background-color' => esc_attr( $color_gb_seven ),
					),
					//custom eight
					'.has-gb-customeight-color' => array(
						'color' => esc_attr( $color_gb_eight ),
					),
					'.has-gb-customeight-background-color' => array(
						'background-color' => esc_attr( $color_gb_eight ),
					),
					//custom nine
					'.has-gb-customnine-color' => array(
						'color' => esc_attr( $color_gb_nine ),
					),
					'.has-gb-customnine-background-color' => array(
						'background-color' => esc_attr( $color_gb_nine ),
					),
					//custom ten
					'.has-gb-customten-color' => array(
						'color' => esc_attr( $color_gb_ten ),
					),
					'.has-gb-customten-background-color' => array(
						'background-color' => esc_attr( $color_gb_ten ),
					),
					//custom eleven
					'.has-gb-customeleven-color' => array(
						'color' => esc_attr( $color_gb_eleven ),
					),
					'.has-gb-customeleven-background-color' => array(
						'background-color' => esc_attr( $color_gb_eleven ),
					),
					//custom twelve
					'.has-gb-customtwelve-color' => array(
						'color' => esc_attr( $color_gb_twelve ),
					),
					'.has-gb-customtwelve-background-color' => array(
						'background-color' => esc_attr( $color_gb_twelve ),
					),
					//custom thirteen
					'.has-gb-customthirteen-color' => array(
						'color' => esc_attr( $color_gb_thirteen ),
					),
					'.has-gb-customthirteen-background-color' => array(
						'background-color' => esc_attr( $color_gb_thirteen ),
					),
				);
			}

			// sidebar colors css output
			$css_colors_sidebar = array(
				'aside#secondary' => array(
					'background-color' => $color_sb_bg ? esc_attr( $color_sb_bg ) : '',
					'color' => $color_sb_text ? esc_attr( $color_sb_text ) : '',
				),
				'aside#secondary .widget-title' => array(
					'color' => $color_sb_widget_titles ? esc_attr( $color_sb_widget_titles ) : '',
				),
				'aside#secondary a' => array(
					'color' => $color_sb_link ? esc_attr( $color_sb_link ) : '',
				),
				'aside#secondary a:hover' => array(
					'color' => $color_sb_link_hov ? esc_attr( $color_sb_link_hov ) : '',
				),
			);


			$css_output = array_merge( $css_colors_common, $css_common, $css_404, $css_colors_sidebar );
			/* Parse CSS from array() */
			$parse_css = gutenbooster_parse_css( $css_output );

			// Resposive desktop css output
			$desktop_css = array();
			$bodyfontsize_desktop = gutenbooster_get_resp_size( 'body_fontsize', 'desktop' );
				$font_h1_size_desktop = gutenbooster_get_resp_size( 'font_h1_size', 'desktop' );
				$font_h1_lh_desktop = gutenbooster_get_resp_size( 'font_h1_lh', 'desktop' );

				$font_h2_size_desktop = gutenbooster_get_resp_size( 'font_h2_size', 'desktop' );
				$font_h2_lh_desktop = gutenbooster_get_resp_size( 'font_h2_lh', 'desktop' );

				$font_h3_size_desktop = gutenbooster_get_resp_size( 'font_h3_size', 'desktop' );
				$font_h3_lh_desktop = gutenbooster_get_resp_size( 'font_h3_lh', 'desktop' );

				$font_h4_size_desktop = gutenbooster_get_resp_size( 'font_h4_size', 'desktop' );
				$font_h4_lh_desktop = gutenbooster_get_resp_size( 'font_h4_lh', 'desktop' );

			$sitewrap_border_desktop = gutenbooster_get_resp_size( 'sitewrap_border', 'desktop' );

			$desktop_css = array(
				// Typography
				'body, button, input, select, textarea' => array(
					'font-size' => $bodyfontsize_desktop ? esc_attr( $bodyfontsize_desktop . 'px' ) : '',
				),
				'h1' => array(
					'font-size' => $font_h1_size_desktop ? esc_attr( $font_h1_size_desktop . 'px' ) : '',
					'line-height' => $font_h1_lh_desktop ? esc_attr( $font_h1_lh_desktop ) : '',
				),
				'h2' => array(
					'font-size' => $font_h2_size_desktop ? esc_attr( $font_h2_size_desktop . 'px' ) : '',
					'line-height' => $font_h2_lh_desktop ? esc_attr( $font_h2_lh_desktop ) : '',
				),
				'h3' => array(
					'font-size' => $font_h3_size_desktop ? esc_attr( $font_h3_size_desktop . 'px' ) : '',
					'line-height' => $font_h3_lh_desktop ? esc_attr( $font_h3_lh_desktop ) : '',
				),
				'h4' => array(
					'font-size' => $font_h4_size_desktop ? esc_attr( $font_h4_size_desktop . 'px' ) : '',
					'line-height' => $font_h4_lh_desktop ? esc_attr( $font_h4_lh_desktop ) : '',
				),
				// Layout: Site Wrap
				'#site-wrap' => array(
					'padding' => $sitewrap_border_desktop ? esc_attr( $sitewrap_border_desktop . 'px' ) : '',
					'box-shadow' => $sitewrap_border_color && $sitewrap_border_desktop ?
									'inset 0 0 0 ' . esc_attr( $sitewrap_border_desktop ) . 'px ' . esc_attr( $sitewrap_border_color )
									: '',
				),
					//menu wrapper needs above values too
					'.header--stick-sticky:not(.jsnav-open) .menu-wrapper,
					.header--stick-headroom:not(.jsnav-open) .menu-wrapper' => array(
						'margin' => $sitewrap_border_color && $sitewrap_border_desktop ?
						esc_attr( $sitewrap_border_desktop . 'px ' ) . '0 0 ' . esc_attr( $sitewrap_border_desktop . 'px ' ) : '',
						'width' =>  $sitewrap_border_color && $sitewrap_border_desktop ? 'calc(100% - ' . esc_attr( $sitewrap_border_desktop * 2 . 'px ' ) . ')' : '',
					),
					//kioken blocks row layout fixes for above
					'.layout-no-sidebar .wp-block-kioken-rowlayout.bust-out-right' => array (
						'margin-right' => $sitewrap_border_color && $sitewrap_border_desktop ? esc_attr( $sitewrap_border_desktop * 2 . 'px' ) : '',
					),
					'.layout-no-sidebar .wp-block-kioken-rowlayout.bust-out-left' => array (
						'margin-left' => $sitewrap_border_color && $sitewrap_border_desktop ? esc_attr( $sitewrap_border_desktop * 2 . 'px' ) : '',
					),
					//fixed footer fixes for above
					'.fixed_footer .site-footer' => array (
						'left' => $sitewrap_border_color && $sitewrap_border_desktop ? esc_attr( $sitewrap_border_desktop . 'px' ) : '',
						'bottom' => $sitewrap_border_color && $sitewrap_border_desktop ? esc_attr( $sitewrap_border_desktop . 'px' ) : '',
						'width' =>  $sitewrap_border_color && $sitewrap_border_desktop ? 'calc(100% - ' . esc_attr( $sitewrap_border_desktop * 2 . 'px ' ) . ')' : '',
					),
			);
			$parse_css .= gutenbooster_parse_css( $desktop_css );

			// Resposive tablet css output
			$tablet_css = array();
			$bodyfontsize_tablet = gutenbooster_get_resp_size( 'body_fontsize', 'tablet' );
				$font_h1_size_tablet = gutenbooster_get_resp_size( 'font_h1_size', 'tablet' );
				$font_h1_lh_tablet = gutenbooster_get_resp_size( 'font_h1_lh', 'tablet' );

				$font_h2_size_tablet = gutenbooster_get_resp_size( 'font_h2_size', 'tablet' );
				$font_h2_lh_tablet = gutenbooster_get_resp_size( 'font_h2_lh', 'tablet' );

				$font_h3_size_tablet = gutenbooster_get_resp_size( 'font_h3_size', 'tablet' );
				$font_h3_lh_tablet = gutenbooster_get_resp_size( 'font_h3_lh', 'tablet' );

				$font_h4_size_tablet = gutenbooster_get_resp_size( 'font_h4_size', 'tablet' );
				$font_h4_lh_tablet = gutenbooster_get_resp_size( 'font_h4_lh', 'tablet' );

			$sitewrap_border_tablet = gutenbooster_get_resp_size( 'sitewrap_border', 'tablet' );
			$tablet_css = array(
				// Typography
				'body, button, input, select, textarea' => array(
					'font-size' => $bodyfontsize_tablet ? esc_attr( $bodyfontsize_tablet . 'px' ) : '',
				),
					'h1' => array(
						'font-size' => $font_h1_size_tablet ? esc_attr( $font_h1_size_tablet . 'px' ) : '',
						'line-height' => $font_h1_lh_tablet ? esc_attr( $font_h1_lh_tablet ) : '',
					),
					'h2' => array(
						'font-size' => $font_h2_size_tablet ? esc_attr( $font_h2_size_tablet . 'px' ) : '',
						'line-height' => $font_h2_lh_tablet ? esc_attr( $font_h2_lh_tablet ) : '',
					),
					'h3' => array(
						'font-size' => $font_h3_size_tablet ? esc_attr( $font_h3_size_tablet . 'px' ) : '',
						'line-height' => $font_h3_lh_tablet ? esc_attr( $font_h3_lh_tablet ) : '',
					),
					'h4' => array(
						'font-size' => $font_h4_size_tablet ? esc_attr( $font_h4_size_tablet . 'px' ) : '',
						'line-height' => $font_h4_lh_tablet ? esc_attr( $font_h4_lh_tablet ) : '',
					),
				// Layout: Site Wrap
				'#site-wrap' => array(
					'padding' => $sitewrap_border_tablet ? esc_attr( $sitewrap_border_tablet . 'px' ) : '',
					'box-shadow' => $sitewrap_border_color && $sitewrap_border_tablet ?
									'inset 0 0 0 ' . esc_attr( $sitewrap_border_tablet ) . 'px ' . esc_attr( $sitewrap_border_color )
									: '',
				),
					//menu wrapper needs above values too
					'.header--stick-sticky:not(.jsnav-open) .menu-wrapper,
					.header--stick-headroom:not(.jsnav-open) .menu-wrapper' => array(
						'margin' => $sitewrap_border_color && $sitewrap_border_tablet ?
						esc_attr( $sitewrap_border_tablet . 'px ' ) . '0 0 ' . esc_attr( $sitewrap_border_tablet . 'px ' ) : '',
						'width' =>  $sitewrap_border_color && $sitewrap_border_tablet ? 'calc(100% - ' . esc_attr( $sitewrap_border_tablet * 2 . 'px ' ) . ')' : '',
					),
					//kioken blocks row layout fixes for above
					'.layout-no-sidebar .wp-block-kioken-rowlayout.bust-out-right' => array (
						'margin-right' => $sitewrap_border_color && $sitewrap_border_tablet ? esc_attr( $sitewrap_border_tablet * 2 . 'px' ) : '',
					),
					'.layout-no-sidebar .wp-block-kioken-rowlayout.bust-out-left' => array (
						'margin-left' => $sitewrap_border_color && $sitewrap_border_tablet ? esc_attr( $sitewrap_border_tablet * 2 . 'px' ) : '',
					),
					//fixed footer fixes for above
					'.fixed_footer .site-footer' => array (
						'left' => $sitewrap_border_color && $sitewrap_border_tablet ? esc_attr( $sitewrap_border_tablet . 'px' ) : '',
						'bottom' => $sitewrap_border_color && $sitewrap_border_tablet ? esc_attr( $sitewrap_border_tablet . 'px' ) : '',
						'width' =>  $sitewrap_border_color && $sitewrap_border_tablet ? 'calc(100% - ' . esc_attr( $sitewrap_border_tablet * 2 . 'px ' ) . ')' : '',
					),
			);
			$parse_css .= gutenbooster_parse_css( $tablet_css, '', '768' );

			// Resposive mobile css output
			$mobile_css = array();
			$bodyfontsize_mobile = gutenbooster_get_resp_size( 'body_fontsize', 'mobile' );
				$font_h1_size_mobile = gutenbooster_get_resp_size( 'font_h1_size', 'mobile' );
				$font_h1_lh_mobile = gutenbooster_get_resp_size( 'font_h1_lh', 'mobile' );

				$font_h2_size_mobile = gutenbooster_get_resp_size( 'font_h2_size', 'mobile' );
				$font_h2_lh_mobile = gutenbooster_get_resp_size( 'font_h2_lh', 'mobile' );

				$font_h3_size_mobile = gutenbooster_get_resp_size( 'font_h3_size', 'mobile' );
				$font_h3_lh_mobile = gutenbooster_get_resp_size( 'font_h3_lh', 'mobile' );

				$font_h4_size_mobile = gutenbooster_get_resp_size( 'font_h4_size', 'mobile' );
				$font_h4_lh_mobile = gutenbooster_get_resp_size( 'font_h4_lh', 'mobile' );

			$mobile_css = array(
				// Typography
				'body, button, input, select, textarea' => array(
					'font-size' => $bodyfontsize_mobile ? esc_attr( $bodyfontsize_mobile . 'px' ) : '',
				),
					'h1' => array(
						'font-size' => $font_h1_size_mobile ? esc_attr( $font_h1_size_mobile . 'px' ) : '',
						'line-height' => $font_h1_lh_mobile ? esc_attr( $font_h1_lh_mobile ) : '',
					),
					'h2' => array(
						'font-size' => $font_h2_size_mobile ? esc_attr( $font_h2_size_mobile . 'px' ) : '',
						'line-height' => $font_h2_lh_mobile ? esc_attr( $font_h2_lh_mobile ) : '',
					),
					'h3' => array(
						'font-size' => $font_h3_size_mobile ? esc_attr( $font_h3_size_mobile . 'px' ) : '',
						'line-height' => $font_h3_lh_mobile ? esc_attr( $font_h3_lh_mobile ) : '',
					),
					'h4' => array(
						'font-size' => $font_h4_size_mobile ? esc_attr( $font_h4_size_mobile . 'px' ) : '',
						'line-height' => $font_h4_lh_mobile ? esc_attr( $font_h4_lh_mobile ) : '',
					),
			);
			$parse_css .= gutenbooster_parse_css( $mobile_css, '', '576' );


			$logo_css = array(
				// height
				'.menu-wrapper .site-logo img' => array(
					'max-height' => $logo_height ? esc_attr( $logo_height . 'px' ) : '',
				),
				//margin
				'.site-logo a' => array(
					'margin-top' => $logo_topmargin ? esc_attr( $logo_topmargin . 'px' ) : '',
					'margin-bottom' => $logo_bottommargin ? esc_attr( $logo_bottommargin . 'px' ) : '',
					'margin-left' => $logo_leftmargin ? esc_attr( $logo_leftmargin . 'px' ) : '',
					'margin-right' => $logo_rightmargin ? esc_attr( $logo_rightmargin . 'px' ) : '',
				),
			);

			$parse_css .= gutenbooster_parse_css( $logo_css );

			$menu_colors_css = array(
				// normal state
				'.menu-wrapper .nav-menu a,
				.menu-wrapper .site-logo a,
				.menu-wrapper .utils ul li a,
				.header--bg-transparent.header--txt-white .menu-wrapper.sticking ul.nav-menu > li > a,
				.header--bg-transparent.header--txt-white .menu-wrapper.sticking .utils a' => array(
					'color' => get_theme_mod('menu_link_color') ? get_theme_mod('menu_link_color') : ''
				),
				'.menu-wrapper .nav-menu .current-menu-item a:after' => array(
					'background-color' => get_theme_mod('menu_link_color') ? get_theme_mod('menu_link_color') : ''
				),
				// hover state
				'.menu-wrapper .nav-menu a:hover,
				.menu-wrapper .utils ul li a:hover,
				.menu-wrapper .site-logo a:hover,
				.header--bg-transparent.header--txt-white .menu-wrapper.sticking ul.nav-menu > li > a:hover,
				.header--bg-transparent.header--txt-white .menu-wrapper.sticking .utils a:hover' => array(
					'color' => get_theme_mod('menu_link_hover_color') ? get_theme_mod('menu_link_hover_color') : ''
				),
				'.menu-wrapper .nav-menu a:after,
				.site-nav-top .nav-menu > li:not(.mega-parent) ul.sub-menu li a:hover,
				.header--v2 .menu-wrapper .main-nav > ul > li.menu-item a:before' => array(
					'background-color' => get_theme_mod('menu_link_hover_color') ? get_theme_mod('menu_link_hover_color') : ''
				),

			);
			$parse_css .= gutenbooster_parse_css( $menu_colors_css );


			$dynamic_css = $parse_css;

			// trim white space for faster page loading.
			$dynamic_css = GutenBooster_Enqueue_Scripts::trim_css( $dynamic_css );

			return apply_filters( 'gutenbooster_theme_dynamic_css', $dynamic_css );

		}

		/**
		 * Return post meta CSS
		 *
		 * @param  boolean $return_css Return the CSS.
		 * @return mixed   Return and print the inline CSS.
		 */
		public static function return_meta_output( $return_css = false ) {

			$meta_style = array();
			$content_style = array();
			$page_banner_meta_style = array();


			$content_toppad = gutenbooster_postmeta( 'content_toppad', true );

			$pbh_postmeta = gutenbooster_postmeta( 'page_banner_height', true );
			$pbgc_postmeta = gutenbooster_postmeta( 'page_banner_bgcolor', true );
			$pbg_opacity_postmeta = gutenbooster_postmeta( 'page_bannerbg_opacity', true );


			$content_style = array(
				'#content' => array(
					'padding-top' => $content_toppad ? esc_attr( $content_toppad . ' !important' ) : ''
				),
			);

			if ( is_singular() && gutenbooster_postmeta( 'override_page_banner_settings', true ) ) {
				$page_banner_meta_style = array(
					'#page-banner-wrap' => array(
						'height' => $pbh_postmeta ? esc_attr( $pbh_postmeta . 'vh' ) : '',
						'background-color' => $pbgc_postmeta ? esc_attr( $pbgc_postmeta  ) : '',
					),
					'#page-banner-wrap .kontain[data-kiomotion="true"].animated-in .page-banner-bg,
					#page-banner-wrap .page-banner-bg' => array(
						'opacity' => $pbg_opacity_postmeta ? esc_attr( $pbg_opacity_postmeta ) : '',
					),
				);
			}

			// WooCommerce only
			if ( gutenbooster_is_shop_home() && gutenbooster_shop_page_id() && get_post_meta( gutenbooster_shop_page_id(), 'override_page_banner_settings', true ) ) {

				$pbh_postmeta = get_post_meta( gutenbooster_shop_page_id(), 'page_banner_height', true );
				$pbgc_postmeta = get_post_meta( gutenbooster_shop_page_id(), 'page_banner_bgcolor', true );
				$pbg_opacity_postmeta = gutenbooster_postmeta( 'page_bannerbg_opacity', true );

				$page_banner_meta_style = array(
					'#page-banner-wrap' => array(
						'height' => $pbh_postmeta ? esc_attr( $pbh_postmeta . 'vh' ) : '',
						'background-color' => $pbgc_postmeta ? esc_attr( $pbgc_postmeta  ) : '',
					),
					'#page-banner-wrap .kontain[data-kiomotion="true"].animated-in .page-banner-bg,
					#page-banner-wrap .page-banner-bg' => array(
						'opacity' => $pbg_opacity_postmeta ? esc_attr( $pbg_opacity_postmeta ) : '',
					),
				);
			}



			$meta_style = array_merge( $content_style, $page_banner_meta_style );



			$parse_css = gutenbooster_parse_css( $meta_style );

			$dynamic_css = $parse_css;
			// trim white space for faster page loading.
			$dynamic_css = GutenBooster_Enqueue_Scripts::trim_css( $dynamic_css );

			if ( false != $return_css ) {
				return $dynamic_css;
			}

			$dynamic_css = $parse_css;



			wp_add_inline_style( 'gutenbooster-theme-css', $dynamic_css );
		}



	}



}
