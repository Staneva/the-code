<?php
/**
 * Gutenberg Editor CSS
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package     GutenBooster
 * @author      GutenBooster
 * @copyright   Copyright (c) 2019, GutenBooster
 * @link        http://kiokengutenberg.com/
 * @since       GutenBooster 1.0
 */

if ( ! class_exists( 'Gutenberg_Editor_CSS' ) ) :

    /**
	 * Admin Helper
	 */
	class Gutenberg_Editor_CSS {
        /**
		 * Get dynamic CSS  required for the block editor to make editing experience similar to how it looks on frontend.
		 *
		 * @return String CSS to be loaded in the editor interface.
		 */
		public static function get_css() {
            global $pagenow;

            $background_color = get_background_color();

            // Color Variables
            $override_colors  = gutenbooster_getmod( 'override_colors' );

			$color_primary    = get_theme_mod( 'color_gb_primary' );
			$color_secondary  = get_theme_mod( 'color_gb_secondary' );
			$color_tertiary   = get_theme_mod( 'color_gb_tertiary' );

            // Layout Variables
			$layout_width = gutenbooster_getmod( 'layout_content_width' );
			$units_topbottom_margin = gutenbooster_getmod( 'units_topbottom_margin' );

            // Typography Variables
			$body_font_family = gutenbooster_get_font_data( 'typo_body' );
			$body_font_weight = gutenbooster_get_font_data( 'typo_body', 'fontweight' );
			$body_line_height = get_theme_mod( 'typobody_lh' );
			$body_fontsize = gutenbooster_get_resp_size( 'body_fontsize', 'desktop' );

				$h1_fontsize = gutenbooster_get_resp_size( 'font_h1_size', 'desktop' );
				$h1_lh = gutenbooster_get_resp_size( 'font_h1_lh', 'desktop' );

				$h2_fontsize = gutenbooster_get_resp_size( 'font_h2_size', 'desktop' );
				$h2_lh = gutenbooster_get_resp_size( 'font_h2_lh', 'desktop' );

				$h3_fontsize = gutenbooster_get_resp_size( 'font_h3_size', 'desktop' );
				$h3_lh = gutenbooster_get_resp_size( 'font_h3_lh', 'desktop' );

				$h4_fontsize = gutenbooster_get_resp_size( 'font_h3_size', 'desktop' );
				$h4_lh = gutenbooster_get_resp_size( 'font_h3_lh', 'desktop' );

				$h4_fontsize = gutenbooster_get_resp_size( 'font_h4_size', 'desktop' );
				$h4_lh = gutenbooster_get_resp_size( 'font_h4_lh', 'desktop' );

			$headings_font_family = gutenbooster_get_font_data( 'typo_headings' );
			$headings_font_weight = gutenbooster_get_font_data( 'typo_headings', 'fontweight' );

            $css = '';

            /*=============================================>>>>>
            = Common =
            ===============================================>>>>>*/
            $common_css = array(
                '#wpwrap .edit-post-visual-editor' => array(
                    'background-color' => esc_attr( '#' . $background_color ),
                ),
                '[data-block],
                .editor-writing-flow > div > div > div > .editor-block-list__layout > .editor-block-list__block > .editor-block-list__block-edit > [data-block]' => array(
                    'margin-top' => esc_attr( $units_topbottom_margin . 'px' ),
                    'margin-bottom' => esc_attr( $units_topbottom_margin . 'px' ),
                ),
                // Typography
				'.editor-block-list__block p, .editor-block-list__block, .wp-block[data-type="core/quote"] p' => array(
					'font-family' => esc_attr( $body_font_family ),
					'font-weight' => esc_attr( $body_font_weight ),
					'line-height' => esc_attr( $body_line_height ),
					'font-size' => $body_fontsize ? esc_attr( $body_fontsize . 'px' ) : '',
				),
				'.edit-post-visual-editor .editor-post-title__block textarea.editor-post-title__input,
				.editor-styles-wrapper [data-block] h1, .editor-styles-wrapper [data-block] h2,
				.editor-styles-wrapper [data-block] h3, .editor-styles-wrapper [data-block] h4,
				.editor-styles-wrapper [data-block] h5, .editor-styles-wrapper [data-block] h6' => array(
					'font-family' => esc_attr( $headings_font_family ),
					'font-weight' => esc_attr( $headings_font_weight ),
				),
				//headings
				'.edit-post-visual-editor .editor-post-title__block textarea.editor-post-title__input,
				.editor-styles-wrapper [data-block] h1' => array(
					'font-size' => $h1_fontsize ? esc_attr( $h1_fontsize . 'px' ) : '',
					'line-height' => $h1_lh ? esc_attr( $h1_lh ) : '',
				),
				'.editor-styles-wrapper [data-block] h2' => array(
					'font-size' => $h2_fontsize ? esc_attr( $h2_fontsize . 'px' ) : '',
					'line-height' => $h2_lh ? esc_attr( $h2_lh ) : '',
				),
				'.editor-styles-wrapper [data-block] h3' => array(
					'font-size' => $h3_fontsize ? esc_attr( $h3_fontsize . 'px' ) : '',
					'line-height' => $h3_lh ? esc_attr( $h3_lh ) : '',
				),
				'.editor-styles-wrapper [data-block] h4' => array(
					'font-size' => $h4_fontsize ? esc_attr( $h4_fontsize . 'px' ) : '',
					'line-height' => $h4_lh ? esc_attr( $h4_lh ) : '',
				),
            );

            $css .= gutenbooster_parse_css( $common_css );


            /*=============================================>>>>>
            = Colors =
            ===============================================>>>>>*/

            $colors_css = array();

            if ( $override_colors ) {
                $colors_css = array(
                    'a, .edit-post-visual-editor a' => array(
                        'color' => esc_attr( $color_primary )
                    ),
					'.editor-post-title__block .editor-post-title__input,
					.editor-post-title__block .editor-post-title__input:focus,
					.editor-styles-wrapper [data-block] h1, .editor-styles-wrapper [data-block] h2, .editor-styles-wrapper [data-block] h3, .editor-styles-wrapper [data-block] h4, .editor-styles-wrapper [data-block] h5, .editor-styles-wrapper [data-block] h6' => array (
						'color' => esc_attr( $color_secondary )
					),
                    '.editor-block-list__block' => array(
                        'color' => esc_attr( $color_tertiary )
                    ),
                );
            }


            $css .= gutenbooster_parse_css( $colors_css );

            /*=============================================>>>>>
            = No Sidebar Layout Content Width =
            ===============================================>>>>>*/

            $layout_width_css = array();

            $layout_width_css = array(
                '.wp-block,
                body.block-editor-page.kt-editor-width-default .wp-block,
                .edit-post-visual-editor .editor-block-list__block' => array(
                    'max-width' => esc_attr( $layout_width . 'px' ),
                ),
            );

            $css .= gutenbooster_parse_css( $layout_width_css, '768' );



            return $css;

        }

    }

endif;