<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package GutenBooster
 */


 /**
  * Adds custom classes to the array of body classes for browser types.
  *
  * @param array $classes Classes for the body element.
  * @return array
  */
if ( ! function_exists( 'gutenbooster_browser_body_classes' ) ) :

	function gutenbooster_browser_body_classes( $classes ) {
		// the list of WordPress global browser checks
		// https://codex.wordpress.org/Global_Variables#Browser_Detection_Booleans
		$browsers = [ 'is_iphone', 'is_chrome', 'is_safari', 'is_NS4', 'is_opera', 'is_gecko', 'is_IE', 'is_edge' ];
		// check the globals to see if the browser is in there and return a string with the match
		$classes[] = join(
			' ',
			array_filter(
				$browsers,
				function ( $browser ) {
					return $GLOBALS[ $browser ];
				}
			)
		);
		return $classes;
	}
	// call the filter for the body class
	add_filter( 'body_class', 'gutenbooster_browser_body_classes' );
endif;

/**
* Adds custom classes to the array of body classes.
*
* @param array $classes Classes for the body element.
* @return array
*/
function gutenbooster_body_classes( $classes ) {

	$override_header = get_post_meta( get_queried_object_id(), 'override_header', true );

	//add theme class
	$classes[] = 'theme-gutenbooster';

	// Adds a class of layout
	$pagepostlayout = gutenbooster_postmeta( 'custom_layout', true );
	if ( $pagepostlayout ) {
		$classes[] = 'layout-' . gutenbooster_postmeta( 'layout', true );
	} else {
		$classes[] = 'layout-' . gutenbooster_get_layout();
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Add a class of blog layout
	if ( is_search() ) {
		$classes[] = 'blog-classic';
	} else {
		if ( ! is_singular() ) {
			$classes[] = 'blog-' . gutenbooster_getmod( 'blog_layout' );
		}
	}

	if ( gutenbooster_is_woocommerce_on() && gutenbooster_has_ajax_preloader() ) {
		$classes[] = 'barbanav-update-cart-count';
	}

	$classes[] = gutenbooster_has_ajax_preloader() ? 'kt-preloader-on' : '';
	/*-------------------------------------------------------------------------*/
	/*	Header Classes
	/*-------------------------------------------------------------------------*/

	// Adds a class of header layout
	$classes[] = 'header--' . gutenbooster_getmod( 'header_layout' );

	if ( gutenbooster_is_woocommerce_on() && ( is_product() || is_cart() || is_account_page() || is_checkout() ) ) {
		$classes[] = '';
	} else {
		if ( is_singular() && $override_header ) {
			// Adds a class of header layout
			// Adds a class of header container
			$classes[] = 'header--wrap-' . gutenbooster_postmeta( 'header_wrapper', true );

			$classes[] = 'header--bg-' . gutenbooster_postmeta( 'headerbg', true );
			// Header Text Color
			// if (get_post_meta( get_queried_object_id(), 'header_txtcolor', true )=='white' && get_post_meta( get_queried_object_id(), 'headerbg', true )==='transparent') {
			$classes[] = gutenbooster_postmeta( 'headerbg', true ) === 'transparent' ? 'header--txt-' . gutenbooster_postmeta( 'header_txtcolor', true ) : '';
			// }
		} else {
			// Adds a class of header container
			$classes[] = 'header--wrap-' . gutenbooster_getmod( 'header_wrapper' );

			// Header BG Color
			$classes[] = 'header--bg-' . gutenbooster_getmod( 'header_bg' );
			// Header Text Color
			$classes[] = gutenbooster_getmod( 'header_bg' ) == 'transparent' ? 'header--txt-' . gutenbooster_getmod( 'header_txt_color' ) : '';
		}
	}

	// Adds a class for header standard sticky
	$classes[] = gutenbooster_getmod( 'header_sticky' ) != 'none' ? 'header--stick-' . gutenbooster_getmod( 'header_sticky' ) : '';

	$classes[] = gutenbooster_getmod( 'shrink' ) == true && gutenbooster_getmod( 'header_sticky' ) != 'none' ? 'header--shrinks' : '';
	$classes[] = gutenbooster_getmod( 'shrink' ) == true && gutenbooster_getmod( 'shrink_txt_color' ) == 'white' ? 'header--shrink-text-white' : '';

	/*-------------------------------------------------------------------------*/
	/*	Page Banner Classes
	/*-------------------------------------------------------------------------*/
	// Page Banner Check. If exists, check text color of the page banner
	if ( gutenbooster_has_page_banner() ) {

		if ( is_singular() && get_post_meta( get_queried_object_id(), 'override_page_banner_settings', true ) ) {
			$pb_txt_color = get_post_meta( get_queried_object_id(), 'page_banner_text_color', true );

		} else {
			$pb_txt_color = gutenbooster_getmod( 'page_banner_text_color' );
		}

		$classes[] = ( 'white' == $pb_txt_color ) ? 'pageban-textcolor-white' : 'pageban-textcolor-default';
	} else {
		if ( is_singular() && has_post_thumbnail() && get_theme_mod( 'meta_title_pos' ) == 'within' && get_theme_mod( 'show_post_thumb' ) == true ) {
			$classes[] = 'postmeta-within-image';
			$classes[] = get_theme_mod( 'show_post_thumb' ) ? '' : 'post-thumb-hidden';
		}
	}

	// don't add no-page-banner class if if the page template is Blank
	// since it overrides the header color settings
	if ( ! is_page_template( 'page-home.php' ) ) {
		$classes[] = gutenbooster_has_page_banner() ? 'has-page-banner' : 'no-page-banner';
		$classes[] = gutenbooster_pagebanner_has_image() ? 'pb-has-img' : '';
	}

	$classes[] = get_theme_mod( 'fixed_footer' ) ? 'fixed_footer' : '';

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'has-empty-sidebar';
	}

	$classes[] = get_theme_mod( 'alignments_bustout' ) && get_theme_mod( 'alignments_bustout' ) !== 'none' ? 'alignments-bustout' : 'alignments-no-bustout';

	return $classes;
}
add_filter( 'body_class', 'gutenbooster_body_classes' );

function gutenbooster_body_atts() {
	$atts;

	$atts[] = gutenbooster_getmod( 'layout_content_width' ) ? ' data-content-width=' . gutenbooster_getmod( 'layout_content_width' ) : '';
	$atts[] = get_theme_mod( 'move_wrapper_down' ) ? ' data-move-wrapper-down=true' : '';

	foreach ( $atts as $attribute ) {
		echo esc_html( $attribute );
	}

}


/**
* Open content container
*/
function gutenbooster_open_content_container() {
	$class = 'container';
	$no_gutters = '';

	if (
		( function_exists( 'WC' ) && ( is_shop() && gutenbooster_getmod( 'layout_shop' ) == 'fullwidth' ) ) ||
			is_page_template( 'templates/home.php' ) ||
		( ( is_post_type_archive( 'portfolio' ) || is_tax( 'portfolio_type' ) ) && 'fullwidth' == get_theme_mod( 'portfolio_style' ) )
		|| ( is_singular( 'post' ) && gutenbooster_getmod( 'layout_post' ) == 'fullwidth' )
		|| ( is_singular( 'page' ) && gutenbooster_getmod( 'layout_page' ) == 'fullwidth' )
	) {
		$class = 'container-fluid';
		$no_gutters = 'no-gutters';
	}

	$class = apply_filters( 'gutenbooster_site_content_container_class', $class );
	// print the container class
	echo '<div id="primary" class="' . esc_attr( $class ) . '">
		<div class="row ' . esc_attr( $no_gutters ) . '">';
}

 add_action( 'gutenbooster_before_content', 'gutenbooster_open_content_container', 5 );


 /**
 * Close content container
 */
function gutenbooster_close_content_container() {
	echo '	</div>
				<!-- close .row -->
			</div>
			<!-- close #primary container -->';
}

add_action( 'gutenbooster_after_content', 'gutenbooster_close_content_container', 50 );


/**
* Display top five categories on blog page
*/
function gutenbooster_blog_category_list() {
	if ( ! is_home() ) {
		return;
	}

	if ( ! get_theme_mod( 'blog_categories' ) ) {
		return;
	}

	$cats = get_terms(
		array(
			'taxonomy' => 'category',
			'number'   => 4,
			'orderby'  => 'count',
			'order'    => 'DESC',
		)
	);

	if ( 'posts' == get_option( 'show_on_front' ) ) {
		$blog_url = home_url();
	} else {
		$blog_url = get_page_link( get_option( 'page_for_posts' ) );
	}

	$link_class = 'underlined';

	if ( 'classic' == get_theme_mod( 'blog_layout' ) && 'minimal' != get_theme_mod( 'page_header_style' ) ) {
		$link_class .= ' line-white';
	}
	?>

	<div class="blog-cats-row d-none d-lg-block col-12">
		<ul class="cat-list">
			<li>
				<a href="<?php echo esc_url( $blog_url ); ?>" class="<?php echo esc_attr( $link_class ); ?> active"><?php esc_html_e( 'All Blog Posts', 'gutenbooster' ); ?></a>
			</li>
			<?php
			foreach ( $cats as $cat ) {
				printf(
					'<li><a href="%s" class="%s">%s</a></li>',
					esc_url( get_term_link( $cat, 'category' ) ),
					esc_attr( $link_class ),
					esc_html( $cat->name )
				);
			}
			?>
		</ul>
	</div>

	<?php
}

 add_action( 'gutenbooster_before_content', 'gutenbooster_blog_category_list', 20 );

function gutenbooster_nobanner_pagetitle( $section ) {

	$show_page_title = gutenbooster_postmeta( 'show_pagetitle', true );
	$pagetitle_alignment = $show_page_title && gutenbooster_postmeta( 'page_title_alignment', true ) !== 'left' ? 'center' : 'left';



	if ( 'show' == $show_page_title && ! gutenbooster_has_page_banner() ) {
		if ( 'blog' !== $section ) {
			the_title( '<header class="title-heading p-4 text-' . $pagetitle_alignment . '"><h1 class="nopb-page-title">', '</h1></header>' );
		}
	} elseif ( 'show' == $show_page_title && ! gutenbooster_has_page_banner() ) {
		if ( 'blog' !== $section ) {
			the_title( '<header class="title-heading p-4 text-center"><h1 class="nopb-page-title">', '</h1></header>' );
		}
	}
}

function gutenbooster_get_font_data( $name, $value='family' ) {
	$font = gutenbooster_getmod( $name, $name );
	$font = ! is_array( $font ) ? json_decode( $font, true ) : $font;

	$output;

	switch ( $value ) {
		case "family":
		$output = $font['font'];
		break;

		case "variants":
		$output = isset( $font[ 'variantlist' ] ) && is_array( $font[ 'variantlist' ] ) ? implode( ',', $font[ 'variantlist' ] ) : '';
		break;

		case "fontweight":
		$weight = $font['fontweight'];
		$output = 'regular' == $weight ? "normal" : $weight;
		break;
	}


	return $output;
}



