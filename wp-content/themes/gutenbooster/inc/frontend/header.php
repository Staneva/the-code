<?php

/**
 * Filter to archive title and add page title for singular pages
 * and woocommerce shop related pages
 * @param string $title
 *
 * @return string
 */
function gutenbooster_the_archive_title( $title ) {
	if ( gutenbooster_is_shop_home() ) {
		$title = get_the_title( get_option( 'woocommerce_shop_page_id' ) );
	} elseif ( function_exists( 'is_checkout' ) && is_checkout() ) {
		$title = get_the_title( get_option( 'woocommerce_checkout_page_id' ) );
	} elseif ( function_exists( 'is_cart' ) && is_cart() ) {
		$title = get_the_title( get_option( 'woocommerce_cart_page_id' ) );
	} elseif ( function_exists( 'yith_wcwl_is_wishlist_page' ) && yith_wcwl_is_wishlist_page() ) {
		$title = get_the_title( get_option( 'yith_wcwl_wishlist_page_id' ) );
	} elseif ( function_exists( 'is_account_page' ) && is_account_page() ) {
		$title = get_the_title( get_option( 'woocommerce_myaccount_page_id' ) );
	} elseif ( is_category() || is_tag() || is_tax() ) {
		$title = single_term_title( '', false );
	} elseif ( is_home() ) {
		$title = 'page' == get_option( 'show_on_front' ) ? get_the_title( get_option( 'page_for_posts' ) ) : esc_html__( 'Blog', 'gutenbooster' );
	} elseif ( is_search() ) {
		$title = esc_html__( 'Search', 'gutenbooster' );
	}

	return $title;
}

add_filter( 'get_the_archive_title', 'gutenbooster_the_archive_title' );



/**
 * Display Site Header
 */
function gutenbooster_header_retrieve() {

	$the_layout = get_theme_mod( 'header_layout' ) ? get_theme_mod( 'header_layout' ) : 'v3';

	get_template_part( 'template-parts/headers/header', $the_layout );

}
add_action( 'gutenbooster_header', 'gutenbooster_header_retrieve' );

/**
 * Display Additional Menu Elements for Header Except V2
 */
if ( ! function_exists( 'gutenbooster_header_icons' ) ) :

	function gutenbooster_header_icons( $position = 'left', $version = 'v1' ) {

		$icons = get_theme_mod( 'header_icons_' . $position . '_' . $version );


		$html = '';

		if ( empty( $icons ) ) {
			return;
		}

		foreach ( (array) $icons as $icon ) {
			switch ( $icon ) {
				case 'cart':
					if ( ! function_exists( 'WC' ) ) {
						break;
					}
					$html .= '<li class="menu-item menu-item-cart top-cart-icon">
						<a href="' . esc_url( wc_get_cart_url() ) . '" class="cart-contents" data-toggle="" data-target="cart-modal" data-tab="cart">
							<span class="count cart-counter cart-count">' . intval( WC()->cart->get_cart_contents_count() ) . '</span>
							' . gutenbooster_icon_url( 'shopping-cart' ) . '
						</a>
					</li>';
					break;

				case 'user':
					if ( ! function_exists( 'WC' ) ) {
						break;
					}
					$html .= '
						<li class="menu-item menu-item-account">
							<a href="' . esc_url( wc_get_account_endpoint_url( 'dashboard' ) ) . '" data-toggle="" data-target="login-modal">' . gutenbooster_icon_url( 'user' ) . '</a>
						</li>';
					break;

				case 'search':
					$html .= '<li class="menu-item menu-item-search"><a href="#" data-toggle="modal" data-target="search-modal">
					' . gutenbooster_icon_url( $icon ) . '
					</a></li>';
					break;

				case 'menu':
					$html .= '<li class="menu-item menu-item-jsnav"><a href="javascript:;">
					' . gutenbooster_icon_url( $icon ) . '
					</a></li>';
					break;

				case 'divider':
					$html .= '<li class="menu-item menu-item-divider"><a>' . gutenbooster_icon_url( 'divider' ) . '</a></li>';
					break;
				case 'divider2':
					$html .= '<li class="menu-item menu-item-divider"><a>' . gutenbooster_icon_url( 'divider' ) . '</a></li>';
					break;
				// case 'divider3':
				// 	echo '<li class="menu-item menu-item-divider"><a>'.gutenbooster_icon_url('divider').'</a></li>';
				// 	break;

				default:
					$html .= '<li class="menu-item"><a href="' . get_theme_mod( $icon ) . '">' . gutenbooster_icon_url( $icon ) . '</a></li>';
					break;
			}
		}

		return $html;

	}

endif;

/**
 * Display Additional Menu Elements for Header V2
 */
function gutenbooster_add_headerv2_icons_right( $items, $args ) {
	$header_layout = '';
	$header_layout = get_theme_mod( 'header_layout' );
	if ( 'gbb-secondary' == $args->theme_location && 'v2' == $header_layout ) {
		$icons = get_theme_mod( 'header_icons_right_v2' );

		foreach ( (array) $icons as $icon ) {
			switch ( $icon ) {
				case 'cart':
					if ( ! function_exists( 'WC' ) ) {
						break;
					}
					$items .= sprintf(
						'<li class="menu-item menu-item-icon menu-item-cart top-cart-icon">
							<a href="%s" class="cart-contents" data-toggle="" data-target="cart-modal" data-tab="cart">

								<span><span class="count cart-counter cart-count">%s</span>%s</span>
							</a>
						</li>',
						esc_url( wc_get_cart_url() ),
						intval( WC()->cart->get_cart_contents_count() ),
						gutenbooster_icon_url( 'shopping-bag' )
					);
					break;

				case 'user':
					if ( ! function_exists( 'WC' ) ) {
						break;
					}
					$items .= sprintf(
						'<li class="menu-item menu-item-icon menu-item-account"><a href="%s" data-toggle="" data-target="login-modal"><span>' . gutenbooster_icon_url( 'user' ) . '</span></a></li>',
						esc_url( wc_get_account_endpoint_url( 'dashboard' ) )
					);
					break;

				case 'menu':
					$items .= '<li class="menu-item menu-item-jsnav"><a href="javascript:;"><span>
					' . gutenbooster_icon_url( $icon ) . '
					</span></a></li>';
					break;

				case 'search':
					$items .= '<li class="menu-item menu-item-icon menu-item-search"><a href="#" data-toggle="modal" data-target="search-modal">
					<span>' . gutenbooster_icon_url( 'search' ) . '</span>
					</a></li>';
					break;

				default:
					$items .= '<li class="menu-item menu-item-icon"><a href="' . get_theme_mod( $icon ) . '"><span>' . gutenbooster_icon_url( $icon ) . '</span></a></li>';
					break;
			}
		}
	}
	return $items;

}
add_filter( 'wp_nav_menu_items', 'gutenbooster_add_headerv2_icons_right', 20, 2 );

function gutenbooster_add_headerv2_icons_left( $items, $args ) {
	$header_layout = '';
	$items_icons   = '';
	$header_layout = get_theme_mod( 'header_layout' );
	if ( 'gbb-primary' == $args->theme_location && 'v2' == $header_layout ) {

		$icons = get_theme_mod( 'header_icons_left_v2' );

		foreach ( (array) $icons as $icon ) {
			switch ( $icon ) {
				case 'cart':
					if ( ! function_exists( 'WC' ) ) {
						break;
					}
					$items_icons .= sprintf(
						'<li class="menu-item menu-item-icon menu-item-cart top-cart-icon">
							<a href="%s" class="cart-contents" data-toggle="" data-target="cart-modal" data-tab="cart">

								<span><span class="count cart-counter cart-count">%s</span>%s</span>
							</a>
						</li>',
						esc_url( wc_get_cart_url() ),
						intval( WC()->cart->get_cart_contents_count() ),
						gutenbooster_icon_url( 'shopping-bag' )
					);
					break;

				case 'user':
					if ( ! function_exists( 'WC' ) ) {
						break;
					}
					$items_icons .= sprintf(
						'<li class="menu-item menu-item-icon menu-item-account"><a href="%s" data-toggle="" data-target="login-modal"><span>' . gutenbooster_icon_url( 'user' ) . '</span></a></li>',
						esc_url( wc_get_account_endpoint_url( 'dashboard' ) )
					);
					break;

				case 'menu':
					$items_icons .= '<li class="menu-item menu-item-jsnav inv2"><a href="javascript:;"><span>
					' . gutenbooster_icon_url( $icon ) . '
					</span></a></li>';
					break;

				case 'search':
					$items_icons .= '<li class="menu-item menu-item-icon menu-item-search"><a href="#" data-toggle="modal" data-target="search-modal">
					<span>' . gutenbooster_icon_url( 'search' ) . '</span>
					</a></li>';
					break;

				default:
					$items_icons .= '<li class="menu-item menu-item-icon"><a href="' . get_theme_mod( $icon ) . '"><span>' . gutenbooster_icon_url( $icon ) . '</span></a></li>';
					break;
			}
		}
	}
	return $items_icons . $items;

	// return $items .= gutenbooster_header_icons_v2('right');
	 // return $items;
}
add_filter( 'wp_nav_menu_items', 'gutenbooster_add_headerv2_icons_left', 10, 2 );

/**
 * Site Header Classes
 */
function gutenbooster_header_class() {

	$hw_class = 'container';

	echo esc_html( $hw_class );
}



/**
* Site Header Menus
*/
function gutenbooster_header_menus( $selective_menu = 'gbb-primary' ) {

	global $post;

	if ( get_theme_mod( 'header_layout' ) == 'v2' ) {

		if ( has_nav_menu( $selective_menu ) ) {
			wp_nav_menu( array(
				'container' => false,
				'fallback_cb' => false,
				'menu_class' => 'nav-menu nav nav-fill',
				'link_before' => '<span>',
				'link_after' => '</span>',
				'theme_location' 	=> $selective_menu,
			) );
		} else {
			?>
			<ul class="nav-menu">
				<?php
				wp_list_pages( array(
					'match_menu_classes' 	=> true,
					'title_li' 				=> false,
				) );
				?>
			</ul>
			<?php
		}

	} else {

		if ( has_nav_menu( $selective_menu ) ) {
			wp_nav_menu( array(
				'theme_location' => $selective_menu,
				'container' => false,
				'fallback_cb' => false,
				'menu_class' => 'nav-menu',
			) );
		} else {
			?>
			<ul class="nav-menu">
				<?php
				wp_list_pages( array(
					'match_menu_classes' 	=> true,
					'title_li' 				=> '',
				) );
				?>
			</ul>
			<?php
		}

	}

}

/**
 * Display Page Banner
 */
function gutenbooster_page_banner() {
	if ( ! gutenbooster_has_page_banner() ) {
		return;
	}

	get_template_part( 'template-parts/pagebanner' );
}
add_action( 'gutenbooster_after_header', 'gutenbooster_page_banner', 20 );


/**
 * Theme Default Fonts Load
 */
function gutenbooster_default_fonts() {
	$fonts = array(
		'Poppins:600,600i,700,700i,800,800i,900,900i',
		'Roboto:100,100i,300,300i,500,500i,700,700i',
	);

	$fonts_collection = add_query_arg(
		array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		),
		'https://fonts.googleapis.com/css'
	);

	return $fonts_collection;
}

/**
 * WP 5.2 wp_body_open() support
 */
if ( ! function_exists( 'gutenbooster_wp_body_open' ) ) {
	function gutenbooster_wp_body_open() {
		if ( function_exists( 'wp_body_open' ) ) {
			wp_body_open();
		}
	}
}
