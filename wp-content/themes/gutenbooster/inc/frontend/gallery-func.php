<?php
if ( ! function_exists( 'gutenbooster_format_gallery_slider' ) ) :

	function gutenbooster_get_gallery_image_ids( $post ) {
		$post = get_post( $post );
		if ( ! $post ) {
			return array();
		}
		if ( ! has_shortcode( $post->post_content, 'gallery' ) ) {
			return array();
		}
		$images_ids = array();
		if ( preg_match_all( '/' . get_shortcode_regex() . '/s', $post->post_content, $matches, PREG_SET_ORDER ) ) {
			foreach ( $matches as $shortcode ) {
				if ( 'gallery' === $shortcode[2] ) {
					$data = shortcode_parse_atts( $shortcode[3] );
					if ( ! empty( $data['ids'] ) ) {
						$images_ids = explode( ',', $data['ids'] );
					}
				}
			}
		}
		return $images_ids;
	}
endif;

if ( ! function_exists( 'gutenbooster_format_gallery_slider' ) ) :
	/**
	* Display an optional post read more link
	*
	*/
	function gutenbooster_format_gallery_slider() {

		$image_ids = gutenbooster_get_gallery_image_ids( get_the_ID() );
		if ( $image_ids ) { ?>
			<!-- Slider main container -->
			<div class="swiper-container format-gallery-slider" id="gallery-for-<?php echo esc_attr( get_the_ID() ); ?>">
				<!-- Additional required wrapper -->
				<div class="swiper-wrapper">
				<?php foreach ( $image_ids as $image_id ) : ?>
					<div class="swiper-slide"><?php echo wp_get_attachment_image( $image_id, 'large', array() ); ?></div>
				<?php endforeach; ?>
				</div><!-- end .swiper-wrapper -->
				<!-- If we need pagination -->
				<div class="swiper-pagination"></div>
				<!-- If we need navigation buttons -->
				<span class="swiper-button-prev">
				<svg width="28" height="16" viewBox="0 0 28 16" xmlns="http://www.w3.org/2000/svg"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-664.000000, -1899.000000)" fill="currentcolor"><g transform="translate(63.000000, 1479.000000)"><g transform="translate(601.000000, 420.000000)"><path d="M19.4374537.250047088C19.6041202.0833805005 19.7812028 476837158e-13 19.9687024 476837158e-13 20.1562019 476837158e-13 20.3332845.0833805005 20.4999511.250047088L27.7499338 7.5000298C27.9166004 7.6250295 27.9999332 7.79169609 27.9999332 8.00002861 27.9999332 8.20836113 27.9166004 8.37502772 27.7499338 8.50002742L20.4999511 15.7500101C20.3332845 15.9166767 20.1562019 16.0000095 19.9687024 16.0000095 19.7812028 16.0000095 19.6041202 15.9166767 19.4374537 15.7500101L18.9999547 15.3125112C18.8332881 15.1875115 18.7499553 15.0208449 18.7499553 14.8125124 18.7499553 14.6041798 18.8332881 14.4166803 18.9999547 14.2500137L24.2499422 9.06252608H.749998212C.541665693 9.06252608.364582146 8.98960927.218749478 8.8437766.0729168107 8.69794393.0 8.52086038.0 8.31252787V7.68752936C0 7.47919684.0729168107 7.30211329.218749478 7.15628062.364582146 7.01044795.541665693 6.93753114.749998212 6.93753114H24.2499422L18.9999547 1.75004351C18.8332881 1.58337692 18.7499553 1.39587737 18.7499553 1.18754485 18.7499553.979212334 18.8332881.812545747 18.9999547.687546045L19.4374537.250047088z" transform="translate(13.999967, 8.000029) scale(-1, 1) translate(-13.999967, -8.000029)"/></g></g></g></g></svg>
				</span>
				<span class="swiper-button-next">
				<svg width="28px" height="16px" viewBox="0 0 28 16" version="1.1" xmlns="http://www.w3.org/2000/svg"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-732.000000, -1899.000000)" fill="currentColor"><g transform="translate(63.000000, 1479.000000)"><g transform="translate(601.000000, 412.000000)"><path d="M87.4374537,8.25004709 C87.6041202,8.0833805 87.7812028,8.00004768 87.9687024,8.00004768 C88.1562019,8.00004768 88.3332845,8.0833805 88.4999511,8.25004709 L95.7499338,15.5000298 C95.9166004,15.6250295 95.9999332,15.7916961 95.9999332,16.0000286 C95.9999332,16.2083611 95.9166004,16.3750277 95.7499338,16.5000274 L88.4999511,23.7500101 C88.3332845,23.9166767 88.1562019,24.0000095 87.9687024,24.0000095 C87.7812028,24.0000095 87.6041202,23.9166767 87.4374537,23.7500101 L86.9999547,23.3125112 C86.8332881,23.1875115 86.7499553,23.0208449 86.7499553,22.8125124 C86.7499553,22.6041798 86.8332881,22.4166803 86.9999547,22.2500137 L92.2499422,17.0625261 L68.7499982,17.0625261 C68.5416657,17.0625261 68.3645821,16.9896093 68.2187495,16.8437766 C68.0729168,16.6979439 68,16.5208604 68,16.3125279 L68,15.6875294 C68,15.4791968 68.0729168,15.3021133 68.2187495,15.1562806 C68.3645821,15.010448 68.5416657,14.9375311 68.7499982,14.9375311 L92.2499422,14.9375311 L86.9999547,9.75004351 C86.8332881,9.58337692 86.7499553,9.39587737 86.7499553,9.18754485 C86.7499553,8.97921233 86.8332881,8.81254575 86.9999547,8.68754604 L87.4374537,8.25004709 Z"></path></g></g></g></g></svg></span>
			</div><!-- end .swiper-container.format-gallery-slider -->
			<?php
		} //end check image_ids
	}
		endif;


//get first image in a post
if ( ! function_exists( 'gutenbooster_grab_first_image' ) ) :
	function gutenbooster_grab_first_image() {
		global $post, $posts;
		$first_img = '';
		ob_start();
		ob_end_clean();
		$output = preg_match_all( '/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches );
		$first_img = $matches[1][0];

		if ( ! empty( $first_img ) ) {
			return '<img src="' . esc_url( $first_img ) . '" alt="' . esc_html__( 'Post Format Image', 'gutenbooster' ) . '" />';
		} else {
			return;
		}
	}
endif;

function gutenbooster_fetch_gallery_images() {
	if ( has_post_thumbnail() ) {
		// use the thumbnail ("featured image")
		$thumb_id = get_post_thumbnail_id();
		the_post_thumbnail( 'thumbnail' );
	} else {
		global $post;
			$attachments = get_children(
				array(
					'post_parent' => $post->ID,
					'post_status' => 'inherit',
					'post_type' => 'attachment',
					'post_mime_type' => 'image',
					'order' => 'ASC',
					'orderby' => 'menu_order ID',
					'posts_per_page' => 2,
				)
			);
		foreach ( $attachments as $thumb_id => $attachment ) {
			echo wp_get_attachment_image( $thumb_id, 'thumbnail' );
		}
	}

}
