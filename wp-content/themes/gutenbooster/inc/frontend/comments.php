<?php
/**
 * Custom functions that act on comments.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package GutenBooster
 */

/**
 * Remove Website field from comment form
 *
 * @param array $fields
 *
 * @return array
 */
function gutenbooster_disable_comment_url( $fields ) {
	unset( $fields['url'] );

	return $fields;
}

add_filter( 'comment_form_default_fields', 'gutenbooster_disable_comment_url' );


/**
 * Template Comment
 *
 * @since  1.0
 *
 * @param  array $comment
 * @param  array $args
 * @param  int   $depth
 *
 * @return mixed
 */
function gutenbooster_comment( $comment, $args, $depth ) {

	if ( 'div' == $args['style'] ) {
		$add_below = 'comment';
	} else {
		$add_below = 'div-comment';
	}
	?>

<li <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?> id="comment-<?php comment_ID(); ?>">
	<article id="div-comment-<?php comment_ID(); ?>" class="comment-body clearfix">
		
		<div class="comment-author vcard">
			<?php
			if ( 0 != $args['avatar_size'] ) {
				echo get_avatar( $comment, $args['avatar_size'] );
			}
			?>
		</div>
		<div class="comment-meta commentmetadata">
			<?php printf( '<cite class="author-name">%s</cite>', get_comment_author_link() ); ?>

			<a class="author-posted" href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
				<?php printf( '%1$s', esc_attr( get_comment_date() ) ); ?>
			</a>

			<?php if ( '0' === $comment->comment_approved ) : ?>
				<em class="comment-awaiting-moderation"><?php echo esc_html_e( 'Your comment is awaiting moderation.', 'gutenbooster' ); ?></em>
			<?php endif; ?>

			<div class="comment-content">
				<?php comment_text(); ?>
			</div>

			<?php
			comment_reply_link(
				array_merge(
					$args,
					array(
						'add_below'  => $add_below,
						'depth'      => $depth,
						'max_depth'  => $args['max_depth'],
						'reply_text' => esc_html__( 'Reply', 'gutenbooster' ),
					)
				)
			);
			edit_comment_link( esc_html__( 'Edit', 'gutenbooster' ), '  ', '' );
			?>
		</div>
	</article>

	<?php
}
