<?php
/**
 * Custom functions that act in the footer.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package GutenBooster
 */

 /**
  *   Display comments and post nav
  *   Hide in Woocommerce Products
  */
function gutenbooster_postnav_comments_before_footer() {
	if ( is_single() ) {

		the_post_navigation( array(
			'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'gutenbooster' ) . '</span> ' .
				'<span class="screen-reader-text">' . __( 'Next:', 'gutenbooster' ) . '</span> ' .
				'<span class="post-title">%title</span>',
			'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'gutenbooster' ) . '</span> ' .
				'<span class="screen-reader-text">' . __( 'Previous:', 'gutenbooster' ) . '</span> ' .
				'<span class="post-title">%title</span>',
			)
		);
		// If comments are open or we have at least one comment, load up the comment template.
		comments_template();
	}
}

add_action( 'gutenbooster_before_footer', 'gutenbooster_postnav_comments_before_footer' );

 /**
 * Display widgets on footer
 */
function gutenbooster_footer_widgets() {
	if ( ! gutenbooster_getmod( 'footer_widgets' ) ) {
		return;
	}

	$layout = gutenbooster_getmod( 'footer_widgets_layout' );
	$colums = array(
		'2-columns'       => 2,
		'3-columns'       => 3,
		'4-columns'       => 4,
	);
	?>
			<div class="row">

				<?php
				for ( $i = 1; $i <= $colums[ $layout ]; $i++ ) {
					$column_class = 12 / $colums[ $layout ];

					if ( '4-columns' == $layout ) {
						$column_class = in_array( $i, array( 1, 4 ) ) ? 4 : 2;
					}

					echo '<div class="footer-widgets-area-' . esc_attr( $i ) . ' footer-widgets col-lg ' . esc_attr( gutenbooster_footercol_maxpad( 'widgets' ) ) . '">';

					dynamic_sidebar( 'footer-' . $i );

					echo '</div>';
				}
				?>

			</div>


	<?php
}

add_action( 'gutenbooster_footer', 'gutenbooster_footer_widgets', 5 );

function gutenbooster_footer_classes() {
	$class   = '';

	$class   .= gutenbooster_getmod( 'footer_wrapper' );
	$class   .= gutenbooster_getmod( 'footer_maxpad' ) /*&& get_theme_mod('footer_wrapper')=='container-fluid'*/ ? ' maxpad' : '';
	$class   .= gutenbooster_getmod( 'footer_widgets' ) ? ' is_widgetized' : '';
	$class   .= gutenbooster_getmod( 'footer_invert_links' ) ? ' invert_links' : '';
	$class   .= gutenbooster_getmod( 'footer_align_links_left' ) ? ' align_links_left' : '';
	$class   .= gutenbooster_getmod( 'footer_hide_submenus' ) ? ' hide-submenus' : '';

	return $class;
}

function gutenbooster_footer_bg_image() {
	$output_bg_img   = '';
	$image_id        = null;
	$image_size      = '1400px-optimized';


	$image_id = attachment_url_to_postid( get_theme_mod( 'footer_bg_image' ) );
	$img_attachment  = wp_get_attachment_image_src( $image_id, $image_size );
	$final_img       = $img_attachment[0];

	$output_bg_img   = ! empty( $image_id ) ? 'style="background-image:url(' . esc_url( $final_img ) . ');"' : '';

	return $output_bg_img;
}

function gutenbooster_footercol_maxpad( $which = '' ) {
	if ( ! gutenbooster_getmod( 'footer_maxpad' ) /*|| get_theme_mod('footer_wrapper')=='container'*/ ) {
		return;
	}
	$class = '';
	if ( 'widgets' == $which ) {
		$class = 'pt-4 pb-0 pl-xl-5 pl-sm-4 pr-xl-5 pr-sm-4';
	} else {
		$class = 'p-xl-5 p-sm-4';
		// $class ='pt-4 pb-0 pl-xl-5 pl-sm-4 pr-xl-5 pr-sm-4';
	}

	return $class;

}

 /**
 * Function to get Small Footer Custom Text
 */
if ( ! function_exists( 'gutenbooster_get_small_footer_custom_text' ) ) {

	/**
	 * Function to get Small Footer Custom Text
	 *
	 * @since 1.0.14
	 * @param string $option Custom text option name.
	 * @return mixed         Markup of custom text option.
	 */
	function gutenbooster_get_small_footer_custom_text( $option = '' ) {

		$output = $option;

		if ( '' != $option ) {
			$output = gutenbooster_getmod( $option );
			$output = str_replace( '[current_year]', date_i18n( 'Y' ), $output );
			$output = str_replace( '[site_title]', '<span class="gb-footer-site-title">' . get_bloginfo( 'name' ) . '</span>', $output );

			$theme_author = apply_filters(
				'gutenbooster_theme_author',
				array(
					'theme_name'       => __( 'GutenBooster', 'gutenbooster' ),
					'theme_author_url' => esc_url( 'https://kiokengutenberg.com/' ),
				)
			);

			$output = str_replace( '[theme_author]', '<a href="' . esc_url( $theme_author['theme_author_url'] ) . '">' . $theme_author['theme_name'] . '</a>', $output );

			/* my additional output */
			$output = str_replace( '[custom_footer_link]', '<a href="http://answare-ltd.com/">ANSware-ltd</a>', $output );
		}

		return do_shortcode( $output );
	}
}

/**
* Render Partial Footer Section 1 Credit
*/
function gutenbooster_render_footer_txt_left() {
	$output = gutenbooster_get_small_footer_custom_text( 'footer_text_left' );
	return do_shortcode( $output );
}
