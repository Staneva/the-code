<?php
/**
 * Functions for preloader option in the theme
 *
 * @package gutenbooster
 */


 /**
  * Ajax Preloader Check. Validate existence.
  */
if ( ! function_exists( 'gutenbooster_has_ajax_preloader' ) ) :

	function gutenbooster_has_ajax_preloader() {



			$has = get_theme_mod( 'preloader' );
			/**
			* Filter for checking has ajax preloader
			*
			* @since  1.0
			*/

		return apply_filters( 'gutenbooster_has_ajax_preloader', $has );
	}
endif;


function gutenbooster_kt_preloader( $prop = '' ) {
	if ( ! get_theme_mod( 'preloader' ) ) {
		return;
	}
	if ( 'brb-contain-on' == $prop ) : ?>
  <div id="barba-wrapper">
		<div data-namespace="<?php echo esc_attr( gutenbooster_get_current_template() ); ?>" class="barba-container">

			<?php
	elseif ( 'brb-contain-off' == $prop ) :
		?>
	  </div><!-- .barba-container -->
	</div><!-- #barba-wrapper -->

		<?php
  endif;
}

function gutenbooster_kt_preloader_open_wrapper() {

	if ( ! get_theme_mod( 'preloader' ) ) {
		return;
	}
	?>
	<div id="barba-wrapper">
		<div data-namespace="<?php echo esc_attr( gutenbooster_get_current_template() ); ?>" class="barba-container">
	<?php
}


function gutenbooster_print_preloader_type() {
	if ( ! get_theme_mod( 'preloader' ) ) {
		return;
	}
	$trans_duration = get_theme_mod( 'preloader_trans_duration' ) ? get_theme_mod( 'preloader_trans_duration' ) : '1.2';

	echo '<div class="kt-preloader-obj" data-trans-duration="' . esc_html($trans_duration) . '">
	  <div class="lds-ellipsis">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>
  </div>';

}
add_action( 'wp_footer', 'gutenbooster_print_preloader_type' );


function gutenbooster_get_current_template() {
	global $template;
	return basename( $template, '.php' );
}
