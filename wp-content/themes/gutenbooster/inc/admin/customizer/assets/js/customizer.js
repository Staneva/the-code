/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {

    var api = wp.customize;

    /*=============================================>>>>>
    = Header =
    ===============================================>>>>>*/
    //header wrapper
    wp.customize( 'header_wrapper', function( value ) {
        value.bind( function( newval ) {

            if ( newval == 'full-width' ) {
                $( 'body' ).removeClass("header--wrap-wrapped").addClass("header--wrap-full-width");
            } else {
                $( 'body' ).removeClass("header--wrap-full-width").addClass("header--wrap-wrapped");
            }

        } );
    } );

        //header background
        wp.customize( 'header_bg', function( value ) {
            value.bind( function( newval ) {

                if ( newval == 'transparent' ) {
                    $( 'body' ).removeClass("header--bg-white").addClass("header--bg-transparent");
                } else {
                    $( 'body' ).removeClass("header--bg-transparent").addClass("header--bg-white");
                }

            } );
        } );

        //header text/link color
        wp.customize( 'header_txt_color', function( value ) {
            value.bind( function( newval ) {

                if ( newval == 'dark' ) {
                    $( 'body' ).removeClass("header--txt-white").addClass("header--txt-dark");
                } else {
                    $( 'body' ).removeClass("header--txt-dark").addClass("header--txt-white");
                }

            } );
        } );

        //header top/bottom padding
        wp.customize( 'header_topbottom_pad', function( value ) {
            value.bind( function( newval ) {

                $( '.menu-wrapper .inner-wrap' ).css( { paddingTop: newval + 'px', paddingBottom: newval + 'px' } );

            } );
        } );

        //header menu typography settings
        wp.customize( 'font_menu_size', function( value ) {
            value.bind( function( newval ) {

                var val = JSON.parse(newval);

                var $styleSelector = $( '.customizer-gutenbooster_menu_font_size' );
                var style = '<style class="customizer-gutenbooster_menu_font_size">.menu-wrapper .nav-menu a{ font-size: ' + val + 'px; }</style>';

    			$styleSelector.length ? $styleSelector.replaceWith( style ) : $( 'head' ).append( style );

            } );
        } );
        wp.customize( 'font_menu_text_transform', function( value ) {
            value.bind( function( newval ) {

                $( '.menu-wrapper .nav-menu a' ).css( { textTransform: newval } );

            } );
        } );

    /*=============================================>>>>>
    = Logo =
    ===============================================>>>>>*/

    wp.customize( 'logo_height', function( value ) {
        value.bind( function( newval ) {

            var val = JSON.parse(newval);

            // $( '.menu-wrapper:not(.shrink) .site-logo img' ).css( { maxHeight: newval + 'px' } );

            // Tablet Size
            var $styleSelector = $( '.customizer-gutenbooster_logo_height' );
            var style = '<style class="customizer-gutenbooster_logo_height">.menu-wrapper:not(.shrink) .site-logo img{ max-height: ' + val + 'px; }</style>';

			$styleSelector.length ? $styleSelector.replaceWith( style ) : $( 'head' ).append( style );

        } );
    } );

    wp.customize( 'logo_topmargin', function( value ) {
        value.bind( function( newval ) {

            $( '.site-logo a' ).css( { marginTop: newval + 'px' } );

        } );
    } );
        wp.customize( 'logo_bottommargin', function( value ) {
            value.bind( function( newval ) {

                $( '.site-logo a' ).css( { marginBottom: newval + 'px' } );

            } );
        } );
        wp.customize( 'logo_leftmargin', function( value ) {
            value.bind( function( newval ) {

                $( '.site-logo a' ).css( { marginLeft: newval + 'px' } );

            } );
        } );
        wp.customize( 'logo_rightmargin', function( value ) {
            value.bind( function( newval ) {

                $( '.site-logo a' ).css( { marginRight: newval + 'px' } );

            } );
        } );

    /*=============================================>>>>>
    = Typography =
    ===============================================>>>>>*/

    // Body Font Size & Line Height
    var bodyFontSelectors = 'body, button, input, select, textarea';

    wp.customize( 'body_fontsize', function( value ) {
        value.bind( function( newval ) {
            var val = JSON.parse(newval);
            var desktopSize = val.desktop;
            var tabletSize = val.tablet;
            var mobileSize = val.mobile;


            $( bodyFontSelectors ).css( 'font-size', desktopSize + 'px' );

            // Tablet Size
            var $tabletStyleSelector = $( '.customizer-gutenbooster_tablet_body_fontsize' );
            var tabletStyle = '<style class="customizer-gutenbooster_tablet_body_fontsize">@media only screen and (max-width: 959px) {' + bodyFontSelectors + '{font-size: ' + tabletSize + 'px !important;}}</style>';

			$tabletStyleSelector.length ? $tabletStyleSelector.replaceWith( tabletStyle ) : $( 'head' ).append( tabletStyle );

            // Mobile Size
            var $mobileStyleSelector = $( '.customizer-gutenbooster_mobile_body_fontsize' );
            var mobileStyle = '<style class="customizer-gutenbooster_mobile_body_fontsize">@media only screen and (max-width: 480px) {' + bodyFontSelectors + '{font-size: ' + mobileSize + 'px !important;}}</style>';

			$mobileStyleSelector.length ? $mobileStyleSelector.replaceWith( mobileStyle ) : $( 'head' ).append( mobileStyle );

        } );
    } );
        // line height
        wp.customize( 'typobody_lh', function( value ) {
            value.bind( function( newval ) {
                $( bodyFontSelectors ).css( 'line-height', newval );
            } );
        } );

    // H1 Font Size & Line Height
    wp.customize( 'font_h1_size', function( value ) {
        value.bind( function( newval ) {
            var val = JSON.parse(newval);
            var desktopSize = val.desktop;
            var tabletSize = val.tablet;
            var mobileSize = val.mobile;


            $( 'h1' ).css( 'font-size', desktopSize + 'px' );

            // Tablet Size
            var $tabletStyleSelector = $( '.customizer-gutenbooster_tablet_h1_fontsize' );
            var tabletStyle = '<style class="customizer-gutenbooster_tablet_h1_fontsize">@media only screen and (max-width: 959px) { h1 {font-size: ' + tabletSize + 'px !important;}}</style>';

			$tabletStyleSelector.length ? $tabletStyleSelector.replaceWith( tabletStyle ) : $( 'head' ).append( tabletStyle );

            // Mobile Size
            var $mobileStyleSelector = $( '.customizer-gutenbooster_mobile_h1_fontsize' );
            var mobileStyle = '<style class="customizer-gutenbooster_mobile_h1_fontsize">@media only screen and (max-width: 480px) { h1 {font-size: ' + mobileSize + 'px !important;}}</style>';

			$mobileStyleSelector.length ? $mobileStyleSelector.replaceWith( mobileStyle ) : $( 'head' ).append( mobileStyle );

        } );
    } );
        // line height
        wp.customize( 'font_h1_lh', function( value ) {
            value.bind( function( newval ) {
                var val = JSON.parse(newval);
                var desktopSize = val.desktop;
                var tabletSize = val.tablet;
                var mobileSize = val.mobile;


                $( 'h1' ).css( 'line-height', desktopSize );

                // Tablet Size
                var $tabletStyleSelector = $( '.customizer-gutenbooster_tablet_h1_lh' );
                var tabletStyle = '<style class="customizer-gutenbooster_tablet_h1_lh">@media only screen and (max-width: 959px) { h1 {line-height: ' + tabletSize + ' !important;}}</style>';

    			$tabletStyleSelector.length ? $tabletStyleSelector.replaceWith( tabletStyle ) : $( 'head' ).append( tabletStyle );

                // Mobile Size
                var $mobileStyleSelector = $( '.customizer-gutenbooster_mobile_h1_lh' );
                var mobileStyle = '<style class="customizer-gutenbooster_mobile_h1_lh">@media only screen and (max-width: 480px) { h1 {line-height: ' + mobileSize + ' !important;}}</style>';

    			$mobileStyleSelector.length ? $mobileStyleSelector.replaceWith( mobileStyle ) : $( 'head' ).append( mobileStyle );

            } );
        } );

    // H2 Font Size & Line Height
    wp.customize( 'font_h2_size', function( value ) {
        value.bind( function( newval ) {
            var val = JSON.parse(newval);
            var desktopSize = val.desktop;
            var tabletSize = val.tablet;
            var mobileSize = val.mobile;


            $( 'h2' ).css( 'font-size', desktopSize + 'px' );

            // Tablet Size
            var $tabletStyleSelector = $( '.customizer-gutenbooster_tablet_h2_fontsize' );
            var tabletStyle = '<style class="customizer-gutenbooster_tablet_h2_fontsize">@media only screen and (max-width: 959px) { h2 {font-size: ' + tabletSize + 'px !important;}}</style>';

			$tabletStyleSelector.length ? $tabletStyleSelector.replaceWith( tabletStyle ) : $( 'head' ).append( tabletStyle );

            // Mobile Size
            var $mobileStyleSelector = $( '.customizer-gutenbooster_mobile_h2_fontsize' );
            var mobileStyle = '<style class="customizer-gutenbooster_mobile_h2_fontsize">@media only screen and (max-width: 480px) { h2 {font-size: ' + mobileSize + 'px !important;}}</style>';

			$mobileStyleSelector.length ? $mobileStyleSelector.replaceWith( mobileStyle ) : $( 'head' ).append( mobileStyle );

        } );
    } );
        // line height
        wp.customize( 'font_h2_lh', function( value ) {
            value.bind( function( newval ) {
                var val = JSON.parse(newval);
                var desktopSize = val.desktop;
                var tabletSize = val.tablet;
                var mobileSize = val.mobile;


                $( 'h2' ).css( 'line-height', desktopSize );

                // Tablet Size
                var $tabletStyleSelector = $( '.customizer-gutenbooster_tablet_h2_lh' );
                var tabletStyle = '<style class="customizer-gutenbooster_tablet_h2_lh">@media only screen and (max-width: 959px) { h2 {line-height: ' + tabletSize + ' !important;}}</style>';

    			$tabletStyleSelector.length ? $tabletStyleSelector.replaceWith( tabletStyle ) : $( 'head' ).append( tabletStyle );

                // Mobile Size
                var $mobileStyleSelector = $( '.customizer-gutenbooster_mobile_h2_lh' );
                var mobileStyle = '<style class="customizer-gutenbooster_mobile_h2_lh">@media only screen and (max-width: 480px) { h2 {line-height: ' + mobileSize + ' !important;}}</style>';

    			$mobileStyleSelector.length ? $mobileStyleSelector.replaceWith( mobileStyle ) : $( 'head' ).append( mobileStyle );

            } );
        } );

    // H3 Font Size & Line Height
    wp.customize( 'font_h3_size', function( value ) {
        value.bind( function( newval ) {
            var val = JSON.parse(newval);
            var desktopSize = val.desktop;
            var tabletSize = val.tablet;
            var mobileSize = val.mobile;


            $( 'h3' ).css( 'font-size', desktopSize + 'px' );

            // Tablet Size
            var $tabletStyleSelector = $( '.customizer-gutenbooster_tablet_h3_fontsize' );
            var tabletStyle = '<style class="customizer-gutenbooster_tablet_h3_fontsize">@media only screen and (max-width: 959px) { h3 {font-size: ' + tabletSize + 'px !important;}}</style>';

			$tabletStyleSelector.length ? $tabletStyleSelector.replaceWith( tabletStyle ) : $( 'head' ).append( tabletStyle );

            // Mobile Size
            var $mobileStyleSelector = $( '.customizer-gutenbooster_mobile_h3_fontsize' );
            var mobileStyle = '<style class="customizer-gutenbooster_mobile_h3_fontsize">@media only screen and (max-width: 480px) { h3 {font-size: ' + mobileSize + 'px !important;}}</style>';

			$mobileStyleSelector.length ? $mobileStyleSelector.replaceWith( mobileStyle ) : $( 'head' ).append( mobileStyle );

        } );
    } );
        // line height
        wp.customize( 'font_h3_lh', function( value ) {
            value.bind( function( newval ) {
                var val = JSON.parse(newval);
                var desktopSize = val.desktop;
                var tabletSize = val.tablet;
                var mobileSize = val.mobile;


                $( 'h3' ).css( 'line-height', desktopSize );

                // Tablet Size
                var $tabletStyleSelector = $( '.customizer-gutenbooster_tablet_h3_lh' );
                var tabletStyle = '<style class="customizer-gutenbooster_tablet_h3_lh">@media only screen and (max-width: 959px) { h3 {line-height: ' + tabletSize + ' !important;}}</style>';

    			$tabletStyleSelector.length ? $tabletStyleSelector.replaceWith( tabletStyle ) : $( 'head' ).append( tabletStyle );

                // Mobile Size
                var $mobileStyleSelector = $( '.customizer-gutenbooster_mobile_h3_lh' );
                var mobileStyle = '<style class="customizer-gutenbooster_mobile_h3_lh">@media only screen and (max-width: 480px) { h3 {line-height: ' + mobileSize + ' !important;}}</style>';

    			$mobileStyleSelector.length ? $mobileStyleSelector.replaceWith( mobileStyle ) : $( 'head' ).append( mobileStyle );

            } );
        } );

    // H4 Font Size & Line Height
    wp.customize( 'font_h4_size', function( value ) {
        value.bind( function( newval ) {
            var val = JSON.parse(newval);
            var desktopSize = val.desktop;
            var tabletSize = val.tablet;
            var mobileSize = val.mobile;


            $( 'h4' ).css( 'font-size', desktopSize + 'px' );

            // Tablet Size
            var $tabletStyleSelector = $( '.customizer-gutenbooster_tablet_h4_fontsize' );
            var tabletStyle = '<style class="customizer-gutenbooster_tablet_h4_fontsize">@media only screen and (max-width: 959px) { h4 {font-size: ' + tabletSize + 'px !important;}}</style>';

			$tabletStyleSelector.length ? $tabletStyleSelector.replaceWith( tabletStyle ) : $( 'head' ).append( tabletStyle );

            // Mobile Size
            var $mobileStyleSelector = $( '.customizer-gutenbooster_mobile_h4_fontsize' );
            var mobileStyle = '<style class="customizer-gutenbooster_mobile_h4_fontsize">@media only screen and (max-width: 480px) { h4 {font-size: ' + mobileSize + 'px !important;}}</style>';

			$mobileStyleSelector.length ? $mobileStyleSelector.replaceWith( mobileStyle ) : $( 'head' ).append( mobileStyle );

        } );
    } );
        // line height
        wp.customize( 'font_h4_lh', function( value ) {
            value.bind( function( newval ) {
                var val = JSON.parse(newval);
                var desktopSize = val.desktop;
                var tabletSize = val.tablet;
                var mobileSize = val.mobile;


                $( 'h4' ).css( 'line-height', desktopSize );

                // Tablet Size
                var $tabletStyleSelector = $( '.customizer-gutenbooster_tablet_h4_lh' );
                var tabletStyle = '<style class="customizer-gutenbooster_tablet_h4_lh">@media only screen and (max-width: 959px) { h4 {line-height: ' + tabletSize + ' !important;}}</style>';

    			$tabletStyleSelector.length ? $tabletStyleSelector.replaceWith( tabletStyle ) : $( 'head' ).append( tabletStyle );

                // Mobile Size
                var $mobileStyleSelector = $( '.customizer-gutenbooster_mobile_h4_lh' );
                var mobileStyle = '<style class="customizer-gutenbooster_mobile_h4_lh">@media only screen and (max-width: 480px) { h4 {line-height: ' + mobileSize + ' !important;}}</style>';

    			$mobileStyleSelector.length ? $mobileStyleSelector.replaceWith( mobileStyle ) : $( 'head' ).append( mobileStyle );

            } );
        } );



    /*=============================================>>>>>
    = Colors =
    ===============================================>>>>>*/

    //body bg
    wp.customize( 'background_color', function( value ) {
		value.bind( function( newval ) {
			$('body, #content, .woocommerce div.product div.images .woocommerce-product-gallery__wrapper .zoomImg, .k-content-inner .kt-large-post .post-summary')
            .css('background-color', newval );

            $('.hfeed .kt-classic-post.sticky, .hfeed .type-post.tag-sticky-2')
            .css('box-shadow', ' -10px 0 0 ' + newval + ', -15px 0 0 ' + newval );
		} );
	} );


    //Primary Color
    var primaryColorselectors = [
        'a',
		'.entry-content a',
		'.has-gb-primary-color',
		'.hfeed .media-body .entry-title a:hover',
		'.hfeed .media-body .entry-meta a:hover',
		'.pagination .page-numbers.current, .pagination .page-numbers:hover',
		'.site-footer .inner.txt-white a:hover',
		'.site-footer .inner.invert_links a',
    ];
    primaryColorselectors = primaryColorselectors.join();

    var primaryBGColorSelectors = [
        '.site-nav-top .nav-menu > li:not(.mega-parent) ul.sub-menu li a:hover',
        '.has-gb-primary-background-color',
        '.comment-respond .comment-form input[type="submit"], a.underlined::after',
        '.hfeed .kt-classic-post.sticky .gb-sticky-indicator',
        '.hfeed .type-post.tag-sticky-2 .gb-sticky-indicator'
    ];
    primaryBGColorSelectors = primaryBGColorSelectors.join();

    wp.customize( 'color_gb_primary', function( value ) {
		value.bind( function( newval ) {
			$(primaryColorselectors).css('color', newval );
			$(primaryBGColorSelectors).css('background-color', newval );
		} );
	} );

    //Secondary Color
    var secondaryColorselectors = [
        'a:hover',
        '.has-gb-secondary-color',
        '.hfeed .media-body .entry-title a',
        '.hfeed .media-body .entry-meta a',
        '.entry-content a:hover',
        '#content h1, #content h2, #content h3, #content h4, #content h5, #content h6',
        '.widget_working_hours .day',
        '.pagination .page-numbers',
        '.blog-cats-row ul.cat-list a',
        '.entry-content .post a:hover',
        '.comment article .comment-meta .author-name, .comment article .comment-meta .author-name a',
        'select',
        '.hfeed .type-post.tag-sticky-2 .gb-sticky-indicator'
    ];
    secondaryColorselectors = secondaryColorselectors.join();

    var secondaryBGColorSelectors = [
        '.has-gb-secondary-background-color',
        '.entry-content .cat-links a, .entry-content .tags-links a',
        '.btn__primary:hover, .btn__primary.btn__arrvrt:hover::before',
    ];
    secondaryBGColorSelectors = secondaryBGColorSelectors.join();

    wp.customize( 'color_gb_secondary', function( value ) {
		value.bind( function( newval ) {
			$(secondaryColorselectors).css('color', newval );
            $(secondaryBGColorSelectors).css('background-color', newval );
		} );
	} );

    /*=============================================>>>>>
    = Page Banner =
    ===============================================>>>>>*/



    // Opacity
    api('page_bannerbg_opacity', function( value ) {

        value.bind( function( newval ) {
            $( '#page-banner-wrap .page-banner-bg' ).css( { Opacity: newval } );
        });
    });

    // Background Color
    api('page_banner_bg', function( value ) {
		value.bind( function( newval ) {

			$( '#page-banner-wrap .page-banner-bg' ).css( { 'background-image' : 'url( ' + newval + ')'} );
		});
	});

    // Background Color
    api('page_banner_bgcolor', function( value ) {
		value.bind( function( newval ) {
			$( '#page-banner-wrap' ).css( { backgroundColor: newval } );
		});
	});

    // Height
    api('page_banner_height', function( value ) {
		value.bind( function( newval ) {
			$( '#page-banner-wrap' ).css( { height: newval + 'vh' } );
		});
	});

    // Text Color
    api('page_banner_text_color', function( value ) {
		value.bind( function( newval ) {

            if ( newval != 'white' ) {
                $( 'body' ).removeClass("pageban-textcolor-white").addClass("pageban-textcolor-dark");
            } else {
                $( 'body' ).removeClass("pageban-textcolor-dark").addClass("pageban-textcolor-white");
            }

		});
	});

    /*=============================================>>>>>
    = Layout =
    ===============================================>>>>>*/

    // Content Width
    api('layout_content_width', function( value ) {
        var classes = [
                '.layout-no-sidebar .entry-content .post > *',
                '.layout-no-sidebar .entry-content .page > *',
                'body.single .post-intro .title-heading h1',
				'.layout-no-sidebar.hfeed.blog-classic #content > .container',
				'.comment-respond',
				'.comment-list',
				'.comments-pagination',
            ].toString();

		value.bind( function( newval ) {
			$( classes ).css( { maxWidth: newval + 'px' } );
		});
	});

    // Content Top Padding
    api('content_toppad', function( value ) {
		value.bind( function( newval ) {
			$( '#content' ).css( { paddingTop: newval + 'px' } );
		});
	});





} )( jQuery );
