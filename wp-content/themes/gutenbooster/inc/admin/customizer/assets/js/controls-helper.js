jQuery(document).ready(function( $ ) {

    // customizer description show/hide
    wp.customize.control.each(function(ctrl, i) {
        var description = ctrl.container.find('.customize-control-description');
        if(description.length) {
            var title = ctrl.container.find('.customize-control-title:not(.typo)');
            title.append(' <i class="dashicons dashicons-editor-help"></i>');
            var button = ctrl.container.find('.customize-control-title > .dashicons');

            button.on('click', function ( e ) {
                e.preventDefault();
                $( this ).toggleClass('active');
                $( description ).toggleClass('active');
            });
        }
    });

});