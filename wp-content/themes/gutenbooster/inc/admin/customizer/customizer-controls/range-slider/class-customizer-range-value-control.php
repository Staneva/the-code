<?php


class Kioken_Customizer_Range_Value_Control extends \WP_Customize_Control {
	public $type = 'range-value-std';


	/**
	 * Enqueue control related scripts/styles.
	 *
	 * @access public
	 */
	public function enqueue() {
		$control_dir = trailingslashit( get_template_directory_uri() ) . 'inc/admin/customizer/customizer-controls/range-slider';
		wp_enqueue_script( 'kiokentheme-rangeslider', $control_dir . '/range-min.js', array( 'jquery' ), false, true );
	}

	/**
	 * Render the control's content.
	 *
	 * @author soderlind
	 * @version 1.2.0
	 */
	public function render_content() {
		?>
		<label>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<div class="range-slider-std"  style="width:100%; display:flex;flex-direction: row;justify-content: flex-start;">
				<span  style="width:100%; flex: 1 0 0; vertical-align: middle;"><input class="range-slider-std__range" type="range" value="<?php echo esc_attr( $this->value() ); ?>"
				  <?php
					$this->input_attrs();
					$this->link();
					?>
				>
				<span class="range-slider-std__value">0</span></span>
			</div>
			<?php if ( ! empty( $this->description ) ) : ?>
			<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
			<?php endif; ?>
		</label>
		<?php
	}

	/**
	 * Plugin / theme agnostic path to URL
	 *
	 * @see https://wordpress.stackexchange.com/a/264870/14546
	 * @param string $path  file path
	 * @return string       URL
	 */
	private function abs_path_to_url( $path = '' ) {
		$url = str_replace(
			wp_normalize_path( untrailingslashit( ABSPATH ) ),
			site_url(),
			wp_normalize_path( $path )
		);
		return esc_url_raw( $url );
	}
}