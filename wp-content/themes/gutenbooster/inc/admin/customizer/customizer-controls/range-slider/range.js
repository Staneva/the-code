wp.customize.controlConstructor['range-value-std'] = wp.customize.Control.extend({


    ready: function() {

		'use strict';
        jQuery.fn.exists = function(){return this.length>0;};
        var slider = jQuery('.range-slider-std'),
            range = jQuery('.range-slider-std__range'),
            value = jQuery('.range-slider-std__value');

        slider.each(function() {

            value.each(function() {
              var value = jQuery(this).prev().attr('value');
                      var suffix = (jQuery(this).prev().attr('suffix')) ? jQuery(this).prev().attr('suffix') : '';
              jQuery(this).html(value + suffix);
            });

          range.on('input', function() {
            var suffix = (jQuery(this).attr('suffix')) ? jQuery(this).attr('suffix') : '';
            jQuery(this).next(value).html(this.value + suffix );
          });
        });


	},


});