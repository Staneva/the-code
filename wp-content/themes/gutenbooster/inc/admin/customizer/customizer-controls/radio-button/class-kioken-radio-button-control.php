<?php

/**
 * Text Radio Button Custom Control
 *
 * @author Anthony Hortin <http://maddisondesigns.com>
 * @license http://www.gnu.org/licenses/gpl-2.0.html
 * @link https://github.com/maddisondesigns
 */
 class Kioken_Radio_Button_Control extends WP_Customize_Control {
		/**
		 * The type of control being rendered
		 */
		public $type = 'text_radio_button';


        public function enqueue() {
            wp_enqueue_style( 'gutenbooster-customizer-styles' );
		}

        /**
         * Handles input value.
         */
         public function to_json() {
             parent::to_json();
             // Default value.
             $this->json['default'] = $this->setting->default;
             if ( isset( $this->default ) ) {
                 $this->json['default'] = $this->default;
             }
             // Output.
             $this->json['output'] = $this->output;
             // Value.
             $this->json['value'] = $this->value();
             // Choices.
             $this->json['choices'] = $this->choices;
             // The link.
             $this->json['link'] = $this->get_link();
             // The ID.
             $this->json['id'] = $this->id;
             // Translation strings.
             // The ajaxurl in case we need it.
             $this->json['ajaxurl'] = admin_url( 'admin-ajax.php' );
             // Input attributes.
             $this->json['inputAttrs'] = '';
             if ( is_array( $this->input_attrs ) ) {
                 foreach ( $this->input_attrs as $attr => $value ) {
                     $this->json['inputAttrs'] .= $attr . '="' . esc_attr( $value ) . '" ';
                 }
             }

         }



        /**
    	 * An Underscore (JS) template for this control's content (but not its container).
    	 *
    	 * Class variables for this control class are available in the `data` JS object;
    	 * export custom variables by overriding {@see WP_Customize_Control::to_json()}.
    	 *
    	 * @see WP_Customize_Control::print_template()
    	 *
    	 * @access protected
    	 * @since 1.0
    	 * @return void
    	 */
         protected function content_template() {
 		?>
        <div class="text_radio_button_control">
     		<# if ( data.label ) { #><span class="customize-control-title">{{{ data.label }}}</span><# } #>
     		<# if ( data.description ) { #><span class="description customize-control-description">{{{ data.description }}}</span><# } #>
     		<div id="input_{{ data.id }}" class="radio-buttons">
     			<# for ( key in data.choices ) { #>
                    <label class="radio-button-label">
                    <input {{{ data.inputAttrs }}} type="radio" value="{{ key }}" name="_customize-radio-{{{ data.id }}}" {{{ data.link }}} <# if ( key === data.value ) { #> checked="checked" <# } #>>

                        <span>{{{ data.choices[ key ] }}}</span>
     				</input>
                    </label>
     			<# } #>
     		</div>
        </div>
 		<?php
 	}

    /**
	 * Check if a string is in json format
	 *
	 * @param  string $string Input.
	 */
	public function is_json( $string ) {
		return is_string( $string ) && is_array( json_decode( $string, true ) ) ? true : false;
	}

	}
$wp_customize->register_control_type( 'Kioken_Radio_Button_Control' );