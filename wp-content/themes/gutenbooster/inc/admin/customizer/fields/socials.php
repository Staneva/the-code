<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$wp_customize->add_setting( 'facebook', array(
  'capability' => 'edit_theme_options',
  'sanitize_callback' => 'gutenbooster_sanitize_url',
) );

    $wp_customize->add_control( 'facebook', array(
      'type' => 'url',
      'section' => 'socials', // Add a default or your own section
      'label' => __( 'Facebook URL', 'gutenbooster' ),
      'description' => __( 'Enter your Facebook url in full', 'gutenbooster' ),
      'input_attrs' => array(
        'placeholder' => __( 'ie. https://www.facebook.com/themekioken/', 'gutenbooster' ),
      ),
    ) );

    $wp_customize->add_setting( 'twitter', array(
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'gutenbooster_sanitize_url',
    ) );

        $wp_customize->add_control( 'twitter', array(
          'type' => 'url',
          'section' => 'socials', // Add a default or your own section
          'label' => __( 'Twitter URL', 'gutenbooster' ),
          'description' => __( 'Enter your Twitter url in full', 'gutenbooster' ),
          'input_attrs' => array(
            'placeholder' => __( 'ie. https://www.twitter.com/kiokentheme/', 'gutenbooster' ),
          ),
        ) );

$wp_customize->add_setting( 'instagram', array(
  'capability' => 'edit_theme_options',
  'sanitize_callback' => 'gutenbooster_sanitize_url',
) );

    $wp_customize->add_control( 'instagram', array(
      'type' => 'url',
      'section' => 'socials', // Add a default or your own section
      'label' => __( 'Instagram URL', 'gutenbooster' ),
      'description' => __( 'Enter your Instagram url in full', 'gutenbooster' ),
      'input_attrs' => array(
        'placeholder' => __( 'ie. https://www.instagram.com/kiokenfoodpics/', 'gutenbooster' ),
      ),
    ) );

$wp_customize->add_setting( 'youtube', array(
  'capability' => 'edit_theme_options',
  'sanitize_callback' => 'gutenbooster_sanitize_url',
) );

    $wp_customize->add_control( 'youtube', array(
      'type' => 'url',
      'section' => 'socials', // Add a default or your own section
      'label' => __( 'Youtube URL', 'gutenbooster' ),
      'description' => __( 'Enter your Youtube channel url in full', 'gutenbooster' ),
      'input_attrs' => array(
        'placeholder' => __( 'ie. https://www.youtube.com/channel/kiokentheme', 'gutenbooster' ),
      ),
    ) );

$wp_customize->add_setting( 'linkedin', array(
  'capability' => 'edit_theme_options',
  'sanitize_callback' => 'gutenbooster_sanitize_url',
) );

    $wp_customize->add_control( 'linkedin', array(
      'type' => 'url',
      'section' => 'socials', // Add a default or your own section
      'label' => __( 'Linkedin URL', 'gutenbooster' ),
      'description' => __( 'Enter your Linkedin channel url in full', 'gutenbooster' ),
      'input_attrs' => array(
        'placeholder' => __( 'ie. https://www.linkedin.com/in/michael', 'gutenbooster' ),
      ),
    ) );