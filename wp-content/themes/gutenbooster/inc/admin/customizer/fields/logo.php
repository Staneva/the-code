<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Enable/disable Page Banner
$wp_customize->add_setting( 'core_display_tagline', array(
	'capability' 		=> 'edit_theme_options',
	'default'			=> gutenbooster_getmod( 'core_display_tagline' ),
	'sanitize_callback' => 'gutenbooster_sanitize_checkbox'
) );

	$wp_customize->add_control( new Kioken_Customizer_Toggle_Control( $wp_customize, 'core_display_tagline', array(
		'label' 		=> __( 'Display tagline in logo area', 'gutenbooster' ),
		'section' 		=> 'title_tagline',
		'active_callback'=> 'gutenbooster_logo_is_text',
		'priority'		=> 10,
			'type'        => 'flat',// light, ios, flat
	) ) );

	// Logo Type
	$wp_customize->add_setting(
		'logo_type',
		array(
		  'capability' => 'edit_theme_options',
		  'default' => gutenbooster_getmod( 'logo_type' ),
		  'sanitize_callback' => 'gutenbooster_sanitize_radio',
		)
	);

		$wp_customize->add_control( 'logo_type', array(
		  'type' => 'radio',
		  'section' => 'logo',
			'priority' => 1,
		  'label' => __( 'Logo Type', 'gutenbooster' ),
		  'description' => __( 'If you choose text, your logo will be your site title and tagline', 'gutenbooster' ),
		  'choices' => array(
		    'image' => __( 'Image', 'gutenbooster' ),
		    'text' => __( 'Text', 'gutenbooster' ),
		  ),
		) );



	// Light Logo
	$wp_customize->add_setting( 'logo_light', array(
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'gutenbooster_sanitize_image',
	) );

		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_light',
				// $args
				array(
					'section'		=> 'logo',
					'priority'		=> 8,
					'mime_type'		=> 'image',
					'active_callback' => 'gutenbooster_logo_is_image',
					'label'			=> __(  'Logo Light', 'gutenbooster' ),
					'description'	=> __( 'Upload a light colored transparent logo that will show up when the header is transparent or on a dark background', 'gutenbooster' ),
				)
			)
		);