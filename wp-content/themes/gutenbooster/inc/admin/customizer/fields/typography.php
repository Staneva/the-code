<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// Body
$wp_customize->add_setting( 'typo_body',
	array(
		'default' => gutenbooster_defaults( 'typo_body' ),
		'sanitize_callback' => 'gutenbooster_google_font_sanitization'
	)
);
$wp_customize->add_control( new Kioken_Font_Control( $wp_customize, 'typo_body',
	array(
		'label' => __( 'Body Font', 'gutenbooster' ),
		'section' => 'typo_general',
		'priority' => 2,
		'input_attrs' => array(
			'font_count' => 50,
			'orderby' => 'popular',
		),
	)
) );










// $wp_customize->add_setting( 'my_control_radio_buttonset', [
// 		'type'              => 'theme_mod',
// 		'capability'        => 'edit_theme_options',
// 		'default'           => 'option-1',
// 		'transport'         => 'refresh', // Or postMessage.
//         'sanitize_callback' => 'gutenbooster_sanitize_radio',
// 	] );
// 	$wp_customize->add_control( new Kioken_Radio_Button_Control( $wp_customize, 'my_control_radio_buttonset', [
// 			'label'   => esc_html__( 'My Radio-Buttonset Control', 'theme_textdomain' ),
// 	        'section' => 'typo_general',
// 	        'choices' => [
// 				'option-1' => esc_html__( 'Option 1', 'theme_textdomain' ),
// 				'option-2' => esc_html__( 'Option 2', 'theme_textdomain' ),
// 			],
// 		] ) );










$wp_customize->add_setting( 'typobody_lh', array(
  	'capability' => 'edit_theme_options',
	'transport' => 'postMessage',
  	'sanitize_callback' => 'gutenbooster_sanitize_responsive_range_value',
) );

	$wp_customize->add_control(
		new Kioken_Responsive_Range_Control(
			$wp_customize, 'typobody_lh', array(
				'label'    => __( 'Line Height ', 'gutenbooster' ),
				'section' => 'typo_general',
				'type' => 'range-value',
				'media_query' => false,
				'input_attr' => array(
					'desktop' => array(
						'min' => 0.1,
						'max' => 5,
						'step' => 0.1,
						'default_value' => 1.8,
					),
				),
			)
		)
	);


	$wp_customize->add_setting(
        'body_fontsize', array(
            'sanitize_callback' => 'gutenbooster_sanitize_responsive_range_value',
			'transport' => 'postMessage',
        )
    );

	    $wp_customize->add_control(
	        new Kioken_Responsive_Range_Control(
	            $wp_customize, 'body_fontsize', array(
	                'label'    => __( 'Body Font Size (px)', 'gutenbooster' ),
	                'section' => 'typo_general',
	                'type' => 'range-value',
	                'media_query' => true,
					'input_attr' => array(
	                    'mobile' => array(
							'min' => 12,
	                        'max' => 48,
	                        'step' => 1,
							'default_value' => '',
	                    ),
	                    'tablet' => array(
							'min' => 12,
	                        'max' => 48,
	                        'step' => 1,
							'default_value' => '',
	                    ),
	                    'desktop' => array(
							'min' => 12,
	                        'max' => 48,
	                        'step' => 1,
	                        'default_value' => 18,
	                    ),
	                ),
	            )
	        )
	    );


// Headings
$wp_customize->add_setting( 'typo_headings',
	array(
		'default' => gutenbooster_defaults( 'typo_headings' ),
		'sanitize_callback' => 'gutenbooster_google_font_sanitization'
	)
);
$wp_customize->add_control( new Kioken_Font_Control( $wp_customize, 'typo_headings',
	array(
		'label' => __( 'Headings Font', 'gutenbooster' ),
		'section' => 'typo_general',
		'input_attrs' => array(
			'font_count' => 50,
			'orderby' => 'popular',
		),
	)
) );





