<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

	$wp_customize->add_setting( 'override_colors', array(
		'capability' 		=> 'edit_theme_options',
		'default'			=> gutenbooster_getmod( 'override_colors' ),
		'sanitize_callback' => 'gutenbooster_sanitize_checkbox'
	) );

		$wp_customize->add_control( new Kioken_Customizer_Toggle_Control( $wp_customize, 'override_colors', array(
			'label' 		=> __( 'Override Theme Colors', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 10,
				'type'        => 'flat',// light, ios, flat
		) ) );

	// Primary Color
	$wp_customize->add_setting( 'color_gb_primary', array(
		'default' 			=> gutenbooster_palette('primary'),
		'type' 				=> 'theme_mod',
		'transport' 		=> 'postMessage',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_primary', array(
			'label' 		=> __( 'Main Accent Color', 'gutenbooster' ),
			'description'	=> __( 'Applies to links, main link/menu hovers, buttons etc.', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 11,
			'settings' 		=> 'color_gb_primary',
			'active_callback' => 'gutenbooster_check_override_colors',

		) ) );

	// Secondary Color
	$wp_customize->add_setting( 'color_gb_secondary', array(
		'default' 			=> gutenbooster_palette('secondary'),
		'type' 				=> 'theme_mod',
		'transport' 		=> 'postMessage',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );


		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_secondary', array(
			'label'       => esc_html__( 'Secondary Accent Color', 'gutenbooster' ),
			'description' => esc_html__( 'Applies to headings, contents that need dark colors, some hovers etc. Suggestion: Use a contrast tone.', 'gutenbooster' ),
			'priority'		=> 12,
			'section' 		=> 'main_colors',
			'settings' 		=> 'color_gb_secondary',
			'active_callback' => 'gutenbooster_check_override_colors',

		) ) );

	// Tertiary - Body Content Color
	$wp_customize->add_setting( 'color_gb_tertiary', array(
		'default' 			=> gutenbooster_palette('tertiary'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );


		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_tertiary', array(
			'label' 		=> __( 'Body Content Color', 'gutenbooster' ),
			'description' => esc_html__( 'Applies to everywhere else. Gutenberg editor color name: Tertiary', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 13,
			'settings' 		=> 'color_gb_tertiary',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );

	// Custom One
	$wp_customize->add_setting( 'color_gb_one', array(
		'default' 			=> gutenbooster_palette('customone'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );


		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_one', array(
			'label'       => esc_html__( 'Custom Color 1 (optional)', 'gutenbooster' ),
			'description' => esc_html__( 'Gutenberg editor color name: Custom 1', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 14,
			'settings' 		=> 'color_gb_one',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );

	// Custom Two
	$wp_customize->add_setting( 'color_gb_two', array(
		'default' 			=> gutenbooster_palette('customtwo'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );


		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_two', array(
			'label'       => esc_html__( 'Custom Color 2 (optional)', 'gutenbooster' ),
			'description' => esc_html__( 'Gutenberg editor color name: Custom 2', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 14,
			'settings' 		=> 'color_gb_two',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );

	// Custom Three
	$wp_customize->add_setting( 'color_gb_three', array(
		'default' 			=> gutenbooster_palette('customthree'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );


		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_three', array(
			'label'       => esc_html__( 'Custom Color 3 (optional)', 'gutenbooster' ),
			'description' => esc_html__( 'Gutenberg editor color name: Custom 3', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 14,
			'settings' 		=> 'color_gb_three',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );

	// Custom Four
	$wp_customize->add_setting( 'color_gb_four', array(
		'default' 			=> gutenbooster_palette('customfour'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );


		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_four', array(
			'label'       => esc_html__( 'Custom Color 4 (optional)', 'gutenbooster' ),
			'description' => esc_html__( 'Gutenberg editor color name: Custom 4', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 14,
			'settings' 		=> 'color_gb_four',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );

	// Custom Five
	$wp_customize->add_setting( 'color_gb_five', array(
		'default' 			=> gutenbooster_palette('customfive'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );


		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_five', array(
			'label'       => esc_html__( 'Custom Color 5 (optional)', 'gutenbooster' ),
			'description' => esc_html__( 'Gutenberg editor color name: Custom 5', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 14,
			'settings' 		=> 'color_gb_five',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );

	// Custom Six
	$wp_customize->add_setting( 'color_gb_six', array(
		'default' 			=> gutenbooster_palette('customsix'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );


		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_six', array(
			'label'       => esc_html__( 'Custom Color 6 (optional)', 'gutenbooster' ),
			'description' => esc_html__( 'Gutenberg editor color name: Custom 6', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 14,
			'settings' 		=> 'color_gb_six',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );

	// Custom Seven
	$wp_customize->add_setting( 'color_gb_seven', array(
		'default' 			=> gutenbooster_palette('customseven'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_seven', array(
			'label'       => esc_html__( 'Custom Color 7 (optional)', 'gutenbooster' ),
			'description' => esc_html__( 'Gutenberg editor color name: Custom 7', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 14,
			'settings' 		=> 'color_gb_seven',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );

	// Custom Eight
	$wp_customize->add_setting( 'color_gb_eight', array(
		'default' 			=> gutenbooster_palette('customeight'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_eight', array(
			'label'       => esc_html__( 'Custom Color 8 (optional)', 'gutenbooster' ),
			'description' => esc_html__( 'Gutenberg editor color name: Custom 8', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 14,
			'settings' 		=> 'color_gb_eight',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );

	// Custom Nine
	$wp_customize->add_setting( 'color_gb_nine', array(
		'default' 			=> gutenbooster_palette('customnine'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_nine', array(
			'label'       => esc_html__( 'Custom Color 9 (optional)', 'gutenbooster' ),
			'description' => esc_html__( 'Gutenberg editor color name: Custom 9', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 14,
			'settings' 		=> 'color_gb_nine',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );

	// Custom Ten
	$wp_customize->add_setting( 'color_gb_ten', array(
		'default' 			=> gutenbooster_palette('customten'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_ten', array(
			'label'       => esc_html__( 'Custom Color 10 (optional)', 'gutenbooster' ),
			'description' => esc_html__( 'Gutenberg editor color name: Custom 10', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 14,
			'settings' 		=> 'color_gb_ten',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );

	// Custom Eleven
	$wp_customize->add_setting( 'color_gb_eleven', array(
		'default' 			=> gutenbooster_palette('customeleven'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_eleven', array(
			'label'       => esc_html__( 'Custom Color 11 (optional)', 'gutenbooster' ),
			'description' => esc_html__( 'Gutenberg editor color name: Custom 11', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 14,
			'settings' 		=> 'color_gb_eleven',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );

	// Custom Twelve
	$wp_customize->add_setting( 'color_gb_twelve', array(
		'default' 			=> gutenbooster_palette('customtwelve'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_twelve', array(
			'label'       => esc_html__( 'Custom Color 12 (optional)', 'gutenbooster' ),
			'description' => esc_html__( 'Gutenberg editor color name: Custom 12', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 14,
			'settings' 		=> 'color_gb_twelve',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );

	// Custom Thirteen
	$wp_customize->add_setting( 'color_gb_thirteen', array(
		'default' 			=> gutenbooster_palette('customthirteen'),
		'type' 				=> 'theme_mod',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_gb_thirteen', array(
			'label'       => esc_html__( 'Custom Color 13 (optional)', 'gutenbooster' ),
			'description' => esc_html__( 'Gutenberg editor color name: Custom 13', 'gutenbooster' ),
			'section' 		=> 'main_colors',
			'priority'		=> 14,
			'settings' 		=> 'color_gb_thirteen',
			'active_callback' => 'gutenbooster_check_override_colors',
		) ) );


/*=============================================>>>>>
= Section comment block =
===============================================>>>>>*/

//Link Color
$wp_customize->add_setting( 'menu_link_color',
	array(
		'sanitize_callback' => 'gutenbooster_hex_rgba_sanitization',
	)
);
	$wp_customize->add_control( new Kioken_Customizer_Alpha_Color_Control( $wp_customize, 'menu_link_color',
		array(
			'label'           => esc_html__( 'Menu link color', 'gutenbooster' ),
			'section' => 'menu_colors',
			'show_opacity' => true,
		)
	) );
//Link Hover Color
$wp_customize->add_setting( 'menu_link_hover_color',
	array(
		'sanitize_callback' => 'gutenbooster_hex_rgba_sanitization',
	)
);
	$wp_customize->add_control( new Kioken_Customizer_Alpha_Color_Control( $wp_customize, 'menu_link_hover_color',
		array(
			'label'           => esc_html__( 'Menu link hover color', 'gutenbooster' ),
			'section' => 'menu_colors',
			'show_opacity' => true,
		)
	) );














