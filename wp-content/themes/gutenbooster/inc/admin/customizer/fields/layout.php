<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// Content Top Padding
$wp_customize->add_setting( 'content_toppad', array(
	'capability' 		=> 'edit_theme_options',
	'transport' 		=> 'postMessage',
	'default'			=> gutenbooster_getmod( 'content_toppad', 0 ),
	'sanitize_callback' => 'gutenbooster_sanitize_responsive_range_value'
) );

	$wp_customize->add_control( new Kioken_Responsive_Range_Control( $wp_customize, 'content_toppad', array(
		// 'type'     => 'range-value-std',
		'section'  => 'layout_general',
		'type' => 'range-value',
		'media_query' => false,
		'label'           => esc_html__( 'Content Top Spacing', 'gutenbooster' ),
		'description'     => esc_html__( 'Change the top padding of the content container. Works best with Page Banner disabled.', 'gutenbooster' ),
		'input_attr' => array(
			'desktop' => array(
				'min' => 0,
				'max' => 1000,
				'step' => 5,
				'default_value' => 0,
			),
		),
	) ) );

$wp_customize->add_setting( 'layout_default',
	array(
		'default'			=> gutenbooster_getmod('layout_default'),
		'sanitize_callback' => 'gutenbooster_sanitize_text'
	)
);
$wp_customize->add_control( new Kioken_Customizer_Image_Radio_Button( $wp_customize, 'layout_default',
	array(
		'label'       => esc_html__( 'Default Layout', 'gutenbooster' ),
		'description' => esc_html__( 'Global layout setting of blog and other pages', 'gutenbooster' ),
		'section' => 'layout_general',
		'choices' => array(
			'no-sidebar' => array(  // Required. Value for this particular radio button choice
				'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/sidebars/empty.svg', // Required. URL for the image
				'name' => __( 'No Sidebar', 'gutenbooster' )
			),
			'sb-right' => array(  // Required. Value for this particular radio button choice
				'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/sidebars/sb-right.svg', // Required. URL for the image
				'name' => __( 'Right Sidebar', 'gutenbooster' )
			),
			'sb-left' => array(  // Required. Value for this particular radio button choice
				'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/sidebars/sb-left.svg', // Required. URL for the image
				'name' => __( 'Left Sidebar', 'gutenbooster' )
			),
		)
	)
) );


$wp_customize->add_setting( 'layout_post',
	array(
		'default'			=> gutenbooster_getmod('layout_post'),
		'sanitize_callback' => 'gutenbooster_sanitize_text'
	)
);
$wp_customize->add_control( new Kioken_Customizer_Image_Radio_Button( $wp_customize, 'layout_post',
	array(
		'label'       => esc_html__( 'Post Layout', 'gutenbooster' ),
		'description' => esc_html__( 'Default layout of single post', 'gutenbooster' ),
		'section' => 'layout_general',
		'choices' => array(
			'no-sidebar' => array(  // Required. Value for this particular radio button choice
				'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/sidebars/empty.svg', // Required. URL for the image
				'name' => __( 'No Sidebar', 'gutenbooster' )
			),
			'sb-right' => array(  // Required. Value for this particular radio button choice
				'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/sidebars/sb-right.svg', // Required. URL for the image
				'name' => __( 'Right Sidebar', 'gutenbooster' )
			),
			'sb-left' => array(  // Required. Value for this particular radio button choice
				'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/sidebars/sb-left.svg', // Required. URL for the image
				'name' => __( 'Left Sidebar', 'gutenbooster' )
			),
		)
	)
) );

// Page Layout
$wp_customize->add_setting( 'layout_page',
	array(
		'default'			=> gutenbooster_getmod('layout_page'),
		'sanitize_callback' => 'gutenbooster_sanitize_text'
	)
);
$wp_customize->add_control( new Kioken_Customizer_Image_Radio_Button( $wp_customize, 'layout_page',
	array(
		'label'       => esc_html__( 'Page Layout', 'gutenbooster' ),
		'description' => esc_html__( 'Default layout of pages', 'gutenbooster' ),
		'section' => 'layout_general',
		'choices' => array(
			'no-sidebar' => array(  // Required. Value for this particular radio button choice
				'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/sidebars/empty.svg', // Required. URL for the image
				'name' => __( 'No Sidebar', 'gutenbooster' )
			),
			'sb-right' => array(  // Required. Value for this particular radio button choice
				'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/sidebars/sb-right.svg', // Required. URL for the image
				'name' => __( 'Right Sidebar', 'gutenbooster' )
			),
			'sb-left' => array(  // Required. Value for this particular radio button choice
				'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/sidebars/sb-left.svg', // Required. URL for the image
				'name' => __( 'Left Sidebar', 'gutenbooster' )
			),
		)
	)
) );


// Shop Layout
if ( gutenbooster_is_woocommerce_on() ) {
	$wp_customize->add_setting( 'layout_shop',
		array(
			'default'			=> gutenbooster_getmod('layout_shop'),
			'sanitize_callback' => 'gutenbooster_sanitize_text'
		)
	);
	$wp_customize->add_control( new Kioken_Customizer_Image_Radio_Button( $wp_customize, 'layout_shop',
		array(
			'label'       => esc_html__( 'Shop Catalog Layout', 'gutenbooster' ),
			'description' => esc_html__( 'Default layout of Woocommerce Shop Catalog', 'gutenbooster' ),
			'section' => 'layout_general',
			'choices' => array(
				'no-sidebar' => array(  // Required. Value for this particular radio button choice
					'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/sidebars/empty.svg', // Required. URL for the image
					'name' => __( 'No Sidebar', 'gutenbooster' )
				),
				'sb-right' => array(  // Required. Value for this particular radio button choice
					'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/sidebars/sb-right.svg', // Required. URL for the image
					'name' => __( 'Right Sidebar', 'gutenbooster' )
				),
				'sb-left' => array(  // Required. Value for this particular radio button choice
					'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/sidebars/sb-left.svg', // Required. URL for the image
					'name' => __( 'Left Sidebar', 'gutenbooster' )
				),
			)
		)
	) );
}

// Layout Content Width
$wp_customize->add_setting( 'layout_content_width', array(
	'capability' 		=> 'edit_theme_options',
	'transport' 		=> 'postMessage',
	'default'			=> gutenbooster_getmod('layout_content_width'),
	'sanitize_callback' => 'gutenbooster_sanitize_number_range'
) );

	$wp_customize->add_control( new Kioken_Customizer_Range_Value_Control( $wp_customize, 'layout_content_width', array(
		'type'     => 'range-value-std',
		'section'  => 'layout_single',
		'label'           => esc_html__( 'No Sidebar Content Max Width', 'gutenbooster' ),
		'description'     => __( 'Set the content maximum width for page and posts when there\'s no sidebar. Browse through a page or post with no sidebar to preview the changes. Also applies to Gutenberg Editor.', 'gutenbooster' ),
		'input_attrs' => array(
			'min'    => 550,
			'max'    => 1600,
			'step'   => 10,
			'suffix' => ' px',
			),
	) ) );





	// Top Bottom Margin of Blocks
	$wp_customize->add_setting( 'units_topbottom_margin', array(
		'capability' 		=> 'edit_theme_options',
		'default'			=> gutenbooster_getmod('units_topbottom_margin'),
		'sanitize_callback' => 'gutenbooster_sanitize_number_range'
	) );

		$wp_customize->add_control( new Kioken_Customizer_Range_Value_Control( $wp_customize, 'units_topbottom_margin', array(
			'type'     => 'range-value-std',
			'section'  => 'layout_single',
			'label'           => esc_html__( 'Elements Top & Bottom Margin', 'gutenbooster' ),
			'description'     => esc_html__( 'Change the top and bottom margin of the content elements.', 'gutenbooster' ),
			'input_attrs' => array(
				'min'    => 0,
				'max'    => 1000,
				'step'   => 5,
				'suffix' => ' px',
				),
		) ) );

