<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

	// Enable/disable Page Banner
	$wp_customize->add_setting( 'enable_page_banner', array(
		'capability' 		=> 'edit_theme_options',
		'default'			=> gutenbooster_getmod( 'enable_page_banner' ),
		'sanitize_callback' => 'gutenbooster_sanitize_checkbox'
	) );

		$wp_customize->add_control( new Kioken_Customizer_Toggle_Control( $wp_customize, 'enable_page_banner', array(
			'label' 		=> __( 'Show Page Banner', 'gutenbooster' ),
			'section' 		=> 'page_banner',
			'priority'		=> 1,
				'type'        => 'flat',// light, ios, flat
		) ) );


	// Page Banner Background
	$wp_customize->add_setting( 'page_banner_bgcolor', array(
		'default'			=> gutenbooster_getmod( 'page_banner_bgcolor' ),
		'type' 				=> 'theme_mod',
		'transport' 		=> 'postMessage',
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_banner_bgcolor', array(
			'label' 		=> __( 'Background Color', 'gutenbooster' ),
			'description'	=> __( 'The default background color for page banner', 'gutenbooster' ),
			'section' 		=> 'page_banner',
			'priority'		=> 2,
			'settings' 		=> 'page_banner_bgcolor',
			'active_callback' => 'gutenbooster_check_page_banner',

		) ) );

	// Page Banner background image setting
	$wp_customize->add_setting( 'page_banner_bg', array(
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'sanitize_callback' => 'gutenbooster_sanitize_image',
	) );

		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'page_banner_bg',
				// $args
				array(
					'section'		=> 'page_banner',
					'priority'		=> 3,
					'mime_type'		=> 'image',
					'label'			=> __(  'Background Image', 'gutenbooster' ),
					'description'	=> __( 'The default background image for page banner', 'gutenbooster' ),
					'active_callback' => 'gutenbooster_check_page_banner'
				)
			)
		);

	// Background Opacity
	$wp_customize->add_setting( 'page_bannerbg_opacity', array(
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'			=> gutenbooster_getmod( 'page_bannerbg_opacity' ),
		'sanitize_callback' => 'gutenbooster_sanitize_number_range'
	) );

		$wp_customize->add_control( new Kioken_Customizer_Range_Value_Control( $wp_customize, 'page_bannerbg_opacity', array(
			'type'     => 'range-value-std',
			'section'  => 'page_banner',
			'label'    => __( 'Background Opacity', 'gutenbooster' ),
			'active_callback' => 'gutenbooster_check_page_banner',
			'input_attrs' => array(
				'min'    => 0.1,
				'max'    => 1,
				'step'   => 0.05,
				),
		) ) );

	// Height
	$wp_customize->add_setting( 'page_banner_height', array(
		'capability' 		=> 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'			=> gutenbooster_getmod( 'page_banner_height' ),
		'sanitize_callback' => 'gutenbooster_sanitize_number_range'
	) );

		$wp_customize->add_control( new Kioken_Customizer_Range_Value_Control( $wp_customize, 'page_banner_height', array(
			'type'     => 'range-value-std',
			'section'  => 'page_banner',
			'label'    => __( 'Page Banner Height', 'gutenbooster' ),
			'description'     => esc_html__( 'Set the default responsive height of the page banner. default is 30vh (100 means fullscreen)', 'gutenbooster' ),
			'active_callback' => 'gutenbooster_check_page_banner',
			'input_attrs' => array(
				'min'    => 25,
				'max'    => 100,
				'step'   => 5,
				'suffix' => 'vh'
				),
		) ) );

	// Page Banner Text color
	$wp_customize->add_setting( 'page_banner_text_color', array(
		'capability' => 'edit_theme_options',
		'transport' 		=> 'postMessage',
		'default'			=> gutenbooster_getmod( 'page_banner_text_color' ),
		'sanitize_callback' => 'gutenbooster_sanitize_radio',
	) );

		$wp_customize->add_control( 'page_banner_text_color', array(
			'type' => 'radio',
			'section' => 'page_banner',
			'active_callback' => 'gutenbooster_check_page_banner',
			'label' => __( 'Text Color', 'gutenbooster' ),
			'choices' => array(
					'dark'  => __( 'Dark', 'gutenbooster' ),
					'white' => __( 'White', 'gutenbooster' ),
				),
		) );