<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Enable/disable Footer Widgets
$wp_customize->add_setting( 'footer_widgets', array(
	'capability' 		=> 'edit_theme_options',
	'default' => gutenbooster_getmod( 'footer_widgets' ),
	'sanitize_callback' => 'gutenbooster_sanitize_checkbox'
) );

	$wp_customize->add_control( new Kioken_Customizer_Toggle_Control( $wp_customize, 'footer_widgets', array(
		'label'       => esc_html__( 'Display Footer Widgets', 'gutenbooster' ),
		'description' => esc_html__( 'Display sidebar widgets on footer', 'gutenbooster' ),
		'section' 		=> 'footer_structure',
		'priority'		=> 1,
			'type'        => 'flat',
	) ) );

// Footer Widgets Layout
$wp_customize->add_setting( 'footer_widgets_layout', array(
	'capability' => 'edit_theme_options',
	'default' => gutenbooster_getmod( 'footer_widgets_layout' ),
	'sanitize_callback' => 'gutenbooster_sanitize_radio',
) );

$wp_customize->add_control( 'footer_widgets_layout', array(
	'label'           => esc_html__( 'Footer Widgets Layout', 'gutenbooster' ),
	'description'     => esc_html__( 'Select number of columns for displaying widgets', 'gutenbooster' ),
	'type' => 'radio',
	'priority'		=> 2,
	'section' => 'footer_structure',
	'active_callback' => 'gutenbooster_check_widgetized_footer',
	'choices' => array(
		'2-columns'       => esc_html__( '2 Columns', 'gutenbooster' ),
		'3-columns'       => esc_html__( '3 Columns', 'gutenbooster' ),
		'4-columns'       => esc_html__( '4 Columns', 'gutenbooster' ),
		),
) );


// Enable/disable Footer Widgets
$wp_customize->add_setting( 'footer_align_links_left', array(
	'capability' 		=> 'edit_theme_options',
	'default' => gutenbooster_getmod( 'footer_align_links_left' ),
	'sanitize_callback' => 'gutenbooster_sanitize_checkbox'
) );

	$wp_customize->add_control( new Kioken_Customizer_Toggle_Control( $wp_customize, 'footer_align_links_left', array(
		'label'       => esc_html__( 'Align widget links to the left', 'gutenbooster' ),
		'section' 		=> 'footer_structure',
		'active_callback' => 'gutenbooster_check_widgetized_footer',
		'priority'		=> 2,
			'type'        => 'flat',
	) ) );

// Footer MaxPad
$wp_customize->add_setting( 'footer_maxpad', array(
	'capability' 		=> 'edit_theme_options',
	'default' => gutenbooster_getmod( 'footer_maxpad' ),
	'sanitize_callback' => 'gutenbooster_sanitize_checkbox'
) );

	$wp_customize->add_control( new Kioken_Customizer_Toggle_Control( $wp_customize, 'footer_maxpad', array(
		'label'       => esc_html__( 'Larger Column Gap', 'gutenbooster' ),
		'description' => esc_html__( 'Once enabled, footer widget columns will gain more space between each other.', 'gutenbooster' ),
		'section' 		=> 'footer_structure',
		'active_callback' => 'gutenbooster_check_widgetized_footer',
		'priority'		=> 3,
			'type'        => 'flat',
	) ) );

// Footer Top Bottom Padding
$wp_customize->add_setting( 'footer_pad', array(
	'capability' 		=> 'edit_theme_options',
	'default' => gutenbooster_getmod( 'footer_pad' ),
	'sanitize_callback' => 'gutenbooster_sanitize_number_range'
) );

	$wp_customize->add_control( new Kioken_Customizer_Range_Value_Control( $wp_customize, 'footer_pad', array(
		'type'     => 'range-value-std',
		'priority'		=> 4,
		'section'  => 'footer_structure',
		'label'   => esc_html__( 'Footer Top/Bottom Padding', 'gutenbooster' ),
		'input_attrs' => array(
			'min'    => 0,
			'max'    => 200,
			'step'   => 5,
			'suffix' => ' px'
			),
	) ) );

$wp_customize->add_setting( 'footer_wrapper', array(
	'capability' 		=> 'edit_theme_options',
	'default' => gutenbooster_getmod( 'footer_wrapper' ),
	'sanitize_callback' => 'gutenbooster_sanitize_select',
) );

	$wp_customize->add_control( 'footer_wrapper', array(
		'type'			=> 'select',
		'priority'		=> 5,
		'section' 		=> 'footer_structure',
		'label'       => esc_html__( 'Footer Wrapper', 'gutenbooster' ),
		'description' => esc_html__( 'Select the width of footer wrapper', 'gutenbooster' ),
		'choices' 		=> array(
			'container-fluid' => esc_html__( 'Full Width', 'gutenbooster' ),
			'container'    => esc_html__( 'Wrapped', 'gutenbooster' ),
		),
	) );

$wp_customize->add_setting( 'footer_text_left', array(
  'capability' => 'edit_theme_options',
  'default' => gutenbooster_getmod( 'footer_text_left' ),
  'sanitize_callback' => 'sanitize_text_field',
) );

	$wp_customize->add_control( 'footer_text_left', array(
	  'type' => 'textarea',
		'priority'		=> 6,
	  	'section' => 'footer_structure',
		'label'       => esc_html__( 'Footer Left Text', 'gutenbooster' ),
	) );

	$wp_customize->add_setting( 'footer_bg_color',
		array(
			'default' => gutenbooster_getmod( 'footer_bg_color' ),
			'sanitize_callback' => 'gutenbooster_hex_rgba_sanitization',
		)
	);
		$wp_customize->add_control( new Kioken_Customizer_Alpha_Color_Control( $wp_customize, 'footer_bg_color',
			array(
				'label'           => esc_html__( 'Background Color', 'gutenbooster' ),
				'section' => 'footer_style',
				'show_opacity' => true, // Optional. Show or hide the opacity value on the opacity slider handle. Default: true
			)
		) );

	$wp_customize->add_setting( 'footer_txt_color',
		array(
			'default' => '',
			'sanitize_callback' => 'gutenbooster_hex_rgba_sanitization',
		)
	);
	$wp_customize->add_control( new Kioken_Customizer_Alpha_Color_Control( $wp_customize, 'footer_txt_color',
		array(
			'label'           => esc_html__( 'Text Color', 'gutenbooster' ),
			'section' => 'footer_style',
			'show_opacity' => true, // Optional. Show or hide the opacity value on the opacity slider handle. Default: true
		)
	) );

	$wp_customize->add_setting( 'footer_link_color',
		array(
			'default' => '',
			'sanitize_callback' => 'gutenbooster_hex_rgba_sanitization',
		)
	);
	$wp_customize->add_control( new Kioken_Customizer_Alpha_Color_Control( $wp_customize, 'footer_link_color',
		array(
			'label'           => esc_html__( 'Links Color', 'gutenbooster' ),
			'section' => 'footer_style',
			'show_opacity' => true, // Optional. Show or hide the opacity value on the opacity slider handle. Default: true
		)
	) );

	$wp_customize->add_setting( 'footer_link_hover_color',
		array(
			'default' => '',
			'sanitize_callback' => 'gutenbooster_hex_rgba_sanitization',
		)
	);
	$wp_customize->add_control( new Kioken_Customizer_Alpha_Color_Control( $wp_customize, 'footer_link_hover_color',
		array(
			'label'           => esc_html__( 'Links Hover Color', 'gutenbooster' ),
			'section' => 'footer_style',
			'show_opacity' => true, // Optional. Show or hide the opacity value on the opacity slider handle. Default: true
		)
	) );

	// Background Image
	$wp_customize->add_setting( 'footer_bg_image', array(
		'capability' 		=> 'edit_theme_options',
		'sanitize_callback' => 'gutenbooster_sanitize_image',
	) );

		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_bg_image',
				// $args
				array(
					'section'		=> 'footer_style',
					'mime_type'		=> 'image',
					'label'           => esc_html__( 'Background Image', 'gutenbooster' ),
				)
			)
		);

	// Footer Top Bottom Padding
	$wp_customize->add_setting( 'footer_bg_opacity', array(
		'capability' 		=> 'edit_theme_options',
		'default' => gutenbooster_getmod( 'footer_bg_opacity' ),
		'sanitize_callback' => 'gutenbooster_sanitize_number_range'
	) );

		$wp_customize->add_control( new Kioken_Customizer_Range_Value_Control( $wp_customize, 'footer_bg_opacity', array(
			'type'     => 'range-value-std',
			'section'  => 'footer_style',
			'label'           => esc_html__( 'Background Image Opacity', 'gutenbooster' ),
			'input_attrs' => array(
				'min'    => 0,
				'max'    => 1,
				'step'   => .05,
				),
		) ) );






















