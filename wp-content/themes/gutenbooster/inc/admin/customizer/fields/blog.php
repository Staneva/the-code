<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}




		//Excerpt Length
		$wp_customize->add_setting( 'blog_excerpt_length', array(
			'capability' 		=> 'edit_theme_options',
			'default'			=> gutenbooster_getmod( 'blog_excerpt_length' ),
			'sanitize_callback' => 'gutenbooster_sanitize_number_range'
		) );

			$wp_customize->add_control( new Kioken_Customizer_Range_Value_Control( $wp_customize, 'blog_excerpt_length', array(
				'type'     => 'range-value-std',
				'section'  => 'blog_general',
				'label'   => esc_html__( 'Excerpt Length', 'gutenbooster' ),
				'input_attrs' => array(
					'min'    => 10,
					'max'    => 200,
					'step'   => 5,
					),
			) ) );

		// disabling page banner for blog singles
		$wp_customize->add_setting( 'disable_blog_pb', array(
			'capability' 		=> 'edit_theme_options',
			'default'			=> gutenbooster_getmod( 'disable_blog_pb' ),
			'sanitize_callback' => 'gutenbooster_sanitize_checkbox'
		) );

		$wp_customize->add_control( new Kioken_Customizer_Toggle_Control( $wp_customize, 'disable_blog_pb', array(
			'label'   => esc_html__( 'Hide Page Banner for Posts ', 'gutenbooster' ),
			'description' => esc_html__( 'Turn off showing page banner in blog single pages', 'gutenbooster' ),
			'section' 		=> 'blog_single',
			'active_callback' => 'gutenbooster_check_page_banner',
			'type'        => 'flat',// light, ios, flat
		) ) );

		// disabling page banner for blog singles
		$wp_customize->add_setting( 'single_show_author', array(
			'capability' 		=> 'edit_theme_options',
			'default'			=> gutenbooster_getmod( 'single_show_author' ),
			'sanitize_callback' => 'gutenbooster_sanitize_checkbox'
		) );

		$wp_customize->add_control( new Kioken_Customizer_Toggle_Control( $wp_customize, 'single_show_author', array(
			'label'   => esc_html__( 'Show Author Box', 'gutenbooster' ),
			'description' => esc_html__( 'Displays author information at the end of post', 'gutenbooster' ),
			'section' 		=> 'blog_single',
			'type'        => 'flat',// light, ios, flat
		) ) );
