<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}





$wp_customize->add_setting( 'header_layout',
	array(
		'default' => gutenbooster_getmod( 'header_layout' ),
		'sanitize_callback' => 'gutenbooster_sanitize_text'
	)
);
$wp_customize->add_control( new Kioken_Customizer_Image_Radio_Button( $wp_customize, 'header_layout',
	array(
		'label'         => esc_html__( 'Header Layout', 'gutenbooster' ),
		'description'   => esc_html__( 'If you choose a logo center aligned layout, you will need to add menus to Primary(left) and Secondary(right) menu areas. And it is best to keep the number of menu items for each side not more than 4 for each', 'gutenbooster' ),
		'section'       => 'header_l',
		'priority'      => 1,
		'choices'       => array(
			'v1' => array(  // Required. Value for this particular radio button choice
				'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/header/v1.svg', // Required. URL for the image
				'name' => __( 'Centered', 'gutenbooster' )
			),
			'v3' => array(
				'image' => trailingslashit( get_template_directory_uri() ) . '/assets/img/admin/customizer/header/v3.svg',
				'name' => __( 'Left Aligned', 'gutenbooster' )
			),
		)
	)
) );




// Header Wrapper
$wp_customize->add_setting( 'header_wrapper', array(
	'capability' 		=> 'edit_theme_options',
	'transport' => 'postMessage',
	'default' => gutenbooster_getmod( 'header_wrapper' ),
	'sanitize_callback' => 'gutenbooster_sanitize_select',
) );

	$wp_customize->add_control( 'header_wrapper', array(
		'type'			=> 'select',
		'section' 		=> 'header_l',
		'priority'		=> 2,
		'label'       => esc_html__( 'Header Wrapper', 'gutenbooster' ),
		'choices' 		=> array(
			'full-width' => esc_html__( 'Full Width', 'gutenbooster' ),
			'wrapped'    => esc_html__( 'Wrapped', 'gutenbooster' ),
		),
	) );


// Header Background
$wp_customize->add_setting( 'header_bg', array(
	'capability' 		=> 'edit_theme_options',
	'transport' => 'postMessage',
	'default' => gutenbooster_getmod( 'header_bg' ),
	'sanitize_callback' => 'gutenbooster_sanitize_select',
) );

	$wp_customize->add_control( 'header_bg', array(
		'type'			=> 'select',
		'section' 		=> 'header_l',
		'priority'		=> 3,
		'label'   		=> __( 'Header Background', 'gutenbooster'  ),
		'choices' 		=> array(
			'white'       => esc_html__( 'White', 'gutenbooster' ),
			'transparent' => esc_html__( 'Transparent (On top of page)', 'gutenbooster' ),
		),
	) );

$wp_customize->add_setting( 'header_txt_color', array(
	'capability' 		=> 'edit_theme_options',
	'transport' => 'postMessage',
	'default' => gutenbooster_getmod( 'header_txt_color' ),
	'sanitize_callback' => 'gutenbooster_sanitize_select',
) );

	$wp_customize->add_control( 'header_txt_color', array(
		'type'			=> 'select',
		'section' 		=> 'header_l',
		'priority'		=> 4,
		'active_callback' => 'gutenbooster_check_page_banner',
		'label'   		=> __( 'Header Text Color', 'gutenbooster'  ),
		'choices' 		=> array(
			'dark'       => esc_html__( 'Dark', 'gutenbooster' ),
			'white' => esc_html__( 'White', 'gutenbooster' ),
		),
	) );

// Height
$wp_customize->add_setting( 'header_topbottom_pad', array(
	'capability' 		=> 'edit_theme_options',
	'transport' => 'postMessage',
	'default' => gutenbooster_getmod( 'header_topbottom_pad' ),
	'sanitize_callback' => 'gutenbooster_sanitize_number_range'
) );

	$wp_customize->add_control( new Kioken_Customizer_Range_Value_Control( $wp_customize, 'header_topbottom_pad', array(
		'type'     => 'range-value-std',
		'section'  => 'header_l',
		'priority'		=> 5,
		'label'   => __( 'Header Top and Bottom Padding (px)', 'gutenbooster' ),
		'input_attrs' => array(
			'min'    => 0,
			'max'    => 100,
			'step'   => 1,
			'suffix' => 'px'
			),
	) ) );

// Header V3 Nav Pos
$wp_customize->add_setting( 'headerv3_nav_pos', array(
	'capability' 		=> 'edit_theme_options',
	'default' => gutenbooster_getmod( 'headerv3_nav_pos' ),
	'sanitize_callback' => 'gutenbooster_sanitize_select',
) );

	$wp_customize->add_control( 'headerv3_nav_pos', array(
		'type'			=> 'select',
		'section' 		=> 'header_l',
		'active_callback' => 'gutenbooster_check_headerv3',
		'label'   => esc_html__( 'Header Menu Position', 'gutenbooster' ),
		'choices' 		=> array(
			'left' => esc_html__( 'Left', 'gutenbooster' ),
			'center' => esc_html__( 'Center', 'gutenbooster' ),
			'right' => esc_html__( 'Right', 'gutenbooster' ),
		),
	) );

/*=============================================>>>>>
= Section: Additional Menu Elements =
===============================================>>>>>*/

	$wp_customize->add_setting( 'header_icons_left_v1',
		array(
			'default' => array(),
			'sanitize_callback' 	=> 'gutenbooster_sanitize_multi_choices',
		)
	);

		$wp_customize->add_control(
				new Kioken_Customizer_Sortable_Control(
						$wp_customize,
						'header_icons_left_v1',
						array(
								'section' => 'header_icons',
								'active_callback' 	=> 'gutenbooster_check_headerv1',
								'label'           => esc_html__( 'Left Side Icons in Header V1 (Active Header)', 'gutenbooster' ),
								'description'     => esc_html__( 'Click on the eye icon to show/hide the element. Make sure you enter your social link urls in the socials links section as well.', 'gutenbooster' ),
								'choices' => gutenbooster_customize_iconlist()
						)
				)
		);


	$wp_customize->add_setting( 'header_icons_right_v1',
		array(
			'default' => array(),
			'sanitize_callback' 	=> 'gutenbooster_sanitize_multi_choices',
		)
	);

		$wp_customize->add_control(
				new Kioken_Customizer_Sortable_Control(
						$wp_customize,
						'header_icons_right_v1',
						array(
								'section' => 'header_icons',
								'active_callback' 	=> 'gutenbooster_check_headerv1',
								'label'           => esc_html__( 'Right Side Icons in Header V1 (Active Header)', 'gutenbooster' ),
								'description'     => esc_html__( 'Click on the eye icon to show/hide the element. Make sure you enter your social link urls in the socials links section as well.', 'gutenbooster' ),
								'choices' => gutenbooster_customize_iconlist()
						)
				)
		);


	$wp_customize->add_setting( 'header_icons_left_v2',
		array(
			'default' => array(),
			'sanitize_callback' 	=> 'gutenbooster_sanitize_multi_choices',
		)
	);

		$wp_customize->add_control(
				new Kioken_Customizer_Sortable_Control(
						$wp_customize,
						'header_icons_left_v2',
						array(
								'section' => 'header_icons',
								'active_callback' 	=> 'gutenbooster_check_headerv2',
								'label'           => esc_html__( 'Left Side Icons in Header V2 (Active Header)', 'gutenbooster' ),
								'description'     => esc_html__( 'Click on the eye icon to show/hide the element. Make sure you enter your social link urls in the socials links section as well.', 'gutenbooster' ),
								'choices' => gutenbooster_customize_iconlist()
						)
				)
		);


	$wp_customize->add_setting( 'header_icons_right_v2',
		array(
			'default' => array(),
			'sanitize_callback' 	=> 'gutenbooster_sanitize_multi_choices',
		)
	);

		$wp_customize->add_control(
				new Kioken_Customizer_Sortable_Control(
						$wp_customize,
						'header_icons_right_v2',
						array(
								'section' => 'header_icons',
								'active_callback' 	=> 'gutenbooster_check_headerv2',
								'label'           => esc_html__( 'Right Side Icons in Header V2 (Active Header)', 'gutenbooster' ),
								'description'     => esc_html__( 'Click on the eye icon to show/hide the element. Make sure you enter your social link urls in the socials links section as well.', 'gutenbooster' ),
								'choices' => gutenbooster_customize_iconlist()
						)
				)
		);

	$wp_customize->add_setting( 'header_icons_right_v3',
		array(
			'default' => array(),
			'sanitize_callback' 	=> 'gutenbooster_sanitize_multi_choices',
		)
	);

    $wp_customize->add_control(
        new Kioken_Customizer_Sortable_Control(
            $wp_customize,
            'header_icons_right_v3',
            array(
                'section' => 'header_icons',
								'active_callback' 	=> 'gutenbooster_check_headerv3',
                'label'           => esc_html__( 'Right Side Elements in Header V3 (Active Header)', 'gutenbooster' ),
								'description'     => esc_html__( 'Click on the eye icon to show/hide the element. Make sure you enter your social link urls in the socials links section as well.', 'gutenbooster' ),
                'choices' => gutenbooster_customize_iconlist()
            )
        )
    );









