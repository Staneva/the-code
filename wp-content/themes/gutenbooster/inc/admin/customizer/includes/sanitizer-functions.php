<?php

/**
* GutenBooster theme customizer functions - Native
*
* @package GutenBooster
*/


/*=============================================>>>>>
= Sanitation Functions  =
===============================================>>>>>*/

// Sanitize boolean for checkbox
function gutenbooster_sanitize_checkbox( $checked ) {
	return ( ( isset( $checked ) && true == $checked ) ? true : false );
}

// Sanitize radio
function gutenbooster_sanitize_radio( $input, $setting ) {
	$input = sanitize_key( $input );
	$choices = $setting->manager->get_control( $setting->id )->choices;
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

// Sanitize select
function gutenbooster_sanitize_select( $input, $setting ) {
	$input = sanitize_key( $input );
	$choices = $setting->manager->get_control( $setting->id )->choices;
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

// Sanitize Number Range
function gutenbooster_sanitize_number_range( $number, $setting ) {

    $atts = $setting->manager->get_control( $setting->id )->input_attrs;
    $min = ( isset( $atts['min'] ) ? $atts['min'] : $number );
    $max = ( isset( $atts['max'] ) ? $atts['max'] : $number );
    $step = ( isset( $atts['step'] ) ? $atts['step'] : 0.001 );
		$number = floor( $number / $atts['step'] ) * $atts['step'];

		return ( $min <= $number && $number <= $max ) ? $number : $setting->default;
}

/**
 * Check if a string is in json format
 *
 * @param  string $string Input.
 *
 * @since 1.1.38
 * @return bool
 */
function gutenbooster_is_json( $string ) {
	return is_string( $string ) && is_array( json_decode( $string, true ) ) ? true : false;
}

/**
 * Sanitize values for range inputs.
 *
 * @param string $input Control input.
 *
 * @since 1.1.38
 * @return float
 */
function gutenbooster_sanitize_responsive_range_value( $input ) {
	if ( ! gutenbooster_is_json( $input ) ) {
		return floatval( $input );
	}
	$range_value            = json_decode( $input, true );
	$range_value['desktop'] = ! empty( $range_value['desktop'] ) || $range_value['desktop'] === '0' ? floatval( $range_value['desktop'] ) : '';
	$range_value['tablet']  = ! empty( $range_value['tablet'] ) || $range_value['tablet'] === '0' ? floatval( $range_value['tablet'] ) : '';
	$range_value['mobile']  = ! empty( $range_value['mobile'] ) || $range_value['mobile'] === '0' ? floatval( $range_value['mobile'] ) : '';

	return json_encode( $range_value );
}

function gutenbooster_sanitize_image( $image, $setting ) {
	/*
	 * Array of valid image file types.
	 *
	 * The array includes image mime types that are included in wp_get_mime_types()
	 */
    $mimes = array(
        'jpg|jpeg|jpe' => 'image/jpeg',
        'gif'          => 'image/gif',
        'png'          => 'image/png',
        'bmp'          => 'image/bmp',
        'tif|tiff'     => 'image/tiff',
        'ico'          => 'image/x-icon',
        'svg'          => 'image/svg'
    );
	// Return an array with file extension and mime_type.
    $file = wp_check_filetype( $image, $mimes );
	// If $image has a valid mime_type, return it; otherwise, return the default.
    return ( $file['ext'] ? $image : $setting->default );
}

/**
 * Sanitize Sharing Services
 * @since 0.1.0
 */
function gutenbooster_sanitize_iconlist( $input ){
	/* Var */
	$output = array();
	/* Get valid services */
	$valid_services = gutenbooster_customize_iconlist();
	/* Make array */
	$the_iconlist = explode( ',', $input );
	/* Bail. */
	if( ! $the_iconlist ){
		return null;
	}
	/* Loop and verify */
	foreach( $the_iconlist as $the_icon ){
		/* Separate service and status */
		$the_icon = explode( ':', $the_icon );
		if( isset( $the_icon[0] ) && isset( $the_icon[1] ) ){
			if( array_key_exists( $the_icon[0], $valid_services ) ){
				$status = $the_icon[1] ? '1' : '0';
				$output[] = trim( $the_icon[0] . ':' . $status );
			}
		}
	}
	return trim( esc_attr( implode( ',', $output ) ) );
}
/**
 * Sanitize Post Types
 * @since 0.1.0
 */
function gutenbooster_sanitize_post_types( $input ){
	/* Var */
	$output = array();
	/* Make input as array */
	$post_types = explode( ',', $input );
	/* Bail. */
	if( ! $post_types ){
		return null;
	}
	/* Get valid post types */
	$valid_post_types = kiotheme_post_types();
	/* Loop and verify */
	foreach( $post_types as $post_type ){
		if( array_key_exists( $post_type, $valid_post_types ) ){
			$output[] = $post_type;
		}
	}
	/* return it back as string. */
	return trim( esc_attr( implode( ',', $output ) ) );
}
/**
 * Sanitize Post Types
 * @since 0.1.0
 */
function gutenbooster_sanitize_options( $input ){
	/* Var */
	$output = array();
	/* Make input as array */
	$options = explode( ',', $input );
	/* Bail. */
	if( ! $options ){
		return null;
	}
	/* Get valid post types */
	$valid_options = array( 'index' );
	/* Loop and verify */
	foreach( $options as $option ){
		if( in_array( $option, $valid_options ) ){
			$output[] = $option;
		}
	}
	/* return it back as string. */
	return trim( esc_attr( implode( ',', $output ) ) );
}

function gutenbooster_sanitize_url( $url ) {
  return esc_url_raw( $url );
}

/**
 * Select choices sanitization callback
 *
 * @since 1.0
 */
function gutenbooster_sanitize_multi_choices( $input, $setting ) {
	// Get list of choices from the control associated with the setting.
	$choices = $setting->manager->get_control( $setting->id )->choices;
	$input_keys = $input;

	foreach ( $input_keys as $key => $value ) {
		if ( ! array_key_exists( $value, $choices ) ) {
			unset( $input[ $key ] );
		}
	}

	// If the input is a valid key, return it;
	// otherwise, return the default.
	return ( is_array( $input ) ? $input : $setting->default );
}

/**
 * Text sanitization
 *
 * @param  string	Input to be sanitized (either a string containing a single string or multiple, separated by commas)
 * @return string	Sanitized input
 */
if ( ! function_exists( 'gutenbooster_sanitize_text' ) ) {
	function gutenbooster_sanitize_text( $input ) {
		if ( strpos( $input, ',' ) !== false) {
			$input = explode( ',', $input );
		}
		if( is_array( $input ) ) {
			foreach ( $input as $key => $value ) {
				$input[$key] = sanitize_text_field( $value );
			}
			$input = implode( ',', $input );
		}
		else {
			$input = sanitize_text_field( $input );
		}
		return $input;
	}
}

/**
* Only allow values between a certain minimum & maxmium range
*
* @param  number	Input to be sanitized
* @return number	Sanitized input
*/
if ( ! function_exists( 'gutenbooster_in_range' ) ) {
function gutenbooster_in_range( $input, $min, $max ){
	if ( $input < $min ) {
		$input = $min;
	}
	if ( $input > $max ) {
		$input = $max;
	}
    return $input;
}
}

/**
 * Alpha Color (Hex & RGBa) sanitization
 *
 * @param  string	Input to be sanitized
 * @return string	Sanitized input
 */
if ( ! function_exists( 'gutenbooster_hex_rgba_sanitization' ) ) {
	function gutenbooster_hex_rgba_sanitization( $input, $setting ) {
		if ( empty( $input ) || is_array( $input ) ) {
			return $setting->default;
		}
		if ( false === strpos( $input, 'rgba' ) ) {
			// If string doesn't start with 'rgba' then santize as hex color
			$input = sanitize_hex_color( $input );
		} else {
			// Sanitize as RGBa color
			$input = str_replace( ' ', '', $input );
			sscanf( $input, 'rgba(%d,%d,%d,%f)', $red, $green, $blue, $alpha );
			$input = 'rgba(' . gutenbooster_in_range( $red, 0, 255 ) . ',' . gutenbooster_in_range( $green, 0, 255 ) . ',' . gutenbooster_in_range( $blue, 0, 255 ) . ',' . gutenbooster_in_range( $alpha, 0, 1 ) . ')';
		}
		return $input;
	}
}


/**
 * Google Font sanitization
 *
 * @param  string	JSON string to be sanitized
 * @return string	Sanitized input
 */
if ( ! function_exists( 'gutenbooster_google_font_sanitization' ) ) {
	function gutenbooster_google_font_sanitization( $input ) {
		$val =  json_decode( $input, true );
		if( is_array( $val ) ) {
			foreach ( $val as $key => $value ) {
				$val[$key] = $value;
			}
			$input = json_encode( $val );
		}
		else {
			$input = json_encode( sanitize_text_field( $val ) );
		}
		return $input;
	}
}


/**
 * Dimension sanitization callback
 *
 * @param string $val Input value.
 */
 if ( ! function_exists( 'gutenbooster_sanitize_dimension' ) ) {
	function gutenbooster_sanitize_dimension( $val ) {
		$decoded_array = json_decode( $val );
		if ( empty( $decoded_array ) ) {
			return '';
		}
		foreach ( $decoded_array as $array_item ) {
			$array_item_decoded = json_decode( $array_item );
			if ( empty( $array_item_decoded ) ) {
				return '';
			}
			foreach ( $array_item_decoded as $dimension ) {
				if ( ! empty( $dimension ) && ! is_numeric( $dimension ) ) {
					return '';
				}
			}
		}
		return $val;
	}
}

/**
 * Number sanitization callback
 *
 * @since 1.0.1
 */
 if ( ! function_exists( 'gutenbooster_sanitize_number' ) ) {
	function gutenbooster_sanitize_number( $val ) {
		return is_numeric( $val ) ? $val : 0;
	}
}

/**
 * Number with blank value sanitization callback
 *
 * @since 1.0.1
 */
 if ( ! function_exists( 'gutenbooster_sanitize_number' ) ) {
	function gutenbooster_sanitize_number_blank( $val ) {
		return is_numeric( $val ) ? $val : '';
	}
}