<?php





/*= GutenBooster Iris Color Picker Palette =*/
/*=============================================<<<<<*/
function gutenbooster_theme_colors() {

	return array(
		'palettes' => array(
			gutenbooster_palette( 'primary' ),
			gutenbooster_palette( 'secondary' ),
			gutenbooster_palette( 'tertiary' ),
			gutenbooster_palette( 'customone' ),
			gutenbooster_palette( 'customtwo' ),
			gutenbooster_palette( 'customthree' ),
			gutenbooster_palette( 'customfour' ),
			gutenbooster_palette( 'customfive' ),
			gutenbooster_palette( 'customsix' ),
			gutenbooster_palette( 'customseven' ),
			gutenbooster_palette( 'customeight' ),
			gutenbooster_palette( 'customnine' ),
			gutenbooster_palette( 'customten' ),
		),
		'alpha' => true,
		'width' => 370,
		'height' => 370,
	);
}//end fallback

/**
 * Icons
 * list of available icons
 */
function gutenbooster_customize_iconlist(){

	$icon_list = array(
		'search'    => esc_attr__( 'Search', 'gutenbooster' ),
		'menu'      => esc_attr__( 'Menu Drawer', 'gutenbooster' ),
		'divider'   => esc_attr__( '| Divider', 'gutenbooster' ),
		'divider2'  => esc_attr__( '| Divider', 'gutenbooster' ),
		'facebook'  => esc_attr__( 'Facebook', 'gutenbooster' ),
		'twitter'   => esc_attr__( 'Twitter', 'gutenbooster' ),
		'instagram' => esc_attr__( 'Instagram', 'gutenbooster' ),
		'youtube'   => esc_attr__( 'Youtube', 'gutenbooster' ),
		'linkedin'  => esc_attr__( 'Linkedin', 'gutenbooster' ),
	);
	if ( gutenbooster_is_woocommerce_on() ) {
		$icon_list['user']= esc_attr__( 'Account / Login', 'gutenbooster' );
		$icon_list['cart']= esc_attr__( 'Cart / Shopping Bag', 'gutenbooster' );
	}


	return apply_filters( 'gutenbooster_customize_iconlist', $icon_list );
}

/*=============================================>>>>>
= Active Callbacks =
===============================================>>>>>*/

// Returns bool for if the logo type is image
function gutenbooster_logo_is_image() {
	$logotype = gutenbooster_getmod( 'logo_type' );
	$logotype = isset( $logotype ) && 'image' == $logotype ? true : false;

	return $logotype;
}
// Returns bool for if the logo type is text
function gutenbooster_logo_is_text() {
	$logotype = gutenbooster_getmod( 'logo_type' );
	$logotype = isset( $logotype ) && 'text' == $logotype ? true : false;

	return $logotype;
}
// Check if Logo image file exists
function gutenbooster_check_logo_image() {
	$logo = get_theme_mod( 'custom_logo' );
	$has_logo = gutenbooster_logo_is_image() && $logo ? true : false;

	return $has_logo;
}


// Page Banner Existence Active Callback
function gutenbooster_check_page_banner() {
	$haspb = get_theme_mod( 'enable_page_banner', true );
	$has = $haspb ? true : false;

	return $has;
}

// Page Banner Existence Active Callback
function gutenbooster_check_widgetized_footer() {
	$widgetized = get_theme_mod( 'footer_widgets' );
	$widgetized = $widgetized ? true : false;

	return $widgetized;
}

// Header V1 Check
function gutenbooster_check_headerv1() {
	$headerv1 = gutenbooster_getmod( 'header_layout' );
	$headerv1 = isset( $headerv1 ) && 'v1' == $headerv1 ? true : false;

	return $headerv1;
}
// Header V2 Check
function gutenbooster_check_headerv2() {
	$headerv2 = gutenbooster_getmod( 'header_layout' );
	$headerv2 = isset( $headerv2 ) && 'v2' == $headerv2 ? true : false;

	return $headerv2;
}
// Header V3 Check
function gutenbooster_check_headerv3() {
	$headerv3 = gutenbooster_getmod( 'header_layout' );
	$headerv3 = isset( $headerv3 ) && 'v3' == $headerv3 ? true : false;

	return $headerv3;
}

// Header Stickyness Check
function gutenbooster_check_header_stick() {
	$stickyness = get_theme_mod( 'header_sticky' );
	$sticky = $stickyness !== 'none' ? true : false;

	return $sticky;
}

// Header Shrink Check
function gutenbooster_check_header_shrink() {
	$shrinked = get_theme_mod( 'shrink' );
	$sticking = get_theme_mod( 'header_sticky' );
	$shrink = $shrinked && $sticking !== 'none' ? true : false;

	return $shrink;
}

// Override Colors Active Callback
function gutenbooster_check_override_colors() {
	$override = gutenbooster_getmod( 'override_colors' );
	$override = $override ? true : false;

	return $override;
}


//*= End of Active Callbacks =*/
/*=============================================<<<<<*/


/**
 * Default Customizer Values
 *
 */
if ( ! function_exists( 'gutenbooster_defaults' ) ) {
	function gutenbooster_defaults( $theme_mod ) {
		$defaults = array(
			// title-tagline
			'core_display_tagline' => 1,
			// colors
			'override_colors' => 0,
			// header
			'header_layout' => esc_attr( 'v3' ),
			'header_wrapper' => esc_attr( 'wrapped' ),
			'header_bg' => esc_attr( 'white' ),
			'header_txt_color' => esc_attr( 'dark' ),
			'header_topbottom_pad' => 40,
			'headerv3_nav_pos' => esc_attr( 'right' ),
			'header_sticky' => 0,
			'shrink' => 0,
			'shrink_txt_color' => 'dark',
			'shrinkshadow' => 0,
			'header_submenu_space' => 50,
			// logo
			'logo_type' => 'text',
			// page banner
			'enable_page_banner' => 1,
			'page_banner_bgcolor' => gutenbooster_palette( 'primary' ),
			'page_bannerbg_opacity' => 1,
			'page_banner_height' => 30,
			'page_banner_text_color' => esc_attr( 'white' ),
			'enable_page_banner_bgparallax' => 0,
			'enable_page_banner_animate' => 0,
			// blog
			'blog_layout' => esc_attr( 'classic' ),
			'blog_excerpt_length' => 25,
			'disable_blog_pb' => 0,
			'single_show_author' => 0,
			// layout
			'layout_default' => esc_attr( 'sb-right' ),
			'layout_post' => esc_attr( 'no-sidebar' ),
			'layout_page' => esc_attr( 'no-sidebar' ),
			'layout_shop' => esc_attr( 'no-sidebar' ),
			'layout_content_width' => 800,
			'content_toppad' => 0,
			'units_topbottom_margin' => 40,
			// footer
			'footer_widgets' => 0,
			'footer_widgets_layout' => esc_attr( '4-columns' ),
			'footer_align_links_left' => 0,
			'footer_maxpad' => 0,
			'footer_pad' => 45,
			'footer_hide_submenus' => 0,
			'footer_wrapper' => esc_attr( 'container' ),
			'footer_text_left' => esc_html__( 'Copyright &copy; [current_year] [site_title] | Powered by [theme_author]', 'gutenbooster' ),
			'footer_bg_color' => esc_attr( '#ffffff' ),
			'footer_bg_opacity' => 1,
			// typography
			'typo_body' => json_encode(
				array(
					'font' => 'Roboto',
					'variantlist' => array( 'regular', '100', '200', '300', '500', '700' ),
					'fontweight' => 'regular',
					'transform' => 'none',
					'category' => 'sans-serif'
				)
			),
			'typo_headings' => json_encode(
				array(
					'font' => 'Poppins',
					'variantlist' => array( 'regular', '100', '200', '300', '500', '600', '700', '800', '900' ),
					'fontweight' => '600',
					'transform' => 'none',
					'category' => 'sans-serif'
				)
			),
			'font_menu' => json_encode(
				array(
					'font' => 'Roboto',
					'variantlist' => array( 'regular', '100', '200', '300', '500', '700' ),
					'fontweight' => 'regular',
					'transform' => 'none',
					'category' => 'sans-serif'
				)
			),

		);
		return isset( $defaults[ $theme_mod ] ) ? $defaults[ $theme_mod ] : false;
	}
}

if ( ! function_exists( 'gutenbooster_get_resp_size' ) ) {
	function gutenbooster_get_resp_size( $mod, $device = 'desktop' ) {

		$value        = '';

		$theme_mod = get_theme_mod( $mod );

		if( is_string( $theme_mod ) && is_array( json_decode( $theme_mod, true ) ) ){
			/* It means that we have media queries active and need to return an array */
			$json = json_decode( $theme_mod );
			$value = (array) $json;

			switch ( $device ) {
				case "mobile":
		        $value = $value['mobile'];
		        break;
				case 'tablet':
		        $value = $value['tablet'];
		        break;
				case 'desktop':
		        $value = $value['desktop'];
		        break;
			}

		} else {
			$value = $theme_mod;
		}

		return $value;
	}
}

if ( ! function_exists( 'gutenbooster_getmod' ) ) {
	function gutenbooster_getmod( $mod ) {
		return get_theme_mod( $mod, gutenbooster_defaults( $mod ) );
	}
}


