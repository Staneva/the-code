<?php

/**
 * Customizer Settings
 *
 * @package   gutenbooster
 * @copyright Copyright (c) 2016, Nose Graze Ltd.
 * @license   GPL2+
 */

 /**
  * Get Typography Settings
  *
  * Enter all font settings here.
  *
  * Returns an array of all the typography settings. This array is used to
  * quickly generate the different Customizer options and the accompanying
  * CSS code. We put this code in a function since we re-use it so many times.
  *
  * @since 1.0
  * @return array
  */


if ( class_exists( 'GutenBooster_Customizer' ) ) {
	return;
}

class GutenBooster_Customizer {

	/**
	 * Theme object
	 *
	 * @var WP_Theme
	 * @access private
	 * @since  1.0
	 */
	private $theme;

	/**
	 * GutenBooster_Customizer constructor.
	 *
	 * @access public
	 * @since  1.0
	 * @return void
	 */
	public function __construct() {

		$theme       = wp_get_theme();
		$this->theme = $theme;

		add_action( 'customize_register', array( $this, 'include_controls' ) );
		add_action( 'customize_register', array( $this, 'include_fields' ) );
		add_action( 'customize_register', array( $this, 'register_customize_sections' ) );

		//scripts
		add_action( 'customize_preview_init', array( $this, 'customizer_enqueue_scripts' ) );
		add_action('customize_controls_enqueue_scripts', array( $this, 'customizer_enqueue_helper_scripts' ) );

		//styles
		add_action( 'customize_controls_print_styles', array( $this, 'customizer_enqueue_styles' ) );


	}

	/**
	 * Include Custom Controls
	 *
	 * Includes all our custom control classes.
	 *
	 * @param WP_Customize_Manager $wp_customize
	 *
	 * @access public
	 * @since  1.0
	 * @return void
	 */
	public function include_controls( $wp_customize ) {



		$controls_dir = GUTENBOOSTER_THEME_DIR . '/inc/admin/customizer/customizer-controls';
		$controls_pro_dir = GUTENBOOSTER_THEME_DIR . '/inc/pro/customizer-pro-controls';

		/*=============================================>>>>>
		= Controls =
		===============================================>>>>>*/

		require_once  $controls_dir . '/range-slider/class-customizer-range-value-control.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		require_once  $controls_dir . '/range-control/class-range-control.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound

		require_once  $controls_dir . '/class-kioken-customizer-toggle-control.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound

		require_once  $controls_dir . '/class-kioken-customizer-image-radio-button.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		require_once  $controls_dir . '/radio-button/class-kioken-radio-button-control.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		
		require_once  $controls_dir . '/sortable/class-kioken-customizer-sortable-control.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		require_once  $controls_dir . '/alpha-color/class-kioken-customizer-alpha-color-control.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound



		//typography
		require_once $controls_dir . '/typography/class-kioken-font-control.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound

		$includes_dir = GUTENBOOSTER_THEME_DIR . '/inc/admin/customizer/includes';
		require_once  $includes_dir . '/sanitizer-functions.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound

		if ( defined('GUTENBOOSTER_PRO') ) {
			require_once  $controls_pro_dir . '/dimensions/class-customizer-dimensions.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		   require_once  $controls_pro_dir . '/notice/class-kioken-notice-control.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		 }


	}

	/**
	 * Include Customizer Fields
	 *
	 * Includes all our customizer fields.
	 *
	 * @param WP_Customize_Manager $wp_customize
	 *
	 * @access public
	 * @since  1.0
	 * @return void
	 */
	public function include_fields( $wp_customize ) {

		$fields_dir     = GUTENBOOSTER_THEME_DIR . '/inc/admin/customizer/fields';
		$fields_pro_dir = GUTENBOOSTER_THEME_DIR . '/inc/pro/customizer-pro-fields/';

		/*=============================================>>>>>
		= Fields =
		===============================================>>>>>*/

		require_once  $fields_dir . '/blog.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		require_once  $fields_dir . '/colors.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		require_once  $fields_dir . '/footer.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		require_once  $fields_dir . '/header.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		require_once  $fields_dir . '/layout.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		require_once  $fields_dir . '/logo.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		require_once  $fields_dir . '/pagebanner.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		require_once  $fields_dir . '/typography.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		require_once  $fields_dir . '/socials.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound

		if ( defined('GUTENBOOSTER_PRO') ) {
			 require_once $fields_pro_dir . 'blog.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
			 require_once $fields_pro_dir . 'colors.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
			 require_once $fields_pro_dir . 'header.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
			 require_once $fields_pro_dir . 'footer.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
			 require_once $fields_pro_dir . 'layout.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
			 require_once $fields_pro_dir . 'logo.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
			 require_once $fields_pro_dir . 'pagebanner.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
			 require_once $fields_pro_dir . 'typography.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
		 }

	}

	/**
	 * Add all panels and sections to the Customizer
	 *
	 * @param WP_Customize_Manager $wp_customize
	 *
	 * @access public
	 * @since  1.0
	 * @return void
	 */
	public function register_customize_sections( $wp_customize ) {

		// Core Customizer title and location updates
		$wp_customize->get_control( 'background_color'  )->section   = 'background_image';
		$wp_customize->get_section( 'background_image'  )->panel   = 'colors';
		$wp_customize->get_section( 'background_image'  )->title   = __( 'Background', 'gutenbooster' );
		$wp_customize->get_section( 'background_image'  )->priority   = 1;
		$wp_customize->get_control( 'custom_logo'  )->section   = 'logo';
		$wp_customize->get_control( 'custom_logo'  )->active_callback   = 'gutenbooster_logo_is_image';
		$wp_customize->get_control( 'custom_logo'  )->description   = __( 'Upload the default logo of your website', 'gutenbooster' );

		/*=============================================>>>>>
		= Panels =
		===============================================>>>>>*/

		// Global
		$wp_customize->add_panel( 'global', array(
		  'title' 		=> __( 'Global Settings', 'gutenbooster' ),
		  'description' 	=> __( 'Global theme settings.', 'gutenbooster' ),
		  'priority' => 1, // Mixed with top-level-section hierarchy.
		) );

		// Colors
		$wp_customize->add_panel( 'colors', array(
		  'title' 		=> __( 'Theme Colors', 'gutenbooster' ),
		  'priority' => 40, // Mixed with top-level-section hierarchy.
		) );

		// Header
		$wp_customize->add_panel( 'header', array(
		  'title' 		=> __( 'Header', 'gutenbooster' ),
		  'description' 	=> __( 'Header settings.', 'gutenbooster' ),
		  'panel' 	=> 'global',
		  'priority' => 43, // Mixed with top-level-section hierarchy.
		) );

		// Layout
		$wp_customize->add_panel( 'layout', array(
		  'title' 		=> __( 'Layout', 'gutenbooster' ),
		  'priority' => 30, // Mixed with top-level-section hierarchy.
		) );

		// Blog
		$wp_customize->add_panel( 'blog', array(
		  'title' 		=> __( 'Blog', 'gutenbooster' ),
		  'priority' => 50, // Mixed with top-level-section hierarchy.
		) );

		// Typography
		$wp_customize->add_panel( 'typography', array(
			'title'    => esc_html__( 'Typography', 'gutenbooster' ),
			'priority' => 45
		) );

		// Footer
		$wp_customize->add_panel( 'footer', array(
		  'title' 		=> __( 'Footer', 'gutenbooster' ),
		  'description' 	=> __( 'Footer settings.', 'gutenbooster' ),
		  'priority' => 100, // Mixed with top-level-section hierarchy.
		) );




		/*=============================================>>>>>
		= Sections =
		===============================================>>>>>*/

		// Globals Section
		$wp_customize->add_section( 'logo', array(
			'title' 		=> __( 'Logo', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'global',
			'priority'       => 1,
		) );

		$wp_customize->add_section( 'title_tagline', array(
			'title' 		=> __( 'Site Identity', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'global',
			'priority'       => 2,
		) );


		$wp_customize->add_section( 'static_front_page', array(
			'title' 		=> __( 'Homepage Settings', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'global',
			'priority'       => 3,
		) );


		// colors
		$wp_customize->add_section( 'main_colors', array(
			'title' 		=> __( 'Main Colors', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'colors',
			'priority'       => 1,
		) );
		$wp_customize->add_section( 'menu_colors', array(
			'title' 		=> __( 'Header Menu Colors', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'colors',
			'priority'       => 3,
		) );
		$wp_customize->add_section( 'colors_sidebar', array(
			'title' 		=> __( 'Sidebar Colors', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'colors',
			'priority'       => 4,
		) );

		// Socials
		$wp_customize->add_section( 'socials', array(
			'title' 		=> __( 'Social Links', 'gutenbooster' ),
			'panel'         => 'header',
			'description' 	=> __( 'Urls to link in the header social icons.', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'priority'      => 44,
		) );


		// Header
		$wp_customize->add_section( 'header_l', array(
			'title' 		=> __( 'Header Layout', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'header',
			'priority'       => 1,
			'description' 	=> __( 'Global site header layout settings.', 'gutenbooster' ),
		) );

		$wp_customize->add_section( 'sticky_header', array(
			'title' 		=> __( 'Sticky Header', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'header',
			'priority'       => 2,
			'description' 	=> __( 'Setting for making header always stick to top.', 'gutenbooster' ),
		) );

		$wp_customize->add_section( 'header_icons', array(
			'title' 		=> __( 'Additional Menu Elements', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'header',
			'priority'       => 3,
			'description' 	=> __( 'Additional icons and links.', 'gutenbooster' ),
		) );

		// Blog Section
		$wp_customize->add_section( 'blog_general', array(
			'title' 		=> __( 'Blog Posts Settings', 'gutenbooster' ),
			'description' 	=> __( 'Settings for post archives.', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'blog',
			'priority'       => 1,
		) );

		$wp_customize->add_section( 'blog_single', array(
			'title' 		=> __( 'Single Post Settings', 'gutenbooster' ),
			'description' 	=> __( 'Settings for single post pages.', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'blog',
			'priority'       => 2,
		) );


		/*=============================================>>>>>
		= Section: Page Banner =
		===============================================>>>>>*/

		$wp_customize->add_section( 'page_banner', array(
			'title' 		=> __( 'Page Banner', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'description' 	=> __( 'Global Page Banner(hero) settings.', 'gutenbooster' ),
			'priority'       => 42,
		) );

		// Layout General
		$wp_customize->add_section( 'layout_general', array(
			'title' 		=> __( 'General Layout Settings', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'layout',
		) );
		// Layout Structure
		$wp_customize->add_section( 'layout_single', array(
			'title' 		=> __( 'Single Page/Post Layout Settings', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'layout',
		) );
		$wp_customize->add_section( 'layout_site_container', array(
			'title' 		=> __( 'Site Container', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'description' 	=> __( 'Main site wrapper settings', 'gutenbooster' ),
			'panel' 	=> 'layout',
		) );

		// Typography
		$wp_customize->add_section( 'typo_general', array(
			'title' 		=> __( 'General Typography', 'gutenbooster' ),
			'description' 		=> __( 'Choose your favorite font for Body and Headings', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'typography',
		) );
		$wp_customize->add_section( 'typo_menu', array(
			'title' 		=> __( 'Menu Typography', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'typography',
		) );
		$wp_customize->add_section( 'typo_head', array(
			'title' 		=> __( 'Headings Typography', 'gutenbooster' ),
			'description' 		=> __( 'Settings for each heading tag', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'typography',
		) );


		// Footer Structure
		$wp_customize->add_section( 'footer_structure', array(
			'title' 		=> __( 'Footer Structure', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'footer',
			'description' 	=> __( 'Footer structural settings.', 'gutenbooster' ),
			'priority'       => 1,
		) );
		// Footer Style
		$wp_customize->add_section( 'footer_style', array(
			'title' 		=> __( 'Footer Style', 'gutenbooster' ),
			'capability' 	=> 'edit_theme_options',
			'panel' 	=> 'footer',
			'description' 	=> __( 'Footer styling settings.', 'gutenbooster' ),
			'priority'       => 2,
		) );


	}

	/**
	 * Customizer Enqueue Scripts
	 *
	 * @param WP_Customize_Manager $wp_customize
	 *
	 * @access public
	 * @since  1.0
	 * @return void
	 */
	 public function customizer_enqueue_scripts() {
		wp_enqueue_script(
	 		'gutenbooster-customizer-js',
	 		GUTENBOOSTER_THEME_URI . 'inc/admin/customizer/assets/js/customizer-min.js', array( 'jquery', 'customize-preview' ), '1.0', true );

	 	// enable underscore.js
	 	wp_enqueue_script( 'wp-util' );

	 }
	 public function customizer_enqueue_helper_scripts() {
		 wp_enqueue_script(
 	 		'gutenbooster-customizer-controls-helper-js',
 	 		GUTENBOOSTER_THEME_URI . 'inc/admin/customizer/assets/js/controls-helper-min.js', array( 'jquery', 'customize-controls' ), false, true );
	 }

	/**
	 * Customizer Enqueue Styles
	 *
	 * @param WP_Customize_Manager $wp_customize
	 *
	 * @access public
	 * @since  1.0
	 * @return void
	 */
	 public function customizer_enqueue_styles() {
		 //styles
    	 wp_register_style( 'gutenbooster-customizer-styles', get_template_directory_uri() . '/inc/admin/customizer/assets/css/customizer.css', null, GUTENBOOSTER_THEMEVERSION, 'all' );
    	 wp_enqueue_style( 'gutenbooster-customizer-styles' );

	 }





}
