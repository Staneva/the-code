<?php
/**
 * GutenBooster functions and definitions.
 * Text Domain: gutenbooster
 * When using a child theme (see https://codex.wordpress.org/Theme_Development
 * and https://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * For more information on hooks, actions, and filters,
 * see https://codex.wordpress.org/Plugin_API
 *
 * GutenBooster is a very powerful theme and virtually anything can be customized
 * via a child theme.
 *
 * @package     GutenBooster
 * @author      GutenBooster
 * @copyright   Copyright (c) 2019, GutenBooster
 * @link        https://kiokengutenberg.com/
 * @since       GutenBooster 1.0.0
 */

 /**
 * GutenBooster_After_Setup_Theme initial setup
 *
 * @since 1.0.0
 */

if ( ! class_exists( 'GutenBooster_After_Setup_Theme' ) ) {

	class GutenBooster_After_Setup_Theme {

		 /**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance;

		/**
		 * Initiator
		 *
		 * @since 1.0.0
		 * @return object
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self();
			}
			return self::$instance;
		}

		/**
		 * Constructor
		 */
		public function __construct() {
			add_action( 'after_setup_theme', array( $this, 'setup_theme' ), 2 );
			add_action( 'template_redirect', array( $this, 'setup_content_width' ) );
		}

		/**
		 * Setup theme
		 *
		 * @since 1.0.0
		 */
		public function setup_theme() {
			do_action( 'gutenbooster_class_loaded' );

			/**
			* Make theme available for translation.
			* Translations can be filed in the /languages/ directory.
			* If you're building a theme based on Next, use a find and replace
			* to change 'gutenberg' to the name of your theme in all the template files.
			*/
			load_theme_textdomain( 'gutenbooster', GUTENBOOSTER_THEME_DIR . '/languages' );

			/**
			* This theme styles the visual editor to resemble the theme style,
			* specifically font, colors, icons, and column width.
			*/
			add_editor_style();
			add_theme_support( 'editor-styles' );

			add_theme_support( 'dark-theme' );

			// Theme supports wide images, galleries and videos.
			add_theme_support( 'align-wide' );
			// Load regular editor styles into the new block-based editor.

			// Load default block styles.
			add_theme_support( 'wp-block-styles' );

			// Add support for responsive embeds.
			add_theme_support( 'responsive-embeds' );

			// Add support for custom color scheme.
			add_theme_support(
				'editor-color-palette',
				array(
					array(
						'name'  => esc_html__( 'GutenBooster Primary', 'gutenbooster' ),
						'slug'  => 'gb-primary',
						'color' => get_theme_mod( 'color_gb_primary' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_primary' ) : gutenbooster_palette( 'primary' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Secondary', 'gutenbooster' ),
						'slug'  => 'gb-secondary',
						'color' => get_theme_mod( 'color_gb_secondary' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_secondary' ) : gutenbooster_palette( 'secondary' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Tertiary', 'gutenbooster' ),
						'slug'  => 'gb-tertiary',
						'color' => get_theme_mod( 'color_gb_tertiary' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_tertiary' ) : gutenbooster_palette( 'tertiary' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Custom 1', 'gutenbooster' ),
						'slug'  => 'gb-customone',
						'color' => get_theme_mod( 'color_gb_one' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_one' ) : gutenbooster_palette( 'customone' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Custom 2', 'gutenbooster' ),
						'slug'  => 'gb-customtwo',
						'color' => get_theme_mod( 'color_gb_two' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_two' ) : gutenbooster_palette( 'customtwo' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Custom 3', 'gutenbooster' ),
						'slug'  => 'gb-customthree',
						'color' => get_theme_mod( 'color_gb_three' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_three' ) : gutenbooster_palette( 'customthree' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Custom 4', 'gutenbooster' ),
						'slug'  => 'gb-customfour',
						'color' => get_theme_mod( 'color_gb_four' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_four' ) : gutenbooster_palette( 'customfour' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Custom 5', 'gutenbooster' ),
						'slug'  => 'gb-customfive',
						'color' => get_theme_mod( 'color_gb_five' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_five' ) : gutenbooster_palette( 'customfive' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Custom 6', 'gutenbooster' ),
						'slug'  => 'gb-customsix',
						'color' => get_theme_mod( 'color_gb_six' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_six' ) : gutenbooster_palette( 'customsix' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Custom 7', 'gutenbooster' ),
						'slug'  => 'gb-customseven',
						'color' => get_theme_mod( 'color_gb_seven' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_seven' ) : gutenbooster_palette( 'customseven' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Custom 8', 'gutenbooster' ),
						'slug'  => 'gb-customeight',
						'color' => get_theme_mod( 'color_gb_eight' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_eight' ) : gutenbooster_palette( 'customeight' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Custom 9', 'gutenbooster' ),
						'slug'  => 'gb-customnine',
						'color' => get_theme_mod( 'color_gb_nine' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_nine' ) : gutenbooster_palette( 'customnine' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Custom 10', 'gutenbooster' ),
						'slug'  => 'gb-customten',
						'color' => get_theme_mod( 'color_gb_ten' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_ten' ) : gutenbooster_palette( 'customten' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Custom 11', 'gutenbooster' ),
						'slug'  => 'gb-customeleven',
						'color' => get_theme_mod( 'color_gb_eleven' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_eleven' ) : gutenbooster_palette( 'customeleven' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Custom 12', 'gutenbooster' ),
						'slug'  => 'gb-customtwelve',
						'color' => get_theme_mod( 'color_gb_twelve' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_twelve' ) : gutenbooster_palette( 'customtwelve' ),
					),
					array(
						'name'  => esc_html__( 'GutenBooster Custom 13', 'gutenbooster' ),
						'slug'  => 'gb-customthirteen',
						'color' => get_theme_mod( 'color_gb_thirteen' ) && get_theme_mod( 'override_colors' ) ? get_theme_mod( 'color_gb_thirteen' ) : gutenbooster_palette( 'customthirteen' ),
					),
				)
			);

			// Add default posts and comments RSS feed links to head.
			add_theme_support( 'automatic-feed-links' );

			// Let WordPress manage the document title.
			add_theme_support( 'title-tag' );

			//Allow Customizing Logo and Background
			add_theme_support('custom-logo');
			add_theme_support( 'custom-background', array(
				'default-color' => esc_attr('#ffffff')
			) );

			// Supports WooCommerce plugin.
			add_theme_support( 'woocommerce' );
			add_theme_support( 'wc-product-gallery-lightbox' );
			add_theme_support( 'wc-product-gallery-zoom' );
			add_theme_support( 'wc-product-gallery-slider' );

			add_theme_support( 'post-formats', array( 'gallery', 'image', 'link', 'quote', 'quote', 'video', 'audio', 'status', 'aside' ) );

			// This theme uses wp_nav_menu() in 4 locations.
			register_nav_menus(
				array(
					'gbb-primary'   => esc_html__( 'Primary Top Menu', 'gutenbooster' ),
					'gbb-secondary' => esc_html__( 'Secondary Top Menu', 'gutenbooster' ),
					'gbb-footer'    => esc_html__( 'Footer Menu', 'gutenbooster' ),
					'gbb-mobile'    => esc_html__( 'Mobile Menu', 'gutenbooster' ),
				)
			);

			/*
			* Switch default core markup for search form, comment form, and comments
			* to output valid HTML5.
			*/
			add_theme_support(
				'html5',
				array(
					'search-form',
					'comment-form',
					'comment-list',
					'gallery',
					'caption',
				)
			);

			// Add theme support for selective refresh for widgets.
			add_theme_support( 'customize-selective-refresh-widgets' );

			// Enable support for Post Thumbnails on posts and pages.
			add_theme_support( 'post-thumbnails' );

			// set_post_thumbnail_size( 150, 150, true );
			add_image_size( 'gutenbooster-small-horizontal', 450, 320, true );
			add_image_size( 'gutenbooster-medium-horizontal', 760, 480, true );
			add_image_size( 'gutenbooster-large-horizontal', 1024, 650, true );
			add_image_size( 'gutenbooster-xlarge', 1400, '', true );

			/**
			* Set the $content_width global variable used by WordPress to set image dimennsions.
			*
			* @since 1.0.0
			* @return void
			*/

			function gutenbooster_custom_size_names( $sizes ) {
				return array_merge(
					$sizes,
					array(
						'gutenbooster-small-horizontal' => esc_html__( 'GutenBooster Small Horizontal', 'gutenbooster' ),
						'gutenbooster-medium-horizontal' => esc_html__( 'GutenBooster Medium Horizontal', 'gutenbooster' ),
						'gutenbooster-large-horizontal' => esc_html__( 'GutenBooster Large Horizontal', 'gutenbooster' ),
						'gutenbooster-xlarge' => esc_html__( 'GutenBooster Very Large(1400px)', 'gutenbooster' ),
					)
				);
			}
			add_filter( 'image_size_names_choose', 'gutenbooster_custom_size_names' );

		}

		public function setup_content_width() {
			global $content_width;

			/**
			* Content Width
			*/
			if ( ! isset( $content_width ) ) {
				$content_width = apply_filters( 'gutenbooster_content_width', get_theme_mod( 'layout_content_width' ) );
			}

		}

	}

}


 /**
 * Kicking this off by calling 'get_instance()' method
 */
GutenBooster_After_Setup_Theme::get_instance();
