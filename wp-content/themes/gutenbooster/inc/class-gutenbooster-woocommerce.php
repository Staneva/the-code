<?php
/**
 * Customize WooCommerce templates
 *
 * @package GutenBooster
 */

/**
 * Class for all WooCommerce template modifications
 *
 * @version 1.0
 */

class GutenBooster_WooCommerce {
	/**
	* The single instance of the class
	*
	* @var GutenBooster_WooCommerce
	*/
	protected static $instance = null;

	/**
	* Number of days to keep set a product as a new one
	* @var int
	*/
	protected $new_duration;

	/**
	* Main instance
	*
	* @return GutenBooster_WooCommerce
	*/
	public static function instance() {
		if ( null == self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}


	/**
	* Construction function
	*/
	public function __construct() {
		// Check if Woocomerce plugin is actived
		if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			return;
		}

		// $this->parse_query();
		$this->hooks();

	}

	public function hooks() {

		// WooCommerce Styles
		add_filter( 'woocommerce_enqueue_styles', array( $this, 'wc_styles' ) );

		// Need an early hook to ajaxify update mini shop cart
		add_filter( 'woocommerce_add_to_cart_fragments', array( $this, 'add_to_cart_fragments' ) );

		// Hide Star Rating in the loop
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

		//Hide Shop Page Title
		add_filter( 'woocommerce_show_page_title', array( $this, 'woo_hide_page_title' ) );

		// Hide Breadcrumbs
		remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

		/*=============================================>>>>>
		= Product Catalog  Loop =
		===============================================>>>>>*/
		//Sale Badges
		remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash' );
		remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash' );
			add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_show_product_loop_sale_flash', 20 ); /*10 moves above link, 20 puts inside the link*/
		// add_action( 'woocommerce_single_product_summary', 'woocommerce_show_product_sale_flash', 1 );

		// Change product link position - @// TODO: this is only wrapping image with <a>
		remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
			add_action( 'woocommerce_before_shop_loop_item', array( $this, 'product_loop_link_open' ), 10 );
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
			add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 20 );

		// Add link to product title in shop loop
		remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title' );
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
			add_action( 'woocommerce_after_shop_loop_item', array( $this, 'product_loop_title_cart_price' ), 10 );

		/*=============================================>>>>>
		= Single Product =
		===============================================>>>>>*/
		// Remove Tab titles from Description, Information, Reviews
		add_filter( 'woocommerce_product_description_heading', '__return_null' );
		add_filter( 'woocommerce_product_additional_information_heading', '__return_null' );
		add_filter( 'woocommerce_product_reviews_heading', '__return_null' );

		// Wrap images and summary into a div
		add_action( 'woocommerce_before_single_product_summary', array( $this, 'open_product_intro' ), 5 );
		add_action( 'woocommerce_after_single_product_summary', array( $this, 'close_product_intro' ), 5 );

	}

	public function product_loop_link_open() {
		woocommerce_template_loop_product_link_open();
	}

	public function product_loop_title_cart_price() { ?>
		<div class="wrapper position-relative">
			<a href="<?php the_permalink(); ?>" class="text-link">
				<div class="product-sum">
					<?php woocommerce_template_loop_product_title(); ?>
					<?php woocommerce_template_loop_price(); ?>
				</div>
			</a>
				<?php woocommerce_template_loop_add_to_cart(); ?>
		</div>
		<?php
	}

	/**
	* Open product intro div
	*/
	public function open_product_intro() {
		echo '<div class="product-intro clearfix">';
	}
	/**
	* Close product intro div
	*/
	public function close_product_intro() {
		echo '</div>';
	}

	/**
	* Style Reset
	*/
	public function wc_styles( $styles ) {

		return $styles;

	}

	/**
	* Hide the Shop Page Title
	*/
	public function woo_hide_page_title() {
		return false;
	}

	/**
	* Ajaxify update cart viewer
	*
	* @since 1.0
	*
	* @param array $fragments
	*
	* @return array
	*/
	public function add_to_cart_fragments( $fragments ) {
		$fragments['span.cart-counter'] = '<span class="count cart-counter">' . WC()->cart->get_cart_contents_count() . '</span>';

		return $fragments;
	}

}


/**
 * Dequeue WooCommerce styles and scripts for pages that are not shop.
 * @todo Runs only when there's no ajax page preloader and NO PRODUCTS BLOCK
 */
function gutenbooster_woocommerce_script_cleaner() {

	// Remove the generator tag
	remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );

	// Unless we're in the store, remove all the cruft!
	if ( ! is_woocommerce() && ! is_cart() && ! is_account_page() && ! is_checkout() && ! has_block( 'woocommerce/products' ) ) {

		wp_dequeue_style( 'woocommerce-smallscreen' );
		wp_dequeue_style( 'woocommerce_fancybox_styles' );
		wp_dequeue_style( 'woocommerce_chosen_styles' );
		wp_dequeue_style( 'woocommerce_prettyPhoto_css' );

		wp_dequeue_style( 'block-library-css' );

		wp_dequeue_script( 'selectWoo' );
		wp_dequeue_script( 'wc-add-payment-method' );
		wp_dequeue_script( 'wc-lost-password' );
		wp_dequeue_script( 'wc_price_slider' );
		wp_dequeue_script( 'wc-single-product' );
		wp_dequeue_script( 'wc-add-to-cart' );
		wp_dequeue_script( 'wc-cart-fragments' );
		wp_dequeue_script( 'wc-credit-card-form' );
		wp_dequeue_script( 'wc-checkout' );
		wp_dequeue_script( 'wc-add-to-cart-variation' );
		wp_dequeue_script( 'wc-single-product' );
		wp_dequeue_script( 'wc-cart' );
		wp_dequeue_script( 'wc-chosen' );
		wp_dequeue_script( 'woocommerce' );
		wp_dequeue_script( 'prettyPhoto' );
		wp_dequeue_script( 'prettyPhoto-init' );
		wp_dequeue_script( 'jquery-blockui' );
		wp_dequeue_script( 'jquery-placeholder' );
		wp_dequeue_script( 'jquery-payment' );
		wp_dequeue_script( 'fancybox' );
		wp_dequeue_script( 'jqueryui' );

	}
}

if ( ! gutenbooster_has_ajax_preloader() && gutenbooster_is_woocommerce_on() ) {
	add_action( 'wp_enqueue_scripts', 'gutenbooster_woocommerce_script_cleaner', 99 );
}

add_action( 'wp_footer', 'gutenbooster_quantity_fields_script' );
function gutenbooster_quantity_fields_script() {
	?>
		<script type='text/javascript'>
			jQuery( function( $ ) {
				if ( ! String.prototype.getDecimals ) {
					String.prototype.getDecimals = function() {
						var num = this,
							match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
						if ( ! match ) {
							return 0;
						}
						return Math.max( 0, ( match[1] ? match[1].length : 0 ) - ( match[2] ? +match[2] : 0 ) );
					}
				}
				// Quantity "plus" and "minus" buttons
				$( document.body ).on( 'click', '.increase, .decrease', function() {
					var $qty        = $( this ).closest( '.quantity' ).find( '.qty'),
						currentVal  = parseFloat( $qty.val() ),
						max         = parseFloat( $qty.attr( 'max' ) ),
						min         = parseFloat( $qty.attr( 'min' ) ),
						step        = $qty.attr( 'step' );

							// Format values
						if ( ! currentVal || currentVal === '' || currentVal === 'NaN' ) currentVal = 0;
						if ( max === '' || max === 'NaN' ) max = '';
						if ( min === '' || min === 'NaN' ) min = 0;
						if ( step === 'any' || step === '' || step === undefined || parseFloat( step ) === 'NaN' ) step = 1;

							// Change the value
						if ( $( this ).is( '.increase' ) ) {
							if ( max && ( currentVal >= max ) ) {
								$qty.val( max );
							} else {
								$qty.val( ( currentVal + parseFloat( step )).toFixed( step.getDecimals() ) );
								}
							} else {
							if ( min && ( currentVal <= min ) ) {
								$qty.val( min );
								} else if ( currentVal > 0 ) {
									$qty.val( ( currentVal - parseFloat( step )).toFixed( step.getDecimals() ) );
								}
							}

							// Trigger change event
							$qty.trigger( 'change' );
				});
			});
		</script>
		<?php
}














