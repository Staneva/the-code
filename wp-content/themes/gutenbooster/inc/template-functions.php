<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package gutenbooster
 */


/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function gutenbooster_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'gutenbooster_pingback_header' );


/**
* Check if WooCommerce is activated
*/
if ( ! function_exists( 'gutenbooster_is_woocommerce_on' ) ) {
	function gutenbooster_is_woocommerce_on() {
		if ( class_exists( 'woocommerce' ) ) {
			return true;
		} else {
			return false;
		}
	}
}

/**
* Return Shop Page ID
*/
if ( ! function_exists( 'gutenbooster_shop_page_id' ) ) :
	function gutenbooster_shop_page_id() {
		if ( gutenbooster_is_woocommerce_on() ) {
			$shop_page_id = get_option( 'woocommerce_shop_page_id' );
		}

		if ( isset( $shop_page_id ) ) {
			return $shop_page_id;
		}

	}
endif;

/**
 * Check if it is the shop page
 */
if ( ! function_exists( 'gutenbooster_is_shop_home' ) ) :
	function gutenbooster_is_shop_home() {

		if ( function_exists( 'is_shop' ) && is_shop() ) {
			return true;
		} else {
			return false;
		}

	}
endif;


/**
 * Retrieve Post Meta
 */
if ( ! function_exists( 'gutenbooster_has_kioken_metaplugin' ) ) {
	function gutenbooster_has_kioken_metaplugin() {
		$has = false;
		if ( class_exists( 'Kioken_GB_Addons_Plugin' ) ) {
			$has = true;
		}

		return apply_filters( 'gutenbooster_has_kioken_metaplugin', $has );
	}
}

/**
 * Retrieve Post Meta
 */
if ( ! function_exists( 'gutenbooster_postmeta' ) ) {
	function gutenbooster_postmeta( $key = '', $single = false ) {
		$meta = get_post_meta( get_queried_object_id(), $key, $single );
		return $meta;
	}
}


/**
* Page Banner Check. Validate existence.
*/
if ( ! function_exists( 'gutenbooster_has_page_banner' ) ) :
	/**
	* Check if current page has page header
	*
	* @return bool
	*/
	function gutenbooster_has_page_banner() {
		$has = gutenbooster_getmod( 'enable_page_banner' );


		$disable_in_blog_posts = gutenbooster_getmod( 'disable_blog_pb' );

		if ( is_404() ) {
			$has = true;
		}

		if ( is_page_template( 'templates/homepage.php' ) ) {
			$has = false;
		} elseif ( is_singular() && 'hide' === get_post_meta( get_the_ID(), 'show_page_banner', true ) ) {
			$has = false;
		} elseif ( is_singular() && '1' === get_post_meta( get_the_ID(), 'show_page_banner', true ) ) {
			$has = true;
		} elseif ( is_single() && $disable_in_blog_posts && '1' !== get_post_meta( get_the_ID(), 'show_page_banner', true ) ) {
			$has = false;
		} elseif ( is_singular( array( /*'post',*/ 'product'/*, 'portfolio'*/ ) ) ) {
			$has = false;
			if ( ! has_post_thumbnail() ) {
				$has = false;
			}
		} elseif ( is_home() ) {
			$posts_page_id = get_option( 'page_for_posts' );

			if ( $posts_page_id && 'hide' === get_post_meta( $posts_page_id, 'show_page_banner', true ) ) {
				$has = false;
			}
		} elseif ( is_post_type_archive( 'portfolio' ) ) {
			if ( 'masonry' != gutenbooster_getmod( 'portfolio_style' ) ) {
				$has = false;
			} else {
				$has = true;
			}
		} elseif ( function_exists( 'yith_wcwl_is_wishlist_page' ) && yith_wcwl_is_wishlist_page() ) {
			$has = false;
		} elseif ( function_exists( 'soow_is_wishlist' ) && soow_is_wishlist() ) {
			$has = false;
		} elseif ( function_exists( 'WC' ) ) {
			if ( is_account_page() || is_cart() || is_checkout() ) {
				$has = false;
			} elseif ( is_shop() && ! get_post_meta( wc_get_page_id( 'shop' ), 'get_post_meta', true ) ) {
				$has = false;
			}
		}

		// elseif ( gutenbooster_is_order_tracking_page() ) {
		// 	$has = false;
		// }
		/*elseif ( is_404() ) {
		$has = false;
		}*/

		/**
		* Filter for checking has page header
		*
		* @since  1.0
		*/
		return apply_filters( 'gutenbooster_has_page_banner', $has );
	}

 endif;

 /**
  * Page Banner Parallax and Animation Props
  */
if ( ! function_exists( 'gutenbooster_pageban_props' ) ) :
	function gutenbooster_pageban_props( $prop = '' ) {

		$object_id = get_queried_object_id();
		if ( gutenbooster_is_shop_home() && gutenbooster_shop_page_id() ) {
			$object_id = gutenbooster_shop_page_id();
		}

		if ( get_post_meta( $object_id, 'override_page_banner_settings', true ) ) {
			// metabox driven settings
			$pageban_parallax = get_post_meta( $object_id, 'enable_page_banner_bgparallax', true );
			$anim_data        = get_post_meta( $object_id, 'enable_page_banner_animate', true ) ? 'data-kiomotion="true" data-animtype="fadereveal"' : 'data-none=""';
		} else {
			// customizer driven settings
			$pageban_parallax = gutenbooster_getmod( 'enable_page_banner_bgparallax' );
			$anim_data        = gutenbooster_getmod( 'enable_page_banner_animate' ) ? 'data-kiomotion="true" data-animtype="fadereveal"' : 'data-none=""';
		}

		switch ( $prop ) {
			case 'parallax_class':
				$parallax_class = true == $pageban_parallax ? 'parallaxed' : 'noparallax';
				return $parallax_class;
			break;

			case 'bg_anim_data':
				return $anim_data;
			break;

			case 'subtitle_around':
				$get_subtitle_around = get_post_meta( $object_id, 'page_banner_subtitle_around', true );
				$subtitle_around = 'subtitle_around_' . get_post_meta( $object_id, 'page_banner_subtitle_around', true );

				if ( empty( $get_subtitle_around ) ) {
					$subtitle_around = 'subtitle_around_line';
				}
				return $subtitle_around;
			break;
		}

	}
endif;

/**
 * Check if Page Banner Has Animation
 * @return bool
 */
if ( ! function_exists( 'gutenbooster_pageban_single_has_anim' ) ) :
	function gutenbooster_pageban_single_has_anim() {


		$override = get_post_meta( get_queried_object_id(), 'override_page_banner_settings', true );
		$theID		= get_queried_object_id();

		if( function_exists( 'is_shop' ) && is_shop() ) {
			$theID = get_option( 'woocommerce_shop_page_id' );
			$theID ? $override = get_post_meta($theID, 'override_page_banner_settings', true) : $theID = get_queried_object_id();
		}


		if (!$override) {
			$has = false;
		} else {
			$has = get_post_meta( $theID, 'enable_page_banner_animate', true );
		}



		return apply_filters( 'gutenbooster_pageban_single_has_anim', $has );

	}
endif;

/**
 * Page Banner Background Image Fetch
 */
if ( ! function_exists( 'gutenbooster_pagebanner_bg' ) ) :
	function gutenbooster_pagebanner_bg(){
		global $post;

		$output_bg_img 		= '';
		$image_id					= null;
		// $image_size				= '1400px-optimized';
		$image_size				= 'full';


		if (is_singular() && has_post_thumbnail()){
			//single pages and posts
			$image_id = get_post_thumbnail_id( $post->ID );
		} elseif( is_home() && get_option('show_on_front') !=='posts' ) {
			//if a page is set as blog, and another page is set as site homepage
			$image_id = get_post_thumbnail_id( get_queried_object_id() );
		} elseif( gutenbooster_is_shop_home() && gutenbooster_shop_page_id() && has_post_thumbnail(gutenbooster_shop_page_id()) ) {
			//if shop homepage
			$image_id = get_post_thumbnail_id( gutenbooster_shop_page_id() );
		} else {
			//everywhere else
			$image_id = attachment_url_to_postid( get_theme_mod( 'page_banner_bg' ) );
		}

		$img_attachment 	= wp_get_attachment_image_src($image_id, $image_size);
		$final_img 				= $img_attachment[0];

		$output_bg_img = !empty($image_id) ? $final_img : '';

		return $output_bg_img;
	}
endif;

/**
 * Page Banner Background Image Fetch
 */
if ( ! function_exists( 'gutenbooster_pb_bg_opacity' ) ) :
	function gutenbooster_pb_bg_opacity(){
		global $post;

		$opacity = null;

		if (gutenbooster_postmeta( 'override_page_banner_settings', true )) {
			// metabox driven settings
			$opacity	= gutenbooster_postmeta( 'page_bannerbg_opacity', true );
		} else {
			// customizer driven settings
			$opacity	= gutenbooster_getmod('page_bannerbg_opacity');
		}

		return $opacity;
	}
endif;

/**
 * Page Banner Background Image Fetch
 */
if ( ! function_exists( 'gutenbooster_pagebanner_has_image' ) ) :
	function gutenbooster_pagebanner_has_image(){
		global $post;

		if ( has_post_thumbnail(get_queried_object_id())) {

			$has = true;

		} else {
			$has = false;
		}
		return apply_filters( 'gutenbooster_pagebanner_has_image', $has );

	}
endif;

/**
 * Page Banner Buttons
 */
if ( ! function_exists( 'gutenbooster_pagebanner_button' ) ) :
	function gutenbooster_pagebanner_button(){
		$button_enable 			= get_post_meta( get_the_ID(), 'enable_pb_button', true );
		$button_text 	 			= get_post_meta( get_the_ID(), 'pb_button_text', true );
		$get_button_url 		= get_post_meta( get_the_ID(), 'pb_button_url', true );
		$get_button_style 	= get_post_meta( get_the_ID(), 'pb_button_style', true );

		$button_url 				= $get_button_url ? esc_url($get_button_url) : '#';
		$button_style 			= $get_button_style ? $get_button_style : 'std';
		$button_class				= 'k-btn btn__primary btn__lg btn__uc ';
		if ($button_style == "gutenbooster") {
			$button_class 	 .=	'btn__innerborder btn__arrvrt';
		}





		if ($button_enable && $button_text) :

			?>
		<div class="heading-content button-wrap">
			 <div>
					<a href="<?php echo esc_url($button_url); ?>" class="<?php echo esc_html($button_class); ?>"><span><?php echo esc_html($button_text); ?></span></a>
			 </div>
		</div>
		<?php
		endif;

	}
endif;


/**
 * Get Page Layout -
 * TODO:	Remember adding gutenbooster_is_order_tracking_page to the line where it says is_404()
 */
if ( ! function_exists( 'gutenbooster_get_layout' ) ) :
	/**
	 * Get layout base on current page
	 *
	 * @return string
	 */
	function gutenbooster_get_layout() {
		$layout = gutenbooster_getmod( 'layout_default' );


		if ( is_404() || is_attachment() || is_singular( array(
				'product',
				'portfolio',
			) ) || is_post_type_archive( array( 'portfolio' ) ) || is_tax( 'portfolio_type' ) ||  is_page_template( 'templates/homepage.php' )
		) {
			$layout = 'no-sidebar';
		} elseif ( function_exists( 'is_cart' ) && is_cart() ) {
			$layout = 'no-sidebar';
		} elseif ( function_exists( 'is_checkout' ) && is_checkout() ) {
			$layout = 'no-sidebar';
		} elseif ( function_exists( 'is_account_page' ) && is_account_page() ) {
			$layout = 'no-sidebar';
		} elseif ( function_exists( 'soow_is_wishlist' ) && soow_is_wishlist() ) {
			$layout = 'no-sidebar';
		} elseif ( function_exists( 'yith_wcwl_is_wishlist_page' ) && yith_wcwl_is_wishlist_page() ) {
			$layout = 'no-sidebar';
		} elseif ( is_singular() && gutenbooster_postmeta( 'custom_layout', true ) ) {
			$layout = gutenbooster_postmeta( 'layout', true );
		} elseif ( is_singular( 'post' ) ) {
			$layout = gutenbooster_getmod( 'layout_post' );
		} elseif ( function_exists( 'is_woocommerce' ) && ( is_woocommerce() || is_post_type_archive( 'product' ) ) ) {
			$layout = gutenbooster_getmod( 'layout_shop' );
		} elseif ( is_page() ) {
			$layout = gutenbooster_getmod( 'layout_page' );
		}

		return $layout;
	}

endif;


if ( ! function_exists( 'gutenbooster_icon_url' ) ) :
	function gutenbooster_icon_url( $the_icon = 'search', $echo = false ) {


		if ($the_icon !== 'divider') {
			$icon = '<svg class="feather"><use xlink:href="' . esc_url( get_template_directory_uri().'/assets/img/feather-sprite.svg#'. $the_icon ) . '"></use></svg>';
		} else {
			$icon = '<svg width="2px" height="20px" viewBox="0 0 2 20" version="1.1" xmlns="http://www.w3.org/2000/svg"><g fill-rule="evenodd"><g transform="translate(-11, -2)" fill="currentColor" fill-rule="nonzero"><polygon id="Line" points="11 2 13 2 13 22 11 22"></polygon></g></g></svg>';
		}


		if ( $echo ) {
			gutenbooster_svg_kses( $icon );
		} else {
			return $icon;
		}
	}

endif;

if ( ! function_exists( 'gutenbooster_svg_kses' ) ) :
	function gutenbooster_svg_kses( $icon ) {
		$kses_defaults = wp_kses_allowed_html( 'post' );

		$svg_args = array(
		    'svg'   => array(
		        'class' => true,
		        'aria-hidden' => true,
		        'aria-labelledby' => true,
		        'role' => true,
		        'xmlns' => true,
		        'width' => true,
		        'height' => true,
		        'height' => true,
		        'viewbox' => true, // <= Must be lower case!
		    ),
			'use' => array(
				'xlink:href' => true,
				'href' => true,
			),
		    'g'     => array( 'fill' => true ),
		    'title' => array( 'title' => true ),
		    'path'  => array( 'd' => true, 'fill' => true,  ),
		);

		$allowed_tags = array_merge( $kses_defaults, $svg_args );

		echo wp_kses( $icon, $allowed_tags );

	}
endif;


/**
 * Get CSS classes for content columns
 */
if ( ! function_exists( 'gutenbooster_content_col_classes' ) ) :
	/**
	 * Get CSS classes for content columns
	 *
	 * @param string $layout
	 *
	 * @return string
	 */
	function gutenbooster_content_col_classes() {
		$layout 	= gutenbooster_get_layout();
		$cols		= "";

		if ( is_singular( 'page' ) && !is_active_sidebar('page-sidebar') ||
			 (function_exists( 'is_woocommerce' ) && is_woocommerce() && !is_active_sidebar('shop-sidebar')) ||
			 !is_active_sidebar('sidebar-default') ) {
				 $centerit = ' mx-auto apply-mw';
		 } else {
			 $centerit = '';
		 }

		if ( $layout == "no-sidebar" || $layout == "fullwidth" ) {
			$cols = "col";
		} else if ( $layout == "sb-left" ) {
			$cols = "col-xl-9 col-lg-8 pl-4 order-lg-2".$centerit;
		} else if ( $layout == "sb-right" ) {
			$cols = "col-xl-9 col-lg-8 pr-4".$centerit;
		}
		echo esc_attr($cols);
	}

endif;

/**
 * Blog Cards Layout Wrapper
 */
if ( ! function_exists( 'gutenbooster_cards_wrapper' ) ) :

	function gutenbooster_cards_wrapper($wrap="start") {

    $blog_layout  = gutenbooster_getmod('blog_layout');

    if ($blog_layout != 'cards') {
      return;
    }
    if ($wrap !== "end" ) : ?>
      <div class="col"><div class="card-columns">
	<?php
    else :
	?>
    </div></div>
	<?php
    endif;


  }

endif;


/**
 * Print Post Meta for blog posts
 */
if ( ! function_exists( 'gutenbooster_post_meta' ) ) :

	function gutenbooster_post_meta($blog_layout="") {
    // global $post;
    $gutenbooster_page_layout    = gutenbooster_getmod('layout_default');


		$time_string    = sprintf(
			'<time class="entry-date published updated" datetime="%1$s">%2$s</time>',
			esc_attr( get_the_date( 'Y-m-d' ) ),
			esc_html( get_the_date( get_option( 'date_format', 'd.m Y' ) ) )
		);

    if ($blog_layout == "cards" && has_post_thumbnail() ) {
      $the_author = ( $gutenbooster_page_layout != 'no-sidebar' ) ? "" : '<span class="kt-author">'. get_the_author() . '</span>';

      $posted_on = is_singular() ? $time_string : '' . $time_string . '';
      echo  '<span class="posted-on">' . $posted_on . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

    } else {

      $posted_on = is_singular() ? $time_string : '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>';
      if ( is_singular() ) {

		the_post(); // queue first post
		$author_id = get_the_author_meta('ID');
		$curauth = get_user_by('id', $author_id);
		$user_nicename    = $curauth->user_nicename;
		$display_name     = $curauth->display_name;
		$user_url         = $curauth->user_url;

		rewind_posts(); // rewind the loop

		$author_link      = '<a href="'.get_author_posts_url($author_id, $user_nicename).'">' . $display_name . '</a>';

        echo get_the_category_list( esc_html__( ', ', 'gutenbooster' ) ) . ' &nbsp; <span class="posted-on">' . $posted_on . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

      } else {
		$separator = has_category() ? ' &nbsp; <span class="faded">|</span> &nbsp; ' : '';
        echo get_the_category_list( esc_html__( ', ', 'gutenbooster' ) ) . $separator . '<span class="posted-on">' . $posted_on . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
      }

    }

	}

endif;

/**
 * Post Tags
 */
if ( ! function_exists( 'gutenbooster_post_tags' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function gutenbooster_post_tags() {

		// Hide category and tag text for pages.
		if ( is_singular() && 'post' === get_post_type() ) {

			// Let's check to see if the option is enabled via the Customizer.
			$option     = true;
			$visibility = ( false === $option ) ? ' hidden' : null;

			if ( ! $option && ! is_customize_preview() ) {
				return;
			}

			$tags_list = get_the_tag_list( '', '' );

			if ( ! $tags_list ) {
				return;
			}

			if ( $tags_list ) {
				printf( '<span class="tags-links sans-serif-font extra-small medium smooth dark-gray %1$s">%2$s</span>', esc_attr( $visibility ), $tags_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
		}
	}
endif;

/**
 * Print Post Meta for blog posts
 */
if ( ! function_exists( 'gutenbooster_single_meta' ) ) :

	function gutenbooster_single_meta() {



	}

endif;


if ( ! function_exists( 'gutenbooster_excerpt_length' ) ) :
function gutenbooster_excerpt_length() {

	$limit_data = gutenbooster_getmod( "blog_excerpt_length" );
    $limit = $limit_data ? $limit_data : 25;

    return $limit;
}
add_filter( 'excerpt_length', 'gutenbooster_excerpt_length', 999 );
endif;

/**
 * Custom Excerpt Length
 */
if ( ! function_exists( 'gutenbooster_thumb_size' ) ) :

	function gutenbooster_thumb_size() {

    global $wp_query;

    $switch   = gutenbooster_getmod('blog_layout');
    $gutenbooster_page_layout = gutenbooster_getmod( 'layout_default' );

    $imgSize  = !isset($switch) ? $imgSize : 'gutenbooster-medium-horizontal';


    if ($switch == "classic" || $switch == "grid") {

       if ( (( $wp_query->current_post + 1 ) % 5 == 1 && has_post_thumbnail()
              /* && $gutenbooster_page_layout != "no-sidebar" */ ) ) {
         $imgSize = 'gutenbooster-xlarge';
       } else {
         $imgSize = "gutenbooster-small-horizontal";
       }

   } else { $imgSize = "large"; }

    return $imgSize;

  }

endif;

/**
 * Shorten Characters by adding ellipsis
 */
if ( ! function_exists( 'gutenbooster_truncate' ) ) :
	function gutenbooster_truncate($text, $chars = 25) {
	   $text = $text." ";
	   $text = substr($text,0,$chars);
	   $text = substr($text,0,strrpos($text,' '));
	   $text = $text."...";
	   return $text;
	}
endif;


/**
 * Blog Post Tags
 */
if ( ! function_exists( 'gutenbooster_archive_title' ) ) :
 /**
	* Prints HTML with meta information for the current post-date/time.
	*/
 function gutenbooster_archive_title() {
	 $has_pb = gutenbooster_has_page_banner();
	 ! $has_pb && the_archive_title( '<div class="container mb-5"><div class="row text-center"><div class="col text-center"><h1 class="archive-title">', '</h1></div></div></div>' );
 }
endif;

/**
 * Change markup of archive and category widget to include .count for post count
 *
 * @param string $output
 *
 * @return string
 */
function gutenbooster_archive_count( $output ) {
	$output = preg_replace( '|\((\d+)\)|', '<span class="count">\\1</span>', $output );

	return $output;
}
function gutenbooster_archive_count_ddown( $links ) {

	$links = str_replace('</a>&nbsp;(', '</a> <span class="count">', $links);

   	$links = str_replace(')', '</span>', $links);
	$links = str_replace('</span></option>', '</option>', $links);
	$links = str_replace('&nbsp;(', '-&nbsp;', $links);

   return $links;
}
add_filter( 'wp_dropdown_categories', 'gutenbooster_archive_count_ddown' );
add_filter( 'wp_list_categories', 'gutenbooster_archive_count' );
add_filter( 'get_archives_link', 'gutenbooster_archive_count_ddown' );


/**
 * Change more string at the end of the excerpt
 *
 * @since  1.0
 *
 * @param string $more
 *
 * @return string
 */

function gutenbooster_excerpt_more( $more ) {
	$more = '&hellip;';

	return $more;
}

add_filter( 'excerpt_more', 'gutenbooster_excerpt_more' );



/**
 * Print HTML for single post header
 */
function gutenbooster_single_post_header() {
	if ( ! is_singular( 'post' ) || gutenbooster_has_page_banner() ) {
		return;
	}
  ?>

  <div class="text-center post-intro w-100">

  	<header class="title-heading p-4">
  		<h1><?php the_title( ); ?></h1>
  		<div class="post-meta">
  			<?php gutenbooster_post_meta(); ?>
  		</div>
  	</header>
  	<?php if (has_post_thumbnail() ): ?>
  		<figure class="postimg-wrapper">
  			<?php the_post_thumbnail( $size = 'gutenbooster-xlarge', $attr = '' ); ?>
  		</figure>

  	<?php endif; ?>


  </div>

  <?php
}

add_action( 'gutenbooster_before_content', 'gutenbooster_single_post_header' );


/**
 * Blog Post Tags
 */
add_filter('comment_reply_link', 'gutenbooster_replace_reply_link_class');
function gutenbooster_replace_reply_link_class($class){
	 $class = str_replace("class='comment-reply-link", "class='comment-reply-link underlined", $class);
	 return $class;
}


/**
 * Parse CSS
 */
if ( ! function_exists( 'gutenbooster_parse_css' ) ) {

	/**
	 * Parse CSS
	 *
	 * @param  array  $css_output Array of CSS.
	 * @param  string $min_media  Min Media breakpoint.
	 * @param  string $max_media  Max Media breakpoint.
	 * @return string             Generated CSS.
	 */
	function gutenbooster_parse_css( $css_output = array(), $min_media = '', $max_media = '' ) {

		$parse_css = '';
		if ( is_array( $css_output ) && count( $css_output ) > 0 ) {

			foreach ( $css_output as $selector => $properties ) {

				if ( ! count( $properties ) ) {
					continue; }

				$temp_parse_css   = $selector . '{';
				$properties_added = 0;

				foreach ( $properties as $property => $value ) {

					if ( '' === $value ) {
						continue; }

					$properties_added++;
					$temp_parse_css .= $property . ':' . $value . ';';
				}

				$temp_parse_css .= '}';

				if ( $properties_added > 0 ) {
					$parse_css .= $temp_parse_css;
				}
			}

			if ( '' != $parse_css && ( '' !== $min_media || '' !== $max_media ) ) {

				$media_css       = '@media ';
				$min_media_css   = '';
				$max_media_css   = '';
				$media_separator = '';

				if ( '' !== $min_media ) {
					$min_media_css = '(min-width:' . $min_media . 'px)';
				}
				if ( '' !== $max_media ) {
					$max_media_css = '(max-width:' . $max_media . 'px)';
				}
				if ( '' !== $min_media && '' !== $max_media ) {
					$media_separator = ' and ';
				}

				$media_css .= $min_media_css . $media_separator . $max_media_css . '{' . $parse_css . '}';

				return $media_css;
			}
		}

		return $parse_css;
	}
}

if ( ! function_exists( 'gutenbooster_get_image_sizes' ) ) {
	/**
	 * Output image sizes for attachment single page.
	 *
	 * @since Gutenbooster 1.0
	 */
	function gutenbooster_get_image_sizes() {

		/* If not viewing an image attachment page, return. */
		if ( ! wp_attachment_is_image( get_the_ID() ) ) {
			return '';
		}

		/* Set up an empty array for the links. */
		$links = array();

		/* Get the intermediate image sizes and add the full size to the array. */
		$sizes   = get_intermediate_image_sizes();
		$sizes[] = 'full';

		/* Loop through each of the image sizes. */
		foreach ( $sizes as $size ) {

			/* Get the image source, width, height, and whether it's intermediate. */
			$image = wp_get_attachment_image_src( get_the_ID(), $size );

			/* Add the link to the array if there's an image and if $is_intermediate (4th array value) is true or full size. */
			if ( ! empty( $image ) && ( ( ! empty( $image[3] ) && true === $image[3] ) || 'full' === $size ) ) {
				$links[] = '<a target="_blank" class="image-size-link" href="' . esc_url( $image[0] ) . '">' . $image[1] . ' &times; ' . $image[2] . '</a>';
			}
		}

		/* Join the links in a string and return. */

		return join( ' <span class="sep">|</span> ', $links );
	}
}


