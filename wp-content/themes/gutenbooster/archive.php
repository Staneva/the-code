<?php
  /**
  * The template for displaying archive pages
  *
  * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
  *
  * @package gutenbooster
  */

  get_header();

	gutenbooster_archive_title();
?>

<div class="k-content-inner <?php gutenbooster_content_col_classes(); ?>">
	<div class="row">
		<?php
			gutenbooster_cards_wrapper( 'start' );
		?>
		<?php
		if ( have_posts() ) :
			// Start the Loop.
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

				// End the loop.
			endwhile;

		else :
			get_template_part( 'template-parts/content', 'none' );
		endif;

			gutenbooster_cards_wrapper( 'end' );

			the_posts_pagination(
				array(
					'prev_text' => gutenbooster_icon_url( 'arrow-left', false ),
					'next_text' => gutenbooster_icon_url( 'arrow-right', false ),
				)
			);
			?>
	</div><!-- .row -->
</div><!-- .k-content-inner -->

<?php

	get_sidebar();
	get_footer();
