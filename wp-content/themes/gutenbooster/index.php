<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gutenbooster
 */

get_header();
?>
			<?php gutenbooster_archive_title(); ?>
			<div class="k-content-inner <?php gutenbooster_content_col_classes(); ?>" >
				<div class="row">


					<?php gutenbooster_cards_wrapper("start"); ?>
				<?php
				if ( have_posts() ) :
					/* Start the Loop */
					while ( have_posts() ) : the_post();
						/*
						 * Include the Post-Type-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content', get_post_format() );
					endwhile; ?>

				<?php
				else :
					get_template_part( 'template-parts/content', 'none' );
				endif;
				?>
				<?php gutenbooster_cards_wrapper("end");
				the_posts_pagination( array(
					'prev_text' => gutenbooster_icon_url("arrow-left"),
					'next_text' => gutenbooster_icon_url("arrow-right"),
				) );
				?>

			</div><!-- .row -->
		</div><!-- .k-content-inner -->

<?php
get_sidebar();
get_footer();
