var $j = jQuery.noConflict();

var gbDelayTime = 1000;


/*= init function that fires all theme js functions: =*/
/*=============================================<<<<<*/
function initGutenBooster( gbDelayTime=500 ) {


    var checkOs = navigator.platform;
    // Smooth Scrollbar
    if ($j('body').hasClass('has-smoothscroll')) {
      //do nothing yet..
    }


    //object fit
    if (typeof objectFitImages !== "undefined") {
      objectFitImages();
    }

    // stickybits
    // $('.single-product div.summary').stickybits();
    if (typeof stickybits !== "undefined") {
      stickybits('.single-product div.summary', {
        useStickyClasses: true,
      });
      // console.log ('stickybits on');
    }


    gutenboosterHeader();
    gutenboosterPageBanner(gbDelayTime);
    gutenboosterImagesLoaded();
    gutenboosterFocusClass();
    gutenboosterFixedFooter();
    gutenboosterNoBarbaClasses();
    gutenboosterWooJS();
    gutenboosterHeaderSearch();
    gutenboosterMobileMenu();
    gutenboosterPostImages();
    // gutenboosterAnimatePB();

    //alignment fixes for some kioken blocks
    gutenboosterAlignmentFixes();
    $j(window).resize(gutenboosterAlignmentFixes);


    gutenboosterLightboxes();
    gutenboosterPostSliders();
    gutenboosterOnepageLinks();


}



/*= Here we go. Functions themselves: =*/
/*=============================================<<<<<*/

//Check browser agent for mobile devices
function gutenboosterMobileCheck() {
   if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/)) {
      return true;
   } else {
      return false;
   }
}
//header stickyness check
function gutenboosterCheckHeaderStickyness() {
  var body          = $j('body');
  if (body.is('.header--stick-headroom, .header--stick-sticky')) {
    return true;
  } else {
    return false;
  }
}

//Header Events
function gutenboosterHeader() {

   var $header       = $j('#masthead');
   var $headerHeight = $header.height();
   var $shrink       = $header.attr('data-shrink');
   var body          = $j('body');
   var $submenu      = $header.find('.nav-menu');
   var actDistance   = 10;
   var barbaC        = $j('.barba-container');
   var barbaCNoPb    = $j('.barba-container:not(.has-page-banner)');


   if (body.hasClass('header--shrinks') || barbaC.hasClass('header--shrinks')) {

     $j(window).on('scroll load', function(){
        var toppos     = $j(this).scrollTop();
        $header.toggleClass("shrink", (toppos > actDistance));
     });


   }//end shrink check

   //add class of sticking to the sticky header
   if (gutenboosterCheckHeaderStickyness()) {
     $j(window).on('scroll load', function(){
        var toppos     = $j(this).scrollTop();
        $header.toggleClass("sticking", (toppos > actDistance));
     });
   }


   // superfish
   $submenu.superfish({
     delay: 700,
  });

  // sticky header. add top margin to main site wrapper
  // when sticky is selected for the position
  // maybe NOT TODAY!
  /*if ($j('body').is('.header--stick-sticky, .header--stick-headroom') ) {
    $j(window).on('resize', function() {
      var $headerHeight = $j('#masthead').height();
      $j('.site-inner').css('padding-top', $headerHeight + 'px');
    });
  }*/

  //headroom
   if ($j('body').hasClass('header--stick-headroom') || barbaC.hasClass('header--stick-headroom') ) {

     var headerElem = document.querySelector('.menu-wrapper');
     var headroom   = new Headroom(headerElem, {
        "offset": 100
     });
     headroom.init();
  }



}
//Page Banner Events
function gutenboosterPageBanner( gbDelayTime ) {


   var $pageBanner      = $j('#page-banner-wrap');


   if ($pageBanner.length) {

      // var $pbHeight        = $pageBanner.height();
      var $kontain          = $pageBanner.find('.kontain');

      var animCheck         = $kontain.attr('data-kiomotion');


      var checkFirstTimeLoad= $j("#barba-wrapper");

      // check if preloader exists, if so add s
      if (checkFirstTimeLoad.length) {
        // console.log('Transition loader exist');
      } else {
        gbDelayTime = 0;
        // console.log('Transition loader dont exist');
      }
      //check if animation attr is applied
      if ( typeof animCheck !== typeof undefined && animCheck !== false ) {
          gutenboosterAnimatePB(gbDelayTime)
      }

      var parallaxCheck     = $kontain.hasClass('parallaxed');
      //check parallax for page banner headings
      if (parallaxCheck == true && ! $j('body').hasClass('is_IE')) {

        var $pageBannerHeight   = $j('#page-banner-wrap').height();
        var $headerMenuHeight   = $j('#masthead.menu-wrapper').height();
        var $totalHeight        = parseInt($headerMenuHeight + $pageBannerHeight );
        var $headingWrap        = $j('.banner-heading-wrap');
        var $el                 = $j('.page-banner-bg');
        var $elOpacity          = $el.data("curr-opacity");


         $j(window).on("scroll load", function() {
            var scroll              = $j('body').hasClass('is_safari') ? Math.round($j(window).scrollTop()) : $j(window).scrollTop();

            //white background or transparent
            if ( $j('body').hasClass('header--bg-white') && !gutenboosterMobileCheck() ) {

              if ( scroll <= $totalHeight ) {
                $el.css({ transform:'translate3d(0, ' + (Math.round(scroll*0.5))+'px, 0)', top: '-'+$headerMenuHeight+'px', height: '+'+ $totalHeight +'px'});
              }

            } else {
              if ( scroll <= $pageBannerHeight && !gutenboosterMobileCheck() ) {
                $el.css({ transform:'translate3d(0, ' + (scroll*0.3)+'px, 0)'});
                if (scroll > 0 && $elOpacity ) { $el.css({ opacity: $elOpacity - Math.round(scroll) / $pageBannerHeight * 0.5, transition:'none' }); }
              }
            }


            //titles and buttons scroll parallax fade
            if (!$j('body').hasClass('is_safari')) {
              $headingWrap.css({ transform : 'translateY(+'+ Math.round(scroll / 2.5) + 'px)', opacity: 1 - Math.round(scroll) / $pageBannerHeight * 1.4, });
            } else {
              $headingWrap.css({ transform: 'translate3d(0, ' + (Math.round(scroll*0.2))+'px, 0)', opacity: 1 - Math.round(scroll) / $pageBannerHeight * 1.4, });
            }


         });
      }


   }
}

//Scroll Document to Top
function gutenboosterPageReturnUp() {
  $j('html, body').animate({scrollTop:0}, 900, 'swing', function() {
  });
}

//Input Focus
function gutenboosterFocusClass() {
  $j( '.woocommerce-account, .woocommerce-checkout' ).on( 'focus', '.input-text', function () {
    $j( this ).closest( '.form-row' ).addClass( 'active' );
  } ).on( 'focusout', '.input-text', function () {
    if ( $j( this ).val() === '' ) {
      $j( this ).closest( '.form-row' ).removeClass( 'active' );
    }
  } ).find( '.input-text' ).each( function () {
    if ( $j( this ).val() !== '' ) {
      $j( this ).closest( '.form-row' ).addClass( 'active' );
    }
  } );

  $j( '#commentform' ).on( 'focus', ':input', function () {
    $j( this ).closest( 'p' ).addClass( 'active' );
  } ).on( 'focusout', ':input', function () {
    if ( $j( this ).val() === '' ) {
      $j( this ).closest( 'p' ).removeClass( 'active' );
    }
  } ).find( ':input' ).each( function () {
    if ( $j( this ).val() !== '' ) {
      $j( this ).closest( 'p' ).addClass( 'active' );
    }
  } );

  // checking for webkit autofill
  (function ( $ ) {
   'use strict';

    setTimeout(function() {

      $j('.woocommerce-account, .woocommerce-checkout').find('.input-text').each(function(){
        if ($j(this).is("*:-webkit-autofill")) {
          // console.log ('Kioken Woo: Autofill exist');
          $j( this ).closest( '.form-row' ).addClass( 'active' );
        }
      });

    }, 200);

  })( jQuery );

}

//Image Preloads
function gutenboosterImagesLoaded() {

}
//Fixed Footer
function gutenboosterFixedFooter() {

  var footerHeight  = $j("#colophon").height();
  function addMargin() {
    $j('.fixed_footer .site-inner').css({'margin-bottom': +footerHeight+'px'});
  }

  addMargin();
  $j(window).resize(addMargin);


}

//no ajax link classes
function gutenboosterNoBarbaClasses() {
  var elemen = ['.widget_meta ul li a',
                '.woocommerce-product-gallery__image a',
                '.woocommerce .cart_item a',
                '.woocommerce.widget_recently_viewed_products a',
                '.woocommerce.widget_products .product_list_widget a',
                '.woocommerce .text-link',
                '.wp-block-gallery .blocks-gallery-item a',
                'a.magnifiq'
                ].join(',');
  $j(elemen).addClass('no-barba');
}


//Woocommerce JS
function gutenboosterWooJS() {

  //Product Description and Review Tabs
  $j('body').on( 'click', '.tabs-nav .tab-nav', function () {
			var $tab = $j( this ),
				tab = $tab.data( 'tab' );

			if ( $tab.hasClass( 'active' ) ) {
				return;
			}

			$tab.addClass( 'active' ).siblings( '.active' ).removeClass( 'active' );
			$tab.closest( '.tabs-nav' ).next( '.tab-panels' )
				.children( '[data-tab="' + tab + '"]' ).addClass( 'active' )
				.siblings( '.active' ).removeClass( 'active' );
		} );

}

function gutenboosterAnimatePB(gbDelayTime) {
  var $kontain          = $j('#page-banner-wrap .kontain');

  	// Check if Animation is Activated
    var animFade          = $j('.kontain[data-animtype*="fadereveal"]');
    var checkLoader       = $j("#barba-wrapper");

    // Fire Fade Reveal Animation
    if ( animFade.length && typeof anime !== typeof undefined ) {

			var titlesReveal			= '#page-banner-wrap .heading-content > div';

      // apply the animated-in class for background scale and opacity animation
      setTimeout(function(){ $kontain.addClass('animated-in'); }, gbDelayTime);

      var tl = anime.timeline({ duration: 1000, easing: 'easeOutCubic' });
					tl.add( { targets: titlesReveal, translateY: ['100%',0], delay: anime.stagger( 120, { start: 700 } ) } );

    } else {
			console.warn('anime js doesn\'t exist');
    }

}

function gutenboosterHeaderSearch() {
  var parentEl      = $j('#masthead');
  var searchEl      = $j('#masthead .header-search');
  var searchOpener  = $j('#masthead a[data-target="search-modal"]');
  var searchCloser  = $j('#masthead .header-search a.close-form');

  if (searchOpener.length) {
    searchOpener.on('click', function(e){
      e.preventDefault();
      searchEl.addClass('inview');
      parentEl.addClass('is-searching');
    });

    searchCloser.on('click', function(e){
      e.preventDefault();
      searchEl.removeClass('inview');
      parentEl.removeClass('is-searching');
    });

    $j(document).keyup(function(e) {
         if (e.key === "Escape") { // escape key maps to keycode `27`
            searchEl.removeClass('inview');
            parentEl.removeClass('is-searching');
        }
    });

  }
}

function gutenboosterMobileMenu() {

    $j('#mobile-navigation .nav-menu li a').addClass('no-barba');
    $j('#mobile-navigation .nav-menu').removeClass('nav');
    $j('#mobile-navigation .nav-menu li.menu-item-has-children').append('<span class="arr-down">&#43;</span>');
    $j('#mobile-navigation .nav-menu li.page_item_has_children').append('<span class="arr-down">&#43;</span>');
    $j('#mobile-navigation .nav-menu li.menu-item-has-children > span.arr-down, #mobile-navigation .nav-menu li.page_item_has_children > span.arr-down').on('click', function(e) {

    		$j(this).toggleClass('active');
    		$j(this).parent().find(' > ul').toggleClass('shown');

    	e.preventDefault();

    });


  $j('.menu-item-jsnav a').on('click', function(){
    // e.preventDefault();
    $j('body').addClass('jsnav-open');
    $j(window).scrollTop(0);
  });

  $j('#mobile-navigation a.close-jsnav, .body-jsnav-overlay').on('click', function(){

    $j('body').removeClass('jsnav-open');
  });





}

function gutenboosterAlignmentFixes() {

  var checkNoSidebar = $j('body').hasClass('layout-no-sidebar') ? true : false;
  var checkNoBustOut = $j('body').hasClass('alignments-no-bustout') ? true : false;

  var childrenContainerW = $j('body').attr('data-content-width');
      childrenContainerW = childrenContainerW ? parseInt(childrenContainerW) : 800;

  var marginValtoPush = childrenContainerW / 2;

  if (window.matchMedia('(min-width: 768px)').matches ) {

    if ( checkNoBustOut ) {

      $j('.entry-content > article > .alignleft:not(.wp-block-button)').each(function() {
        $j(this).css({ marginLeft: 'calc(50% - ' + marginValtoPush + 'px)'});
      });

      $j('.entry-content > article > .alignright:not(.wp-block-button)').each(function() {
        $j(this).css({ marginRight: 'calc(50% - ' + marginValtoPush + 'px)'});
      });
    } else {
      //default button block will not get that, regardless...
      $j('.entry-content > article > .alignleft.wp-block-button, .alignleft.wp-block-kioken-icon').each(function() {
        $j(this).css({ marginLeft: 'calc(50% - ' + marginValtoPush + 'px)'});
      });

      $j('.entry-content > article > .alignright.wp-block-button, .alignright.wp-block-kioken-icon').each(function() {
        $j(this).css({ marginRight: 'calc(50% - ' + marginValtoPush + 'px)'});
      });
    }
    // console.log (childrenContainerW);
    // console.log (marginValtoPush);


  }



}

function gutenboosterPostImages() {

  $j('.feed-item .entry-title a').each(function(){

    $j(this).hover(function(){
      $j(this).closest('.media').find('a.post-thumbnail').toggleClass('hovered');
    });
  });

}

function gutenboosterLightboxes() {
  //magnific popup for wp gallery block image links
  $j(".wp-block-gallery .blocks-gallery-item a").each(function(i, el) {
    var href_value = el.href;

    if (/\.(jpg|png|gif)$/.test(href_value)) {
       // console.log(href_value + " is a pic");
       $j(".wp-block-gallery").magnificPopup({
          type: 'image',
          delegate: 'a',
          gallery:{enabled:true},
          removalDelay: 300,
          // Class that is added to popup wrapper and background
          // make it unique to apply your CSS animations just to this exact popup
          mainClass: 'mfp-with-zoom', // this class is for CSS animation below
          zoom: {
            enabled: true, // By default it's false, so don't forget to enable it

            duration: 300, // duration of the effect, in milliseconds
            easing: 'ease-in-out', // CSS transition easing function

            // The "opener" function should return the element from which popup will be zoomed in
            // and to which popup will be scaled down
            // By defailt it looks for an image tag:
            opener: function(openerElement) {
              // openerElement is the element on which popup was initialized, in this case its <a> tag
              // you don't need to add "opener" option if this code matches your needs, it's defailt one.
              return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
          }
        });
    }

  });

  $j("a.magnifiq").each(function() {
    $j(this).magnificPopup({
      type:'image',
      mainClass: 'mfp-with-zoom', // this class is for CSS animation below
      removalDelay: 300,
      // Class that is added to popup wrapper and background
      // make it unique to apply your CSS animations just to this exact popup
      mainClass: 'mfp-with-zoom', // this class is for CSS animation below
      zoom: {
        enabled: true, // By default it's false, so don't forget to enable it

        duration: 300, // duration of the effect, in milliseconds
        easing: 'ease-in-out', // CSS transition easing function

        // The "opener" function should return the element from which popup will be zoomed in
        // and to which popup will be scaled down
        // By defailt it looks for an image tag:
        opener: function(openerElement) {
          // openerElement is the element on which popup was initialized, in this case its <a> tag
          // you don't need to add "opener" option if this code matches your needs, it's defailt one.
          return openerElement.is('img') ? openerElement : openerElement.find('img');
        }
      }
    });
  });

}

function gutenboosterPostSliders() {
  $j('.format-gallery-slider').each(function(){
    var galID = '#' + $j(this).attr('id');
    var galHeight = $j(this).height();

    var eachImg = $j(this).find('img.attachment-large');

    if (typeof objectFitImages !== "undefined") {
        objectFitImages(eachImg);
    }

    if (typeof Swiper !== "undefined") {
        var mySwiper = new Swiper (galID, {
          grabCursor: true,
          speed: 600,
          navigation: {
           nextEl: '.swiper-button-next',
           prevEl: '.swiper-button-prev',
         },
         pagination: {
            el: '.swiper-pagination',
            clickable: true,
            // type: 'progressbar',
            // progressbarOpposite: true,
          },
        })
    }



  });
}

function gutenboosterOnepageLinks() {
  var $menuItem = $j(".scrollto"),
        $window = $j(window);

  $menuItem.on("click","a", function(){
      var $this = $j(this),
          href = $this.attr("href");

      var $elemToScrollTo = $j(href);

      if ($elemToScrollTo.length) {
        var topY = $j(href).offset().top;
        TweenMax.to($window, 1, { scrollTo:{ y: topY, autoKill: true }, ease:Power3.easeOut });
      } else {
        // console.warn('Trying to scroll to an element ID that doesn\'t exist');
      }

    return false;
  });
}


(function ( $ ) {
 'use strict';

  initGutenBooster( 1000 );

})( jQuery );
