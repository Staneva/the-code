<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited

if ( ! empty( $tabs ) ) : ?>

	<div class="woocommerce-tabs wc-tabs-wrapper mb-5">
		<ul class="tabs wc-tabs">
			<?php foreach ( $tabs as $key => $tab ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited ?>
				<li class="<?php echo esc_attr( $key ); ?>_tab">
					<a href="#tab-<?php echo esc_attr( $key ); ?>" class="underlined"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', wp_kses( $tab['title'], array( 'span' => array( 'class' => array() ) ) ), $key ); ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php foreach ( $tabs as $key => $tab ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited ?>
			<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--<?php echo esc_attr( $key ); ?> panel wc-tab" id="tab-<?php echo esc_attr( $key ); ?>">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-xl-8 mx-auto">
							<?php call_user_func( $tab['callback'], $key, $tab ); ?>
						</div>
					</div>
					</div>
			</div>
		<?php endforeach; ?>
	</div>

<?php endif; ?>
