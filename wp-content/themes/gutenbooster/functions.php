<?php

/**
 * GutenBooster functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package GutenBooster
 */

define( 'GUTENBOOSTER_THEMEVERSION', '1.1.3' );
define( 'GUTENBOOSTER_THEME_DIR', trailingslashit( get_template_directory() ) );
define( 'GUTENBOOSTER_THEME_URI', trailingslashit( esc_url( get_template_directory_uri() ) ) );

/**
 * Register widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gutenbooster_widgets_init() {

	$widgetized_footer = gutenbooster_getmod('footer_widgets');

	register_sidebar(
		array(
			'name'          => esc_html__( 'Default Sidebar', 'gutenbooster' ),
			'id'            => 'sidebar-default',
			'description'   => esc_html__( 'Default Sidebar for your theme', 'gutenbooster' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
		register_sidebar(
			array(
				'name'          => esc_html__( 'Page Sidebar', 'gutenbooster' ),
				'id'            => 'page-sidebar',
				'description'   => esc_html__( 'Add widgets here in order to display on pages', 'gutenbooster' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			)
		);
		// if woocommerce is active, register the shop sidebar
		if ( gutenbooster_is_woocommerce_on() ) {
			register_sidebar(
				array(
					'name'          => esc_html__( 'Shop Sidebar', 'gutenbooster' ),
					'id'            => 'shop-sidebar',
					'description'   => esc_html__( 'Add widgets here in order to display on shop pages', 'gutenbooster' ),
					'before_widget' => '<section id="%1$s" class="widget %2$s">',
					'after_widget'  => '</section>',
					'before_title'  => '<h2 class="widget-title">',
					'after_title'   => '</h2>',
				)
			);
		}

		if ( $widgetized_footer ) {

			for ( $i = 1; $i < 5; $i++ ) {
				register_sidebar(
					array(
						/* translators: %s: sidebar number. */
						'name'          => sprintf( esc_html__( 'Footer Sidebar %s', 'gutenbooster' ), $i ),
						'id'            => 'footer-' . $i,
						'description'   => esc_html__( 'Add widgets here in order to display on footer', 'gutenbooster' ),
						'before_widget' => '<div id="%1$s" class="widget %2$s">',
						'after_widget'  => '</div>',
						'before_title'  => '<h4 class="widget-title">',
						'after_title'   => '</h4>',
					)
				);
			}
		}

}


	add_action( 'widgets_init', 'gutenbooster_widgets_init' );


/*=============================================>>>>>
= Color Palette of the theme =
===============================================>>>>>*/
function gutenbooster_palette( $colorname ) {
	switch ( $colorname ) {
		case 'primary':
			$color = '#4138D4';
			break;
		case 'secondary':
			$color = '#1f213a';
			break;
		case 'tertiary':
			$color = '#42444c';
			break;
		case 'customone':
			$color = '#f3f3f5';
			break;
		case 'customtwo':
			$color = '#d50000';
			break;
		case 'customthree':
			$color = '#f50057';
			break;
		case 'customfour':
			$color = '#aa00ff';
			break;
		case 'customfive':
			$color = '#6200ea';
			break;
		case 'customsix':
			$color = '#00c853';
			break;
		case 'customseven':
			$color = '#00b0ff';
			break;
		case 'customeight':
			$color = '#ffd600';
			break;
		case 'customnine':
			$color = '#ff3d00';
			break;
		case 'customten':
			$color = '#fff';
			break;
		case 'customeleven':
			$color = '#4e342e';
			break;
		case 'customtwelve':
			$color = '#546e7a';
			break;
		case 'customthirteen':
			$color = '#cfd8dc';
			break;
		default:
			$color = '#4138D4';
	}
	return $color;
}//end fallback


/**
* After Setup Functions and definitions.
*/
require_once GUTENBOOSTER_THEME_DIR . 'inc/class-gutenbooster-after-setup-theme.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require_once GUTENBOOSTER_THEME_DIR . 'inc/template-functions.php';

/**
 * TGMPA.
 */
require_once GUTENBOOSTER_THEME_DIR . 'inc/admin/tgmpa/plugin_includes.php';

/**
 * Customizer Function Group
 */
require_once GUTENBOOSTER_THEME_DIR . 'inc/admin/customizer/includes/customizer-functions.php';
require_once GUTENBOOSTER_THEME_DIR . 'inc/admin/customizer/class-gutenbooster-customizer.php';
if ( file_exists( GUTENBOOSTER_THEME_DIR . 'inc/pro/class-gutenbooster-pro-init.php') ) {
	 require_once GUTENBOOSTER_THEME_DIR . 'inc/pro/class-gutenbooster-pro-init.php';
 }
new GutenBooster_Customizer();


/**
 * Custom functions for the front end.
 */
require_once GUTENBOOSTER_THEME_DIR . 'inc/frontend/preloader.php';
require_once GUTENBOOSTER_THEME_DIR . 'inc/frontend/frontend.php';
require_once GUTENBOOSTER_THEME_DIR . 'inc/frontend/header.php';
require_once GUTENBOOSTER_THEME_DIR . 'inc/frontend/footer.php';
require_once GUTENBOOSTER_THEME_DIR . 'inc/frontend/comments.php';
require_once GUTENBOOSTER_THEME_DIR . 'inc/frontend/gallery-func.php';
// Core Classes
require_once GUTENBOOSTER_THEME_DIR . 'inc/core/class-gutenbooster-google-fonts.php';
require_once GUTENBOOSTER_THEME_DIR . 'inc/core/class-gutenberg-editor-css.php';
require_once GUTENBOOSTER_THEME_DIR . 'inc/core/class-gutenbooster-enqueue-scripts.php';
require_once GUTENBOOSTER_THEME_DIR . 'inc/core/class-gutenbooster-dynamic-css.php';

/**
 * Custom functions for WooCommerce
 */
if ( gutenbooster_is_woocommerce_on() ) {

	require_once GUTENBOOSTER_THEME_DIR . 'inc/class-gutenbooster-woocommerce.php';

	/**
	* Initialize instances
	*
	* Priority 20 to make sure it's run after plugin's callback, such as register custom post types...
	*/
	function gutenbooster_wc_init() {
		GutenBooster_WooCommerce::instance();
	}
	add_action( 'init', 'gutenbooster_wc_init', 50 );
}


/* MY CUSTOM SCRIPTS */
add_action( 'wp_enqueue_scripts', 'add_my_script' );
function add_my_script() {
    wp_enqueue_script(
        'your-script', // name your script so that you can attach other scripts and de-register, etc.
        get_template_directory_uri() . '/js/custom-scripts.js', // this is the location of your script file
        array('jquery') // this array lists the scripts upon which your script depends
    );
}
