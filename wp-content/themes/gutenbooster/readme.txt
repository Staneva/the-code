=== GutenBooster ===
Contributors: sj_o
Tags: two-columns, right-sidebar, footer-widgets, blog, news, custom-background, custom-menu, post-formats, sticky-post, editor-style, threaded-comments, translation-ready, buddypress, custom-colors, featured-images, full-width-template, theme-options, e-commerce, block-editor-styles
Tested up to: 5.2.2
Stable tag: 1.1.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

GutenBooster is is a feature rich WordPress theme for your Gutenberg driven website. It’s super lightweight and fast, while extremely feature rich. GutenBooster has premium level theme features. Combined with the Kioken Blocks Gutenberg page builder plugin, you can easily create a website four business, portfolio, blog or WooCommerce store with a modern, beautiful and fast loading design. GutenBooster takes full advantage of the new WordPress editor, Gutenberg by fully matching and reflecting your typography, color and layout settings to the editor and frontend. It is almost fully WYSIWYG capable. Translation ready with the best SEO friendly approach. It is super responsive as well. You can adjust responsive settings in customizer and your site will look great with any device and scale.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Steps to Recreate the Screenshot ==

1. The screenshot of Gutenbooster (the one displayed on WordPress.org and in Themes → Add New) uses  a photo as page banner image and a couple blocks from WordPress block editor.
2. Go to Customizer → Page Banner. Make sure it's enabled. Then select a background image, assign opacity and set page banner height.
3. Go to Customizer → Header. Select transparent as header background and set header text color to white.
4. Go to Posts/Pages → Add New.
5. Enter a title for the page/post.
6. Next, click the "Plus" symbol in the content and select the block "Columns" from the list. In the sidebar block settings, change "Columns: 2" to "Columns: 3". Add an image, a title, and a paragraph of text to each column in the "Columns" block, and you've pretty much recreated the basic structure of the screenshot in GutenBooster.

-------------------------------------------------------
Features:
-------------------------------------------------------

== Appearance > Widgets ==
GutenBooster has up to 5 widget area. 1 for each sidebar and up to 4 in your footer. To enable widgets in the footer, you would need to go to Customizer -> Footer -> Display Footer Widgets and then add widgets in the Customizer -> Widgets section. When you have no widgets in a sidebar, it will not show up on your page or posts archive.

== Appearance > Customize ==
	- Site Identity: Hide your site title or tagline, and upload your own logo. Upload a light logo for cases like transparent header background on da dark background page.
	- Page Banner: Enable/Disable Site and Page Hero area. Change settings for each page after installing the GutenBooster Addons plugin.
	- Header: Change things like header background, text color etc.
	- Colors & background: Choose your default background color, body color, theme palette and link colors.
	- Custom Google Typography: Choose from a list of your favorite Google Webfonts for body and headings from Customizer, adjust font sizes for body.

== License ==

GutenBooster WordPress Theme, Copyright (C) 2019, Kioken Theme
GutenBooster is distributed under the terms of the GNU GPL


== Credits ==

- Underscores
Source: Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc.
License: [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

- Bootstrap
Source: http://getbootstrap.com/, (C) 2011-2019 Bootstrap Team
License: [MIT](http://opensource.org/licenses/MIT)

- Google Fonts
Source: https://fonts.google.com/
License: SIL Open Font License, 1.1 - scripts.sil.org/OFL

- Feather Icons:
Source: https://feathericons.com/, (C) Cole Bemis
License: [MIT](http://opensource.org/licenses/MIT)

- Barba:
Source: https://barba.js.org/, Copyright (C) 2018 Luigi De Rosa, Thierry Michel
License: [MIT](http://opensource.org/licenses/MIT)

- Headroom:
Source: https://wicky.nillia.ms/headroom.js/, Copyright (C) 2013 Nick Williams
License: [MIT](http://opensource.org/licenses/MIT)

- Magnific Popup:
Source: https://dimsemenov.com/plugins/magnific-popup/, Copyright (C) 2014-2016 Dmitry Semenov
License: [MIT](http://opensource.org/licenses/MIT)

- Photoswipe:
Source: https://photoswipe.com/, Copyright (c) 2014-2019 Dmitry Semenov
License: [MIT](http://opensource.org/licenses/MIT)

- Object Fit:
Source: https://github.com/bfred-it/object-fit-images/, Copyright (C) Federico Brigante <bfred-it@users.noreply.github.com> (twitter.com/bfred_it)
License: [MIT](http://opensource.org/licenses/MIT)

- Stickybits:
Source: https://dollarshaveclub.github.io/stickybits/, Copyright (c) 2016 Dollar Shave Club, Inc.
License: [MIT](http://opensource.org/licenses/MIT)

- Superfish:
Source: https://superfish.joelbirch.design/, Copyright (C) Joel Birch
License: [MIT](http://opensource.org/licenses/MIT)

- Swiper:
Source: https://idangero.us/swiper/, Copyright (C) 2019 Swiper by Vladimir Kharlampidi from iDangero.us
License: [MIT](http://opensource.org/licenses/MIT)

- Anime.js:
Source: https://github.com/juliangarnier/anime,  Copyright (C) 2019 Julian Garnier.
License: [MIT](http://opensource.org/licenses/MIT)

== Images ==

* Image used in screenshot, License CC0 Public Domain - Copyright (C) 2019 Onur Oztaskiran.

== Changelog ==

Version 1.1.3 (2019-11-06)
-------------------------
- System fonts added to Typography options in Customizer
- Fixed alignment for ISSUU block in Gutenberg
- Customizer bug fixes
- WooCommerce default styling column fixes

Version 1.1.2 (2019-11-06)
-------------------------
- Page options plugin recommendation included. Now you can change customizer settings for each page by installing the plugin.

Version 1.1.1 (2019-11-06)
-------------------------
- Fixed a text showing up on pages

Version 1.1.0 (2019-11-06)
-------------------------
- Fixed light logo svg upload sanitization
- Added missing editor style








