<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gutenbooster
 */

 if ( in_array(gutenbooster_get_layout(), array('no-sidebar', 'fullwidth')) ) {
 	return;
 }
 $gutenbooster_sidebar_classes = array( 'col-xl-3 col-lg-4' );


 if ( function_exists( 'is_woocommerce' ) && is_woocommerce() && is_active_sidebar('shop-sidebar') ) {
 	$gutenbooster_sidebar = 'shop-sidebar';
 } elseif ( is_page() && is_active_sidebar('page-sidebar') ) {
 	$gutenbooster_sidebar         = 'page-sidebar';
} else {
    $gutenbooster_sidebar = is_active_sidebar('sidebar-default') ? 'sidebar-default' : 'none';
}

 $gutenbooster_sidebar_classes[] = $gutenbooster_sidebar;

 if ( $gutenbooster_sidebar !== 'none' ) : ?>
     <aside id="secondary" class="widget-area primary-sidebar fade-in-right <?php echo esc_attr( join( ' ', $gutenbooster_sidebar_classes ) ) ?> " role="complementary" itemtype="https://schema.org/WPSideBar" itemscope>
     	<?php dynamic_sidebar( $gutenbooster_sidebar ); ?>
     </aside><!-- #secondary -->
<?php endif; ?>
