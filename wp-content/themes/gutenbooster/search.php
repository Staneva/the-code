<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package gutenbooster
 */

get_header();
?>



<div class="k-content-inner <?php gutenbooster_content_col_classes(); ?>">
	<div class="row">

	<?php
	if ( have_posts() ) : ?>

	<header class="page-header col-12">
		<h1 class="page-title">
			<?php
			/* translators: %s: search query. */
			printf( esc_html__( 'Search Results for: %s', 'gutenbooster' ), '<span>' . get_search_query() . '</span>' );
			?>
		</h1>
	</header><!-- .page-header -->
	<?php gutenbooster_cards_wrapper("start"); ?>
	<?php
		/* Start the Loop */
		while ( have_posts() ) : the_post();
			
			get_template_part( 'template-parts/content', 'search');
		endwhile; ?>

	<?php
	else :
		get_template_part( 'template-parts/content', 'none' );
	endif;
	?>
	<?php gutenbooster_cards_wrapper("end");
	the_posts_pagination( array(
		'prev_text' => gutenbooster_icon_url("arrow-left"),
		'next_text' => gutenbooster_icon_url("arrow-right"),
	) );
	?>

</div><!-- .row -->
</div><!-- .k-content-inner -->


<?php
get_sidebar();
get_footer();
