<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gutenbooster
 */

?>

<?php
	do_action( 'gutenbooster_after_content' );
	//bring up post nav and comments
	do_action( 'gutenbooster_before_footer' );
?>
		</div><!-- #content .site-content -->
	</div><!-- .site-inner -->
<?php
	gutenbooster_kt_preloader( 'brb-contain-off' );

	get_template_part( 'template-parts/footers/footer', 'base' );
?>

</main><!-- #site-wrap -->

<?php wp_footer(); ?>

</body>
</html>
